/*
* Actions ATC2603C PMIC battery driver
*
 * Copyright (c) 2015 Actions Semiconductor Co., Ltd.
 * Terry Chen chenbo@actions-semi.com
*
 * SPDX-License-Identifier:	GPL-2.0+
 */
#ifndef __ATC2603C_BATTERY_H__
#define __ATC2603C_BATTERY_H__

int atc2603c_bat_init(const void *blob);


#endif
