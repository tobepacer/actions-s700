/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ASM_ARCH_CLK_S700_H__
#define __ASM_ARCH_CLK_S700_H__

/* Module Clock ID */

#define CLOCK_DE				0
#define CLOCK_LCD0				1
#define CLOCK_DSI				2
#define CLOCK_TVOUT				3
#define CLOCK_TVOUT24M			4
#define CLOCK_HDMI				5
#define CLOCK_HDCP2				6
#define CLOCK_GPU3D				8
#define CLOCK_HDE				9
#define CLOCK_VDE				10
#define CLOCK_VCE				11
#define CLOCK_CSI				13
#define CLOCK_SI				14
#define CLOCK_SE				16
#define CLOCK_DMAC				17
#define CLOCK_SHARESRAM			18
#define CLOCK_SRAMI				19
#define CLOCK_DDR				20
#define CLOCK_NANDC				21
#define CLOCK_SD0				22
#define CLOCK_SD1				23
#define CLOCK_SD2				24
#define CLOCK_USB3				25
#define CLOCK_USBH0				26
#define CLOCK_USBH1				27
#define CLOCK_SS12				29
#define CLOCK_SS9				30
#define CLOCK_TS				31

#define CLOCK_I2C0				32
#define CLOCK_I2C1				33
#define CLOCK_I2C2				34
#define CLOCK_I2C3				35
#define CLOCK_SPI0				36
#define CLOCK_SPI1				37
#define CLOCK_SPI2				38
#define CLOCK_SPI3				39
#define CLOCK_UART0				40
#define CLOCK_UART1				41
#define CLOCK_UART2				42
#define CLOCK_UART3				43
#define CLOCK_UART4				44
#define CLOCK_UART5				45
#define CLOCK_UART6				46
#define CLOCK_IRC				47
#define CLOCK_PWM0				48
#define CLOCK_PWM1				49
#define CLOCK_PWM2				50
#define CLOCK_PWM3				51
#define CLOCK_PWM4				52
#define CLOCK_PWM5				53
#define CLOCK_TIMER				54
#define CLOCK_ETHERNET			55
#define CLOCK_KEY				56
#define CLOCK_GPIO				57
#define CLOCK_I2STX				58
#define CLOCK_I2SRX				59
#define CLOCK_HDMIA				60
#define CLOCK_SPDIF				61
#define CLOCK_PCM0				62
#define CLOCK_PCM1				63


#endif	/* __ASM_ARCH_CLK_S700_H__ */
