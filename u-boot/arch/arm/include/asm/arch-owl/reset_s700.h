/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ASM_ARCH_RESET_S700_H__
#define __ASM_ARCH_RESET_S700_H__


/* Module Reset ID */
#define RESET_DE				0
#define RESET_LCD0				1
#define RESET_DSI				2
#define RESET_TVOUT				3
#define RESET_HDMI				5
#define RESET_HDCP2				6
#define RESET_GPU3D				8
#define RESET_HDE				9
#define RESET_VDE				10
#define RESET_VCE				11
#define RESET_CSI				13
#define RESET_SI				14
#define RESET_SE				16
#define RESET_DMAC				17
#define RESET_SRAMI				19
#define RESET_DDR				20
#define RESET_NANDC				21
#define RESET_SD0				22
#define RESET_SD1				23
#define RESET_SD2				24
#define RESET_USB3				25
#define RESET_USBH0				26
#define RESET_USBH1				27
#define RESET_CHIPID			29
#define RESET_CPU_SCNT			30
#define RESET_GIC				31

#define RESET_I2C0				32
#define RESET_I2C1				33
#define RESET_I2C2				34
#define RESET_I2C3				35
#define RESET_SPI0				36
#define RESET_SPI1				37
#define RESET_SPI2				38
#define RESET_SPI3				39
#define RESET_UART0				40
#define RESET_UART1				41
#define RESET_UART2				42
#define RESET_UART3				43
#define RESET_UART4				44
#define RESET_UART5				45
#define RESET_UART6				46
#define RESET_ETHERNET			55
#define RESET_KEY				56
#define RESET_GPIO				57
#define RESET_AUDIO				61
#define RESET_PCM0				62
#define RESET_PCM1				63

#endif	/* __ASM_ARCH_RESET_S700_H__ */
