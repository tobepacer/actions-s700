/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s700.dtsi"

/ {
	model = "s700 upgrade";
	compatible = "actions,s700_upgrade", "actions,s700";

	chosen {
		stdout-path = &serial5;
	};

	clock:e0168000 {
		compatible = "actions,owl-clk";
		core_pll = <600>;
		dev_pll = <600>;
		display_pll = <720>;
	};

	pwm: pwm@e01b0000 {
		pwm0 {
			id = <0>;
			mfp = <0>;
		};

		pwm1 {
			id = <1>;
			mfp = <0>;
		};

		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	xhci@e0400000 {
		actions,vbus-gpio = <&gpioc 14 0>;  /* GPIOC14,  1: low active; 0: high active */
	};

	i2c0: i2c@e0170000 {
		#address-cells = <1>;
		#size-cells = <1>;
		atc2603c_pmic {
			compatible = "actions,atc2603c";
			reg = <0x65 1>;


			total_steps = <2625>;
			pwms_gpu = <&pwm 1 2625 0>;
			vdd_gpu = <485>;
			vdd_core = <490>;
			pwms_core = <&pwm 2 2625 0>;
			atc2603c_pstore {
				compatible = "actions,atc2603c-pstore";
			};

			voltage-regulators {
				dcdc1: dcdc1 {
					regulator-name = "dcdc1";
					regulator-min-microvolt = <1150000>;
					regulator-max-microvolt = <1150000>;
					regulator-always-on;
				};

				dcdc3: dcdc3 {
					regulator-name = "dcdc3";
					regulator-min-microvolt = <3100000>;
					regulator-max-microvolt = <3100000>;
					regulator-always-on;
				};

				ldo1: ldo1{
					regulator-name = "ldo1";
					regulator-min-microvolt  = <2700000>;
					regulator-max-microvolt = <2700000>;
					regulator-always-on;
				};


			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};
		};

		atc2603c-battery{
			compatible = "actions,atc2603c-battery";
			capacity = <2000>;/*unit:mAh*/
			shutdown_current = <50>;/*unit:ua*/
		};
		atc2603c-charger{
			compatible = "actions,atc2603c-charger";
			rsense = <20>;/*unit:mohm*/
			support_adaptor_type = <3>; /*1: DCIN  2: USB  3:DCIN+USB*/
			usb_pc_ctl_mode = <1>; /*0:disable vbus ctl;1:current limited;2:voltage limited*/

		};

		};
	};

	i2c3: i2c@e017c000 {
		clock-frequency = <90000>;
		hdmi_edid {
			compatible = "actions,hdmi-edid";
		};
	};

	hdmi@e02c0000 {
		channel_invert = <1>;
		bit_invert = <0>;

		panel@ghp {
			compatible = "actions,panel-ghp";

			is_primary = <0>;

			draw_width = <1920>;
			draw_height = <1080>;

			videomode-0 = <&default_mode>;

			/* default mode used when EDID read error */
			default_mode: videomode {
				refresh_rate = <60>;

				xres = <1280>;
				yres = <720>;
			};
		};
	};


	backlight {
		compatible = "actions,s700-pwm-backlight";

		/* GPIOA20, active high */
		en-gpios = <&gpiod 19 0>;

		/*1.pwm num; 2. period in ns; */
		/*3.plarity, 0: high active, 1: low active*/
		pwms = <&pwm 0 50000 1>;

		total_steps = <1024>;
		min_brightness = <100>;
		max_brightness = <1000>;
		dft_brightness = <500>;

		delay_bf_pwm = <0>; /*in ms*/
		delay_af_pwm = <20>; /*in ms*/
	};

	lcd: lcd@e02a0000 {

		panel_configs = <&config0>;
		/*
		 * lcd_configs, used to configure lcd controller,
		 * its values should set according to lcd panel's spec.
		 * Currently, the configration is for fpga panel.
		 */
		config0: lcd_configs {
			port_type = <2>;	/* 0, RGB; 1, CPU; 2, LVDS; 3, EDP */

			vsync_inversion = <0>;
			hsync_inversion = <0>;
			dclk_inversion = <1>;
			lde_inversion = <0>;

			pclk_parent = <0>;	/* 0, DISPLAY PLL; 1, NAND PLL */
			pclk_rate = <72000000>;/* 72MHz, DISPLAY PLL, divider 10 */
		};

		/* lcd panel */
		panel@glp {
			compatible = "actions,panel-glp";

			/* panel's fixed info */
			width_mm = <197>;
			height_mm = <147>;
			bpp = <24>;

			is_primary = <1>;

			/* operation delay in ms */
			power_on_delay = <0>;
			power_off_delay = <0>;
			enable_delay = <0>;
			disable_delay = <0>;

			power-gpio = <&gpiob 4 0>;/* GPIOA23, high power on */

			videomode-0 = <&mode0>;

			/* 800x1280p60 */
			mode0: videomode {
				refresh_rate = <60>;

				xres = <800>;
				yres = <1280>;
				/*
				 * pclk_rate = 66M
				 * pixel_clock = picoseconds / pclk_rate
				 */
				pixel_clock = <15152>;

				hsw = <12>;
				hbp = <20>;
				hfp = <40>;

				vsw = <2>;
				vbp = <10>;
				vfp = <33>;

				/* 0: FB_VMODE_NONINTERLACED, 1:FB_VMODE_INTERLACED */
				vmode = <0>;
			};
		};
	};

};
