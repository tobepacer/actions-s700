/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s700.dtsi"

/ {
	model = "s700 upgrade";
	compatible = "actions,s700_upgrade", "actions,s700";

	chosen {
		stdout-path = &serial2;
	};

	clock:e0168000 {
		compatible = "actions,owl-clk";
		core_pll = <1008>;
		dev_pll = <600>;
		display_pll = <720>;
	};

	pwm: pwm@e01b0000 {
		pwm0 {
			id = <0>;
			mfp = <0>;
		};

		pwm1 {
			id = <1>;
			mfp = <0>;
		};

		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	xhci@e0400000 {
		actions,vbus-gpio = <&gpiob 5 0>;  /* GPIOB5,  1: low active; 0: high active */
	};


	i2c0: i2c@e0170000 {
		#address-cells = <1>;
		#size-cells = <1>;
		atc2603c_pmic {
			compatible = "actions,atc2603c";
			reg = <0x65 1>;


			total_steps = <2625>;
			pwms_gpu = <&pwm 1 2625 0>;
			vdd_gpu = <485>;
			vdd_core = <490>;
			pwms_core = <&pwm 2 2625 0>;
			atc2603c_pstore {
				compatible = "actions,atc2603c-pstore";
			};

			voltage-regulators {
				dcdc1: dcdc1 {
					regulator-name = "dcdc1";
					regulator-min-microvolt = <1150000>;
					regulator-max-microvolt = <1150000>;
					regulator-always-on;
				};

				dcdc3: dcdc3 {
					regulator-name = "dcdc3";
					regulator-min-microvolt = <3100000>;
					regulator-max-microvolt = <3100000>;
					regulator-always-on;
				};

				ldo1: ldo1{
					regulator-name = "ldo1";
					regulator-min-microvolt  = <2600000>;
					regulator-max-microvolt = <3300000>;
					regulator-always-on;
				};


			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};
		};


		};
	};

	i2c3: i2c@e017c000 {
		eg2801_eeprom {
			compatible = "actions,eg2801_eeprom";
		};
		eg2801_ram {
			compatible = "actions,eg2801_ram";
		};
		hdmi_edid {
			compatible = "actions,hdmi-edid";
		};
	};

	//backlight {
	//	compatible = "actions,s700-pwm-backlight";

	//	/* GPIOD28, active high */
	//	en-gpios = <&gpioe 0 0>;

	//	/*1.pwm num; 2. period in ns; */
	//	/*3.plarity, 0: high active, 1: low active*/
	//	pwms = <&pwm 0 50000 1>;

	//	total_steps = <1024>;
	//	min_brightness = <100>;
	//	max_brightness = <1000>;
	//	dft_brightness = <500>;

	//	delay_bf_pwm = <0>; /*in ms*/
	//	delay_af_pwm = <20>; /*in ms*/
	//};

	//dsi: dsi@e0310000 {
	//	panel_configs = <&config0>;

	//	/* 1280x800p60 */
	//	config0: mipi_configs {
	//		lcd_mode = <1>;		/* 0: command mode 1: video mode */
	//		lane_count = <4>;	/* 1: 1,2:1~2, 3:1~3, 4:1~4
	//		lane_polarity = <0>;	/* 0: normal,1: reversed*/

	//		burst_bllp = <60>;	/* 0 ~ 333, just burst mode needed
	//					 * It will affect the frame rate */
	//		video_mode = <2>;	/* 0: sync mode, 1:de mode, 2: burst mode */
	//		pclk_rate = <68>;	/* 64MHz, HSCLK, divider 3*/
	//	};
  //
	//	/* hsd101puw1-a */
	//	panel@gmp {
	//		compatible = "actions,panel-gmp";
  //
	//		/* panel's fixed info */
	//		width_mm = <217>;
	//		height_mm = <135>;
	//		bpp = <24>;
  //
	//		//is_primary = <1>;
  //
	//		/* operation delay in ms */
	//		power_on_delay = <10>;
	//		power_off_delay = <0>;
	//		enable_delay = <10>;
	//		disable_delay = <0>;
  //
	//		power-gpio = <&gpiod 30 1>;
	//		reset-gpio = <&gpiod 19 1>;
	//		/* screen init command info */
	//		/* at the end of cmd array, "0x0" isn`t cmd, just the flag of the end */
	//		mipi_cmd = <0x0>;

	//		videomode-0 = <&mode0>;
	//		/* 1280x800p60 */
	//		mode0: videomode {
	//			refresh_rate = <60>;
	//			xres = <1280>;
	//			yres = <800>;
  //
	//			/* in pico second, 0.000 000 000 001s */
	//			pixel_clock = <12500>;
  //
	//			hsw = <10>;
	//			hbp = <20>;
	//			/*if the burst mode transmission, hfp need to includ burst_bllp*/
	//			hfp = <120>;

	//			vsw = <6>;
	//			vbp = <5>;
	//			/* vfp can be as small as possible,
	//			 * it related to preline time */
	//			vfp = <1>;
  //
	//			/* 0: FB_VMODE_NONINTERLACED, 1:FB_VMODE_INTERLACED */
	//			vmode = <0>;
	//		};
	//	};
	//};

	hdmi@e02c0000 {
		//hdcp_onoff = <0>;
		channel_invert = <0>;
		bit_invert = <0>;

		panel@ghp {
			compatible = "actions,panel-ghp";

			is_primary = <1>;
			draw_width = <1920>;
			draw_height = <1080>;
			videomode-0 = <&default_mode>;

			/* default mode used when EDID read error */
			default_mode: videomode {
				refresh_rate = <60>;

				xres = <1920>;
				yres = <1080>;
			};
		};
	};
	pmu {
		ldo_cfgs_1 = <0x7>;
	};
};
