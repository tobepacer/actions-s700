/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s900.dtsi"

/ {
	model = "gt9_ebox";
	compatible = "actions,gt9_ebox", "actions,s900";

	chosen {
		stdout-path = &serial5;
	};

	clock:e0160000 {
		compatible = "actions,owl-clk";
		core_pll = <1104>;
		dev_pll = <660>;
		display_pll = <1056>;
	};

	pwm: pwm@e01b0000 {
		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	i2c0: i2c@e0170000 {
		hdmi_edid {
			compatible = "actions,hdmi-edid";
		};
	};

	i2c3: i2c@e0176000 {
		atc2609a_pmic {
			compatible = "actions,atc2609a";
			reg = <0x65>;

			atc2609a_rtc {
					compatible = "actions,atc2609a-rtc";
			};

			atc2609a_pstore {
					compatible = "actions,atc2609a-pstore";
			};

			atc2609a_misc {
					compatible = "actions,atc2609a-misc";
			};

			atc2609a_auxadc {
					compatible = "actions,atc2609a-auxadc";
			};

			voltage-regulators {

			/* DCDC0       VDD_CORE
			 * DCDC1       VDD_CPU
			 * DCDC2       VDDR
			 * DCDC3       VCC
			 * DCDC4       VDD_CPUM
			 */
			dcdc0: dcdc0 {
				regulator-name = "dcdc0";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <925000>;
				regulator-always-on;
			};

			dcdc1: dcdc1 {
				regulator-name = "dcdc1";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <925000>;
				regulator-always-on;
			};

			dcdc3: dcdc3 {
				regulator-name = "dcdc3";
				regulator-min-microvolt = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			dcdc4: dcdc4 {
				regulator-name = "dcdc4";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <925000>;
				regulator-always-on;
			};

			/* LDO0        SD_VCC     
			 * LDO1        WIFI_3V3  
			 * LDO2        AVCC       
			 * LDO3        ATC2609_VDD   
			 * LDO4        AVCC_1V8   
			 * LDO6        AVDD_1V0   
			 * LDO7        UNUSED  
			 * LDO8        ETHERNET_3V3     
			 * LDO9        RTC_VDD     */

			ldo0: ldo0{
				regulator-name = "ldo0";
				regulator-min-microvolt  = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo1: ldo1{
				regulator-name = "ldo1";
				regulator-min-microvolt  = <3300000>;
				regulator-max-microvolt = <3300000>;
			};

			ldo2: ldo2{
				regulator-name = "ldo2";
				regulator-min-microvolt  = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo3: ldo3{
				regulator-name = "ldo3";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};

			ldo4: ldo4{
				regulator-name = "ldo4";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};


			ldo6: ldo6{
				regulator-name = "ldo6";
				regulator-min-microvolt  = <1100000>;
				regulator-max-microvolt = <1100000>;
				regulator-always-on;
			};

			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <700000>;
				regulator-max-microvolt = <2200000>;
			};

			ldo8: ldo8{
				regulator-name = "ldo8";
				regulator-min-microvolt  = <3300000>;
				regulator-max-microvolt = <3300000>;
			};

			ldo9: ldo9{
				regulator-name = "ldo9";
				regulator-min-microvolt  = <2600000>;
				regulator-max-microvolt = <3300000>;
			};
			};
		};
		atc2609a_adckeypad {
			#define KEY_VOLUMEDOWN	114
			#define KEY_VOLUMEUP	115
			compatible = "actions,atc2609a-adckeypad";
			keymapsize = <2>;
			adc_channel_name = "AUX0";
			key_val = <KEY_VOLUMEUP KEY_VOLUMEDOWN>;
			left_adc_val =  <0 3300>;
			right_adc_val = <100 3700>;
		};
		atc2609a_irkey {
			compatible = "actions,atc2609a-ir";
			protocol = <1>; /* 0:9012 1:NEC8 2:RC5 */
			customer_code = <0x0dff40>;
			keymapsize = <45>;
			ir_code  =<0x0 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10 0x11  0x12 0x13 0x14 0x15  0x16 0x17 0x18 0x19 0x1A  0x1B 0x1C  0x1D 0x1E 0x1F 0x40 0x41 0x42  0x43  0x44 0x45  0x46 0x47 0x4D  0x53 0x54  0x57 0x5B>;
			key_code =<11   2   3    4    5    6    7    8    9 	 10 	 100    103  126   353   108   52    105  106    14   60   63  115    80   61   65   76   172   78   114   97   79   75   77  81   158    113  65   139   64   71   116   72   175   82   73>;
		};
		atc260x-charger{
			compatible = "actions,atc2609a-charger";
			rsense = <20>;/*unit:mohm*/
			support_adaptor_type = <3>; /*1: DCIN  2: USB  3:DCIN+USB*/
		};
	};

	backlight {
		compatible = "actions,s900-pwm-backlight";

		/* GPIOA14, active high */
		en-gpios = <&gpioa 14 0>;

		/*1.pwm num; 2. period in ns; */
		/*3.plarity, 0: high active, 1: low active*/
		pwms = <&pwm 2 50000 0>;

		total_steps = <1024>;
		min_brightness = <100>;
		max_brightness = <1000>;
		dft_brightness = <500>;

		delay_bf_pwm = <0>; /*in ms*/
		delay_af_pwm = <200>; /*in ms*/
	};

	hdmi@e0250000 {
		hdcp_onoff = <0>;
		channel_invert = <0>;
		bit_invert = <0>;

		panel@ghp {
			compatible = "actions,panel-ghp";

			is_primary = <1>;

			draw_width = <1920>;
			draw_height = <1080>;
		};
	};

	xhci@e0400000 {
		actions,vbus-gpio = <&gpiod 1 0>;  /* GPIOD1,  0:  high active ; 1: low active */
	};

};
