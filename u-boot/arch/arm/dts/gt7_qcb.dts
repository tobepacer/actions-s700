/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s700.dtsi"

/ {
	model = "s700 upgrade";
	compatible = "actions,s700_upgrade", "actions,s700";

	chosen {
		stdout-path = &serial5;
	};

	clock:e0168000 {
		compatible = "actions,owl-clk";
		core_pll = <600>;
		dev_pll = <600>;
		display_pll = <480>;
	};

	pwm: pwm@e01b0000 {
		pwm0 {
			id = <0>;
			mfp = <0>;
		};

		pwm1 {
			id = <1>;
			mfp = <0>;
		};

		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	xhci@e0400000 {
		actions,vbus-gpio = <&gpioc 8 0>;  /* GPIOC8,  1: low active; 0: high active */
	};

	adfu{
		compatible = "actions,s700-adfu";
		detect_times = <130>;
		detect_interval = <5>;	/* total time =  detect_times * detect_interval(ms) */
	};

	i2c2: i2c@e0178000 {
			#address-cells = <1>;
			#size-cells = <1>;
		atc2603c_pmic {
			compatible = "actions,atc2603c";
			reg = <0x65 1>;


			total_steps = <2625>;
			pwms_gpu = <&pwm 1 2625 0>;
			vdd_gpu = <350>;
			vdd_core = <490>;
			pwms_core = <&pwm 2 2625 0>;
			atc2603c_pstore {
					compatible = "actions,atc2603c-pstore";
			};
			voltage-regulators {

			dcdc1: dcdc1 {
				regulator-name = "dcdc1";
					regulator-min-microvolt = <1150000>;
					regulator-max-microvolt = <1150000>;
				regulator-always-on;
			};

			dcdc3: dcdc3 {
				regulator-name = "dcdc3";
				regulator-min-microvolt = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo1: ldo1{
				regulator-name = "ldo1";
				regulator-min-microvolt  = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			ldo5: ldo5{
				regulator-name = "ldo5";
				regulator-min-microvolt  = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			ldo6: ldo6{
				regulator-name = "ldo6";
				regulator-min-microvolt  = <1100000>;
				regulator-max-microvolt = <1100000>;
				regulator-always-on;
			};

			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};


		};
		atc2603c_adckeypad {
			#define KEY_VOLUMEDOWN	114
			#define KEY_VOLUMEUP	115
			compatible = "actions,atc2603c-adckeypad";
			keymapsize = <1>;
			adc_channel_name = "REMCON";
			key_val = <KEY_VOLUMEUP>;
			left_adc_val =  <144>;
			right_adc_val = <239>;
		};
		atc2603c_irkey {
			compatible = "actions,atc2603c-ir";
			config {
				protocol = <1>; /* 0:9012 1:NEC8 2:RC5 */
				customer_code = <0x0dff40>;
				wk_code = <0x4d>;
				keymapsize = <45>;
				ir_code  =<0x0 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10 0x11  0x12 0x13 0x14 0x15  0x16 0x17 0x18 0x19 0x1A  0x1B 0x1C  0x1D 0x1E 0x1F 0x40 0x41 0x42  0x43  0x44 0x45  0x46 0x47 0x4D  0x53 0x54  0x57 0x5B>;
				key_code =<11   2   3    4    5    6    7    8    9 	 10 	 100    103  126   353   108   52    105  106    14   60   63  115    80   61   65   76   172   78   114   97   79   75   77  81   158    113  65   139   64   71   116   72   175   82   73>;
			};
			config-1 {
				protocol = <1>; /* 0:9012 1:NEC8 2:RC5 */
				customer_code = <0x0dff40>;
				wk_code = <0x4d>;
				keymapsize = <45>;
				ir_code  =<0x0 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10 0x11  0x12 0x13 0x14 0x15  0x16 0x17 0x18 0x19 0x1A  0x1B 0x1C  0x1D 0x1E 0x1F 0x40 0x41 0x42  0x43  0x44 0x45  0x46 0x47 0x4D  0x53 0x54  0x57 0x5B>;
				key_code =<11   2   3    4    5    6    7    8    9 	 10 	 100    103  126   353   108   52    105  106    14   60   63  115    80   61   65   76   172   78   114   97   79   75   77  81   158    113  65   139   64   71   116   72   175   82   73>;
			};
		};
		atc2603c_power_led {
			compatible = "actions,atc2603c-power-led";
			
			led-1 {
				mux = <0x66 8 1>;
				mfp_io = <0x6a 9>;
				gpio = <0x6b 3 1>;
				default-state = <1>;/*on*/
			};
			led-2 {
				mux = <0x66 10 0>;
				mfp_io = <0x6a 9>;/*reg, start bit*/
				gpio = <0x6b 4 1>;/*reg, index, low active*/
				default-state = <0>;/*off*/
			};
		};

		};
	};

	i2c3: i2c@e017c000 {
		clock-frequency = <90000>;
		hdmi_edid {
			compatible = "actions,hdmi-edid";
		};
	};

	hdmi@e02c0000 {
		channel_invert = <0>;
		bit_invert = <0>;

		panel@ghp {
			compatible = "actions,panel-ghp";

			is_primary = <1>;

			draw_width = <1920>;
			draw_height = <1080>;

			videomode-0 = <&default_mode>;

			/* default mode used when EDID read error */
			default_mode: videomode {
				refresh_rate = <60>;

				xres = <1280>;
				yres = <720>;
			};
		};
	};

};
