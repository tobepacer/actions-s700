/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>

void reset_cpu(unsigned long ignored)
{
	while (1)
		;
}
