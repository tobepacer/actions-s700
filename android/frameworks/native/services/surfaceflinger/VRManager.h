namespace ActsVR {
    class CoordsMapper;
    class VRManager;
    class RecompositeThread;
};

#ifndef __VR_MANAGER_H__
#define __VR_MANAGER_H__

#include <stdint.h>
#include <sys/types.h>
#include <string.h>
#include <ui/vec2.h>

#include <utils/Thread.h>
#include "Transform.h"
#include "RenderEngine/RenderEngine.h"
#include "SurfaceFlinger.h"
#include "Layer.h"

//VR Class
using namespace android;

namespace ActsVR {

class CoordsMapper;
class VRManager;
class ReccompositeThread;

struct VRConfig {
    float scale; // scaleing size ,1.0 show orig size  , 0.5 full screen  0.0 not show
    float offsetX; // X offset 
    float offsetY; // y offset 
    float stretch; // 
};

struct VRFactor {
    //float scale, stretchX, stretchY;
    float offsetY, offsetX_left, offsetX_right;
    float scaleNStretchX, scaleNStretchY; // scale * stretch
};

class VRManager {
public:
    enum VR_MODE {
        VR_NONE = 0,
        VR_2D = 1,
        VR_3D_FULL = 2,
        VR_3D_16_9 = 3,
    };
private:
    friend class RecompositeThread;

    sp<SurfaceFlinger> mFlinger;
    sp<RecompositeThread> mRefreshThread;

    CoordsMapper* mCoordsMapper;

    bool mGlobalVREnabled;
    bool mVREnabled;
    bool mAllowUserConfigSystemApp = false;

    // Whether video 2D/3D auto-detection is enabled, only works through OMX.
    bool mIsAutoVideoModeEnabled = false;
    // Auto video mode set by video 2D/3D auto-detection function.
    // When mIsAutoVideoModeEnabled disabled, mAutoVideoMode could be only used
    // to decide whether there is video playing, that is, equal to VR_NONE or not.
    VR_MODE mAutoVideoMode = VR_NONE;
    // Video mode set manually, have precedence over mAutoVideoMode
    VR_MODE mForceVideoMode = VR_NONE;
    // Full screen or not, used to decide whether in video previewing or playing
    bool mIsFullScreenVideo = false;

    struct VRConfig mConfig;
    struct VRConfig mOriginConfig;

    float mScreenWidth, mScreenHeight;
    bool mLandscape;
    uint32_t mScreenFBWidth, mScreenFBHeight;
    uint32_t mOrientation;

    struct VRFactor mFactor;
    struct VRFactor mOriginFactor;

    android::Mutex mMutex;
    // android::Condition mCond;
    bool mFloatingAnimating;
    nsecs_t mLastRefreshTime, mTargetRefreshTime; //now = systemTime(SYSTEM_TIME_MONOTONIC);
    float mFloatingSpeedX_left, mFloatingSpeedX_right, mFloatingSpeedY;
    float mFloatingX_left, mFloatingX_right, mFloatingY;
    float mFloatingX_left_target, mFloatingX_right_target, mFloatingY_target;

    bool isLandscape(uint32_t orientation);
    // recalculate factor
    void recalcFactor(struct VRConfig& config, struct VRFactor& factor, bool left_right);
    void resetAllConfigHelper(float scale, float stretch, float offsetX, float offsetY);

    void resetVideo3DConfig() {
        resetAllConfigHelper(1.0, 1.0, 0.0, 0.0); // 0.5x1.0
    }

    void resetVideo2DConfig() {
        resetAllConfigHelper(0.64, 0.4375, 0.0, 0.0); // 0.5x0.64
        //resetAllConfigHelper(0.5, 0.0, 0.0, 0.0); // 0.5x0.5
    }

    bool hasVideoLayer() {
        return mAutoVideoMode != VR_NONE;
    }

    bool isVideoPlaying() {
        return hasVideoLayer() && mIsFullScreenVideo;
    }

    int getUsedVideoMode() {
        return (mForceVideoMode != VR_NONE) ? mForceVideoMode : mAutoVideoMode;
    }

    bool canSplitVideoLayer() {
        return !mIsFullScreenVideo || (getUsedVideoMode() != VR_3D_FULL && getUsedVideoMode() != VR_3D_16_9);
    }

  public:
    VRManager(SurfaceFlinger* flinger);

    ~VRManager() {}

    void setScreenSize(float width, float height, uint32_t orientation);

    bool isFullScreenLayer(const Rect &frame) const;

    CoordsMapper& getCoordsMapper() const {
        return *mCoordsMapper;
    }

    void doCoordsMapping(vec2* position, vec2* texCoords, vec2* positionLeft,
            vec2* positionRight, vec2* texCoordsLeft, vec2* texCoordsRight,
            bool* leftVisible, bool* rightVisible);

    void setScreenFloating(float leftX, float rightX, float y);
    void computeScreenFloating();

    void initSplitScreenAppList();
    void initNewSplitingPart1(); // state stuff: decided whether to seperate
    void initNewSplitingPart2(bool bMixedSeperated); // layout stuff: decided how to seperate

    bool needUpdateSurfaceViewSeperated();

    bool checkAllowToBeSeperated();
    bool checkNeedDrawBootAnimation(const String8 &name);
    bool needToBeSeperated(const String8 &name);
    bool allowSurfaceViewToBeSeperated();
    bool allowSurfaceViewToBeSeperated(const Rect &frame);
    bool allowToBeSeperated(const String8 &name);
    bool addToSeperateAppList(const String8 &name);
    bool addToNotSeperateAppList(const String8 &name);
    bool removeFromSeperateAppList(const String8 &name);
    bool removeFromNotSeperateAppList(const String8 &name);
    
	void checkAndSetSurfaceViewTo16V9Mode(Mesh& mesh);
		
    // Only take effect in video scene and auto 2D/3D enabled
    bool toggleSurfaceViewSeperated();
	//check is system config app , if config by system .not take effect on user
	bool toggleSystemConfigAppSeperated(const String8 &name);                      

	// mode: VR_NONE - toggle; VR_2D - force 2D; VR_3D - force 3D
    bool toggleSeperated(const String8 &name, VR_MODE mode);

    //Acts VR APP list
    Vector<String8> mSystemConfigAppList;
    Vector<String8> mSplitScreenAppList;
    Vector<String8> mNotSplitScreenAppList;
    Vector<String8> mSystemNotSplitScreenAppList;
    Vector<String8> mAutoSplitScreenAppList;
};

// Recomposite Thead
class RecompositeThread: public android::Thread, private virtual RefBase {
private:
    friend class VRManager;

    VRManager& mVRManager;

    bool mStop;
    bool mDisable;

    android::Mutex mMutex;
    android::Condition mCond;

    virtual bool threadLoop();
    virtual void onFirstRef();

    void doRecomposite();
public:
    RecompositeThread(VRManager& manager);

    ~RecompositeThread() {
    }

    void stop() {
        android::Mutex::Autolock lock(mMutex);
        mStop = true;
        mCond.signal();
    }

    void enable() {
        android::Mutex::Autolock lock(mMutex);
        mDisable = false;
        mCond.signal();
    }

    void disable() {
        android::Mutex::Autolock lock(mMutex);
        mDisable = true;
        mCond.signal();
    }
};

};//namespace ActsVR

#endif //__VR_MANAGER_H__
