#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <sys/types.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include "clz.h"
#include "VRManager.h"
#include "CoordsMapper/CoordsMapper.h"
//VR Class
//using namespace android;

namespace ActsVR {

VRManager::VRManager(android::SurfaceFlinger* flinger) :
        mFlinger(flinger), mCoordsMapper(NULL), mGlobalVREnabled(true), mVREnabled(true),
        mScreenWidth(0), mScreenHeight(0), mLandscape(false), mOrientation(0),
        mFloatingAnimating(false), mLastRefreshTime(0), mTargetRefreshTime(0),
        mFloatingX_left(0), mFloatingX_right(0), mFloatingY(0), mFloatingX_left_target(0),
        mFloatingX_right_target(0), mFloatingY_target(0) {
    mConfig.scale = 0.5;
    mConfig.offsetX = 0.0;
    mConfig.offsetY = 0.0;
    mConfig.stretch = 0.0;

    mOriginConfig.scale = 0.5;
    mOriginConfig.offsetX = 0.0;
    mOriginConfig.offsetY = 0.0;
    mOriginConfig.stretch = 0.0;

    mFactor.scaleNStretchX = 1.0;
    mFactor.scaleNStretchY = 1.0;
    mFactor.offsetY = 0.0;
    mFactor.offsetX_left = 0.0;
    mFactor.offsetX_right = 0.0;

    bool vr_enable = property_get_bool("ro.ui.2d_3d_mode_switch", false);
    bool ui_vr_mode = property_get_bool("ro.wm.ui_2d_3d_mode_switch", false);
    if (!vr_enable || ui_vr_mode) {
        mGlobalVREnabled = false;
        mVREnabled = false;
    }
    
    mAllowUserConfigSystemApp = property_get_bool("ro.ui.allow_system_app_split", true);

    mIsAutoVideoModeEnabled = property_get_bool("ro.system.3dvideo.autodetect", false);

    mCoordsMapper = CoordsMapper::create();

    // init recomposite thread
    mRefreshThread = new RecompositeThread(*this);
    mRefreshThread->disable();

    initSplitScreenAppList();
}

bool VRManager::isLandscape(uint32_t orientation) {
    bool _isLandscape = !(!(orientation & Transform::ROT_90));
    return _isLandscape;
}

void VRManager::recalcFactor(struct VRConfig& config, struct VRFactor& factor, bool left_right) {
    float scale, offsetX, offsetY, stretch;
    float stretchX, stretchY, scaleNStretchX, scaleNStretchY;
    float centX, centY, centX_left, centX_right;
    float offsetX_left, offsetX_right;

    if (mScreenWidth == 0 || mScreenHeight == 0)
        return;

    scale = config.scale;
    offsetX = config.offsetX * mScreenWidth;
    offsetY = config.offsetY * mScreenHeight;
    stretch = config.stretch;

    // recalculate real scale
    if (mLandscape) {
        // do nothing
    } else {
        float r50 = mScreenHeight / mScreenWidth;
        if (scale > 0.5) {
            scale = (2 - 2 * r50) * scale + 2 * r50 - 1;
        } else {
            scale = 2 * r50 * scale;
        }
    }

    stretchX = 1;
    stretchY = 1;

    // recalculate stretch
    if (mLandscape) {
        stretchX = 1 - 0.5 * stretch;
    } else {
        float k = 2 * (mScreenHeight * mScreenHeight)
                / (mScreenWidth * mScreenWidth);
        stretchY = 1 + (k - 1) * stretch;
    }
//    ALOGD(" ---------------------scale=%f)",scale);
    if (!mVREnabled && scale < 1 && !left_right) {
        scaleNStretchX = scale * stretchX / 2;
    } else {
        scaleNStretchX = scale * stretchX;
    }

    scaleNStretchY = scale * stretchY;

    // calculate center position
    if (mLandscape) {
        if (left_right)
            centX = mScreenWidth / 2 * scaleNStretchX / 2;
        else
            centX = mScreenWidth / 2 * scaleNStretchX;
        centY = mScreenHeight / 2 * scaleNStretchY;
    } else {
        if (left_right)
            centX = mScreenHeight / 2 * scaleNStretchX / 2;
        else
            centX = mScreenHeight / 2 * scaleNStretchX;
        centY = mScreenWidth / 2 * scaleNStretchY;
    }
    centX_left = mScreenWidth / 4;
    centX_right = mScreenWidth * 3 / 4;

    // calculate offset
    offsetX_left = centX_left - centX + offsetX;
    offsetX_right = centX_right - centX - offsetX;
    offsetY = mScreenHeight / 2 - centY + offsetY;

    //factor.scale = scale;
    //factor.stretchX = stretchX;
    //factor.stretchY = stretchY;
    factor.scaleNStretchX = scaleNStretchX;
    factor.scaleNStretchY = scaleNStretchY;
    factor.offsetY = offsetY;
    factor.offsetX_left = offsetX_left;
    factor.offsetX_right = offsetX_right;
}

void VRManager::resetAllConfigHelper(float scale, float stretch, float offsetX, float offsetY) {
    mConfig.scale = scale;
    mConfig.offsetX = offsetX;
    mConfig.offsetY = offsetY;
    mConfig.stretch = stretch;

    mOriginConfig.scale = scale;
    mOriginConfig.offsetX = offsetX;
    mOriginConfig.offsetY = offsetY;
    mOriginConfig.stretch = stretch;

    recalcFactor(mConfig, mFactor, false);
    recalcFactor(mOriginConfig, mOriginFactor, false);
}

void  VRManager::setScreenSize(float width, float height, uint32_t orientation) {
    if (mScreenWidth == width && mScreenHeight == height
            && mOrientation == orientation) {
        return;
    }
    CoordsMapper& mapper(getCoordsMapper());

    mScreenWidth = width;
    mScreenHeight = height;
    mOrientation = orientation;
    mapper.setOrientation(orientation);
    mLandscape = isLandscape(orientation);

    if (mOrientation & Transform::ROT_90) {
        mScreenFBWidth = static_cast<uint32_t>(height);
        mScreenFBHeight = static_cast<uint32_t>(width);
    } else {
        mScreenFBWidth = static_cast<uint32_t>(width);
        mScreenFBHeight = static_cast<uint32_t>(height);
    }

    recalcFactor(mConfig, mFactor, false);
    recalcFactor(mOriginConfig, mOriginFactor, false);
}

void VRManager::doCoordsMapping(vec2* position, vec2* texCoords,
        vec2* positionLeft, vec2* positionRight, vec2* texCoordsLeft,
        vec2* texCoordsRight, bool* leftVisible, bool* rightVisible) {
    float posOrig[8] = { 0 };
    float posLeft[8] = { 0 };
    float posRight[8] = { 0 };
    float scaleX, scaleY;
    float offsetY, offsetX_left, offsetX_right;
    float resWidth, resHeight, texWidth, texHeight;
    bool leftV = true, rightV = true;

    CoordsMapper& mapper(getCoordsMapper());

    mapper.mappingScreen2Virtual(mScreenWidth, mScreenHeight, position, posOrig,
            mOrientation);

    struct VRFactor *selectedFactor = &mOriginFactor;

    scaleX = selectedFactor->scaleNStretchX;
    scaleY = selectedFactor->scaleNStretchY;
    offsetY = selectedFactor->offsetY + mFloatingY;
    offsetX_left = selectedFactor->offsetX_left + mFloatingX_left;
    offsetX_right = selectedFactor->offsetX_right + mFloatingX_right;

    //coords transform in virutal coords
    mapper.coordsMapping(scaleX, scaleY, offsetY, offsetX_left, offsetX_right, posOrig, 4, posLeft, posRight);

    resWidth = posLeft[6] - posLeft[0];
    resHeight = posLeft[3] - posLeft[1];
    if (resWidth == 0 || resHeight == 0) {
        *leftVisible = false;
        *rightVisible = false;
        return;
    }

    texWidth = texCoords[2].x - texCoords[0].x;
    texHeight = texCoords[1].y - texCoords[0].y;
    if (texWidth == 0 || texHeight == 0) {
        *leftVisible = false;
        *rightVisible = false;
        return;
    }

    mapper.texCoordsMapping(mScreenWidth, mScreenHeight, resWidth, resHeight,
            texWidth, texHeight, posLeft, posRight,
            texCoords, texCoordsLeft, texCoordsRight,
            &leftV, &rightV, mVREnabled);

    // translate position to display coords
    if (leftV) {
        mapper.mappingVirtual2Screen(mScreenWidth, mScreenHeight, posLeft,
                positionLeft, mOrientation);
    }
    if (rightV) {
        mapper.mappingVirtual2Screen(mScreenWidth, mScreenHeight, posRight,
                positionRight, mOrientation);
    }

    *leftVisible = leftV;
    *rightVisible = rightV;
    return;
}

void VRManager::setScreenFloating(float leftX, float rightX, float y) {
    android::Mutex::Autolock lock(mMutex);

    mFloatingX_left_target = leftX;
    mFloatingX_right_target = rightX;
    mFloatingY_target = y;

    float dx_l = mFloatingX_left_target - mFloatingX_left,
          dx_r = mFloatingX_right_target - mFloatingX_right,
            dy = mFloatingY_target - mFloatingY;

    float d_max = fmaxf(fabsf(dx_l), fabsf(dx_r));
    d_max = d_max * d_max + dy * dy;

    if (d_max < 9) { // (540 / 60)^2  9px / frame
        mFloatingX_left = mFloatingX_left_target;
        mFloatingX_right = mFloatingX_right_target;
        mFloatingY = mFloatingY_target;
        mFloatingAnimating = false;
        mRefreshThread->disable();
        mFlinger->signalTransaction();
        return;
    }

    //mFloatingAnimating = true;
    //mRefreshThread->enable();
    //mLastRefreshTime = systemTime(SYSTEM_TIME_MONOTONIC);

    nsecs_t animDuration = (nsecs_t)(sqrt(d_max) * 1000000000) / 1080;

    if (animDuration > 1000000000)
        animDuration = 1000000000; // max: 1s

    // calculate speed
    float durationInUs = animDuration / 1000;
    mFloatingSpeedX_left = dx_l / durationInUs;
    mFloatingSpeedX_right = dx_r / durationInUs;
    mFloatingSpeedY = dy / durationInUs;

    mLastRefreshTime = systemTime(SYSTEM_TIME_MONOTONIC);
    mTargetRefreshTime = mLastRefreshTime + animDuration;
    mFloatingAnimating = true;
    mRefreshThread->enable();
    mLastRefreshTime = systemTime(SYSTEM_TIME_MONOTONIC);
}

void VRManager::computeScreenFloating() {
    android::Mutex::Autolock lock(mMutex);

    if (!mFloatingAnimating) {
        return;
    }

    nsecs_t currentTime = systemTime(SYSTEM_TIME_MONOTONIC);

    if (currentTime >= mTargetRefreshTime) {
        mFloatingX_left = mFloatingX_left_target;
        mFloatingX_right = mFloatingX_right_target;
        mFloatingY = mFloatingY_target;
        mFloatingAnimating = false;
        mRefreshThread->disable();
        return;
    }

    bool animNotFinish = false;

    float dTimeInUs = (currentTime - mLastRefreshTime) / 1000.0f;
    float tX_l, tX_r, tY;
    tX_l = mFloatingX_left + mFloatingSpeedX_left * dTimeInUs;
    tX_r = mFloatingX_right + mFloatingSpeedX_right * dTimeInUs;
    tY = mFloatingY + mFloatingSpeedY * dTimeInUs;

    if (mFloatingSpeedX_left > 0 && tX_l >= mFloatingX_left_target) {
        mFloatingX_left = mFloatingX_left_target;
    } else if (mFloatingSpeedX_left < 0 && tX_l <= mFloatingX_left_target) {
        mFloatingX_left = mFloatingX_left_target;
    } else {
        mFloatingX_left = tX_l;
        animNotFinish = true;
    }

    if (mFloatingSpeedX_right > 0 && tX_r >= mFloatingX_right_target) {
        mFloatingX_right = mFloatingX_right_target;
    } else if (mFloatingSpeedX_right < 0 && tX_r <= mFloatingX_right_target) {
        mFloatingX_right = mFloatingX_right_target;
    } else {
        mFloatingX_right = tX_r;
        animNotFinish = true;
    }

    if (mFloatingSpeedY > 0 && tY >= mFloatingY_target) {
        mFloatingY = mFloatingY_target;
    } else if (mFloatingSpeedY < 0 && tY <= mFloatingY_target) {
        mFloatingY = mFloatingY_target;
    } else {
        mFloatingY = tY;
        animNotFinish = true;
    }

    if (animNotFinish) {
        mLastRefreshTime = currentTime;
    } else {
        mFloatingAnimating = false;
        mRefreshThread->disable();
    }
}

static int readToBuffer(char *file, char *buf, size_t size) {
    int fd;
    int len;

    fd = open(file, O_RDONLY);
    if (fd < 0)
        return -1;

    len = read(fd, buf, size - 1);
    close(fd);

    if (len < 0)
        return -1;

    buf[len] = '\0';
    return 0;
}

static int readToAppList(const char *filename, Vector<String8> &applist)
{
    char buffer[512];
    FILE *fptr = fopen(filename, "r");

    if (fptr != NULL) {
        ALOGD("parsing %s", filename);
        while (fgets(buffer, sizeof(buffer), fptr)) {
            if (!isspace(buffer[0])) {
                int len = strlen(buffer);
                while (len > 0 && isspace(buffer[len - 1]))
                    len--;
                buffer[len] = '\0';
                applist.add(String8(buffer));
                ALOGD("    %s", buffer);
            }
        }
        fclose(fptr);
        fptr = NULL;
    } else {
        ALOGW("%s does not exist!", filename);
        return -1;
    }

    return 0;
}

static int writeToAppList(const char *filename, String8 name)
{
    char buffer[512];
    FILE *fptr = fopen(filename, "a+");

    if (fptr != NULL) {
        ALOGD("Writing %s", filename);
        fseek(fptr, 0, SEEK_SET);
        while (fgets(buffer, sizeof(buffer), fptr)) {
            if (isspace(buffer[0]))
                continue;

            int len = strlen(buffer);
            while (len > 0 && isspace(buffer[len - 1]))
                len--;
            buffer[len] = '\0';

            if (strstr(name.string(), buffer)) {
                ALOGD("    %s already exists", name.string());
                goto EXIT_CLOSE;
            }
        }

        fprintf(fptr, "%s\n", name.string());

EXIT_CLOSE:
        fclose(fptr);
		fptr = NULL;
    } else {
        ALOGE("%s open failed", filename);
        return -1;
    }

    return 0;
}
static int rmFromAppList(const char *filename, String8 name)
{
    char buffer[256];
    char tmp[50][256]={0};
    int  line_numer =0;
    int i = 0; 
    FILE *fptr = fopen(filename, "a+");

    if (fptr != NULL) {
        ALOGD("rm %s from  %s", name.string(), filename);
        fseek(fptr, 0, SEEK_SET);
        while (fgets(buffer, sizeof(buffer), fptr)) {
            if (isspace(buffer[0]))
                continue;

            int len = strlen(buffer);
            while (len > 0 && isspace(buffer[len - 1]))
                len--;
            buffer[len] = '\0';
            if (strstr(name.string(), buffer)) {
                ALOGD("  %s removed", name.string());                
            }else{
            	ALOGD("  %s add ", buffer);
            	buffer[len]='\n';
            	strcpy(tmp[line_numer++], buffer); 
            }
            if(line_numer >= 50){
            	break;
            }            
        }   
		rewind(fptr);     
		fclose(fptr);       
		remove(filename);				
		if((fptr = fopen(filename, "w")) == NULL)     
		{
			ALOGE(" create file failed");
			return -1;     
		}  
		ALOGD(" line_numer %d ",line_numer);
		for(i = 0; i <= line_numer; i++)     
		{  
			ALOGD(" add line i %d %s ",i,tmp[i]);
			fputs(tmp[i],fptr);     
		}		  
		fclose(fptr);  	
	}
    return 0;
}

void VRManager::initSplitScreenAppList() {
	readToAppList("/system/etc/auto_splitscreen_app.cfg", mSystemConfigAppList);
	readToAppList("/system/etc/not_splitscreen_app.cfg", mSystemConfigAppList);
	readToAppList("/system/etc/splitscreen_app.cfg", mSystemConfigAppList);
	
    // add not split screen config to list
    readToAppList("/system/etc/auto_splitscreen_app.cfg", mAutoSplitScreenAppList);
    // add not split screen config to list
    readToAppList("/system/etc/splitscreen_app.cfg", mSplitScreenAppList);
    // add not split screen config to list
    readToAppList("/system/etc/not_splitscreen_app.cfg", mNotSplitScreenAppList);
    readToAppList("/data/not_splitscreen_app.cfg", mNotSplitScreenAppList);

    for (size_t i = 0; i < mNotSplitScreenAppList.size(); i++) {
        if (removeFromSeperateAppList(mSplitScreenAppList[i])) {
            ALOGE("%s exists both in not_splitscreen_app.cfg and splitscreen_app.cfg, so ignore the one in splitscreen_app.cfg",
                  mNotSplitScreenAppList[i].string());
        }
    }
    
    for (size_t i = 0; i < mSystemConfigAppList.size(); i++)
        ALOGD("mSystemConfigAppList name %s ", mSystemConfigAppList[i].string());

    for (size_t i = 0; i < mAutoSplitScreenAppList.size(); i++)
        ALOGD("mSystemAutoSplitScreenAppList name %s ", mAutoSplitScreenAppList[i].string());

    for (size_t i = 0; i < mNotSplitScreenAppList.size(); i++)
        ALOGD("mNotSplitScreenAppList name %s ", mNotSplitScreenAppList[i].string());

    for (size_t i = 0; i < mSplitScreenAppList.size(); i++)
        ALOGD("mSplitScreenAppList name %s ", mSplitScreenAppList[i].string());
}

void VRManager::initNewSplitingPart1() {
	bool has_surfaceView = false;
	bool has_system_config_app = false;
	bool has_3D_video_play = false;
    if (!mVREnabled)
        return;

    // Though video mode not accurate, but enough to judege whether there is any
    // video playing decoded via OMX
    mAutoVideoMode = (VR_MODE)property_get_int32("system.glass.3dvideo_detect", VR_NONE);
	const size_t count = mFlinger->mCurrentState.layersSortedByZ.size();
    for (size_t i = 0; i < count; i++) {
        const sp<Layer>& layer(mFlinger->mCurrentState.layersSortedByZ[i]);
        if (layer->isSurfaceView()) {
            has_surfaceView = true ;
        }else{
        	for (size_t j = 0;j < mSystemConfigAppList.size(); j++) {
				if (layer->getName().find(mSystemConfigAppList[j].string()) > -1){
					has_system_config_app = true;				
				}
				if(strstr(layer->getName().string(),"com.bobo.splayer") != NULL){
					has_3D_video_play = true;
				}
			}
        }
    }
    
    if(!has_surfaceView){
    	mAutoVideoMode = VR_NONE;
    }
	
    // No video playing, so reset the force video mode
    if (mAutoVideoMode == VR_NONE)
        mForceVideoMode = VR_NONE;
    else if (!mIsAutoVideoModeEnabled && mForceVideoMode == VR_NONE){       
        if(has_system_config_app){
        	mForceVideoMode = VR_3D_FULL;
        }else if(has_3D_video_play){
        	mForceVideoMode = VR_3D_16_9;
        }else{
        	mForceVideoMode = VR_2D;
        }
    }

    mIsFullScreenVideo = true;
}

void VRManager::initNewSplitingPart2(bool bMixedSeperated) {
    if (bMixedSeperated && (getUsedVideoMode() == VR_3D_FULL || getUsedVideoMode() == VR_3D_16_9)) {
        resetVideo3DConfig();
    } else {
        resetVideo2DConfig();
    }
}

bool VRManager::checkAllowToBeSeperated() {
    if (!mVREnabled)
        return false;

    const size_t count = mFlinger->mCurrentState.layersSortedByZ.size();
    for (size_t i = 0; i < count; i++) {
        const sp<Layer>& layer(mFlinger->mCurrentState.layersSortedByZ[i]);
        if (!allowToBeSeperated(layer->getName())) {
            return false;
        }
    }

    return true;
}

bool VRManager::checkNeedDrawBootAnimation(const String8 &name) {
	bool bootAnimation = false;
    if (!mVREnabled)
        return true;        
    if(name.find("BootAnimation") > -1){
    	return true;
    }     
    const size_t count = mFlinger->mCurrentState.layersSortedByZ.size();    
    for (size_t i = 0; i < count; i++) {
        const sp<Layer>& layer(mFlinger->mCurrentState.layersSortedByZ[i]);
        if (!strcmp(layer->getName().string(), String8("BootAnimation"))) {
            bootAnimation = true;
        }
    }        
    if(bootAnimation){    	
    	return false;
    }    
    return true;
}

bool VRManager::needToBeSeperated(const String8 &name) {
    if (!mVREnabled)
        return false;

    for (size_t i = 0; i < mSplitScreenAppList.size(); i++) {
        if (name.find(mSplitScreenAppList[i].string()) > -1)
            return true;
    }
    return false;
}

// When OMX loaded, prop "system.glass.3dvideo_detect" is set to be VR_3D.
// So prop "system.glass.3dvideo_detect" will not be VR_NONE, when OMX is 
// loaded to decode videos.
bool VRManager::needUpdateSurfaceViewSeperated() {
    return mIsAutoVideoModeEnabled && mIsFullScreenVideo &&
           (mForceVideoMode == VR_NONE) && (mAutoVideoMode != VR_NONE);
}

bool VRManager::isFullScreenLayer(const Rect &frame) const {
    uint32_t width = frame.right - frame.left;
    uint32_t height = frame.bottom - frame.top;
    return width >= mScreenFBWidth || height >= mScreenFBHeight;
}

bool VRManager::allowSurfaceViewToBeSeperated(const Rect &frame) {
    mIsFullScreenVideo = isFullScreenLayer(frame);
    return allowSurfaceViewToBeSeperated();
}

void VRManager::checkAndSetSurfaceViewTo16V9Mode(Mesh& mesh) {
	if(getUsedVideoMode() == VRManager::VR_3D_16_9){
		Mesh::VertexArray<vec2> position(mesh.getPositionArray<vec2>());
		bool rotaton = false;
		float width = float(position[0].x) - float(position[2].x);
		float height = float(position[0].y) - float(position[2].y);
			    		
		if(width < height){
		   swap(width, height);
		   rotaton = true;    		   
		}
		height = width / 2 * 9 / 16;		    		
		if(rotaton){    			
			position[1].x = (position[0].x - height) / 2; 
		    position[1].y = width;		     
		    position[2].x = (position[0].x - height) / 2; 
		    position[2].y = 0.000000;		     
		    position[3].x = height + position[1].x; 
		    position[3].y = 0.000000; 
		    position[0].x = height + position[1].x;
		    position[0].y = width; 
		}else{
			position[1].x = (position[0].x - width) / 2; 
		    position[1].y = height;		     
		    position[1].x = (position[0].x - width) / 2; 
		    position[2].y = 0.000000;		     
		    position[3].x = width + position[1].x; 
		    position[3].y = 0.000000; 
		    position[0].x = width + position[1].x;
		    position[0].y = height; 
		}  		
	}
}


bool VRManager::allowSurfaceViewToBeSeperated() {
    if (!mVREnabled)
        return false;

    // FIXME: Fetch the video mode again for the online video, since the one 
    // obtained in initNewSplitingPart1() may be incorrect due to the bad network.
    if (needUpdateSurfaceViewSeperated()) {
        mAutoVideoMode = (VR_MODE)property_get_int32("system.glass.3dvideo_detect", VR_NONE);
    }

    if (isVideoPlaying()) {
        return canSplitVideoLayer();
    } else {
        return checkAllowToBeSeperated();
    }
}

bool VRManager::allowToBeSeperated(const String8 &name) {
    if (!mVREnabled)
        return false;

    if (isVideoPlaying()) {
        for (size_t i = 0; i < mAutoSplitScreenAppList.size(); i++) {
            if (name.find(mAutoSplitScreenAppList[i].string()) > -1)
                return canSplitVideoLayer();
        }
    }

    for (size_t i = 0; i < mNotSplitScreenAppList.size(); i++) {
        if (name.find(mNotSplitScreenAppList[i].string()) > -1)
            return false;
    }

    return true;
}

bool VRManager::toggleSurfaceViewSeperated() {
    if (isVideoPlaying()) {
        if (mForceVideoMode == VR_3D_16_9)
            mForceVideoMode = VR_2D;
        else if (mForceVideoMode == VR_3D_FULL)
            mForceVideoMode = VR_3D_16_9;
        else if (mForceVideoMode == VR_2D)
            mForceVideoMode = VR_3D_FULL;
        else
            mForceVideoMode = (mAutoVideoMode == VR_2D) ? VR_3D_FULL : VR_2D;

        return true;
    } else {
        return false;
    }
}
bool VRManager::toggleSystemConfigAppSeperated(const String8 &name){
	
	if(mAllowUserConfigSystemApp)
	{
		return false;
	}
	
	for (size_t i = 0; i < mSystemConfigAppList.size(); i++){
		if (name.find(mSystemConfigAppList[i].string()) > -1)
			return true;
	}

	return false;
}
bool VRManager::toggleSeperated(const String8 &name, VR_MODE mode) {

    // FIXME: Checking of SurfaceView is incorrect, so just ignore the param "mode"
	if(toggleSystemConfigAppSeperated(name)){
		ALOGD("system config app ,can't toggle from user");
		return false;
	}
    if (toggleSurfaceViewSeperated()) {
        ALOGD("toggle video seperated to be %d", mForceVideoMode == VR_2D ? 1 : 0);
        return true;
    }

    if (strcmp(name.string(), ("")) != 0) {
        bool old_seperated = allowToBeSeperated(name);
        bool new_seperated = (mode == VR_NONE) ? !old_seperated : ((mode == VR_3D_FULL) ? false : true);
        if (old_seperated != new_seperated) {
            if (new_seperated) {
                addToSeperateAppList(name);
            } else {
                addToNotSeperateAppList(name);
            }

            ALOGD("toggle app %s seperated to be %d", name.string(), new_seperated);

            return true;
        }
    }

    return false;
}

bool VRManager::addToSeperateAppList(const String8 &name) {
    bool exist = needToBeSeperated(name);
    if (!exist) {
        //ALOGD("Add %s to mSplitScreenAppList", name.string());
        mSplitScreenAppList.add(name);

        // FIXME: solve the conflict of mNotSplitScreenAppList
        removeFromNotSeperateAppList(name);
    }

    return !exist;
}

bool VRManager::addToNotSeperateAppList(const String8 &name) {
    bool notexist = allowToBeSeperated(name);
    if (notexist) {
        //ALOGD("Add %s to mNotSplitScreenAppList", name.string());
        mNotSplitScreenAppList.add(name);
        writeToAppList("/data/not_splitscreen_app.cfg", name);

        // FIXME: solve the conflict of mSplitScreenAppList
        removeFromSeperateAppList(name);
    }

    return notexist;
}

bool VRManager::removeFromSeperateAppList(const String8 &name) {
    // if exist, remove item
    bool result = false;
    for (size_t i = 0; i < mSplitScreenAppList.size(); i++) {
        if (name.find(mSplitScreenAppList[i].string()) > -1) {
            //ALOGD("remove %s from mSplitScreenAppList", name.string());
            mSplitScreenAppList.removeAt(i);
            result = true;
        }
    }

    return result;
}

bool VRManager::removeFromNotSeperateAppList(const String8 &name) {
    // if exist, remove item
    bool result = false;
        
    for (size_t i = 0; i < mNotSplitScreenAppList.size(); i++) {
        if (name.find(mNotSplitScreenAppList[i].string()) > -1) {
            //ALOGD("remove %s from mNotSplitScreenAppList", name.string());
            mNotSplitScreenAppList.removeAt(i);
            rmFromAppList("/data/not_splitscreen_app.cfg", name);
            result = true;
        }
    }

    return result;
}

// ---------------------------------------------------------------------------------------

RecompositeThread::RecompositeThread(VRManager& manager) :
        mVRManager(manager), mStop(false), mDisable(true) {
}

void RecompositeThread::onFirstRef() {
    ALOGD("RecompositeThread started!");
    run("RecompositeThread", PRIORITY_URGENT_DISPLAY + PRIORITY_MORE_FAVORABLE);
}

void RecompositeThread::doRecomposite() {

    mVRManager.mFlinger->signalTransaction();
}

bool RecompositeThread::threadLoop() {
    status_t err;
    nsecs_t now = systemTime(SYSTEM_TIME_MONOTONIC);
    nsecs_t nextEventTime = now + 10000000; //16666667; // 16.666667 ms

    while (true) {
        //bool isWakeup = false;

        { // Scope for lock
            android::Mutex::Autolock lock(mMutex);

            if (mStop) {
                return false;
            }

            if (mDisable) {
                err = mCond.wait(mMutex);
                if (err != NO_ERROR) {
                    ALOGE("error waiting for new events: %s (%d)",
                            strerror(-err), err);
                    return false;
                }
                continue;
            }

            if (now < nextEventTime) { // should always true
                err = mCond.waitRelative(mMutex, nextEventTime - now);

                if (err == TIMED_OUT) {
                    //isWakeup = true;
                } else if (err != NO_ERROR) {
                    ALOGE("error waiting for next event: %s (%d)",
                            strerror(-err), err);
                    return false;
                } else {
                    continue;
                }
            }

            now = systemTime(SYSTEM_TIME_MONOTONIC);
        }

        //if (isWakeup) {
        nextEventTime = now + 10000000;
        doRecomposite();
        //}
    }
    return false;
}

};//namespace ActsVR
