package com.android.settings.tvout;

import com.android.settings.R;
import android.app.Activity;
import android.view.Window;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.os.Bundle;
import android.graphics.Rect;
import android.util.Log;
import android.util.DisplayMetrics;

import java.util.ArrayList;


public class TvoutScreenResizeActivity extends Activity  {
    private Button mConfirmButton;
    private Button mCancelButton;
    private SeekBar mWidthSeekBar;
    private SeekBar mHeightSeekBar;
    private Rect mCurrScale;
    private TvoutUtils mTvoutUtils;
    private ArrayList<Integer> mDisplayTypeList;
    static final String TAG = "TvoutScreenResizeActivity";

    private static final int MAX_RANGE = 100;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());

        setContentView(R.layout.tvout_screen_resize);
        mWidthSeekBar = (SeekBar) findViewById(R.id.seek_width);
        mHeightSeekBar = (SeekBar) findViewById(R.id.seek_height);
        mConfirmButton = (Button) findViewById(R.id.confirm_button);
        mCancelButton = (Button) findViewById(R.id.cancel_button);
        mWidthSeekBar.setOnSeekBarChangeListener(mWidthListener);
        mHeightSeekBar.setOnSeekBarChangeListener(mHeightListener);

        mTvoutUtils = TvoutUtils.getInstance();
        mCurrScale = mTvoutUtils.getOverScan();
        mWidthSeekBar.setProgress(MAX_RANGE - mCurrScale.right);
        mHeightSeekBar.setProgress(MAX_RANGE - mCurrScale.top);

        mConfirmButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mTvoutUtils.saveConfig();
                finish();
            }
        });

        mCancelButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mTvoutUtils.cancelSetOverScan();
                finish();
            }
        });
    }

    protected void onDestroy() {
        mTvoutUtils.saveConfig();
        super.onDestroy();
    }

    private SeekBar.OnSeekBarChangeListener mWidthListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar seekBar) {}
        public void onStopTrackingTouch(SeekBar seekBar) {}
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mTvoutUtils.setOverScanX(MAX_RANGE - progress);
        }
    };

    private SeekBar.OnSeekBarChangeListener mHeightListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar seekBar) {}
        public void onStopTrackingTouch(SeekBar seekBar) {}
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mTvoutUtils.setOverScanY(MAX_RANGE - progress);
        }
    };
}
