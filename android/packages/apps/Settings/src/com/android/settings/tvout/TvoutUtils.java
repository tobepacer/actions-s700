package com.android.settings.tvout;

import com.android.settings.R;

import java.util.ArrayList;

import android.util.Log;
import android.graphics.Rect;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.DisplayOutputManager;

public class TvoutUtils {
    private static final String TAG = "TvoutUtils";

    public static TvoutUtils getInstance() {
        if (mTvoutUtils == null) {
            mTvoutUtils = new TvoutUtils();
        }
        return mTvoutUtils;
    }

    public boolean supportHdmi() {
    	updateauxiface();
        return mSupportHdmi;
    }

    public boolean supportCvbs() {
    	updateauxiface();
        return mSupportCvbs;
    }

    public boolean supportAdjustScreen() {
        if (mSupportHdmi || mSupportCvbs) {
            return true;
        }
        return false;
    }

    public String getHdmiCurrentMode() {
        return mSupportHdmi ? mIfaceInfoList.get(mHdmiIndex).currentMode : "";
    }

    public String getCvbsCurrentMode() {
        return mSupportCvbs ? mIfaceInfoList.get(mCvbsIndex).currentMode : "";
    }

    public String[] getHdmiModeList() {
        return mSupportHdmi ? mIfaceInfoList.get(mHdmiIndex).resolutionList : null;
    }

    public String[] getCvbsModeList() {
        return mSupportCvbs ? mIfaceInfoList.get(mCvbsIndex).resolutionList : null;
    }

    public void setHdmiMode(String mode) {
        if (mHdmiIndex != -1) {
            setMode(mHdmiIndex, mode);
        }
    }

    public void setCvbsMode(String mode) {
        if (mCvbsIndex != -1) {
            setMode(mCvbsIndex, mode);
        }
    }

    public Rect getOverScan() {
        return mCurrScale;
    } 

    public void cancelSetOverScan() {
        for (Integer val : mDisplayTypeList) {
            mDisplayManager.setOverScan(val, mDisplayManager.DISPLAY_OVERSCAN_X, mCurrScale.right);
            mDisplayManager.setOverScan(val, mDisplayManager.DISPLAY_OVERSCAN_Y, mCurrScale.top);
        }
    }

    public void setOverScanX(int scanVal) {
        for (Integer val : mDisplayTypeList) {
            mDisplayManager.setOverScan(val, mDisplayManager.DISPLAY_OVERSCAN_X, scanVal);
        }
    }

    public void setOverScanY(int scanVal) {
        for (Integer val : mDisplayTypeList) {
            mDisplayManager.setOverScan(val, mDisplayManager.DISPLAY_OVERSCAN_Y, scanVal);
        }
    }

    public void saveConfig() {
        mDisplayManager.saveConfig();
    }


    private void makeIfaceInfoList(int[] ifaceList, int whichDisplay) {
        if (ifaceList == null) {
            return;
        }

        for (int i = 0; i < ifaceList.length; i++) {
            DisplayInfo ifaceInfo = new DisplayInfo();

            ifaceInfo.displayType = whichDisplay;
            ifaceInfo.ifaceType = ifaceList[i];
            ifaceInfo.resolutionList = mDisplayManager.getModeList(ifaceInfo.displayType, ifaceInfo.ifaceType);
            ifaceInfo.currentMode = mDisplayManager.getCurrentMode(ifaceInfo.displayType, ifaceInfo.ifaceType);

            if (ifaceList[i] == mDisplayManager.DISPLAY_IFACE_TV) {
                //ifaceInfo.name = getString(R.string.device_display_cvbs);
                ifaceInfo.listAction = ActionType.LIST_CVBS;
                ifaceInfo.setAction = ActionType.SET_CVBS;
                mDisplayTypeList.add(ifaceInfo.displayType);
            } else if (ifaceList[i] == mDisplayManager.DISPLAY_IFACE_YPbPr) {
                //ifaceInfo.name = getString(R.string.device_display_ypbpr);
                ifaceInfo.listAction = ActionType.LIST_YPBPR;
                ifaceInfo.setAction = ActionType.SET_YPBPR;
            } else if (ifaceList[i] == mDisplayManager.DISPLAY_IFACE_VGA) {
                //ifaceInfo.name = getString(R.string.device_display_vga);
                ifaceInfo.listAction = ActionType.LIST_VGA;
                ifaceInfo.setAction = ActionType.SET_VGA;
            } else if (ifaceList[i] == mDisplayManager.DISPLAY_IFACE_HDMI) {
                //ifaceInfo.name = getString(R.string.device_display_hdmi);
                ifaceInfo.listAction = ActionType.LIST_HDMI;
                ifaceInfo.setAction = ActionType.SET_HDMI;
                mDisplayTypeList.add(ifaceInfo.displayType);
            } else if (ifaceList[i] == mDisplayManager.DISPLAY_IFACE_LCD) {
                //ifaceInfo.name = getString(R.string.device_display_lcd);
                ifaceInfo.listAction = ActionType.LIST_LCD;
                ifaceInfo.setAction = ActionType.SET_LCD;
            }
            mIfaceInfoList.add(ifaceInfo);
        }
    }

    //just use the first HDMI iface, others ignored! 
    private void ensureSupportHdmi() {
        mSupportHdmi = false;
        for (int index = 0; index < mIfaceInfoList.size(); index++) {
            if (mIfaceInfoList.get(index).listAction == ActionType.LIST_HDMI) {
                mHdmiIndex = index;
                mSupportHdmi = true;
                break;
            }
        }
    }

    //just use the first CVBS iface, others ignored! 
    private void ensureSupportCvbs() {
        mSupportCvbs = false;
        for (int index = 0; index < mIfaceInfoList.size(); index++) {
            if (mIfaceInfoList.get(index).listAction == ActionType.LIST_CVBS) {
                mCvbsIndex = index;
                mSupportCvbs = true;
                break;
            }
        }
    }

    private void ensureOverScan() {
        int display = mDisplayManager.MAIN_DISPLAY;
        if (mDisplayTypeList.size() > 0) {
            display = mDisplayTypeList.get(0);
        }
        mCurrScale = mDisplayManager.getOverScan(display);
    }

    private void setMode(int index, String mode) {
        if (!mIfaceInfoList.get(index).currentMode.equals(mode)) {
            mDisplayManager.setMode(mIfaceInfoList.get(index).displayType, mIfaceInfoList.get(index).ifaceType, mode);
            mDisplayManager.saveConfig();

            mIfaceInfoList.get(index).currentMode = mode;
        }
    }
    private void updateauxiface(){
    	
    	int[] mainIfaceList = mDisplayManager.getIfaceList(mDisplayManager.MAIN_DISPLAY);
        if (mainIfaceList == null || mainIfaceList.length == 0) {
            Log.e(TAG, "no available MAIN_DISPLAY interface!");
        }
        
    	int[] auxIfaceList = mDisplayManager.getIfaceList(mDisplayManager.AUX_DISPLAY);
    	Log.e(TAG, "mDisplayManager.getIfaceList(mDisplayManager.AUX_DISPLAY)="+auxIfaceList);
        if (auxIfaceList == null || auxIfaceList.length == 0) {
            Log.e(TAG, "no available AUX_DISPLAY interface!");
        }
        mIfaceInfoList.clear();
        makeIfaceInfoList(mainIfaceList, mDisplayManager.MAIN_DISPLAY);
        makeIfaceInfoList(auxIfaceList, mDisplayManager.AUX_DISPLAY);
        ensureSupportHdmi();
        ensureSupportCvbs();
        ensureOverScan();
    }

    private TvoutUtils() {
        try {
		    mDisplayManager = new DisplayOutputManager();
        } catch (RemoteException e) {
            Log.e(TAG, "DisplayOutputManager create failed!");
            return;
        }

        int[] mainIfaceList = mDisplayManager.getIfaceList(mDisplayManager.MAIN_DISPLAY);
        if (mainIfaceList == null || mainIfaceList.length == 0) {
            Log.e(TAG, "no available MAIN_DISPLAY interface!");
        }
        int[] auxIfaceList = mDisplayManager.getIfaceList(mDisplayManager.AUX_DISPLAY);
        if (auxIfaceList == null || auxIfaceList.length == 0) {
            Log.e(TAG, "no available AUX_DISPLAY interface!");
        }

        mDisplayTypeList = new ArrayList<Integer>();
        mIfaceInfoList = new ArrayList<DisplayInfo>();
        makeIfaceInfoList(mainIfaceList, mDisplayManager.MAIN_DISPLAY);
        makeIfaceInfoList(auxIfaceList, mDisplayManager.AUX_DISPLAY);

        ensureSupportHdmi();
        ensureSupportCvbs();
        ensureOverScan();
    }


    enum ActionType { 
        LIST_CVBS,
        LIST_YPBPR,
        LIST_VGA,
        LIST_HDMI,
        LIST_LCD,
        SET_CVBS,
        SET_YPBPR,
        SET_VGA,
        SET_HDMI,
        SET_LCD,
        RESIZE_SCREEN,
        RESERVED;
    }

    private class DisplayInfo {
        public int displayType;
        public int ifaceType;
        public String name;
        public ActionType listAction;
        public ActionType setAction;
        public String currentMode;
        public String[] resolutionList;

        public DisplayInfo() {
            displayType = -1;
            ifaceType = -1;
            name = null;
            listAction = ActionType.RESERVED;
            setAction = ActionType.RESERVED;
            currentMode = null;
            resolutionList = null;
        }
    }

    private static TvoutUtils mTvoutUtils = null;
    private DisplayOutputManager mDisplayManager;
    private ArrayList<DisplayInfo> mIfaceInfoList;
    private ArrayList<Integer> mDisplayTypeList;
    private boolean mSupportHdmi = false;
    private boolean mSupportCvbs = false;
    private int mHdmiIndex = -1;
    private int mCvbsIndex = -1;
    private Rect mCurrScale;
}
