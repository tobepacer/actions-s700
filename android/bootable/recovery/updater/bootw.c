#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "minzip/DirUtil.h"
#include "cutils/properties.h"
#include "minzip/Zip.h"

#define EMMC_DISKDEV "/dev/block/mmcblk0"
#define NAND_DISKDEV "/dev/block/nand0"

#define MBRC_FILEPATH 	"/tmp/mbrec.bin"
#define UBOOT_FILEPATH	"/tmp/uboot.bin"

#define MBRC_IN_ZIP			"bootbin/bootloader.bin"
#define UBOOT_IN_ZIP		"bootbin/uboot.bin"

#define BLOG(...) fprintf(cmd_pipe, "ui_print D:" __VA_ARGS__)

extern FILE* cmd_pipe;

/*unpack a file(zipfile) from zip to dstpath*/
static int uppack_file(ZipArchive *zip, const char *zipfile, const char *dstpath)
{
	const ZipEntry* binary_entry =  mzFindZipEntry(zip, zipfile);
	if (binary_entry == NULL) {
		BLOG("file=%s is not in zip\n",zipfile);
	    return -1;
	}

	unlink(dstpath);
	int fd = creat(dstpath, 0755);
	if (fd < 0) {
	    BLOG("Can't make %s\n", dstpath);
	    return -1;
	}
	int ret = mzExtractZipEntryToFile(zip, binary_entry, fd);
	if ( ret ) {
		BLOG("unpack file %s to %s, ok \n", zipfile, dstpath);
		ret = 0;
	}else{
		BLOG("unpack file %s to %s fail \n", zipfile, dstpath);
		ret = -1;
	}
	close(fd);
	return ret;
}
ssize_t  safe_read(int fd, void *buf, size_t count)
{
	ssize_t n;
	do {
		n = read(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}
ssize_t  safe_write(int fd, const void *buf, size_t count)
{
	ssize_t n;

	do {
		n = write(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}
/*write img to block device*/
int image_write(const char *dstfile, ssize_t offset, const char *srcflie)
{
    char buffer [4096];
    int rc , ret;
    int fd1, fd2 ;
    ssize_t len, flen;

	BLOG("cpy file %s to %s ....\n", srcflie, dstfile);

    if ((fd1 = open(srcflie, O_RDONLY)) < 0) {
		BLOG("open =%s fail:%s \n", dstfile, strerror(errno));
		return -1;
    }
    if ((fd2 = open(dstfile, O_WRONLY)) < 0) {
		close(fd1);
		BLOG("open =%s fail: %s \n", dstfile, strerror(errno));
		return -1;
    }
    flen = lseek(fd1, 0, SEEK_END);
    lseek(fd1, 0, SEEK_SET);
    lseek(fd2, offset, SEEK_SET);
    len = 0;
    do{
		rc = safe_read(fd1, buffer, 4096);
		if (rc < 0) {
			BLOG("image_write: failed to read %s\n", strerror(errno));
			break;
		}
		ret = safe_write(fd2, buffer, rc);
		if ( ret != rc ) {
			BLOG("image_write: failed to write %s\n", strerror(errno));
			rc = -1;
		}
		len += rc;
		if ( len >= flen )
			break;
		if ( (len & 0x1ffffff) == 0 ) { //32M sync once)
			sync();
		}
    } while ( rc == 4096 );

    close(fd1);
    BLOG("image_write close \n");
    close(fd2);
    sync();

    if ( rc >= 0 && flen <= len) {
    	BLOG("image_write %s file flen=%d,len=%d\n", dstfile, flen, len);
		return 0;
	}
	BLOG("image_write fail,flen=%d, wlen=%d \n", flen, len);
    return -1;
}

/*return 0 is nand, else is emmc*/
static int bootdev_check(char *sbuf)
{
	int ret = 1;
	int len;
	char *pstr, str_tmp[128];
	char value[PROPERTY_VALUE_MAX+1];

	pstr = strstr(sbuf, "misc.img");
	if ( pstr == NULL )
		return 1;
	pstr = pstr +10;
	len= 0;
	while(pstr[len]&& pstr[len] != '\n')
		len++;
	len = (len > 127? 127:len);
	memcpy(str_tmp, pstr ,len);
	str_tmp[len] = 0;
	BLOG("misc dev: %s", str_tmp);
	if ( strstr(str_tmp, "mmc") )
		return 1;
	if ( strstr(str_tmp, "nand") )
		return 0;

	memset(value, 0x0, PROPERTY_VALUE_MAX+1);
	len = property_get("ro.bootdev", value, NULL);
	BLOG("ro.bootdev prop len %d, %s\n", len, value);
	if (len  > 2 && strstr(value, "nand"))
		return  0;
	return 1;
}
int nand_boot_sync(void)
{
	FILE* fp;
	fp = fopen("/sys/kernel/debug/nanddisk/debug", "w");
	if (fp == NULL) {
		BLOG("open nand debug node fail \n");
		return -1;
	}
	fputs("debug_WriteBackBootloader()", fp);
	BLOG("nand sync ok\n");
	fclose(fp);
	return 0;
}
int bootloader_write(int is_emmc)
{
	int ret ;
	if ( is_emmc ) {
		ret = image_write(EMMC_DISKDEV, 4097*512, MBRC_FILEPATH);
		ret |= image_write(EMMC_DISKDEV, 8193*512, MBRC_FILEPATH);
	} else {
		ret = image_write(NAND_DISKDEV, 4096*512, MBRC_FILEPATH);
	}
	return  ret;
}

int uboot_write(int is_emmc)
{
	int ret ;
	if ( is_emmc) {
		ret = image_write(EMMC_DISKDEV, 3072*1024, UBOOT_FILEPATH);
		ret |= image_write(EMMC_DISKDEV, 5120*1024, UBOOT_FILEPATH);
	} else {
		ret = image_write(NAND_DISKDEV, 6144*512, UBOOT_FILEPATH);
		nand_boot_sync();
	}
	return  ret;
}
int boot_update_check( ZipArchive *za, FILE* cmd_fp, char *script)
{
	int ret = 0;
	int is_emmc;
	cmd_pipe = cmd_fp;
	fprintf(cmd_fp, "ui_print boot_update_check....\n");

	is_emmc = bootdev_check(script);
	fprintf(cmd_fp, "ui_print bootdev=%s\n", (is_emmc?"emmc":"nand"));
	if ( !uppack_file(za, MBRC_IN_ZIP, MBRC_FILEPATH) ) {
		fprintf(cmd_fp, "ui_print uppack bootloader to  %s ok\n", MBRC_FILEPATH);
		ret |= bootloader_write(is_emmc);
		fprintf(cmd_fp, "ui_print write bootloader: ret=%d\n", ret);
	}
	if ( !uppack_file(za, UBOOT_IN_ZIP, UBOOT_FILEPATH) ) {
		fprintf(cmd_fp, "ui_print uppack uboot to  %s ok\n", UBOOT_FILEPATH);
		ret |= uboot_write(is_emmc);
		fprintf(cmd_fp, "ui_print write uboot: ret=%d\n", ret);
	}
	fprintf(cmd_fp, "ui_print boot_update_check finished:ret=%d\n", ret);
	return ret;
}
