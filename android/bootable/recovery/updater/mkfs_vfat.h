
#ifndef _MKFS_VFAT_H_
#define _MKFS_VFAT_H_

#ifdef __cplusplus
extern "C" {
#endif

int mkfs_vfat(const char * bpath);

#ifdef __cplusplus
}
#endif

#endif


