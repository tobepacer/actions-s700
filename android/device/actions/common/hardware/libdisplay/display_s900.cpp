/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hardware/hardware.h>

#include <fcntl.h>
#include <errno.h>

#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <cutils/ashmem.h>
#include <cutils/log.h>
#include <cutils/atomic.h>
#include <cutils/properties.h>
#include <stdlib.h>
#include <linux/fb.h>

#include "display.h"

#define HDMI_SUPPORT_VID_SETTINGX

#define OWL_SETTING		"/data/setting"
#define OWL_SETTING_HDMI_ENABLE	"/data/setting/setting_hdmi_enable"
#define OWL_SETTING_HDMI_VID	"/data/setting/setting_hdmi_vid"
#define OWL_SETTING_HDMI_SIZE	"/data/setting/setting_hdmi_size"

#define OWL_HDMI_ENABLE		"/sys/class/owl_panel/hdmi/hpd_enable"
#define OWL_HDMI_PLUG		"/sys/class/owl_panel/hdmi/connected"
#define OWL_HDMI_VID		"/sys/class/owl_panel/hdmi/mode"
#define OWL_HDMI_AVAIL_VIDS	"/sys/class/owl_panel/hdmi/mode_list"
#define OWL_HDMI_SIZE		"/sys/class/owl_panel/hdmi/scale_factor"

#define OWL_HDMI_DEFAULT_VID	"1280x720@50"

/* HDMI size range from Setting Apk in percent, always is 0% ~ 100% */
#define HDMI_SIZE_MIN		(0)
#define HDMI_SIZE_STEPS		(100)

/*
 * Real HDMI size range, now is 90% ~ 100%,
 * we should convert 'SIZE' to "Real Size'
 */
#define HDMI_REAL_SIZE_MIN	(90)
#define HDMI_REAL_SIZE_STEPS	(10)

#define HDMI_SIZE_RATIO		(HDMI_SIZE_STEPS / HDMI_REAL_SIZE_STEPS)

#define SIZE_TO_REAL_SIZE(x) \
	(HDMI_REAL_SIZE_MIN + (x) / HDMI_SIZE_RATIO)

#define REAL_SIZE_TO_SIZE(x) \
	(HDMI_SIZE_MIN + ((x) - HDMI_REAL_SIZE_MIN) * HDMI_SIZE_RATIO)

struct videomode_tvmode {
	char *mode;
	int tv_mode;
};

struct disp_manager_context_t {
	
	struct owldisp_device_t device;
	
	int mDispFd;
	
	int mDispNum;
		
	struct owlfb_disp_device * mDisplays[MAX_DISPLAY_NUMBER];	
};

static struct videomode_tvmode mode_table[] = {
	{
		.mode = "1280x720@50",
		.tv_mode = OWL_TV_MOD_720P_50HZ,
	},
	{
		.mode = "1280x720@60",
		.tv_mode = OWL_TV_MOD_720P_60HZ,
	},
	{
		.mode = "1920x1080@50",
		.tv_mode = OWL_TV_MOD_1080P_50HZ,
	},
	{
		.mode = "1920x1080@60",
		.tv_mode = OWL_TV_MOD_1080P_60HZ,
	},
	{
		.mode = "720x576@50",
		.tv_mode = OWL_TV_MOD_576P,
	},
	{
		.mode = "720x480@60",
		.tv_mode = OWL_TV_MOD_480P,
	},
	{
		.mode = "3840x2160@30",
		.tv_mode = OWL_TV_MOD_3840_2160P_30HZ,
	},
	{
		.mode = "4096x2160@30",
		.tv_mode = OWL_TV_MOD_4096_2160P_30HZ,
	},
	{
		.mode = "3840x1080@60",
		.tv_mode = OWL_TV_MOD_3840_1080P_60HZ,
	},
	{
		.mode = "2560x1024@75",
		.tv_mode = OWL_TV_MOD_2560_1024P_75HZ,
	},
	{
		.mode = "2560x1024@60",
		.tv_mode = OWL_TV_MOD_2560_1024P_60HZ,
	},
};

#define MODE_TABLE_SIZE	(sizeof(mode_table) / sizeof(struct videomode_tvmode))

/**
 * Common hardware methods
 */
static int open_display_manager(const struct hw_module_t* module,
		const char* name, struct hw_device_t** device);

static struct hw_module_methods_t owldisp_module_methods = {
	open: open_display_manager,
};

/* The hal module for de */
/*****************************************************************************/

struct owldisp_module_t HAL_MODULE_INFO_SYM =
{
	common:
	{
		tag: HARDWARE_MODULE_TAG,
		version_major: 1,
		version_minor: 0, 
		id: DM_HARDWARE_MODULE_ID,
		name: "actions display manager",
		author: "ywwang",
		methods: &owldisp_module_methods,
		dso:0,
		reserved:
		{	0,}
	},
};

static int write_file_int(int fd, int value)
{
	char buf[256];
	int ret;

	ret = sprintf(buf, "%d\n", value);

	return write(fd, buf, ret);
}

static int read_file_int(int fd, int *value)
{
	char buf[256] = {0};

	read(fd, buf, 256);
	return sscanf(buf, "%d\n", value);
}

/**/
static int owldisp_get_disp_info(struct owldisp_device_t *dev,int disp,int * info){
	struct disp_manager_context_t* ctx = (struct disp_manager_context_t*) dev;
	int rc = 0;
	int i = 0;
	int offset = 1;
	ALOGD("owldisp_get_disp_info called ctx->mDispNum %d disp %d",ctx->mDispNum,disp);
	for(i = 0 ; i < ctx->mDispNum; i++){
		struct owlfb_disp_device * newInfo = ctx->mDisplays[i];
		if(newInfo != NULL && newInfo->mType == disp){
			rc = ioctl(ctx->mDispFd, OWLFB_GET_DISPLAY_INFO, newInfo);
			if(rc < 0){
				ALOGE("owldisp_get_disp_info error rc %d",rc);
				return rc ;
			}
			//info[offset++] = newInfo->mType; 
			info[offset++] = newInfo->mState; 		
			info[offset++] = newInfo->mPluginState; 
			info[offset++] = newInfo->mWidth; 		
			info[offset++] = newInfo->mHeight; 
			info[offset++] = newInfo->mRefreshRate; 		
			info[offset++] = newInfo->mWidthScale; 
			info[offset++] = newInfo->mHeightScale;
			info[offset++] = newInfo->mCmdMode;
			info[offset++] = newInfo->mIcType;
						   
//			ALOGE("owldisp_get_disp_info ~~~ mType %d mState %d mPluginState %d mWidth %d mHeight %d ",newInfo->mType,newInfo->mState,newInfo->mPluginState,newInfo->mWidth,newInfo->mHeight); 		
			ALOGE("owldisp_get_disp_info ~~~ PD_LCD_TYPE_OFF %d \n PD_LCD_LIGHTENESS_OFF %d\n PD_LCD_SATURATION_OFF %d\n PD_LCD_CONSTRAST_OFF %d\n \
					PD_LCD_DITHER_OFF %d\n PD_LCD_GAMMA_OFF %d\n PD_LCD_GAMMA_RX %d\n PD_LCD_GAMMA_GX %d\n PD_LCD_GAMMA_BX %d\n PD_LCD_GAMMA_RY %d\n PD_LCD_GAMMA_GY %d\n PD_LCD_GAMMA_BY %d\n",newInfo->mPrivateInfo[0],newInfo->mPrivateInfo[1],\
					newInfo->mPrivateInfo[2],newInfo->mPrivateInfo[3],newInfo->mPrivateInfo[4],newInfo->mPrivateInfo[5],newInfo->mPrivateInfo[6],newInfo->mPrivateInfo[7],\
					newInfo->mPrivateInfo[8],newInfo->mPrivateInfo[9],newInfo->mPrivateInfo[10],newInfo->mPrivateInfo[11]); 		
			memcpy((void *)&info[offset],(void *)&newInfo->mPrivateInfo[0],MAX_PRIVATE_DATA_SIZE * 4);

		}		
	}
	return rc;	
}



static int owldisp_set_disp_info(struct owldisp_device_t *dev , int disp,int * info){
	struct disp_manager_context_t* ctx = (struct disp_manager_context_t*) dev;
	int rc = 0;
	int i = 0;
	int offset = 1;
	ALOGD("owldisp_set_disp_info called disp %d",disp);
	for(i = 0 ; i < ctx->mDispNum; i++){
		struct owlfb_disp_device *newInfo = ctx->mDisplays[i];
		if(newInfo != NULL && newInfo->mType == disp){
			newInfo->mState = info[offset++]; 		
			newInfo->mPluginState = info[offset++]; 
			newInfo->mWidth =info[offset++]; 		
			newInfo->mHeight = info[offset++]; 
			newInfo->mRefreshRate = info[offset++]; 		
			newInfo->mWidthScale = info[offset++]; 
			newInfo->mHeightScale = info[offset++]; 
			newInfo->mCmdMode = info[offset++];
			newInfo->mIcType = info[offset++];
			memcpy((void *)&newInfo->mPrivateInfo[0],(void *)&info[offset],MAX_PRIVATE_DATA_SIZE*4);			
			rc = ioctl(ctx->mDispFd, OWLFB_SET_DISPLAY_INFO, newInfo);
			if(rc < 0){
				return rc ;
			}
		}
		
	}
	return rc;
}
static int init_display_device(struct disp_manager_context_t *dev){
	int i = 0;
	int rc = 0;
	struct disp_manager_context_t* ctx = (struct disp_manager_context_t*) dev;
	struct owlfb_disp_device info;
	ctx->mDispNum = 0;
	for(i = 0 ; i < MAX_DISPLAY_NUMBER; i++){
		info.mType = (1 << i);
		
		rc = ioctl(ctx->mDispFd, OWLFB_GET_DISPLAY_INFO, &info);
		
		if (rc < 0) {
		    ALOGW("failed to get mdisplays info rc :%d\n",rc);
		    continue;
		}
						
		struct owlfb_disp_device * newInfo = static_cast<struct owlfb_disp_device *>(malloc(sizeof(struct owlfb_disp_device)));
		
		newInfo->mType = info.mType; 
		newInfo->mState = info.mState; 		
		newInfo->mPluginState = info.mPluginState; 
		newInfo->mWidth = info.mWidth; 		
		newInfo->mHeight = info.mHeight; 
		newInfo->mRefreshRate = info.mRefreshRate; 		
		newInfo->mWidthScale = info.mWidthScale; 
		newInfo->mHeightScale = info.mHeightScale; 		
		memcpy(&info.mPrivateInfo[0],&newInfo->mPrivateInfo[0],MAX_PRIVATE_DATA_SIZE);
		ctx->mDisplays[ctx->mDispNum++] = newInfo;
		ALOGE("init_display_device i %d",i);
	}
	return 0;
}

static int owldisp_set_hdmi_enable(struct owldisp_device_t *dev, bool enable)
{
	ALOGD("owldisp_set_hdmi_enable ~~~ enable %d", enable);

	int fd1 = open(OWL_SETTING_HDMI_ENABLE, O_WRONLY);	
	int fd2 = open(OWL_HDMI_ENABLE, O_WRONLY);	
	if (fd1 < 0 || fd2 < 0){
		ALOGE("open file(%s or %s) error",
			OWL_SETTING_HDMI_ENABLE, OWL_HDMI_ENABLE);
		return -1;
	}
	
	write_file_int(fd1, enable ? 1 : 0);
	write_file_int(fd2, enable ? 1 : 0);
	
	close(fd1);
	close(fd2);

	return 0;
}

static int owldisp_get_hdmi_enable(struct owldisp_device_t *dev)
{
	int enable = 0;

	ALOGD("owldisp_get_hdmi_enable ~~~");

	int fd = open(OWL_SETTING_HDMI_ENABLE, O_RDONLY);	
	if (fd < 0) {
		ALOGE("open file(%s) error", OWL_SETTING_HDMI_ENABLE);
		return 0;
	}
	read_file_int(fd, &enable);
	close(fd);
	
	ALOGD("owldisp_get_hdmi_enable ~~~%d", enable);

	return enable;
}

   
static int owldisp_set_hdmi_vid(struct owldisp_device_t *dev, int vid)
{
#ifdef HDMI_SUPPORT_VID_SETTING
	int fd1, fd2;
	int i;
	int enable = owldisp_get_hdmi_enable(dev);

	ALOGD("owldisp_set_hdmi_vid ~~~ vid %d", vid);

	if (vid <= 0) {
		ALOGD("err: owldisp_set_hdmi_vid ~~~ vid %d", vid);
		return -1;
	}

	fd1 = open(OWL_SETTING_HDMI_VID, O_WRONLY);	
	fd2 = open(OWL_HDMI_VID, O_WRONLY);
	if (fd1 < 0 || fd2 < 0){
		ALOGE("open file(%s or %s) error",
			OWL_SETTING_HDMI_VID, OWL_HDMI_VID);
		return -1;
	}

	for (i = 0; i < MODE_TABLE_SIZE; i++) {
		if (mode_table[i].tv_mode == vid) {
			if (enable == 1) {
				owldisp_set_hdmi_enable(dev, 0);
				sleep(3);
			}

			write(fd2, mode_table[i].mode,
			      strlen(mode_table[i].mode));

			if (enable == 1) {
				owldisp_set_hdmi_enable(dev, 1);
				sleep(3);
			}

			break;
		}
	}
	
	write_file_int(fd1, vid);
	
	close(fd1);
	close(fd2);
#endif

	return 0;
}

static int owldisp_get_hdmi_vid(struct owldisp_device_t *dev)
{
	char mode[64];

	int vid_file = 0;
	int vid_driver = 0;
	int fd1;
	int fd2;
	int i;

	ALOGD("owldisp_get_hdmi_vid ~~~");

	fd1 = open(OWL_SETTING_HDMI_VID, O_RDWR);	
	fd2 = open(OWL_HDMI_VID, O_RDONLY);	
	if (fd1 < 0 || fd2 < 0){
		ALOGE("open file(%s or %s) error",
			OWL_SETTING_HDMI_VID, OWL_HDMI_VID);
		return -1;
	}

	read_file_int(fd1, &vid_file);

	read(fd2, mode, 64);
	for (i = 0; i < MODE_TABLE_SIZE; i++) {
		if (strstr(mode, mode_table[i].mode) != NULL) {
			vid_driver = mode_table[i].tv_mode;
			break;
		}
	}

	ALOGD("owldisp_get_hdmi_vid ~~~%d/%d", vid_file, vid_driver);

	if (vid_file != vid_driver && vid_driver > 0) {
		vid_file = vid_driver;
		write_file_int(fd1, vid_file);
	}

	close(fd1);
	close(fd2);

	return vid_file;
}

static int owldisp_get_hdmi_supported_vid_list(struct owldisp_device_t *dev,
					       int *vidlist)
{
	int cnt = 0;

#ifdef HDMI_SUPPORT_VID_SETTING
	char buf[1024] = { 0 };

	int fd;	
	int i;

	ALOGD("owldisp_get_hdmi_supported_vid_list");

	if ((fd = open(OWL_HDMI_AVAIL_VIDS, O_RDONLY)) < 0){
		ALOGE("open file(%s) error", OWL_HDMI_AVAIL_VIDS);
		return 0;
	}
	read(fd, buf, 1024);
	close(fd);
	
	for (i = 0; i < MODE_TABLE_SIZE; i++) {
		if (strstr(buf, mode_table[i].mode) != NULL) {
			vidlist[cnt] = mode_table[i].tv_mode;
			cnt++;
		}
	}
#endif

	return cnt;
} 

static int owldisp_get_hdmi_cable_state(struct owldisp_device_t *dev)
{
	struct disp_manager_context_t* ctx = (struct disp_manager_context_t*)dev;
	int state = 0;
	int fd;

	ALOGD("owldisp_get_hdmi_cable_state");

	if ((fd = open(OWL_HDMI_PLUG, O_RDONLY)) < 0) {
		ALOGE("open file(%s) error", OWL_HDMI_PLUG);
		return 0;
	}
	read_file_int(fd, &state);
	close(fd);
	
	ALOGD("%s ~~~%d", __func__, state);
	return state;
}

static int owldisp_set_hdmi_fitscreen (struct owldisp_device_t *dev,int value)
{
	ALOGD("owldisp_set_hdmi_fitscreen value %d ",value);
	return 0;
}

static int owldisp_get_hdmi_fitscreen (struct owldisp_device_t *dev)
{
	ALOGD("owldisp_get_hdmi_fitscreen ");
	return 0;
}

static int owldisp_set_hdmi_size(struct owldisp_device_t *dev, int xres,
				 int yres)
{
	char buf[256];
	int ret;
	int fd1, fd2;	

	ALOGD("owldisp_set_hdmi_size ~~~ xres %d, yres %d", xres, yres);

	fd1 = open(OWL_SETTING_HDMI_SIZE, O_WRONLY);	
	fd2 = open(OWL_HDMI_SIZE, O_WRONLY);	
	if (fd1 < 0 || fd2 < 0) {
		ALOGE("open file(%s or %s) error",
			OWL_SETTING_HDMI_SIZE, OWL_HDMI_SIZE);
		return -1;
	}
	
	ret = sprintf(buf, "%d %d\n", SIZE_TO_REAL_SIZE(xres),
		      SIZE_TO_REAL_SIZE(yres));

	ALOGD("owldisp_set_hdmi_size ~~~ %s, %d", buf, ret);
	write(fd1, buf, ret);
	write(fd2, buf, ret);
	
	close(fd1);
	close(fd2);
	
	return 0;
}

static int owldisp_get_hdmi_size(struct owldisp_device_t *dev, int *xres_yres)
{
	char buf[256] = {0};
	int xres, yres;

	int fd = open(OWL_SETTING_HDMI_SIZE, O_RDONLY);	

	if (fd < 0) {
		ALOGE("open file(%s) error", OWL_SETTING_HDMI_SIZE);
		return -1;
	}

	read(fd, buf, 256);
	close(fd);	
	
	sscanf((char*)buf, "%d %d\n", &xres, &yres);
	xres_yres[0] = REAL_SIZE_TO_SIZE(xres);
	xres_yres[1] = REAL_SIZE_TO_SIZE(yres);
	
	ALOGD("owldisp_get_hdmi_size ~~~ xres %d, yres %d",
	      xres_yres[0], xres_yres[1]);

	return 0;
}

static void owldisp_init_check_and_set(struct owldisp_device_t *dev)
{
	int vid = owldisp_get_hdmi_vid(dev);
	int enable = owldisp_get_hdmi_enable(dev);
	int xres_yres[2];

	owldisp_get_hdmi_size(dev, xres_yres);

	/* 
	 * set default vid, maybe invalid, in this case,
	 * driver will choose a default one
	 */
	owldisp_set_hdmi_vid(dev, vid);

	owldisp_set_hdmi_size(dev, xres_yres[0], xres_yres[1]);

	if (enable == 1) {
		/* HW always disabled, enable it if need */
		owldisp_set_hdmi_enable(dev, 1);
	}
}
 
/* Close mdisplays manager device */
static int close_display_manager(struct hw_device_t *dev) {	
	struct disp_manager_context_t* ctx = (struct disp_manager_context_t*) dev;
	if (ctx) {
		close(ctx->mDispFd);
		free(ctx);
	}
	return 0;
}

/* Open mdisplays manager device */

static int open_display_manager(const struct hw_module_t* module,
		const char* name, struct hw_device_t** device) {
			
	int status = -EINVAL;
	struct disp_manager_context_t *ctx = 
		static_cast<struct disp_manager_context_t *>(malloc(sizeof(struct disp_manager_context_t)));
		
	memset(ctx, 0, sizeof(*ctx));
	
	ALOGD("enter open_display_manager\n");
	
	ctx->device.common.tag = HARDWARE_DEVICE_TAG;
	ctx->device.common.version = 0;
	ctx->device.common.module = const_cast<struct hw_module_t *>(module);
	ctx->device.common.close = close_display_manager;
	//ctx->device.get_disp_num = get_disp_num;
	ctx->device.get_disp_info = owldisp_get_disp_info;
	ctx->device.set_disp_info = owldisp_set_disp_info;
	
 	ctx->device.set_hdmi_enable = owldisp_set_hdmi_enable;    
	ctx->device.get_hdmi_enable = owldisp_get_hdmi_enable;  
	ctx->device.set_hdmi_vid = owldisp_set_hdmi_vid;  
	ctx->device.get_hdmi_vid =  owldisp_get_hdmi_vid; 
	ctx->device.set_hdmi_size = owldisp_set_hdmi_size;
	ctx->device.get_hdmi_size = owldisp_get_hdmi_size;
	ctx->device.get_hdmi_supported_vid_list = owldisp_get_hdmi_supported_vid_list;
	ctx->device.get_hdmi_cable_state = owldisp_get_hdmi_cable_state;
	ctx->device.set_hdmi_fitscreen = owldisp_set_hdmi_fitscreen;
	ctx->device.get_hdmi_fitscreen = owldisp_get_hdmi_fitscreen;

	mkdir(OWL_SETTING, S_IRWXU | S_IRWXG);
	if ((access(OWL_SETTING_HDMI_ENABLE, F_OK)) < 0) {
		int fd = open(OWL_SETTING_HDMI_ENABLE, O_RDWR | O_CREAT,
					S_IRWXU | S_IRWXG);
		if (fd < 0) {
			ALOGE("create %s failed\n", OWL_SETTING_HDMI_ENABLE);
			return fd;
		}
		write_file_int(fd, 1);	/* set default value */
	}
	if ((access(OWL_SETTING_HDMI_VID, F_OK)) < 0) {
		int fd = open(OWL_SETTING_HDMI_VID, O_RDWR | O_CREAT,
					S_IRWXU | S_IRWXG);
		if (fd < 0) {
			ALOGE("create %s failed\n", OWL_SETTING_HDMI_VID);
			return fd;
		}
		write_file_int(fd, 19);	/* set default value */
	}
	if ((access(OWL_SETTING_HDMI_SIZE, F_OK)) < 0) {
		char buf[256];
		int ret;
		int fd = open(OWL_SETTING_HDMI_SIZE, O_RDWR | O_CREAT,
					S_IRWXU | S_IRWXG);
		if (fd < 0) {
			ALOGE("create %s failed\n", OWL_SETTING_HDMI_SIZE);
			return fd;
		}

		/* set default value */
		ret = sprintf(buf, "%d %d\n", SIZE_TO_REAL_SIZE(100),
			      SIZE_TO_REAL_SIZE(100));
		write(fd, buf, ret);
	}

	owldisp_init_check_and_set(&ctx->device);

#if 0
	ctx->mDispFd = open(DM_HARDWARE_DEVICE, O_RDWR, 0);

	init_display_device(ctx);

	if (ctx->mDispFd >= 0) {
		ALOGI("open de successfully! fd: %d", ctx->mDispFd);
		*device = &ctx->device.common;
		status = 0;
	} else {
		status = errno;
		close_display_manager(&ctx->device.common);
		ALOGE("ctx->mDispFd:%d,Error open mdisplays manager failed: %d %s", ctx->mDispFd,status, strerror(status));
		status = -status;
	}
#else
	ALOGI("open de successfully! fd: %d", ctx->mDispFd);
	*device = &ctx->device.common;
	status = 0;
#endif
	return status;
}

