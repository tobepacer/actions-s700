#define LOG_TAG "Displaymanager"

#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <signal.h>
#include <sys/wait.h>
#include <cutils/properties.h>
#include <cutils/log.h>
#include <sysutils/SocketClient.h>
#include "Config.h"
#include "DisplayManager.h"
#include "ResponseCode.h"

#define DISPLAY_SYSFS_NODE	"/sys/class/owl_panel/"
#define DISPLAY_SYSFS_NODE_TYPE_1	"/sys/class/adf/owl-adf-interface1/type"
#define DISPLAY_SYSFS_NODE_CONNECTED_1	"/sys/class/adf/owl-adf-interface1/hotplug_detect"

#define DISPLAY_SYSFS_NODE_TYPE_0	"/sys/class/adf/owl-adf-interface0/type"
#define DISPLAY_SYSFS_NODE_CONNECTED_0	"/sys/class/adf/owl-adf-interface0/hotplug_detect"


#define DISPLAY_TYPE_LCD	"LCD"
#define DISPLAY_TYPE_HDMI	"HDMI"
#define DISPLAY_TYPE_VGA	"VGA"
#define DISPLAY_TYPE_YPbPr	"YPbPr"
#define DISPLAY_TYPE_TV		"TV"

#define HDMI_VIDEO_YUV420	(4 << 8)

#define DISPLAY_CONFIG_FILE	"/cache/display.cfg"
#define DISPLAY_CONFIG_FORMAT						\
	"display=%d,iface=%d,enable=%d,overscan=%d,%d,%d,%d,mode=%s\n"

/* read config from 'buf' to 'node' */
#define DISPLAY_CONFIG_READ(buf, node)					\
	sscanf((buf), DISPLAY_CONFIG_FORMAT, &(node)->property, 	\
		&(node)->type, &(node)->enable, &(node)->overscan_left,	\
		&(node)->overscan_right, &(node)->overscan_top,		\
		&(node)->overscan_bottom, (node)->mode)

/* write config from 'node' to 'buf' */
#define DISPLAY_CONFIG_WRITE(buf, node)					\
	sprintf((buf), DISPLAY_CONFIG_FORMAT, (node)->property, 	\
		(node)->type, (node)->enable, (node)->overscan_left,	\
		(node)->overscan_right, (node)->overscan_top,		\
		(node)->overscan_bottom, (node)->mode)

#define PROPETY_OVERSCAN_MAIN	"persist.sys.overscan.main"
#define PROPETY_OVERSCAN_AUX	"persist.sys.overscan.aux"

enum {
	DISPLAY_INTERFACE_TV = 1,
	DISPLAY_INTERFACE_YPbPr,
	DISPLAY_INTERFACE_VGA,
	DISPLAY_INTERFACE_HDMI,
	DISPLAY_INTERFACE_LCD
};

enum {
	DISPLAY_OPERATE_READ = 0,
	DISPLAY_OPERATE_WRITE
};

/*============================================================================
 *				Public APIs
 *==========================================================================*/

DisplayManager::DisplayManager()
{

	ALOGD("[%s] VER 3.0", __FUNCTION__);
	powerup = 0;

#if (DISPLAY_UNTIL_WAKEUP == 0)
	init();
#endif

	ALOGD("[%s] success", __FUNCTION__);
}

void DisplayManager::init()
{
	int rc, found = 0, i, enable;
	int enabled;

	struct displaynode *head, *node;

	ALOGD("[%s] powerup %d", __FUNCTION__, powerup);

	if (powerup)
		return;
	powerup = 1;

	main_display_list = NULL;
	aux_display_list = NULL;

	readSysfs();

	rc = readConfig();
	if (rc)
		ALOGI("Read CFG File error, using configs from driver");

	for (node = main_display_list; node != NULL; node = node->next) {
		operateIfaceOverscan(node, DISPLAY_OPERATE_WRITE);

		/* enable all device at default, TODO */
		node->enable = 1;
		operateIfaceEnable(node, DISPLAY_OPERATE_WRITE);
	}

	for (node = aux_display_list; node != NULL; node = node->next) {
		operateIfaceOverscan(node, DISPLAY_OPERATE_WRITE);

		/* enable all device at default, TODO */
		node->enable = 1;
		operateIfaceEnable(node, DISPLAY_OPERATE_WRITE);
	}
}
int DisplayManager::getConnectInfo(){
	ALOGD(" [%s] ", __FUNCTION__);
	
	FILE *fd = NULL;
	DIR *dir = NULL;
	struct dirent * dirent = NULL;
	char buf[BUFFER_LENGTH];
	int i, rc,result;
	if(is_lcd_primary==1){
			fd = fopen(DISPLAY_SYSFS_NODE_CONNECTED_1, "r");
	}else{
			fd = fopen(DISPLAY_SYSFS_NODE_CONNECTED_0, "r");
	}
	if (NULL != fd) {
		memset(buf, 0, BUFFER_LENGTH);
		fgets(buf, BUFFER_LENGTH, fd);
		result
			= (atoi(buf) == 1 ? 1 : 0);
		fclose(fd);
		if(result == 0) return result;
	} else {
		ALOGE("[%s] [errno=%s] %s", __FUNCTION__,
			      strerror(errno), buf);
	}
	if(is_lcd_primary==1){
		fd = fopen(DISPLAY_SYSFS_NODE_TYPE_1, "r");
	}else{
			fd = fopen(DISPLAY_SYSFS_NODE_TYPE_0, "r");
	}
	if (NULL != fd) {
		memset(buf, 0, BUFFER_LENGTH);
		fgets(buf, BUFFER_LENGTH, fd);
		buf[4]='\0' ;
		if(strcmp("HDMI",buf)==0){
			result= 10;
		}else if(strcmp("cvbs",buf)==0){
			result= 20;
		}
		fclose(fd);
	} else {
			ALOGE("[%s] [errno=%s] %s", __FUNCTION__,
			      strerror(errno), buf);
	}

		return result;
}




void DisplayManager::getIfaceInfo(SocketClient *cli, int display)
{
	struct displaynode *head, *node;

	ALOGD("[%s] display %d", __FUNCTION__, display);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	if (head == NULL) {
		cli->sendMsg(ResponseCode::OperationFailed,
			     "Missing display node", false);
	} else {
		if(support_cvbs_and_hdmi==1){
			int res =getConnectInfo();
			if(res!=0){
				for (node = head; node != NULL; node = node->next){
					if((res==10 && node->type ==DISPLAY_INTERFACE_TV ) ||(res==20 && node->type ==DISPLAY_INTERFACE_HDMI)) 
						continue;
					cli->sendMsg(ResponseCode::InterfaceListResult,
						     type2string(node->type), false);

				cli->sendMsg(ResponseCode::CommandOkay,
					     "Interface list completed", false);
					}
			}else{
				cli->sendMsg(ResponseCode::OperationFailed,
					     "Missing display node", false);
			}
		}else{
			
			for (node = head; node != NULL; node = node->next)
				cli->sendMsg(ResponseCode::InterfaceListResult,
						 type2string(node->type), false);
			
			cli->sendMsg(ResponseCode::CommandOkay,
					 "Interface list completed", false);

		}
	}
}

void DisplayManager::getCurIface(SocketClient *cli, int display)
{
	struct displaynode *head, *node;

	ALOGD("[%s] display %d", __FUNCTION__, display);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		operateIfaceConnect(node, DISPLAY_OPERATE_READ);

		if (node->enable == 1 && node->connect == 1) {
			cli->sendMsg(ResponseCode::CommandOkay,
				     type2string(node->type), false);
			return;
		}
	}
	cli->sendMsg(ResponseCode::CommandOkay, type2string(head->type), false);
}

int DisplayManager::enableIface(int display, char *iface, int enable)
{
	struct displaynode *head, *node;
	int type = string2type(iface);

	if (!powerup)
		return -1;

	ALOGD("[%s] display %d iface %s enable %d", __FUNCTION__,
	      display, iface, enable);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if (node->type == type) {
			node->enable = enable;
			operateIfaceEnable(node, DISPLAY_OPERATE_WRITE);
			break;
		}
	}

	return 0;
}

void DisplayManager::getModeList(SocketClient *cli, int display, char *iface)
{
	FILE *fd = NULL;
	char buf[BUFFER_LENGTH];
	struct displaynode *head, *node;
	int type = string2type(iface);

	ALOGD("[%s] display %d iface %s", __FUNCTION__, display, iface);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if(node->type == type)
			break;
	}

	if (node == NULL) {
		cli->sendMsg(ResponseCode::CommandParameterError,
			     "Missing iface", false);
		return;
	}

	// Read modelist;
	memset(buf, 0, BUFFER_LENGTH);
	strcpy(buf, node->path);
	strcat(buf, "/mode_list");
	fd = fopen(buf, "r");

	memset(buf, 0, BUFFER_LENGTH);
	while (fgets(buf, BUFFER_LENGTH, fd) != NULL) {
		if (strlen(buf)) {
			ALOGD("%s", buf);
			buf[strlen(buf) - 1] = 0;
			cli->sendMsg(ResponseCode::ModeListResult, buf, false);
		}
		memset(buf, 0, BUFFER_LENGTH);
	}
	fclose(fd);
	cli->sendMsg(ResponseCode::CommandOkay, "Mode list completed", false);
}

void DisplayManager::getCurMode(SocketClient *cli, int display, char *iface)
{
	struct displaynode *head, *node;
	int type = string2type(iface);

	ALOGD("[%s] display %d iface %s", __FUNCTION__, display, iface);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if(node->type == type)
			break;
	}

	if (node == NULL) {
		cli->sendMsg(ResponseCode::CommandParameterError,
			     "Missing iface", false);
		return;
	}

	operateIfaceMode(node, DISPLAY_OPERATE_READ, node->mode);

	cli->sendMsg(ResponseCode::CommandOkay, node->mode, false);

	return;
}

int DisplayManager::setMode(int display, char *iface, char *mode)
{
	struct displaynode *head, *node;
	int type = string2type(iface);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	ALOGD("[%s] display %d iface %s mode %s", __FUNCTION__,
	      display, iface, mode);

	for (node = head; node != NULL; node = node->next) {
		if(node->type == type)
			break;
	}

	if (node == NULL)
		return -1;

	operateIfaceMode(node, DISPLAY_OPERATE_WRITE, mode);

	return 0;
}

void DisplayManager::get3DModes(SocketClient *cli, int display, char *iface)
{
	char buf[BUFFER_LENGTH];
	char modes[10];

	int type = string2type(iface);
	int modes_int;

	struct displaynode *head, *node;
	FILE *fd = NULL;

	ALOGD("[%s] display %d iface %s", __FUNCTION__, display, iface);

	if (type != DISPLAY_INTERFACE_HDMI) {
		cli->sendMsg(ResponseCode::CommandParameterError,
			     "Wrong HDMI iface", false);
		return;
	}

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if(node->type == type)
			break;
	}

	if (node == NULL) {
		cli->sendMsg(ResponseCode::CommandParameterError,
			     "Missing iface", false);
		return;
	}

	// Read 3d modes;
	memset(buf, 0, BUFFER_LENGTH);
	strcpy(buf, node->path);
	strcat(buf, "/3d_modes");
	fd = fopen(buf, "r");
	if (fd == NULL) {
		cli->sendMsg(ResponseCode::CommandOkay, "0", false);
		return;
	}

	modes_int = 0;

	memset(buf, 0, BUFFER_LENGTH);
	while (fgets(buf, BUFFER_LENGTH, fd) != NULL) {
		if (strstr(buf, "FRAME") != NULL)
			modes_int |= DISPLAY_3D_FRAME;

		if (strstr(buf, "LR") != NULL)
			modes_int |= DISPLAY_3D_LR;

		if (strstr(buf, "TB") != NULL)
			modes_int |= DISPLAY_3D_TB;
	}

	fclose(fd);

	sprintf(modes, "%d", modes_int);
	ALOGD("modes: %s", modes);

	cli->sendMsg(ResponseCode::CommandOkay, modes, false);
}

void DisplayManager::get3DMode(SocketClient *cli, int display, char *iface)
{
	FILE *fd = NULL;
	char buf[BUFFER_LENGTH];
	struct displaynode *head, *node;
	int type = string2type(iface);
	char mode[10];

	ALOGD("[%s] display %d iface %s", __FUNCTION__, display, iface);

	if (type != DISPLAY_INTERFACE_HDMI) {
		cli->sendMsg(ResponseCode::CommandParameterError,
			     "Wrong HDMI iface", false);
		return;
	}

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if(node->type == type)
			break;
	}

	if(node == NULL) {
		cli->sendMsg(ResponseCode::CommandParameterError,
			     "Missing iface", false);
		return;
	}

	// Read 3d modes;
	memset(buf, 0, BUFFER_LENGTH);
	strcpy(buf, node->path);
	strcat(buf, "/3d_mode");
	fd = fopen(buf, "r");
	if (fd == NULL) {
		cli->sendMsg(ResponseCode::CommandOkay, "-1", false);
		return;
	}

	memset(buf, 0, BUFFER_LENGTH);
	fgets(buf, BUFFER_LENGTH, fd);
	fclose(fd);

	if (strncmp(buf, "FRAME", 5) == 0)
		sprintf(mode, "%d", DISPLAY_3D_FRAME);
	else if (strncmp(buf, "LR", 2) == 0)
		sprintf(mode, "%d", DISPLAY_3D_LR);
	else if (strncmp(buf, "TB", 2) == 0)
		sprintf(mode, "%d", DISPLAY_3D_TB);
	else
		sprintf(mode, "%d", DISPLAY_3D_DISABLE);

	cli->sendMsg(ResponseCode::CommandOkay, mode, false);
}

int DisplayManager::set3DMode(int display, char *iface, int mode)
{
	FILE *fd = NULL;
	char buf[BUFFER_LENGTH];
	struct displaynode *head, *node;
	int type = string2type(iface);

	ALOGD("[%s] display %d iface %s mode %d", __FUNCTION__,
	      display, iface, mode);

	if (type != DISPLAY_INTERFACE_HDMI)
		return -1;

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if(node->type == type)
			break;
	}

	if (node == NULL)
		return -1;

	memset(buf, 0, BUFFER_LENGTH);
	strcpy(buf, node->path);
	strcat(buf, "/3d_mode");
	fd = fopen(buf, "w");
	if (fd == NULL)
		return -1;

	switch (mode) {
	case DISPLAY_3D_FRAME:
		fputs("FRAME", fd);
		break;

	case DISPLAY_3D_LR:
		fputs("LR", fd);
		break;

	case DISPLAY_3D_TB:
		fputs("TB", fd);
		break;

	case DISPLAY_3D_DISABLE:
	default:
		fputs("2D", fd);
		break;
	}
	fclose(fd);

	return 0;
}

void DisplayManager::setOverscan(int display, int direction, int scalevalue)
{
	struct displaynode *head, *node;

	ALOGD("[%s] display %d direction %d scalevalue %d", __FUNCTION__,
	      display, direction, scalevalue);

	/* set all nodes in 'display' list, TODO, FIXME */
	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if (direction == DISPLAY_OVERSCAN_X) {
			node->overscan_left = scalevalue;
			node->overscan_right = scalevalue;
		} else if (direction == DISPLAY_OVERSCAN_Y) {
			node->overscan_top = scalevalue;
			node->overscan_bottom = scalevalue;
		} else if (direction == DISPLAY_OVERSCAN_LEFT) {
			node->overscan_left = scalevalue;
		} else if (direction == DISPLAY_OVERSCAN_TOP) {
			node->overscan_top = scalevalue;
		} else if (direction == DISPLAY_OVERSCAN_RIGHT) {
			node->overscan_right = scalevalue;
		} else if (direction == DISPLAY_OVERSCAN_BOTTOM) {
			node->overscan_bottom = scalevalue;
		} else if (direction == DISPLAY_OVERSCAN_ALL) {
			node->overscan_left = scalevalue;
			node->overscan_right = scalevalue;
			node->overscan_top = scalevalue;
			node->overscan_bottom = scalevalue;
		}

		operateIfaceOverscan(node, DISPLAY_OPERATE_WRITE);
	}
}

void DisplayManager::setHDMIEnable(int display)
{
	struct displaynode *head, *node;
	struct displaynode *iface_hdmi = NULL, *iface_enabled = NULL;
	int enable, count = 0;

	if (!powerup)
		return;

	ALOGD("[%s] display %d", __FUNCTION__, display);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if (node->type == DISPLAY_INTERFACE_HDMI)
			iface_hdmi = node;

		if (node->enable == 1 && node != iface_hdmi)
			iface_enabled = node;

		count++;
	}

	//Theres is only one interface in input screen, no need to auto switch.
	if (count == 1)
		return;

	operateIfaceConnect(iface_hdmi, DISPLAY_OPERATE_READ);
	if ((iface_hdmi == iface_enabled || iface_enabled == NULL) &&
	    iface_hdmi != NULL) {
		operateIfaceMode(iface_hdmi, DISPLAY_OPERATE_WRITE,
				 iface_hdmi->mode);
		iface_hdmi->enable = 1;
		operateIfaceEnable(iface_hdmi, DISPLAY_OPERATE_WRITE);
	} else {
		if (ENABLE_AUTO_SWITCH &&
		    (iface_hdmi->type > iface_enabled->type)) {
			iface_enabled->enable = 0;
			operateIfaceEnable(iface_enabled,
					   DISPLAY_OPERATE_WRITE);
			//iface_hdmi->enable = 1;
			operateIfaceMode(iface_hdmi, DISPLAY_OPERATE_WRITE,
					 iface_hdmi->mode);
			operateIfaceEnable(iface_hdmi, DISPLAY_OPERATE_WRITE);
		}
	}
}

void DisplayManager::setHDMIDisable(int display)
{
	struct displaynode *head, *node;
	struct displaynode *iface_hdmi = NULL, *iface_enabled = NULL;
	int count = 0;
	int i = 0;
	int connect = 0;

	if (!powerup)
		return;

	ALOGD("[%s] display %d", __FUNCTION__, display);

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if (node->type == DISPLAY_INTERFACE_HDMI)
			iface_hdmi = node;

		if (node->enable == 1)
			iface_enabled = node;

		count++;
	}

	//Theres is only one interface in input screen, no need to auto switch.
	if (count == 1)
		return;

	for (i = 0; i < 30; i++) {
		usleep(10000);
		connect = operateIfaceConnect(iface_hdmi, DISPLAY_OPERATE_READ);
		if (connect == 1)
			return;
	}

	if (iface_hdmi == iface_enabled && iface_hdmi != NULL) {
		if (ENABLE_AUTO_SWITCH) {
			//iface_hdmi->enable = 0;
			//operateIfaceEnable(iface_hdmi, DISPLAY_OPERATE_WRITE);
			//for(i=0; i<100; i++)
			//{
			//	usleep(10000);
			//	operateIfaceEnable(iface_hdmi, DISPLAY_OPERATE_READ);
			//	if(iface_hdmi->enable == 0)
			//	break;
			//}

			for (node = head; node != NULL; node = node->next) {
				operateIfaceConnect(node, DISPLAY_OPERATE_READ);
				if (node->connect && node->enable == 0) {
					node->enable = 1;
					operateIfaceMode(node,
							 DISPLAY_OPERATE_WRITE,
							 node->mode);
					operateIfaceEnable(node,
						   DISPLAY_OPERATE_WRITE);
				}
			}
		}
	}
}

void DisplayManager::selectNextIface(int display)
{
	struct displaynode *head, *node;
	struct displaynode *iface_enabled = NULL, *iface_next = NULL;
	int enable;

	if (display == MAIN_DISPLAY)
		head = main_display_list;
	else
		head = aux_display_list;

	for (node = head; node != NULL; node = node->next) {
		if (node->enable == 1) {
			iface_enabled = node;
			break;
		}
	}

	//If there is no iface enabled or iface enabled is last interface,
	//set first iface as next selected iface.
	if (iface_enabled != node)
		iface_next = head;
	else if(node->next == NULL)
		iface_next = head;
	else
		iface_next = node->next;

	// Disable current interface.
	if (iface_enabled != NULL) {
		iface_enabled->enable = 0;
		operateIfaceEnable(iface_enabled, DISPLAY_OPERATE_WRITE);
	}

	// Enable next interface.
	iface_next->enable = 1;
	operateIfaceMode(iface_next, DISPLAY_OPERATE_WRITE, iface_next->mode);
	operateIfaceEnable(iface_next, DISPLAY_OPERATE_WRITE);
}

void DisplayManager::switchFramebuffer(int display, int xres, int yres)
{
	char name[64];

	if (display == 0)
		snprintf(name, 64, "/dev/graphics/fb0");
	else
		snprintf(name, 64, "/dev/graphics/fb2");

	ALOGD("%s display %d new res is x %d y %d", __FUNCTION__,
	      display, xres, yres);
	int fd = open(name, O_RDWR, 0);
	if (fd < 0) {
		ALOGE("%s open fb %s failed", __FUNCTION__, name);
		return;
	}

	struct fb_var_screeninfo info;
	if (ioctl(fd, FBIOGET_VSCREENINFO, &info) == -1) {
		ALOGE("%s %s get fb_var_screeninfo failed", __FUNCTION__, name);
		return;
	}
	info.xres = xres;
	info.yres = yres;
	//info.xres_virtual = xres;
	//info.yres_virtual = info.yres * 2;

	if (ioctl(fd, FBIOPUT_VSCREENINFO, &info) == -1)
		ALOGE("%s %s set fb_var_screeninfo failed", __FUNCTION__, name);

	close(fd);
}

void DisplayManager::saveConfig(void)
{
	FILE *fd = NULL;
	struct displaynode *node;
	char buf[BUFFER_LENGTH];

	fd = fopen(DISPLAY_CONFIG_FILE, "w");

	if (fd != NULL) {
		for (node = main_display_list; node != NULL;
		     node = node->next) {
			memset(buf, 0 , BUFFER_LENGTH);
			DISPLAY_CONFIG_WRITE(buf, node);
			fwrite(buf, 1, strlen(buf), fd);
		}

		for (node = aux_display_list; node != NULL;
		     node = node->next) {
			memset(buf, 0 , BUFFER_LENGTH);
			DISPLAY_CONFIG_WRITE(buf, node);
			fwrite(buf, 1, strlen(buf), fd);
		}
		fflush(fd);
		fclose(fd);
	}
	sync();
}

/*============================================================================
 *				Private APIs
 *==========================================================================*/

const char *DisplayManager::type2string(int type)
{

	switch(type) {
	case DISPLAY_INTERFACE_TV:
		return DISPLAY_TYPE_TV;
	case DISPLAY_INTERFACE_YPbPr:
		return DISPLAY_TYPE_YPbPr;
	case DISPLAY_INTERFACE_VGA:
		return DISPLAY_TYPE_VGA;
	case DISPLAY_INTERFACE_HDMI:
		return DISPLAY_TYPE_HDMI;
	case DISPLAY_INTERFACE_LCD:
		return DISPLAY_TYPE_LCD;
	}
	return NULL;
}

int DisplayManager::string2type(const char *str)
{
	if (!strcmp(str, DISPLAY_TYPE_TV))
		return DISPLAY_INTERFACE_TV;
	else if (!strcmp(str, DISPLAY_TYPE_YPbPr))
		return DISPLAY_INTERFACE_YPbPr;
	else if (!strcmp(str, DISPLAY_TYPE_VGA))
		return DISPLAY_INTERFACE_VGA;
	else if (!strcmp(str, DISPLAY_TYPE_HDMI))
		return DISPLAY_INTERFACE_HDMI;
	else if (!strcmp(str, DISPLAY_TYPE_LCD))
		return DISPLAY_INTERFACE_LCD;
	else
		return 0;
}

int DisplayManager::readConfig(void)
{
	char *buf = (char *)malloc(BUFFER_LENGTH);
	char *ptr, *ptr_space;

	int rc = 0, i = 0;
	int main_enabled = 0, aux_enabled = 0;
	int *enabled;

	FILE *fd = NULL;
	struct displaynode *node, *head;
	struct displaynode tmp_node;

	if (buf == NULL) {
		ALOGE("setHDMIEnable no memeory malloc buf\n");
		return -1;
	}

	fd = fopen(DISPLAY_CONFIG_FILE, "r");
	if (fd == NULL ) {
		ALOGE("%s not exist", DISPLAY_CONFIG_FILE);
		free(buf);
		return -1;
	}

	memset(buf, 0 , BUFFER_LENGTH);
	while (fgets(buf, BUFFER_LENGTH, fd) != NULL) {
		ALOGD("read cfg: %s", buf);

		DISPLAY_CONFIG_READ(buf, &tmp_node);

		// Check if this type exist in sysfs
		if (tmp_node.property == MAIN_DISPLAY) {
			head = main_display_list;
			enabled = &main_enabled;
		} else {
			head = aux_display_list;
			enabled = &aux_enabled;
		}

		for (node = head; node != NULL; node = node->next) {
			if (node->type == tmp_node.type)
				break;
		}

		if (node == NULL) {
			ALOGI("[%s] kernel not support iface %d\n",
			      __FUNCTION__, tmp_node.type);
			continue;
		}

		// update node's enable from config file
		node->enable = tmp_node.enable;

		// update node's overscan from config file
		node->overscan_left = tmp_node.overscan_left;
		node->overscan_right = tmp_node.overscan_right;
		node->overscan_top = tmp_node.overscan_top;
		node->overscan_bottom = tmp_node.overscan_bottom;

		ALOGD("[%s] display %d iface %d connect %d enable %d mode %s overscan %d,%d,%d,%d\n",
		      __FUNCTION__, node->property, node->type, node->connect,
		      node->enable, node->mode,
		      node->overscan_left, node->overscan_right,
		      node->overscan_top, node->overscan_bottom);
	}

	fclose(fd);
	free(buf);
	return 0;
}

int DisplayManager::readSysfs(void)
{
	FILE *fd = NULL;
	DIR *dir = NULL;
	struct dirent * dirent = NULL;
	char buf[BUFFER_LENGTH];
	int i, rc;
	support_cvbs_and_hdmi=0;
	is_lcd_primary=0;
	int support_hdmi=0;
	int support_cvbs=0;
	struct displaynode *node;

	dir = opendir(DISPLAY_SYSFS_NODE);
	if (dir == NULL) {
		ALOGE("[%s] Cannot open sysfs display node", __FUNCTION__);
		return -1;
	}

	while ((dirent = readdir(dir)) != NULL) {
		if (!strcmp(dirent->d_name, ".") ||
		    !strcmp(dirent->d_name, ".."))
			continue;

		node = (struct displaynode*)malloc(sizeof(struct displaynode));
		if (node == NULL) {
			ALOGE("[%s] Cannot malloc memory for display node",
			      __FUNCTION__);
			return -1;
		}
		memset(node, 0, sizeof(struct displaynode));

		// Get node path
		strcpy(node->path, DISPLAY_SYSFS_NODE);
		strcat(node->path, dirent->d_name);

		// Read node type
		if (!strcmp(dirent->d_name, "hdmi")){
			node->type = DISPLAY_INTERFACE_HDMI;
			support_hdmi=1;
		}
		else if (!strcmp(dirent->d_name, "cvbs")){
			node->type = DISPLAY_INTERFACE_TV;
			support_cvbs=1;
		}
		else
			node->type = DISPLAY_INTERFACE_LCD;

		// Read node property;
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/is_primary");
		fd = fopen(buf, "r");
		if (NULL != fd) {
			memset(buf, 0, BUFFER_LENGTH);
			fgets(buf, BUFFER_LENGTH, fd);
			node->property
				= (atoi(buf) == 1 ? MAIN_DISPLAY : AUX_DISPLAY);
			if((node->type==DISPLAY_INTERFACE_LCD) && (node->property==MAIN_DISPLAY)){
				is_lcd_primary=1;
				ALOGE("[%s]    is_lcd_primary=%d", __FUNCTION__, is_lcd_primary);
			}
			fclose(fd);
		} else {
			node->property = 0;
			ALOGE("[%s] [errno=%s] %s", __FUNCTION__,
			      strerror(errno), buf);
		}

		// Read node name
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/name");
		fd = fopen(buf, "r");
		if (NULL != fd) {
			fgets(node->name, NAME_LENGTH, fd);

			if (node->name[strlen(node->name) - 1] == '\n')
				node->name[strlen(node->name) - 1] = 0;

			fclose(fd);
		} else {
			ALOGE("[%s] [errno=%s] %s", __FUNCTION__, strerror(errno), buf);
		}

		// Read enable;
		operateIfaceEnable(node, DISPLAY_OPERATE_READ);

		// Read connect
		operateIfaceConnect(node, DISPLAY_OPERATE_READ);

		// Read mode
		operateIfaceMode(node, DISPLAY_OPERATE_READ, node->mode);

		// Read Overscan
		operateIfaceOverscan(node, DISPLAY_OPERATE_READ);

		display_list_add(node);

	}
	if(support_cvbs==1 && support_hdmi==1) support_cvbs_and_hdmi =1;
	closedir(dir);

	return 0;
}

void DisplayManager::display_list_add(struct displaynode *node)
{
	struct displaynode *head = NULL, *pos;

	ALOGD("[%s] display %d iface %s connect %d enable %d mode %s\n",
	      __FUNCTION__, node->property, type2string(node->type),
	      node->connect, node->enable, node->mode);

	if (node->property == MAIN_DISPLAY) {
		if (main_display_list == NULL)
			main_display_list = node;
		else
			head = main_display_list;
	} else {
		if (aux_display_list == NULL)
			aux_display_list = node;
		else
			head = aux_display_list;
	}

	if (head == NULL)
		return;

	// input node priority is higher than nodes in list
	if (node->type > head->type) {
		node->next = head;
		head->prev = node;
		if (node->property == MAIN_DISPLAY)
			main_display_list = node;
		else
			aux_display_list = node;
		return;
	}

	// input node priority is lower than first node
	for (pos = head; pos->next != NULL; pos = pos->next) {
		if(pos->next->type < node->type)
			break;
	}
	node->next = pos->next;
	pos->next = node;
}

int DisplayManager::operateIfaceEnable(struct displaynode *node, int type)
{
	FILE *fd = NULL;
	char *buf = NULL;

	buf = (char*)malloc(BUFFER_LENGTH);
	if (buf == NULL)
		return -1;

	ALOGD("[%s] property %d iface %d type %d value %d", __FUNCTION__,
	      node->property, node->type, type, node->enable);

	memset(buf, 0, BUFFER_LENGTH);
	strcpy(buf, node->path);
	strcat(buf, "/hpd_enable");

	if (type == DISPLAY_OPERATE_READ) {
		fd = fopen(buf, "r");

		memset(buf, 0, BUFFER_LENGTH);
		fgets(buf, BUFFER_LENGTH, fd);

		fclose(fd);

		node->enable = atoi(buf);
	} else if (type == DISPLAY_OPERATE_WRITE) {
		fd = fopen(buf, "w");

		if (node->enable == 1)
			fputc('1', fd);
		else if(node->enable == 0)
			fputc('0', fd);

		fclose(fd);
	}

	free(buf);

	return 0;
}

int DisplayManager::operateIfaceConnect(struct displaynode *node, int type)
{
	FILE *fd = NULL;
	char *buf = NULL;
	int connect;

	if (node == NULL)
		return -1;

	/* readonly */
	if (type != DISPLAY_OPERATE_READ)
		return -1;

	buf = (char*)malloc(BUFFER_LENGTH);
	if (buf == NULL)
		return -1;

	// Read enable;
	memset(buf, 0, BUFFER_LENGTH);

	strcpy(buf, node->path);
	strcat(buf, "/connected");
	fd = fopen(buf, "r");

	memset(buf, 0, BUFFER_LENGTH);
	fgets(buf, BUFFER_LENGTH, fd);

	fclose(fd);

	node->connect = atoi(buf);

	free(buf);
	return node->connect;
}

int DisplayManager::operateIfaceMode(struct displaynode *node, int type,
				     char *mode)
{
	FILE *fd = NULL;
	char *buf = NULL;
	int modelen = 0;

	if (node == NULL || mode == NULL)
		return -1;

	buf = (char*)malloc(BUFFER_LENGTH);
	if (buf == NULL)
		return -1;

	if (type == DISPLAY_OPERATE_READ) {
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/mode");
		fd = fopen(buf, "r");

		memset(buf, 0, BUFFER_LENGTH);
		fgets(buf, MODE_LENGTH, fd);

		fclose(fd);

		//There is a '\n' at last char, should be delelted.
		modelen = strlen(buf);
		if (modelen) {
			buf[modelen - 1] = 0;
			memset(mode, 0, MODE_LENGTH);
			memcpy(mode, buf, modelen -1);
		}
	} else if (type == DISPLAY_OPERATE_WRITE) {
		ALOGD("[%s] property %d iface %d type %d mode %s\n",
		      __FUNCTION__, node->property, node->type, type, mode);

		// Check the mode inputted is exist in mode list.
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/mode_list");
		fd = fopen(buf, "r");

		int exist = 0;
		memset(buf, 0, BUFFER_LENGTH);
		while (fgets(buf, BUFFER_LENGTH, fd) != NULL) {
			modelen = strlen(buf);
			if(!modelen)
				continue;

			//There is a '\n' at last char, should be delelted.
			buf[modelen - 1] = 0;
			if (strcmp(buf, mode) == 0) {
				exist = 1;
				break;
			}
		}

		// If input mode is not exist, just return.
		if (!exist) {
			ALOGV("[%s] Input mode is not exist in mode list",
			      __FUNCTION__);

			if (buf)
				free(buf);

			return 0;
			//operateIfaceMode(node, DISPLAY_OPERATE_READ, mode);
		}

		// Set the mode.
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/default_mode");
		fd = fopen(buf, "w");

		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, mode);
		strcat(buf, "\n");
		fputs(buf, fd);

		fclose(fd);
		if (mode != node->mode) {
			memset(node->mode, 0, MODE_LENGTH);
			strcpy(node->mode, mode);
		}

		// disable and enable to bring the new mode to force
		node->enable = 0;
		operateIfaceEnable(node, DISPLAY_OPERATE_WRITE);
		sleep(2);
		node->enable = 1;
		operateIfaceEnable(node, DISPLAY_OPERATE_WRITE);
	}

	free(buf);
	return 0;
}

int DisplayManager::operateIfaceOverscan(struct displaynode *node, int type)
{
	FILE *fd = NULL;
	char *buf = NULL;

	if (node == NULL)
		return -1;

	buf = (char*)malloc(BUFFER_LENGTH);
	if (buf == NULL)
		return -1;

	ALOGD("[%s] property %d iface %d type %d", __FUNCTION__,
	      node->property, node->type, type);

	if (type == DISPLAY_OPERATE_READ) {
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/overscan");
		fd = fopen(buf, "r");

		memset(buf, 0, BUFFER_LENGTH);
		fgets(buf, BUFFER_LENGTH, fd);

		fclose(fd);

		sscanf(buf, "%d,%d,%d,%d",
		       &node->overscan_left, &node->overscan_top,
		       &node->overscan_right, &node->overscan_bottom);
	} else if (type == DISPLAY_OPERATE_WRITE) {
		memset(buf, 0, BUFFER_LENGTH);
		strcpy(buf, node->path);
		strcat(buf, "/overscan");
		fd = fopen(buf, "w");

		memset(buf, 0, BUFFER_LENGTH);
		sprintf(buf, "%d,%d,%d,%d",
			node->overscan_left, node->overscan_top,
			node->overscan_right, node->overscan_bottom);
		fputs(buf, fd);

		fclose(fd);

		memset(buf, 0, BUFFER_LENGTH);
		sprintf(buf, "overscan %d,%d,%d,%d",
			node->overscan_left, node->overscan_top,
			node->overscan_right, node->overscan_bottom);

		if (node->property == MAIN_DISPLAY)
			property_set(PROPETY_OVERSCAN_MAIN, buf);
		else
			property_set(PROPETY_OVERSCAN_AUX, buf);
	}

	free(buf);
	return 0;
}
