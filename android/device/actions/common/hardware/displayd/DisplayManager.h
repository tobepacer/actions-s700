#ifndef _DISPLAYMANAGER_H__
#define _DISPLAYMANAGER_H__

#define BUFFER_LENGTH		256
#define NAME_LENGTH		48
#define MODE_LENGTH		36
#define TYPE_LENGTH		12

enum {
	MAIN_DISPLAY = 0,
	AUX_DISPLAY
};

enum {
	DISPLAY_OVERSCAN_X = 0,
	DISPLAY_OVERSCAN_Y,
	DISPLAY_OVERSCAN_LEFT,
	DISPLAY_OVERSCAN_RIGHT,
	DISPLAY_OVERSCAN_TOP,
	DISPLAY_OVERSCAN_BOTTOM,
	DISPLAY_OVERSCAN_ALL,
};

enum {
	DISPLAY_3D_DISABLE = -1,
	DISPLAY_3D_FRAME = 0,
	DISPLAY_3D_TB = 6,
	DISPLAY_3D_LR = 8,
};

struct displaynode {
	char path[BUFFER_LENGTH];
	char mode[MODE_LENGTH];
	char name[NAME_LENGTH];

	int type;
	int property;
	int enable;
	int connect;

	/* overscan adjust in pixel */
	int overscan_left;
	int overscan_right;
	int overscan_top;
	int overscan_bottom;

	struct displaynode *prev;
	struct displaynode *next;
};


class DisplayManager {
	public:
		DisplayManager();
		virtual ~DisplayManager() {}

		void init(void);
		
		void getIfaceInfo(SocketClient *cli, int display);
		void getCurIface(SocketClient *cli, int display);
		int enableIface(int display, char *iface, int enable);
		
		void getModeList(SocketClient *cli, int display, char *iface);
		void getCurMode(SocketClient *cli, int display, char *iface);
		int setMode(int display, char *iface, char *mode);

		void get3DModes(SocketClient *cli, int display, char *iface);
		void get3DMode(SocketClient *cli, int display, char *iface);
		int set3DMode(int display, char *iface, int mode);

		void setOverscan(int display, int direction, int scalevalue);

		void setHDMIEnable(int display);
		void setHDMIDisable(int display);
		void selectNextIface(int display);
		void switchFramebuffer(int display, int xres, int yres);

		void saveConfig(void);
	private:
		int powerup;
		/* support_cvbs_and_hdmi*/
		int is_lcd_primary;
		int support_cvbs_and_hdmi;

		struct displaynode *main_display_list;
		struct displaynode *aux_display_list;

		const char *type2string(int type);
		int string2type(const char *str);

		int readConfig(void);
		int readSysfs(void);

		void display_list_add(struct displaynode *node);

		int operateIfaceEnable(struct displaynode *node, int type);
		int operateIfaceConnect(struct displaynode *node, int type);
		int operateIfaceMode(struct displaynode *node, int type,
				     char *mode);
		int operateIfaceOverscan(struct displaynode *node, int type);
		int getConnectInfo();
};

#endif
