#
# Copyright (C) 2008 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# bluetooth packages
PRODUCT_PACKAGES += \
	dhcpcd.conf \
	wpa_supplicant_overlay.conf \
	p2p_supplicant_overlay.conf \
	init.wifi.rc \
	fw_bcm43455c0_ag.bin \
	fw_bcm4339a0_ag.bin \
	fw_bcm43455c0_ag_apsta.bin \
	fw_bcm4339a0_ag_apsta.bin \
	fw_bcm43455c0_ag_p2p.bin \
	fw_bcm4339a0_ag_p2p.bin \
	nvram_ap6255.txt \
	nvram_ap6335.txt \
	config.txt
	
