/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include <nativehelper/jni.h>
#include <utils/Log.h>
#include <android/native_window_jni.h>
#include <gui/Surface.h>
#include <sys/ioctl.h>
#include "adf.h"
#include "adfhwc.h"
#include "owl_adf_ext.h"

extern "C" {
    JNIEXPORT void JNICALL Java_com_android_vrjni_api_single_buffer_mode(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT void JNICALL Java_com_android_vrjni_api_wait_for_vsync(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_android_vrjni_api_init_for_vsync(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_android_vrjni_api_uninit_for_vsync(JNIEnv * env, jobject obj);
    bool vr_api_single_buffer_mode(ANativeWindow* theNativeWindow);
    long long vr_api_wait_for_vsync();
    bool vr_api_init_for_vsync();
    bool vr_api_uninit_for_vsync();
};

bool vr_api_single_buffer_mode(ANativeWindow* theNativeWindow)
{
	if(theNativeWindow == NULL)
	{
		__android_log_print(ANDROID_LOG_INFO,"libvrapi"," theNatvieWindow is null ,please recheck !");
		return false;
	}
	native_window_set_buffer_count(theNativeWindow, 1);	
	return true;
}
typedef struct
{
	/* File descriptor for ioctls on this interface */
	int									iFd;

	/* List of overlay engine IDs */
	adf_id_t							*aEngineIDs;

	/* Number of overlay engines */
	ssize_t								numEngines;

	/* Interface data. Some data may change (power/hotplug state, current
	 * mode) so should not be used without refreshing this.
	 */
	struct adf_interface_data			sData;
}
HWC_ADF_interface_t;

typedef struct
{
	struct adf_hwc_helper				*psHelper;
	struct adf_device					sDevice;
	/* List of interface IDs */
	adf_id_t							*aInterfaceIDs;
	/* List of interfaces */
	HWC_ADF_interface_t					*asInterfaces;

	/* Number of interfaces */
	ssize_t								numInterfaces;
}
ACTS_ADF_context_t; 

static ACTS_ADF_context_t * psCtx = NULL;

long long vr_api_wait_for_vsync()
{
	int err;
	int i ;
	int iFirstPrimaryInterface;
	long long time_stamp ;

	if(psCtx == NULL){

		adf_id_t *IDs;

		psCtx = (ACTS_ADF_context_t*)calloc(1, sizeof(ACTS_ADF_context_t));

		if (!psCtx)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to allocate context");
			goto err_out;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","psCtx alloc ok");

		if (adf_devices(&IDs) <= 0)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","No ADF devices detected. Fatal error.");
			goto err_free;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_devices ok");
		err = adf_device_open(IDs[0], O_RDWR, &psCtx->sDevice);
		if (err)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to open adf device (%s)", strerror(errno));
			free(IDs);
			goto err_free;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_device_open ok");
		psCtx->numInterfaces = adf_interfaces(&psCtx->sDevice,
											  &psCtx->aInterfaceIDs);
		if (psCtx->numInterfaces <= 0)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","No ADF interfaces detected. Fatal error.");
			goto err_close_device;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_interfaces ok");
		psCtx->asInterfaces = (HWC_ADF_interface_t*)calloc(psCtx->numInterfaces, sizeof(HWC_ADF_interface_t));
		if (!psCtx->asInterfaces)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to allocate interfaces");
			psCtx->numInterfaces = 0;
			goto err_close_device;
		}

		/* Zero is a valid fd so we need to initialize to -1 for error paths */
		for (i = 0; i < psCtx->numInterfaces; i++)
			psCtx->asInterfaces[i].iFd= -1;

		/* The primary interface may not be enumerated first by ADF. We assume
		 * later on that the primary interface is first, so we re-order the
		 * interfaces here.
		 */
		for (i = 0; i < psCtx->numInterfaces; i++)
		{

			psCtx->asInterfaces[i].iFd =
				adf_interface_open(&psCtx->sDevice, psCtx->aInterfaceIDs[i], O_RDWR);

			if (psCtx->asInterfaces[i].iFd < 0)
			{
				__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to open adf interface (%s)", strerror(errno));
				goto err_close_device;
			}

			err = adf_get_interface_data(psCtx->asInterfaces[i].iFd,
				&psCtx->asInterfaces[i].sData);
			if (err)
			{
				__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to get adf interface data (%s)", strerror(err));
				goto err_close_device;
			}

			if (psCtx->asInterfaces[i].sData.flags & ADF_INTF_FLAG_PRIMARY)
				iFirstPrimaryInterface = i;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","iFirstPrimaryInterface %d ok 1",iFirstPrimaryInterface);

		if (iFirstPrimaryInterface > 0)
		{
			adf_id_t tmpId = psCtx->aInterfaceIDs[0];
			HWC_ADF_interface_t sTmpIntf = psCtx->asInterfaces[0];

			i = iFirstPrimaryInterface;

			psCtx->aInterfaceIDs[0]    = psCtx->aInterfaceIDs[i];
			psCtx->aInterfaceIDs[i]    = tmpId;

			psCtx->asInterfaces[0] = psCtx->asInterfaces[i];
			psCtx->asInterfaces[i] = sTmpIntf;
		}
	}

	err = ioctl(psCtx->asInterfaces[0].iFd, OWL_ADF_WAIT_HALF_VSYNC, &time_stamp);

	if(err == 0)
	{
		return time_stamp;
	}

	return err;

err_close_device:
	adf_device_close(&psCtx->sDevice);
err_free:
	free(psCtx);
err_out:
	return 0;
}

bool vr_api_init_for_vsync()
{
	int err;
	int i ;
	int iFirstPrimaryInterface;
	long long time_stamp ;

	if(psCtx == NULL){

		adf_id_t *IDs;

		psCtx = (ACTS_ADF_context_t*)calloc(1, sizeof(ACTS_ADF_context_t));

		if (!psCtx)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to allocate context");
			goto err_out;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","psCtx alloc ok");

		if (adf_devices(&IDs) <= 0)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","No ADF devices detected. Fatal error.");
			goto err_free;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_devices ok");
		err = adf_device_open(IDs[0], O_RDWR, &psCtx->sDevice);
		if (err)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to open adf device (%s)", strerror(errno));
			free(IDs);
			goto err_free;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_device_open ok");
		psCtx->numInterfaces = adf_interfaces(&psCtx->sDevice,
											  &psCtx->aInterfaceIDs);
		if (psCtx->numInterfaces <= 0)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","No ADF interfaces detected. Fatal error.");
			goto err_close_device;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_interfaces ok");
		psCtx->asInterfaces = (HWC_ADF_interface_t*)calloc(psCtx->numInterfaces, sizeof(HWC_ADF_interface_t));
		if (!psCtx->asInterfaces)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to allocate interfaces");
			psCtx->numInterfaces = 0;
			goto err_close_device;
		}

		/* Zero is a valid fd so we need to initialize to -1 for error paths */
		for (i = 0; i < psCtx->numInterfaces; i++)
			psCtx->asInterfaces[i].iFd= -1;

		/* The primary interface may not be enumerated first by ADF. We assume
		 * later on that the primary interface is first, so we re-order the
		 * interfaces here.
		 */
		for (i = 0; i < psCtx->numInterfaces; i++)
		{

			psCtx->asInterfaces[i].iFd =
				adf_interface_open(&psCtx->sDevice, psCtx->aInterfaceIDs[i], O_RDWR);

			if (psCtx->asInterfaces[i].iFd < 0)
			{
				__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to open adf interface (%s)", strerror(errno));
				goto err_close_device;
			}

			err = adf_get_interface_data(psCtx->asInterfaces[i].iFd,
				&psCtx->asInterfaces[i].sData);
			if (err)
			{
				__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to get adf interface data (%s)", strerror(err));
				goto err_close_device;
			}

			if (psCtx->asInterfaces[i].sData.flags & ADF_INTF_FLAG_PRIMARY)
				iFirstPrimaryInterface = i;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","iFirstPrimaryInterface %d ok 1",iFirstPrimaryInterface);

		if (iFirstPrimaryInterface > 0)
		{
			adf_id_t tmpId = psCtx->aInterfaceIDs[0];
			HWC_ADF_interface_t sTmpIntf = psCtx->asInterfaces[0];

			i = iFirstPrimaryInterface;

			psCtx->aInterfaceIDs[0]    = psCtx->aInterfaceIDs[i];
			psCtx->aInterfaceIDs[i]    = tmpId;

			psCtx->asInterfaces[0] = psCtx->asInterfaces[i];
			psCtx->asInterfaces[i] = sTmpIntf;
		}
	}

	err = ioctl(psCtx->asInterfaces[0].iFd, OWL_ADF_INIT_HALF_VSYNC,NULL);

	return err;

err_close_device:
	adf_device_close(&psCtx->sDevice);
err_free:
	free(psCtx);
err_out:
	return 0;
}

bool vr_api_uninit_for_vsync()
{
	int err;
	int i ;
	int iFirstPrimaryInterface;
	long long time_stamp ;

	if(psCtx == NULL){

		adf_id_t *IDs;

		psCtx = (ACTS_ADF_context_t*)calloc(1, sizeof(ACTS_ADF_context_t));

		if (!psCtx)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to allocate context");
			goto err_out;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","psCtx alloc ok");

		if (adf_devices(&IDs) <= 0)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","No ADF devices detected. Fatal error.");
			goto err_free;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_devices ok");
		err = adf_device_open(IDs[0], O_RDWR, &psCtx->sDevice);
		if (err)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to open adf device (%s)", strerror(errno));
			free(IDs);
			goto err_free;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_device_open ok");
		psCtx->numInterfaces = adf_interfaces(&psCtx->sDevice,
											  &psCtx->aInterfaceIDs);
		if (psCtx->numInterfaces <= 0)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","No ADF interfaces detected. Fatal error.");
			goto err_close_device;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","adf_interfaces ok");
		psCtx->asInterfaces = (HWC_ADF_interface_t*)calloc(psCtx->numInterfaces, sizeof(HWC_ADF_interface_t));
		if (!psCtx->asInterfaces)
		{
			__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to allocate interfaces");
			psCtx->numInterfaces = 0;
			goto err_close_device;
		}

		/* Zero is a valid fd so we need to initialize to -1 for error paths */
		for (i = 0; i < psCtx->numInterfaces; i++)
			psCtx->asInterfaces[i].iFd= -1;

		/* The primary interface may not be enumerated first by ADF. We assume
		 * later on that the primary interface is first, so we re-order the
		 * interfaces here.
		 */
		for (i = 0; i < psCtx->numInterfaces; i++)
		{

			psCtx->asInterfaces[i].iFd =
				adf_interface_open(&psCtx->sDevice, psCtx->aInterfaceIDs[i], O_RDWR);

			if (psCtx->asInterfaces[i].iFd < 0)
			{
				__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to open adf interface (%s)", strerror(errno));
				goto err_close_device;
			}

			err = adf_get_interface_data(psCtx->asInterfaces[i].iFd,
				&psCtx->asInterfaces[i].sData);
			if (err)
			{
				__android_log_print(ANDROID_LOG_INFO,"libvrapi","Failed to get adf interface data (%s)", strerror(err));
				goto err_close_device;
			}

			if (psCtx->asInterfaces[i].sData.flags & ADF_INTF_FLAG_PRIMARY)
				iFirstPrimaryInterface = i;
		}
		__android_log_print(ANDROID_LOG_INFO,"libvrapi","iFirstPrimaryInterface %d ok 1",iFirstPrimaryInterface);

		if (iFirstPrimaryInterface > 0)
		{
			adf_id_t tmpId = psCtx->aInterfaceIDs[0];
			HWC_ADF_interface_t sTmpIntf = psCtx->asInterfaces[0];

			i = iFirstPrimaryInterface;

			psCtx->aInterfaceIDs[0]    = psCtx->aInterfaceIDs[i];
			psCtx->aInterfaceIDs[i]    = tmpId;

			psCtx->asInterfaces[0] = psCtx->asInterfaces[i];
			psCtx->asInterfaces[i] = sTmpIntf;
		}
	}

	err = ioctl(psCtx->asInterfaces[0].iFd, OWL_ADF_UNINIT_HALF_VSYNC,NULL);

	return err;

err_close_device:
	adf_device_close(&psCtx->sDevice);
err_free:
	free(psCtx);
err_out:
	return 0;	
}

JNIEXPORT void JNICALL Java_com_android_vrjni_api_single_buffer_mode(JNIEnv * env, jobject obj, jobject jsurface)
{
    ANativeWindow* theNativeWindow = NULL;  

    theNativeWindow = ANativeWindow_fromSurface(env, jsurface);

    if(theNativeWindow == NULL){
    	__android_log_print(ANDROID_LOG_INFO,"libvrapi","theNativeWindow is NULL,please check surface is ok");
    }

    if(vr_api_single_buffer_mode(theNativeWindow))
    {
    	__android_log_print(ANDROID_LOG_INFO,"libvrapi","vr_api_single_buffer_mode ok");
    }
    else
    {
    	__android_log_print(ANDROID_LOG_INFO,"libvrapi","vr_api_single_buffer_mode failed");
    }
}

JNIEXPORT void JNICALL Java_com_android_vrjni_api_wait_for_vsync(JNIEnv * env, jobject obj)
{
	vr_api_wait_for_vsync();
}

JNIEXPORT void JNICALL Java_com_android_vrjni_api_init_for_vsync(JNIEnv * env, jobject obj)
{
	vr_api_init_for_vsync();
}

JNIEXPORT void JNICALL Java_com_android_vrjni_api_uninit_for_vsync(JNIEnv * env, jobject obj)
{
	vr_api_uninit_for_vsync();
}