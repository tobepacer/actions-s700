#ifndef __ISP_GC5004_H__
#define __ISP_GC5004_H__

#include <string.h>
#include "isp_ctl.h"
#include "isp_gc5004_lsc.h"
#ifndef FIXED_14
#define FIXED_14 16384
#endif

static act_dpc_iso_info_t dpc_iso_table_gc5004[4] =
{
    {1, 4, 1, 7, 320, 0, 320, 320},
    {4, 8, 1, 7, 320, 0, 320, 320},
    {8, 16, 1, 7, 320, 0, 320, 320},
    {16, 32, 1, 7, 320, 0, 320, 320},
};

static unsigned char rgb_gamma_table_gc5004[6 * 3][64] =
{
	{0, 11, 22, 34, 45, 56, 67, 77, 85, 91, 97, 103, 108, 113, 117, 122, 126, 130, 134, 138, 141, 145, 148, 152, 155, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 188, 191, 194, 196, 199, 201, 204, 206, 209, 211, 214, 216, 218, 221, 223, 225, 227, 230, 232, 234, 236, 238, 240, 243, 245, 247, 249, 251, 253},
	{0, 11, 22, 34, 45, 56, 67, 77, 85, 91, 97, 103, 108, 113, 117, 122, 126, 130, 134, 138, 141, 145, 148, 152, 155, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 188, 191, 194, 196, 199, 201, 204, 206, 209, 211, 214, 216, 218, 221, 223, 225, 227, 230, 232, 234, 236, 238, 240, 243, 245, 247, 249, 251, 253},
	{0, 11, 22, 34, 45, 56, 67, 77, 85, 91, 97, 103, 108, 113, 117, 122, 126, 130, 134, 138, 141, 145, 148, 152, 155, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 188, 191, 194, 196, 199, 201, 204, 206, 209, 211, 214, 216, 218, 221, 223, 225, 227, 230, 232, 234, 236, 238, 240, 243, 245, 247, 249, 251, 253},
};

/*d65,d50,cwf,tl84,a,h*/
static int mcc_table_0x0219_gc5004[6][9] =
{
#if 0
    {1719, FIXED_14 - 360, FIXED_14 - 334, FIXED_14 - 406, 1588, FIXED_14 - 157, 29, FIXED_14 - 837, 1831},
    {1719, FIXED_14 - 360, FIXED_14 - 334, FIXED_14 - 406, 1588, FIXED_14 - 157, 29, FIXED_14 - 837, 1831},
    {1922, FIXED_14 - 703, FIXED_14 - 195, FIXED_14 - 515, 1507, 32, 47, FIXED_14 - 1049, 2026},
    {1922, FIXED_14 - 703, FIXED_14 - 195, FIXED_14 - 515, 1507, 32, 47, FIXED_14 - 1049, 2026},
    {1516, FIXED_14 - 75, FIXED_14 - 417, FIXED_14 - 678, 1745, FIXED_14 - 42, FIXED_14 - 129, FIXED_14 - 1449, 2602},
    {1516, FIXED_14 - 75, FIXED_14 - 417, FIXED_14 - 678, 1745, FIXED_14 - 42, FIXED_14 - 129, FIXED_14 - 1449, 2602},
#else
    {1715, FIXED_14 - 496, FIXED_14 - 274, FIXED_14 - 370, 1429, FIXED_14 - 34, 84, FIXED_14 - 833, 1853},
    {1715, FIXED_14 - 496, FIXED_14 - 274, FIXED_14 - 370, 1429, FIXED_14 - 34, 84, FIXED_14 - 833, 1853},
    {1715, FIXED_14 - 496, FIXED_14 - 274, FIXED_14 - 370, 1429, FIXED_14 - 34, 84, FIXED_14 - 833, 1853},
    {1715, FIXED_14 - 496, FIXED_14 - 274, FIXED_14 - 370, 1429, FIXED_14 - 34, 84, FIXED_14 - 833, 1853},
    {1715, FIXED_14 - 496, FIXED_14 - 274, FIXED_14 - 370, 1429, FIXED_14 - 34, 84, FIXED_14 - 833, 1853},
    {1715, FIXED_14 - 496, FIXED_14 - 274, FIXED_14 - 370, 1429, FIXED_14 - 34, 84, FIXED_14 - 833, 1853},
#endif
};

static int fcs_gain_table_gc5004[256] =
{
    255, 255, 255, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 251, 251, 251, 251, 251, 251, 251, 251, 251, 251, 250, 250, 250, 250, 250, 250, 250, 250, 249, 249, 249, 249, 249, 249, 249, 248, 248, 248, 248, 248, 248, 248, 247, 247, 247, 247, 247, 246, 246, 246, 246, 246, 245, 245, 245, 245, 244, 244, 244, 244, 243, 243, 243, 243, 242, 242, 242, 242, 241, 241, 241, 240, 240, 240, 239, 239, 239, 238, 238, 238, 237, 237, 236, 236, 236, 235, 235, 234, 234, 233, 233, 232, 232, 231, 231, 230, 230, 229, 229, 228, 228, 227, 227, 226, 225, 225, 224, 223, 223, 222, 221, 221, 220, 219, 218, 218, 217, 216, 215, 214, 213, 213, 212, 211, 210, 209, 208, 207, 206, 205, 204, 203, 202, 201, 199, 198, 197, 196, 195, 193, 192, 191, 189, 188, 187, 185, 184, 182, 181, 179, 178, 176, 174, 173, 171, 169, 167, 166, 164, 162, 160, 158, 156, 154, 152, 149, 147, 145, 143, 140, 138, 136, 133, 131, 128, 125, 123, 120, 117, 114, 111, 108, 105, 102, 99, 96, 93, 89, 86, 82, 79, 75, 71, 67, 64, 60, 56, 51, 47, 43, 39, 34, 29, 25, 20, 15, 10, 5, 0,
};

static act_awb_info_t awb_info_table_gc5004 =
{
    {375, 256, 353},
    {64, 64, 64},
    {1024, 1024, 1024},
    {50, 50, 50},
    {1, 0, 0},
    {1, 0, 0},
};

/*d65 d50 cwf tl84 a h*/
static act_awb_ct_param_t awb_ct_param_table_gc5004 =
{
    {1200, 1200, 1200, 1200, 1200, 1200},
    {600, 600, 600, 600, 600, 600},
    {230, 250, 253, 220, 260, 260},
    {270, 290, 290, 253, 300, 300},
    { - 155,  - 70, 12, 50, 102, 210},
    { - 65, 20, 102, 115, 210, 307},
    {1, 1, 1, 1, 1, 1},
    {80, 80, 80, 80, 80, 80},
};

static act_wps_info_t awb_wps_table_gc5004[4] =
{
#if 0
    {320, 4080, 4080, 4080, 320, 320, 3520, 480},
    {640, 4080, 4080, 4080, 320, 320, 3520, 480},
    {960, 4080, 4080, 4080, 320, 320, 3520, 480},
    {1600, 4080, 4080, 4080, 320, 320, 3520, 480},
#else
	{520, 4080, 4080, 4080, 300, 50, 1000, 600,{0.05f,0.2f,0.4f,0.6f}},
    {480, 4080, 4080, 4080, 220, 160, 1000, 600,{0.05f,0.2f,0.4f,0.6f}},
    {520, 4080, 4080, 4080, 120, 220, 1000, 600,{0.05f,0.2f,0.4f,0.6f}},
    {500, 4080, 4080, 4080, 80, 280, 1000, 600,{0.05f,0.2f,0.4f,0.6f}},
#endif
};

/*d65 d50 cwf tl84 a h*/
static act_wb_distribution_t wb_dsb_table_gc5004 =
{
    {0.5653f, 0.6365f, 0.7200f, 0.8255f, 0.9111f, 1.0577f},
    {0.7277f, 0.5945f, 0.5141f, 0.5047f, 0.3990f, 0.3384f},
};

static int ae_param_gc5004[3][7 + 8 + 4] =
{
    {3657, 2608, 2756, 1, 2756, 64, 64*32, /*hist param*/5, 95, 22, 233, 15, 30, 225, 240, /*slow gain*/64, 64, 4, 4},
    {2400, 2608, 1988, 1, 3980, 64, 64*32, /*hist param*/5, 95, 22, 233, 15, 30, 225, 240, /*slow gain*/64, 64, 4, 4},
    {3512, 2608, 1988, 1, 1988, 64, 64*32, /*hist param*/5, 95, 22, 233, 15, 30, 225, 240, /*slow gain*/64, 64, 4, 4},
};//fps*256,

static act_af_res_param_t af_res_param_table_972p_gc5004 =
{
	{0,16,3, 2,3,230,80,15,10},
	{{1,1,1,1,1,1,1,1,1}},
};

static act_af_res_param_t af_res_param_table_1080p_gc5004 =
{
	{0,16,3, 2,3,320,80,60,20},
	{{1,1,1,1,1,1,1,1,1}},
};

static  act_af_com_param_t af_com_param_table_gc5004 =
{
	{1,0,500},
	{2,20,8,{1000,1000,1000}},
	{80,5,3,8, {40000,100000},{60000,120000},  {6,60,6,1000}, {16,60,10,1000}, {20,60,20,1000}},
};

static void isphal_set_param_gc5004(act_isp_init_param_t *init_param)
{
    int i, j;

    init_param->nres = 3;
    init_param->res_param[0].nWidth = 1296;
    init_param->res_param[0].nHeight = 972;
    init_param->res_param[1].nWidth = 2592;
    init_param->res_param[1].nHeight = 1944;
    init_param->res_param[2].nWidth = 1920;
    init_param->res_param[2].nHeight = 1080;

    for(i = 0; i <  init_param->nres; i++)
    {
        init_param->res_param[i].ae_param.FrameRate = ae_param_gc5004[i][0];
        init_param->res_param[i].ae_param.TimingHTS = ae_param_gc5004[i][1];
        init_param->res_param[i].ae_param.TimingVTS = ae_param_gc5004[i][2];
        init_param->res_param[i].ae_param.ShutterMin = ae_param_gc5004[i][3];
        init_param->res_param[i].ae_param.ShutterMax = ae_param_gc5004[i][4];
        init_param->res_param[i].ae_param.GainMin = ae_param_gc5004[i][5];
        init_param->res_param[i].ae_param.GainMax = ae_param_gc5004[i][6];

        init_param->res_param[i].ae_param.AlgsAE = ACT_AE_AUTO;
        init_param->res_param[i].ae_param.AlgsAEMeter = ACT_AE_METER_MEAN;
        init_param->res_param[i].ae_param.AlgsAEMeterMean = ACT_AE_TARGET_HIST;

        init_param->res_param[i].ae_param.TargetMeanFix.yTargetLow = 120;
        init_param->res_param[i].ae_param.TargetMeanFix.yTargetHigh = 130;
		for (j = 0; j < 64; j++) init_param->res_param[i].ae_param.TargetCenterWeight[j] = 64;

        init_param->res_param[i].ae_param.TargetMeanHist.nLowMaxPecent = ae_param_gc5004[i][7 + 0];
        init_param->res_param[i].ae_param.TargetMeanHist.nHighMaxPecent = ae_param_gc5004[i][7 + 1];
        init_param->res_param[i].ae_param.TargetMeanHist.nLowTargetLevel = ae_param_gc5004[i][7 + 2];
        init_param->res_param[i].ae_param.TargetMeanHist.nHighTargetLevel = ae_param_gc5004[i][7 + 3];
        init_param->res_param[i].ae_param.TargetMeanHist.nLowMinLevel = ae_param_gc5004[i][7 + 4];
        init_param->res_param[i].ae_param.TargetMeanHist.nLowMaxLevel = ae_param_gc5004[i][7 + 5];
        init_param->res_param[i].ae_param.TargetMeanHist.nHighMinLevel = ae_param_gc5004[i][7 + 6];
        init_param->res_param[i].ae_param.TargetMeanHist.nHighMaxLevel = ae_param_gc5004[i][7 + 7];
        init_param->res_param[i].ae_param.GainStep.SlowGainYL = ae_param_gc5004[i][15 + 0];
        init_param->res_param[i].ae_param.GainStep.SlowGainYH = ae_param_gc5004[i][15 + 1];
        init_param->res_param[i].ae_param.GainStep.SlowGainStep1 = ae_param_gc5004[i][15 + 2];
        init_param->res_param[i].ae_param.GainStep.SlowGainStep2 = ae_param_gc5004[i][15 + 3];
        
        init_param->res_param[i].ae_param.FrmRateCtrl.bVarFrmRate = 0;
        init_param->res_param[i].ae_param.FrmRateCtrl.nDarkEnvFrmRateRatio = 20;   // fps_dark = fps_normal * 10 / x;
        init_param->res_param[i].ae_param.FrmRateCtrl.nGainAllow = 4;   // gain value got vs. set
        
        init_param->res_param[i].ae_param.nFrameIntervalAEC = 1;
        
        init_param->res_param[i].ae_param.nRationFlash2Torch = 30;
    }

    init_param->blc_param.blc_en = 1;
    init_param->blc_param.r_cut = 2;
    init_param->blc_param.gr_cut = 2;
    init_param->blc_param.gb_cut = 2;
    init_param->blc_param.b_cut = 2;

    isphal_set_param_gc5004_lsc(init_param);

    init_param->dpc_param.enable = 1;
    init_param->dpc_param.iso_num = 4;
    for (i = 0; i < 4; i++)
    {
        init_param->dpc_param.dpc_iso[i] = dpc_iso_table_gc5004[i];
    }

    init_param->bpithd_param = 0;

    init_param->rgbgamma_param.rgbgamma_en = 1;
    for (i = 0; i < MAX_TEMP_NUM; i++)
    {
        for (j = 0; j < 3; j++)
        {
            memcpy(init_param->rgbgamma_param.rgb_gamma[i][j], rgb_gamma_table_gc5004[/*i*3+j*/j], 64 * sizeof(unsigned char));
        }
    }

    init_param->mcc_param.mcc_en = 1;
    for (i = 0; i < MAX_TEMP_NUM; i++)
    {
        memcpy(init_param->mcc_param.mcc[i], mcc_table_0x0219_gc5004[i], 9 * sizeof(int));
    }

    init_param->fcs_param.fcs_en = 1;
    for (i = 0; i < MAX_TEMP_NUM; i++)
    {
        memcpy(init_param->fcs_param.fcs_gain[i], fcs_gain_table_gc5004, 256 * sizeof(int));
    }

    init_param->ccs_param.ccs_en = 1;
    for (i = 0; i < MAX_TEMP_NUM; i++)
    {
        init_param->ccs_param.nEt[i] = 1024 / 16;
        init_param->ccs_param.nCt1[i] = 16;
        init_param->ccs_param.nCt2[i] = 8;
        init_param->ccs_param.nYt1[i] = 10;
    }

	init_param->bsch_param.bsch_en = 1;
    for (i = 0; i < MAX_TEMP_NUM; i++)
    {
        init_param->bsch_param.nbrightness[i] = 0;
        init_param->bsch_param.nstaturation[i] = 4;
        init_param->bsch_param.ncontrast[i] = 0;
        init_param->bsch_param.hue[i] = 0;
    }

    init_param->banding_on = ACT_BANDING_NONE;

    /*awb*/
	init_param->awb_param.bfix_cur_tempure = ACT_NONE;
    init_param->awb_param.awb_temp.nAwbTempNum = 6;
    for(i = 0; i < MAX_TEMP_NUM; i++)
    {
        init_param->awb_param.awb_temp.nAwbTempValid[i] = 1;
    }
	/*CTT Tools:iCam_Tempture_Xe*/
	init_param->awb_param.awb_temp.nAwbTempIdx[0] = 1;
	init_param->awb_param.awb_temp.nAwbTempIdx[1] = 2;
	init_param->awb_param.awb_temp.nAwbTempIdx[2] = 3;
	init_param->awb_param.awb_temp.nAwbTempIdx[3] = 4;
	init_param->awb_param.awb_temp.nAwbTempIdx[4] = 7;
	init_param->awb_param.awb_temp.nAwbTempIdx[5] = 8;

    for (i = 0; i < 3; i++)
    {
        init_param->awb_param.awb_info.gain_default[i] = awb_info_table_gc5004.gain_default[i];
        init_param->awb_param.awb_info.gain_min[i] = awb_info_table_gc5004.gain_min[i];
        init_param->awb_param.awb_info.gain_max[i] = awb_info_table_gc5004.gain_max[i];
        init_param->awb_param.awb_info.gain_step[i] = awb_info_table_gc5004.gain_step[i];
        init_param->awb_param.awb_info.ctemp_weight[i] = awb_info_table_gc5004.ctemp_weight[i];
        init_param->awb_param.awb_info.rgbgain_weight[i] = awb_info_table_gc5004.rgbgain_weight[i];
    }

    init_param->awb_param.awb_zone_info.min_ratio = 0.05f;
    init_param->awb_param.awb_zone_info.rg_bot = 0.5600f;
    init_param->awb_param.awb_zone_info.rg_top = 1.2627f;
    init_param->awb_param.awb_zone_info.bg_bot = 0.4068f;
    init_param->awb_param.awb_zone_info.bg_top = 0.9500f;
    init_param->awb_param.awb_zone_info.rb_bot = 1.20f;
    init_param->awb_param.awb_zone_info.rb_top = 1.55f;
    init_param->awb_param.awb_zone_info.y_bot = 20;
    init_param->awb_param.awb_zone_info.y_top = 220;

    for (i = 0; i < MAX_TEMP_NUM; i++)
    {
        init_param->awb_param.awb_ct_param.ytop[i] = awb_ct_param_table_gc5004.ytop[i];
        init_param->awb_param.awb_ct_param.ybot[i] = awb_ct_param_table_gc5004.ybot[i];
        init_param->awb_param.awb_ct_param.xs[i] = awb_ct_param_table_gc5004.xs[i];
        init_param->awb_param.awb_ct_param.xe[i] = awb_ct_param_table_gc5004.xe[i];
        init_param->awb_param.awb_ct_param.ys[i] = awb_ct_param_table_gc5004.ys[i];
        init_param->awb_param.awb_ct_param.ye[i] = awb_ct_param_table_gc5004.ye[i];
		init_param->awb_param.awb_ct_param.bmax[i] = awb_ct_param_table_gc5004.bmax[i];
		init_param->awb_param.awb_ct_param.rmin[i] = awb_ct_param_table_gc5004.rmin[i];
    }

    for(i = 0; i < 4; i ++)
    {
		init_param->awb_param.awb_wps_info.wps[i] = awb_wps_table_gc5004[i];
    }
    
    init_param->awb_param.wb_dsb = wb_dsb_table_gc5004;

    /*abc*/
    init_param->abc_param.bc_algs = ACT_BC_AUTO;
    init_param->abc_param.abc_info.ratio_cut = 0.0005f;
    init_param->abc_param.abc_info.down_cut = 32;
    init_param->abc_param.abc_info.up_cut = 256 - 64;
    init_param->abc_param.abc_info.updata_step = 20;

	/*af*/
	init_param->res_param[0].af_res_param = af_res_param_table_972p_gc5004;
	init_param->res_param[1].af_res_param = af_res_param_table_1080p_gc5004;
	init_param->res_param[2].af_res_param = af_res_param_table_1080p_gc5004;
	init_param->af_com_param =af_com_param_table_gc5004;
}

#endif /* __ISP_GC5004_H__ */