ifeq ($(wildcard $(TOP)/device/actions/common/packages/ApkManager_pad/Android.mk),)  #source code not exist

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := prebuild_apk_ApkManager_pad
LOCAL_MODULE_STEM := $(LOCAL_MODULE).apk
LOCAL_SRC_FILES := app/ApkManager.apk
LOCAL_MODULE_TAGS := eng optional
LOCAL_MODULE_CLASS := APPS
LOCAL_DEX_PREOPT := false
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_PATH := $(TARGET_OUT)/app
include $(BUILD_PREBUILT)

endif 
