ifeq ($(wildcard $(TOP)/device/actions/common/packages/AgingTest/Android.mk),)

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := prebuild_apk_AgingTest
LOCAL_MODULE_STEM := prebuild_apk_AgingTest.apk
LOCAL_SRC_FILES := app/AgingTest.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := APPS
LOCAL_DEX_PREOPT := false
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_PATH := $(TARGET_OUT)/app
include $(BUILD_PREBUILT)

endif #here need a space, why? without it, compile failed!
