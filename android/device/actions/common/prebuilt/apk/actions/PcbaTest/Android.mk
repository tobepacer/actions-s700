ifeq ($(wildcard $(TOP)/device/actions/common/packages/PcbaTest/Android.mk),)

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := prebuild_apk_PcbaTest
LOCAL_MODULE_STEM := prebuild_apk_PcbaTest.apk
LOCAL_SRC_FILES := app/PcbaTest.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := APPS
LOCAL_DEX_PREOPT := false
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_PATH := $(TARGET_OUT)/app
include $(BUILD_PREBUILT)

endif 
