###############################################################################
# dangbei
LOCAL_PATH := $(call my-dir)

my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))

include $(CLEAR_VARS)
LOCAL_MODULE := dangbei
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := app/dangbei_market.apk
LOCAL_PREBUILT_JNI_LIBS := 
LOCAL_MODULE_TARGET_ARCH := 
include $(BUILD_PREBUILT)
