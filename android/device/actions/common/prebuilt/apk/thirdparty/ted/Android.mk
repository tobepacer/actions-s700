###############################################################################
# ted
LOCAL_PATH := $(call my-dir)



include $(CLEAR_VARS)
LOCAL_MODULE := ted
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := app/ted.apk
LOCAL_PREBUILT_JNI_LIBS := 
LOCAL_MODULE_TARGET_ARCH := 
include $(BUILD_PREBUILT)
