###############################################################################
# youtube
LOCAL_PATH := $(call my-dir)



include $(CLEAR_VARS)
LOCAL_MODULE := youtube
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := app/youtube_tv.apk
LOCAL_PREBUILT_JNI_LIBS := \
    lib/libcronet.so\
    lib/libm2ts_player.so
LOCAL_MODULE_TARGET_ARCH := arm
include $(BUILD_PREBUILT)
