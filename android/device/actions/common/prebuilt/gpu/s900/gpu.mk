# modules/
#PRODUCT_COPY_FILES += \
#$(LOCAL_PATH)/modules/pvrsrvkm.ko:system/modules/pvrsrvkm.ko

# vendor/bin/
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/bin/pvrsrvctl:system/vendor/bin/pvrsrvctl \
$(LOCAL_PATH)/vendor/bin/pvrdebug:system/vendor/bin/pvrdebug \
$(LOCAL_PATH)/vendor/bin/pvrlogdump:system/vendor/bin/pvrlogdump

# vendor/lib/
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/libsrv_um.so:system/vendor/lib/libsrv_um.so \
$(LOCAL_PATH)/vendor/lib/libIMGegl.so:system/vendor/lib/libIMGegl.so \
$(LOCAL_PATH)/vendor/lib/libpvrANDROID_WSEGL.so:system/vendor/lib/libpvrANDROID_WSEGL.so \
$(LOCAL_PATH)/vendor/lib/libusc.so:system/vendor/lib/libusc.so \
$(LOCAL_PATH)/vendor/lib/libglslcompiler.so:system/vendor/lib/libglslcompiler.so \
$(LOCAL_PATH)/vendor/lib/libPVRScopeServices.so:system/vendor/lib/libPVRScopeServices.so

# vendor/lib/: opencl
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/libPVROCL.so:system/vendor/lib/libPVROCL.so \
$(LOCAL_PATH)/vendor/lib/liboclcompiler.so:system/vendor/lib/liboclcompiler.so \
$(LOCAL_PATH)/vendor/lib/libufwriter.so:system/vendor/lib/libufwriter.so \
$(LOCAL_PATH)/vendor/lib/libLLVMIMG.so:system/vendor/lib/libLLVMIMG.so \
$(LOCAL_PATH)/vendor/lib/libclangIMG.so:system/vendor/lib/libclangIMG.so


# vendor/lib/: rs
#PRODUCT_COPY_FILES += \
#$(LOCAL_PATH)/vendor/lib/libPVRRS.so:system/vendor/lib/libPVRRS.so \
#$(LOCAL_PATH)/vendor/lib/librsccompiler.so:system/vendor/lib/librsccompiler.so \
#$(LOCAL_PATH)/vendor/lib/librsccore.bc:system/vendor/lib/librsccore.bc

# vendor/lib/egl
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/egl/libEGL_POWERVR_ROGUE.so:system/vendor/lib/egl/libEGL_POWERVR_ROGUE.so \
$(LOCAL_PATH)/vendor/lib/egl/libGLESv1_CM_POWERVR_ROGUE.so:system/vendor/lib/egl/libGLESv1_CM_POWERVR_ROGUE.so \
$(LOCAL_PATH)/vendor/lib/egl/libGLESv2_POWERVR_ROGUE.so:system/vendor/lib/egl/libGLESv2_POWERVR_ROGUE.so

# vendor/lib/hw
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/hw/gralloc.default.so:system/vendor/lib/hw/gralloc.default.so \
$(LOCAL_PATH)/vendor/lib/hw/hwcomposer.default.so:system/vendor/lib/hw/hwcomposer.default.so


# vendor/lib64/
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib64/libsrv_init.so:system/vendor/lib64/libsrv_init.so \
$(LOCAL_PATH)/vendor/lib64/libsrv_um.so:system/vendor/lib64/libsrv_um.so \
$(LOCAL_PATH)/vendor/lib64/libIMGegl.so:system/vendor/lib64/libIMGegl.so \
$(LOCAL_PATH)/vendor/lib64/libpvrANDROID_WSEGL.so:system/vendor/lib64/libpvrANDROID_WSEGL.so \
$(LOCAL_PATH)/vendor/lib64/libusc.so:system/vendor/lib64/libusc.so \
$(LOCAL_PATH)/vendor/lib64/libglslcompiler.so:system/vendor/lib64/libglslcompiler.so \
$(LOCAL_PATH)/vendor/lib64/libPVRScopeServices.so:system/vendor/lib64/libPVRScopeServices.so

# vendor/lib64/: opencl
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib64/libPVROCL.so:system/vendor/lib64/libPVROCL.so \
$(LOCAL_PATH)/vendor/lib64/liboclcompiler.so:system/vendor/lib64/liboclcompiler.so \
$(LOCAL_PATH)/vendor/lib64/libufwriter.so:system/vendor/lib64/libufwriter.so

# vendor/lib64/: rs
#PRODUCT_COPY_FILES += \
#$(LOCAL_PATH)/vendor/lib64/libPVRRS.so:system/vendor/lib64/libPVRRS.so \
#$(LOCAL_PATH)/vendor/lib64/librsccompiler.so:system/vendor/lib64/librsccompiler.so \
#$(LOCAL_PATH)/vendor/lib64/librsccore.bc:system/vendor/lib64/librsccore.bc

# vendor/lib64/egl
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib64/egl/libEGL_POWERVR_ROGUE.so:system/vendor/lib64/egl/libEGL_POWERVR_ROGUE.so \
$(LOCAL_PATH)/vendor/lib64/egl/libGLESv1_CM_POWERVR_ROGUE.so:system/vendor/lib64/egl/libGLESv1_CM_POWERVR_ROGUE.so \
$(LOCAL_PATH)/vendor/lib64/egl/libGLESv2_POWERVR_ROGUE.so:system/vendor/lib64/egl/libGLESv2_POWERVR_ROGUE.so

# vendor/lib64/hw
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib64/hw/gralloc.default.so:system/vendor/lib64/hw/gralloc.default.so \
$(LOCAL_PATH)/vendor/lib64/hw/hwcomposer.default.so:system/vendor/lib64/hw/hwcomposer.default.so

# egl.cfg
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/egl.cfg:system/lib/egl/egl.cfg

# etc/powervr.ini
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/etc/powervr.ini:system/etc/powervr.ini \
$(LOCAL_PATH)/etc/firmware/rgx.fw.signed:system/etc/firmware/rgx.fw.signed

