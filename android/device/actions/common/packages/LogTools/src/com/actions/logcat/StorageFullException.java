/**
 * @author caichsh<caichsh@artekmicro.com>
 * this class just a exception, it will be throw when d storage is full.
 */
package com.actions.logcat;

import java.io.IOException;

public class StorageFullException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2405079324596238419L;
	
}
