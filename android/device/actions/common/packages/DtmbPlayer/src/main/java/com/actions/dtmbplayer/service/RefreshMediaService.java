/*
 * @author Actions Semi.
 */

package com.actions.dtmbplayer.service;

import java.io.File;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.Files;
import android.provider.MediaStore.Files.FileColumns;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Video.VideoColumns;
import android.util.Log;

public class RefreshMediaService extends IntentService {
	private static final String TAG = RefreshMediaService.class.getSimpleName();

	long startNotify;
	static final String EXTERNAL_VOLUME = "external";
	public static final String KEY_REFRESH_FILE_PATH = "mediaPath";
	public static final String KEY_REFRESH_MODE = "mode";
	public static final String ACTION_MEDIA_SCANNER_SCAN_DIRECTORY = "android.intent.action.MEDIA_SCANNER_SCAN_DIR";
	public static final int MODE_DELETE = 0;
	public static final int MODE_ADD = 1;

	public RefreshMediaService() {
		super("FeRefreshMediaService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "oncreate:" + "start service");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.v(TAG, "--onHandleIntent--");
		String file = intent.getStringExtra(KEY_REFRESH_FILE_PATH);
		Log.d(TAG, file);
		int mode = intent.getIntExtra(KEY_REFRESH_MODE, MODE_DELETE);

		startNotify = System.currentTimeMillis();
		switch (mode) {
		case MODE_DELETE:
			notifyMediaDelete(file);
			break;

		case MODE_ADD:
			Log.i(TAG, "MODE_ADD");
			notifyMediaAdd(file);
			break;
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void notifyMediaDelete(String file) {

		Uri[] mediatypes = new Uri[] { Audio.Media.getContentUri(EXTERNAL_VOLUME), Video.Media.getContentUri(EXTERNAL_VOLUME),
				Images.Media.getContentUri(EXTERNAL_VOLUME), Files.getContentUri(EXTERNAL_VOLUME), };
		ContentResolver cr = getApplicationContext().getContentResolver();
		int a = 0;
		for (int i = 0; i < mediatypes.length; i++) {
			switch (i) {
			case 0:
				a = cr.delete(mediatypes[i], AudioColumns.DATA + "=?", new String[] { file });

				break;
			case 1:
				a = cr.delete(mediatypes[i], VideoColumns.DATA + "=?", new String[] { file });

				break;
			case 2:
				a = cr.delete(mediatypes[i], ImageColumns.DATA + "=?", new String[] { file });

				break;
			case 3:
				a = cr.delete(mediatypes[i], FileColumns.DATA + "=?", new String[] { file });

				break;
			}

		}

		Log.d(TAG, "notifyMediaDelete:" + file + "crud result" + a);

	}

	public void notifyMediaAdd(String file) {
		File mfile = new File(file);
		if (mfile.exists()) {
			/*
			 * notify the media to scan
			 */
			Uri mUri = Uri.fromFile(mfile);
			Intent mIntent = new Intent();
			mIntent.setData(mUri);
			if (mfile.isDirectory()) {
				Log.d(TAG, "notifyMediaAdd dir:" + file);
				mIntent.setAction(ACTION_MEDIA_SCANNER_SCAN_DIRECTORY);
			} else {
				Log.d(TAG, "notifyMediaAdd file:" + file);
				mIntent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			}
			getApplicationContext().sendBroadcast(mIntent);
		}
	}

	public static void notifyFileChange(Context context, String path, int mode) {
		Intent intent = new Intent(context, RefreshMediaService.class);
		intent.putExtra(KEY_REFRESH_FILE_PATH, path);
		intent.putExtra(KEY_REFRESH_MODE, mode);
		context.startService(intent);
	}

}
