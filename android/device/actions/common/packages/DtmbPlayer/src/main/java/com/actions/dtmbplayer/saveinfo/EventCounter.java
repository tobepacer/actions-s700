package com.actions.dtmbplayer.saveinfo;

import android.os.Handler;
import android.os.Message;

public class EventCounter {

	private static final int MESSAGE_CLEAR_COUNT = 0;

	private int count = 0;
	private int limit = 10;
	private OnEventCountEnoughListener mOnEventCountEnoughListener;
	private int EVENT_INTERVAL = 500; // 500ms

	public void count() {
		count++;
		if (count >= limit) {
			count = 0;
			if (mOnEventCountEnoughListener != null) {
				mOnEventCountEnoughListener.onEventCountEnough();
			}
		}
		mHandler.removeMessages(MESSAGE_CLEAR_COUNT);
		mHandler.sendEmptyMessageDelayed(MESSAGE_CLEAR_COUNT, EVENT_INTERVAL);
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setOnEventCountEnoughListener(OnEventCountEnoughListener listener) {
		mOnEventCountEnoughListener = listener;
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == MESSAGE_CLEAR_COUNT) {
				count = 0;
			}
		}

	};

	public interface OnEventCountEnoughListener {
		void onEventCountEnough();
	}
}
