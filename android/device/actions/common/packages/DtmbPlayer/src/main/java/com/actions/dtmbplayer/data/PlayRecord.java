package com.actions.dtmbplayer.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class PlayRecord {
	private static final String TAG = PlayRecord.class.getSimpleName();
	private static final String SAVE_FILE = "playrecord.txt";
	private static final int MAX_RECORD = 20;

	private static PlayRecord sPlayRecord;
	private static final Object mObject = new Object();
	private LinkedList<Channel> mPlayChannels;
	private Context mContext;

	private PlayRecord(Context context) {
		mContext = context;
		mPlayChannels = new LinkedList<Channel>();
		initPlayRecord();
	}

	public static PlayRecord getInstance(Context context) {
		if (sPlayRecord == null) {
			synchronized (mObject) {
				if (sPlayRecord == null) {
					sPlayRecord = new PlayRecord(context);
				}
			}
		}
		return sPlayRecord;
	}

	public void addFirst(Channel channel) {
		if (mPlayChannels.contains(channel)) {
			mPlayChannels.remove(channel);
			mPlayChannels.addFirst(channel);
			return;
		}
		mPlayChannels.addFirst(channel);
		if (mPlayChannels.size() > MAX_RECORD) {
			mPlayChannels.pollLast();
		}
	}

	public Channel peek() {
		return mPlayChannels.peek();
	}

	public void save() {
		JSONArray jsonArray = new JSONArray();
		try {
			for (int i = 0; i < mPlayChannels.size(); i++) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("frequency", mPlayChannels.get(i).frequency);
				jsonObject.put("programId", mPlayChannels.get(i).programId);
				jsonArray.put(jsonObject);
			}
			write(jsonArray.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private String read() {
		FileInputStream fis = null;
		StringBuilder builder = new StringBuilder();
		try {
			fis = mContext.openFileInput(SAVE_FILE);
			int hasRead = 0;
			byte[] buffer = new byte[1024];
			while ((hasRead = fis.read(buffer)) > 0) {
				builder.append(new String(buffer, 0, hasRead));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return builder.toString();
	}

	private void write(String content) {
		FileOutputStream fos = null;
		try {
			fos = mContext.openFileOutput(SAVE_FILE, Context.MODE_PRIVATE);
			fos.write(content.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void initPlayRecord() {
		mPlayChannels.clear();
		String jsonString = read();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Channel channel = new Channel();
				channel.frequency = jsonObject.getInt("frequency");
				channel.programId = jsonObject.getInt("programId");
				mPlayChannels.add(channel);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void release() {
		mPlayChannels.clear();
		sPlayRecord = null;
	}
}
