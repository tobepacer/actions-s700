package com.actions.dtmbplayer.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ProgramDatabaseOpenHelper extends SQLiteOpenHelper {
	private static final String TAG = ProgramDatabaseOpenHelper.class.getSimpleName();
	public static final String DB_NAME = "program_info.db3";
	private static final int DB_VERSION = 1;
	public static final String DB_TABLE_NAME = "program";

	public static final String DB_COLUMN_ID = "_id";
	public static final String DB_COLUMN_FREQUENCY = "frequency";
	public static final String DB_COLUMN_PROGRAM_ID = "program_id";
	public static final String DB_COLUMN_PROGRAM_NAME = "program_name";

	public static final int DB_INDEX_ID = 0;
	public static final int DB_INDEX_FREQUENCY = 1;
	public static final int DB_INDEX_PROGRAM_ID = 2;
	public static final int DB_INDEX_PROGRAM_NAME = 3;

	public static final String[] DB_PROJECTION = new String[] { DB_COLUMN_ID, DB_COLUMN_FREQUENCY, DB_COLUMN_PROGRAM_ID, DB_COLUMN_PROGRAM_NAME };
	private Context mContext;

	public ProgramDatabaseOpenHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "onCreate");
		String sql = String.format("CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY, %s INTEGER, %s INTEGER, %s VARCHAR)", DB_TABLE_NAME,
				DB_PROJECTION[0], DB_PROJECTION[1], DB_PROJECTION[2], DB_PROJECTION[3]);
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
		onCreate(db);
	}

}
