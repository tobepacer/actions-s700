package com.actions.dtmbplayer.widget;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actions.dtmbplayer.R;
import com.actions.dtmbplayer.data.Channel;

public class TestFragment extends Fragment implements OnClickListener {
	private static final String TAG = TestFragment.class.getSimpleName();

	private View mRootView;
	private TextView mTextViewShowBER;
	private EditText mEditTextSetChannel;
	private TextView mTextViewShowFreqInfo;
	private Button mButtonSetChannel;
	private Handler mHandler;

	private DtmbPlayer mDtmbPlayer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mDtmbPlayer = DtmbPlayer.getInstance(getActivity());
		mHandler = new BERShowHandler();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = LayoutInflater.from(getActivity()).inflate(R.layout.test_fragment_layout, container, false);
		mTextViewShowBER = (TextView) mRootView.findViewById(R.id.text_ber);
		mEditTextSetChannel = (EditText) mRootView.findViewById(R.id.edittext_channel);
		mButtonSetChannel = (Button) mRootView.findViewById(R.id.button_set_channel);
		mButtonSetChannel.setOnClickListener(this);
		mTextViewShowFreqInfo = (TextView) mRootView.findViewById(R.id.textview_show_freq_detail);
		return mRootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		startShowBER();
	}

	@Override
	public void onPause() {
		super.onPause();
		stopShowBER();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stopShowBER();
		mHandler = null;
	}

	@Override
	public void onClick(View v) {
		if (mDtmbPlayer != null) {
			mDtmbPlayer.releaseMediaPlayer();
			int freq = Integer.valueOf(mEditTextSetChannel.getText().toString());
			List<Channel> result = mDtmbPlayer.getFreqInfo(freq);
			if (result != null) {
				StringBuilder builder = new StringBuilder();
				for (Channel channel : result) {
					builder.append(channel.toString());
					builder.append("\n");
				}
				mTextViewShowFreqInfo.setText(builder);
			}
		}
	}

	private void startShowBER() {
		mHandler.sendEmptyMessage(0);
	}

	private void stopShowBER() {
		mHandler.removeMessages(0);
	}

	class BERShowHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			boolean isLocked = mDtmbPlayer.getLockedStatus();
			int ber = mDtmbPlayer.getBER();
			Channel channel = mDtmbPlayer.getChannel();
			String text;
			if (channel != null) {
				text = "Locked:" + isLocked + "   BER:" + ber + "    fre:" + channel.frequency;
			} else {
				text = "Locked:" + isLocked + "   BER:" + ber + "    fre:null";
			}
			mTextViewShowBER.setText(text);
			mHandler.sendEmptyMessageDelayed(0, 1000);
		}

	}

}
