package com.actions.dtmbplayer.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;

public class ProgramList extends Observable {
	private static final String TAG = ProgramList.class.getSimpleName();
	private List<Channel> mChannelList = new ArrayList<Channel>();
	private Context mContext;

	public ProgramList(Context context) {
		mContext = context;
		mContext.getContentResolver().registerContentObserver(ProgramProvider.PROGRAM_INFO_CONTENT_URI, true, mContentObserver);
		updateFromDatabase();

	}

	public void add(Channel channel) {
		mChannelList.add(channel);
		notifyProgramChange();
	}

	public void clear() {
		mChannelList.clear();
		notifyProgramChange();
	}

	public Channel get(int position) {
		return mChannelList.get(position);
	}

	public int size() {
		return mChannelList.size();
	}

	private void notifyProgramChange() {
		setChanged();
		notifyObservers();
	}

	public void release() {
		mContext.getContentResolver().unregisterContentObserver(mContentObserver);
		deleteObservers();
	}

	public void updateFromDatabase() {
		mChannelList.clear();
		Cursor cursor = mContext.getContentResolver()
				.query(ProgramProvider.PROGRAM_INFO_CONTENT_URI, ProgramDatabaseOpenHelper.DB_PROJECTION, null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Channel channel = new Channel(cursor);
				cursor.moveToNext();
				mChannelList.add(channel);
			}
		}
		cursor.close();
		notifyProgramChange();
	}

	private ContentObserver mContentObserver = new ContentObserver(new Handler()) {

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			updateFromDatabase();
		}

	};

	public int find(Channel channel) {
		return mChannelList.indexOf(channel);
	}
}
