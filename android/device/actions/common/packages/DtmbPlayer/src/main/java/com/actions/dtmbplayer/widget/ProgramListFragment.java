package com.actions.dtmbplayer.widget;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.actions.dtmbplayer.MainPlayActivity;
import com.actions.dtmbplayer.R;
import com.actions.dtmbplayer.data.Channel;
import com.actions.dtmbplayer.data.PlayRecord;
import com.actions.dtmbplayer.data.ProgramList;
import com.actions.dtmbplayer.widget.BaseRecyclerViewAdapter.OnItemClickListener;
import com.actions.dtmbplayer.widget.DtmbPlayerInterface.OnScanListener;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

public class ProgramListFragment extends Fragment implements OnClickListener {
	private static final String TAG = ProgramListFragment.class.getSimpleName();

	private static final int MESSAGE_HIDE_MENU = 0;
	private static final int TIME_SHOW_MENU = 5000; // 5ms

	private View mRootView;
	private DtmbPlayer mDtmbPlayer;
	private ProgramList mProgramList;
	private ImageButton mButtonScan;
	private RecyclerView mRecyclerViewProgramList;
	private ProgramListAdapter mProgramListAdapter;
	private MainPlayActivity mMainPlayActivity;
	private TextView mButtonBack;
	private PlayRecord mPlayRecord;
	private CustomizeProgressDialog mCustomizeProgressDialog;
	private ExecutorService mSingleThreadExecutor;
	private SetProgramTask mSetProgramTask;
	private Runnable mRunnable;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		mMainPlayActivity = (MainPlayActivity) getActivity();
		mDtmbPlayer = DtmbPlayer.getInstance(getActivity());
		mSingleThreadExecutor = Executors.newSingleThreadExecutor();
		mDtmbPlayer.setThreadPool(mSingleThreadExecutor);
		mProgramList = new ProgramList(getActivity());
		mPlayRecord = PlayRecord.getInstance(mMainPlayActivity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		mRootView = inflater.inflate(R.layout.program_list_layout, container, false);
		mRootView.setOnTouchListener(mOnTouchListener);
		init();
		return mRootView;
	}

	private void init() {
		initDtmbPlayer();
		initView();
	}

	private void initView() {
		mButtonScan = (ImageButton) mRootView.findViewById(R.id.button_scanner);
		mButtonScan.setOnClickListener(mButtonScanOnClickListener);

		mButtonBack = (TextView) mRootView.findViewById(R.id.button_back);
		mButtonBack.setOnClickListener(this);

		mRecyclerViewProgramList = (RecyclerView) mRootView.findViewById(R.id.recyclerview_program_list);
		mRecyclerViewProgramList.setOnTouchListener(mOnTouchListener);
		mRecyclerViewProgramList.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerViewProgramList.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).sizeResId(R.dimen.program_item_divider_width)
				.colorResId(R.color.grey).build());
		mProgramListAdapter = new ProgramListAdapter(mProgramList);
		mProgramListAdapter.setOnItemClickListener(mOnItemClickListener);
		mRecyclerViewProgramList.setAdapter(mProgramListAdapter);		
	}

	private void initDtmbPlayer() {
		mDtmbPlayer.setOnScanListener(mOnScanListener);
		mDtmbPlayer.setOnChannelChangeListener(mOnChannelChangeListener);
	}

	@Override
	public void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		autoPlayProgram(getCurrentChannelPosition());
	}

	@Override
	public void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		mDtmbPlayer.releaseMediaPlayer();
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		if(mSetProgramTask!=null) {
			mSetProgramTask.cancel(true);
			mSetProgramTask = null;
		}
		mRecyclerViewProgramList.removeCallbacks(mRunnable);
		mPlayRecord.save();
		mPlayRecord.release();
		removeHideMenu();
	}

	private OnClickListener mButtonScanOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.d(TAG, "scan onClick");
			if (mSetProgramTask!=null) {
				mSetProgramTask.cancel(true);
				mSetProgramTask = null;
			}
			if (!getActivity().isFinishing() && !mDtmbPlayer.isScanning()) {
				mProgramListAdapter.clearSelection();
				startScan();
				showMenu(true);
			}
		}
	};

	private OnChannelChangeListener mOnChannelChangeListener = new OnChannelChangeListener() {

		@Override
		public void onChannelChange(Channel channel, boolean success) {
			if (!success) {
				mMainPlayActivity.showDefaultBackground(0);
			}
		}
	};

	private void startScan() {
		if (mDtmbPlayer != null) {
			mDtmbPlayer.startScan();
			mProgramList.clear();
		}
		showProgressDialog();
	}

	private void stopScan() {
		if (mDtmbPlayer != null) {
			mDtmbPlayer.stopScan();
		}
		hideProgressDialog();
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(View view, int position) {
			Log.d(TAG, "onItemClick:"+position);
			int last = mProgramList.find(mPlayRecord.peek());
			mPlayRecord.addFirst(mProgramList.get(position));
			mPlayRecord.save();
			mProgramListAdapter.setSelection(position);
			Log.d(TAG, "last:"+last+"   current:"+position);
			if(last!=-1 && last!=position) {
				mProgramListAdapter.notifyItemChanged(last);
			}
			mProgramListAdapter.notifyItemChanged(position);
			if(mSetProgramTask!=null) {
				mSetProgramTask.cancel(true);
			}
			mSetProgramTask = new SetProgramTask(position);
			mSetProgramTask.executeOnExecutor(mSingleThreadExecutor);
		}
	};
	
	private int getCurrentChannelPosition() {
		int position = -1;
		if(mPlayRecord!=null) {
			position = mProgramList.find(mPlayRecord.peek());
		}
		return position;
	}

	private OnScanListener mOnScanListener = new OnScanListener() {

		@Override
		public void onEnd() {
			Log.d(TAG, "onEnd");
			int count = 0;
			if (mProgramListAdapter != null) {
				count = mProgramListAdapter.getItemCount();
			}
			String message = String.format(getResources().getString(R.string.message_scan_finish), count);
			Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
			hideProgressDialog();

			// auto play first program
			if (count > 0) {
				autoPlayProgram(0);
			}
		}

		@Override
		public void onStart() {
			mMainPlayActivity.showDefaultBackground(0);
		}

		@Override
		public void onFound(Channel[] channel, int index) {

		}

		@Override
		public void onFrequencyScan(int index) {
			updateProgressDialog(DtmbPlayer.FREQUENCY_SET[index], index);
		}

		@Override
		public void onStop() {
			Log.d(TAG, "onStop");
			int count = 0;
			if (mProgramListAdapter != null) {
				count = mProgramListAdapter.getItemCount();
			}
			String message = String.format(getResources().getString(R.string.message_scan_stop), count);
			Toast.makeText(mMainPlayActivity, message, Toast.LENGTH_SHORT).show();
			
			// auto play first program
			if (count > 0) {
				autoPlayProgram(0);
			}
		}
	};
	
	private void autoPlayProgram(final int index) {
		mRecyclerViewProgramList.post(mRunnable = new Runnable() {

			@Override
			public void run() {
				Log.d(TAG, "auto play program");
				if(index!=-1) {
					ViewHolder vh = mRecyclerViewProgramList.findViewHolderForPosition(index);
					if (vh != null) {
						vh.itemView.callOnClick();
					}
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_back:
			getActivity().finish();
			break;

		default:
			break;
		}
	}

	private void removeHideMenu() {
		mHandler.removeMessages(MESSAGE_HIDE_MENU);
	}

	public void hideMenu(int delay) {
		if(!mDtmbPlayer.isScanning()) {
			mHandler.sendEmptyMessageDelayed(MESSAGE_HIDE_MENU, delay);
		}
	}

	public void hideMenu() {
		if (!isHidden()) {
			FragmentTransaction fragmentTransaction = mMainPlayActivity.getFragmentManager().beginTransaction();
			fragmentTransaction.hide(this);
			fragmentTransaction.commit();
		}
	}

	private void showMenu(boolean keepShow) {
		removeHideMenu();
		if(isHidden()) {
			FragmentTransaction fragmentTransaction = mMainPlayActivity.getFragmentManager().beginTransaction();
			fragmentTransaction.show(this);
			fragmentTransaction.commit();
		}
		if (!keepShow) {
			hideMenu(TIME_SHOW_MENU);
		}
	}

	public void toggleMenu(boolean keepShow) {
		if (isHidden()) {
			showMenu(keepShow);
		} else {
			hideMenu();
		}
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_HIDE_MENU:
				hideMenu();
				break;
			default:
				break;
			}
		}
	};

	private OnTouchListener mOnTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				removeHideMenu();
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				showMenu(false);
			}
			if (v.equals(mRootView)) {
				return true;
			}
			return false;
		}
	};

	private void initProgressDialog() {
		mCustomizeProgressDialog = new CustomizeProgressDialog(mMainPlayActivity);
		mCustomizeProgressDialog.setTitle(R.string.text_progress_title_scanning);
		mCustomizeProgressDialog.setProgressStyle(CustomizeProgressDialog.STYLE_HORIZONTAL);
		mCustomizeProgressDialog.setMax(DtmbPlayer.FREQUENCY_SET.length);
		mCustomizeProgressDialog.setProgress(0);
		mCustomizeProgressDialog.setCancelable(true);
		mCustomizeProgressDialog.setCanceledOnTouchOutside(false);
		mCustomizeProgressDialog.setProgressPercentFormat(null);
		mCustomizeProgressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				Log.d(TAG, "mProgressDialog onCancel");
				stopScan();
			}
		});
		mCustomizeProgressDialog.setStopButtonListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				stopScan();
			}
		});
		mCustomizeProgressDialog.setIndeterminate(false);
		mCustomizeProgressDialog.create();
	}

	public void showProgressDialog() {
		initProgressDialog();
		mCustomizeProgressDialog.show();
	}

	public void hideProgressDialog() {
		mCustomizeProgressDialog.dismiss();
		mCustomizeProgressDialog = null;
	}

	public void updateProgressDialog(int freq, int index) {
		if (mCustomizeProgressDialog != null) {
			mCustomizeProgressDialog.setTitle(mMainPlayActivity.getString(R.string.text_progress_title_scanning) + String.format("%5.2f", freq / 10.0)
					+ mMainPlayActivity.getString(R.string.text_frequency_mhz));
			mCustomizeProgressDialog.setProgress(index + 1);
		}
	}

	class SetProgramTask extends AsyncTask<Void, Void, Boolean> {
		private int position = -1;

		public SetProgramTask(int position) {
			super();
			this.position = position;
		}

		@Override
		protected void onPreExecute() {
			showMenu(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			if (getCurrentChannelPosition() != position || isCancelled()) {
				return false;
			}
			if (mDtmbPlayer != null) {
				mDtmbPlayer.setChannel(mProgramList.get(position), false, this);
			}
			if (isCancelled()) {
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean success) {
			if (success) {
				hideMenu(1000);
			}
		}
	}
}
