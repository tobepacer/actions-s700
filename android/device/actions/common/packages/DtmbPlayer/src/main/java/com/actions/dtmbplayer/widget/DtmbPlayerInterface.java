package com.actions.dtmbplayer.widget;

import android.os.AsyncTask;
import android.view.SurfaceView;

import com.actions.dtmbplayer.data.Channel;

public interface DtmbPlayerInterface {
	public void setOnChannelAvaiableListener(OnChannelAvaiableListener listener);

	public void setOnScanListener(OnScanListener listener);

	public void startScan();

	public void stopScan();

	public boolean setChannel(Channel channel, boolean forceLock, AsyncTask task);

	public void release();

	public void setSurfaceView(SurfaceView surfaceView);

	public interface OnChannelAvaiableListener {
		public void onChannelAvaiable(Channel[] channel);
	}

	public interface OnScanListener {
		public void onEnd();
		public void onStart();
		public void onFound(Channel[] channel, int index);
		public void onFrequencyScan(int index);
		public void onStop();
	}
}
