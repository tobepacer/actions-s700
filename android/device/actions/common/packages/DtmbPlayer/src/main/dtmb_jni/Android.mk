LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PREBUILT_LIBS := \
		libDTMB.so     \
		libavd_channel.so \
		libdrm_algorithm.so

LOCAL_MODULE_TAGS := optional

include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := dtmbplayer_jni.cpp 

LOCAL_C_INCLUDES :=              \
        $(JNI_H_INCLUDE)         \
        $(LOCAL_PATH) 
		
LOCAL_SHARED_LIBRARIES :=        \
        libandroid_runtime       \
        libnativehelper          \
        libutils                 \
        libcutils                \
        libmedia                 \
		libDTMB					 \
		libavd_channel			 \
		libdrm_algorithm
		
LOCAL_MODULE := libdtmbplayer_jni

include $(BUILD_SHARED_LIBRARY)
