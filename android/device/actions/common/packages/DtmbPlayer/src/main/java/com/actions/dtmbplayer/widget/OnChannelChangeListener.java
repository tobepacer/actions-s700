package com.actions.dtmbplayer.widget;

import com.actions.dtmbplayer.data.Channel;

public interface OnChannelChangeListener {
	public void onChannelChange(Channel channel, boolean success);
}
