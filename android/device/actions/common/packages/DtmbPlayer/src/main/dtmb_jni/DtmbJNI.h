#ifndef __DTMB_JNI_H___
#define __DTMB_JNI_H___

#include "dtmb.h"

DtmbController* openDTMB();
void clearSource();
sp<IStreamSource> getSource();
channels_info getFreqInfo(unsigned freq);
int setChannel(unsigned freq, unsigned pid, bool forceLock);
int releaseDTMB();
int getBER();
bool getLockedStatus();

#endif