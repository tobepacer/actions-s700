#ifndef __PARSER_H___
#define __PARSER_H___

//#include "dtmb.h"

typedef struct{
	unsigned char *buf;
	unsigned buf_len;
	unsigned offset;
} dtmb_buf;


typedef struct{
	unsigned int program_num;  //节目号
	unsigned char chanstr[50];  // 节目名称
}channel;

typedef struct{
	unsigned int tot_num;       //总的节目数目
	channel *chanstr;           //指向节目信息
}channels_info;

enum{
	DTMB_ERROR = -1,
	DTMB_COMPLETE = 0,
	DTMB_MORE_DATA = 1,
	DTMB_NOCONTENT = 2,
}; 


int parser_open(dtmb_buf *buf, channels_info *cInfo);
int parser_data(unsigned int program_num, dtmb_buf *in_buf, dtmb_buf *out_buf);
int parser_dispose();

#endif