package com.actions.dtmbplayer.data;

import java.util.ArrayList;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class ProgramProvider extends ContentProvider {
	private static final String TAG = ProgramProvider.class.getSimpleName();
	public static final String AUTHORITY = "com.actions.dtmbplayer.data.program";
	public static final String PATH = "program";
	private ProgramDatabaseOpenHelper mDBOpenHelper;
	private SQLiteDatabase mSqLiteDatabase;
	private static final int MATCH_PROGRAM = 1;
	private static final UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	public static final Uri PROGRAM_INFO_CONTENT_URI = Uri.parse("content://com.actions.dtmbplayer.data.program/program");

	static {
		mUriMatcher.addURI(AUTHORITY, PATH, MATCH_PROGRAM);
	}

	@Override
	public boolean onCreate() {
		mDBOpenHelper = new ProgramDatabaseOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		checkUri(uri);
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(ProgramDatabaseOpenHelper.DB_TABLE_NAME);
		if (mSqLiteDatabase == null || !mSqLiteDatabase.isOpen()) {
			mSqLiteDatabase = mDBOpenHelper.getReadableDatabase();
		}
		Cursor cursor = queryBuilder.query(mSqLiteDatabase, projection, selection, selectionArgs, null, null, sortOrder);
		if (cursor != null) {
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		}
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		int match = mUriMatcher.match(uri);
		switch (match) {
		case MATCH_PROGRAM:
			return AUTHORITY + "/" + PATH;
		default:
			throw new IllegalArgumentException("Unknown URL");
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		checkUri(uri);
		if (mSqLiteDatabase == null || !mSqLiteDatabase.isOpen() || mSqLiteDatabase.isReadOnly()) {
			mSqLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}
		long rowId = mSqLiteDatabase.insert(ProgramDatabaseOpenHelper.DB_TABLE_NAME, null, values);
		if (rowId < 0) {
			throw new SQLException("Failed to insert row into " + uri);
		}

		Uri newUrl = ContentUris.withAppendedId(uri, rowId);
		getContext().getContentResolver().notifyChange(newUrl, null);
		return newUrl;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		Log.d(TAG, "delete");
		checkUri(uri);
		int count = 0;
		if (mSqLiteDatabase == null || !mSqLiteDatabase.isOpen() || mSqLiteDatabase.isReadOnly()) {
			mSqLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}
		count = mSqLiteDatabase.delete(ProgramDatabaseOpenHelper.DB_TABLE_NAME, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		checkUri(uri);
		int count = 0;
		if (mSqLiteDatabase == null || !mSqLiteDatabase.isOpen() || mSqLiteDatabase.isReadOnly()) {
			mSqLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}
		count = mSqLiteDatabase.update(ProgramDatabaseOpenHelper.DB_TABLE_NAME, values, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	private void checkUri(Uri uri) {
		if (uri == null || mUriMatcher.match(uri) != MATCH_PROGRAM) {
			throw new IllegalArgumentException("Uri:" + uri + " do not exits");
		}
	}

	@Override
	public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
		if (mSqLiteDatabase == null || !mSqLiteDatabase.isOpen() || mSqLiteDatabase.isReadOnly()) {
			mSqLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}
		mSqLiteDatabase.beginTransaction();
		try {
			ContentProviderResult[] results = super.applyBatch(operations);
			mSqLiteDatabase.setTransactionSuccessful();
			return results;
		} finally {
			mSqLiteDatabase.endTransaction();
		}
	}

	@Override
	public void shutdown() {
		if (mSqLiteDatabase != null && mSqLiteDatabase.isOpen()) {
			mSqLiteDatabase.close();
		}
	}

}
