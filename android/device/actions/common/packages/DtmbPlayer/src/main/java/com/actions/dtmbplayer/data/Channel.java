package com.actions.dtmbplayer.data;

import android.content.ContentValues;
import android.database.Cursor;

public class Channel {
	public int frequency;
	public int programId;
	public String programName;
	public String description;

	public Channel() {

	}

	public Channel(Cursor cursor) {
		if (cursor != null && !cursor.isClosed()) {
			frequency = cursor.getInt(cursor.getColumnIndex(ProgramDatabaseOpenHelper.DB_COLUMN_FREQUENCY));
			programId = cursor.getInt(cursor.getColumnIndex(ProgramDatabaseOpenHelper.DB_COLUMN_PROGRAM_ID));
			programName = cursor.getString(cursor.getColumnIndex(ProgramDatabaseOpenHelper.DB_COLUMN_PROGRAM_NAME));
		}
	}

	public ContentValues getContentValues() {
		ContentValues values = new ContentValues();
		values.put(ProgramDatabaseOpenHelper.DB_COLUMN_FREQUENCY, frequency);
		values.put(ProgramDatabaseOpenHelper.DB_COLUMN_PROGRAM_ID, programId);
		values.put(ProgramDatabaseOpenHelper.DB_COLUMN_PROGRAM_NAME, programName);
		return values;
	}

	@Override
	public String toString() {
		return "Channel Info: frequency:" + frequency + "   program ID:" + programId + "    program name:" + programName;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Channel) {
			Channel cn = (Channel) o;
			return frequency == cn.frequency && programId == cn.programId;
		}
		return false;
	}

}
