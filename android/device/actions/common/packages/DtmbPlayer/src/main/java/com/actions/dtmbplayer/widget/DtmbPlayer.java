package com.actions.dtmbplayer.widget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.actions.dtmbplayer.data.Channel;
import com.actions.dtmbplayer.data.ProgramProvider;

public class DtmbPlayer implements DtmbPlayerInterface {
	private static final String TAG = DtmbPlayer.class.getSimpleName();

	public static final int MESSAGE_MEDIA_DATA_ERROR = -1;
	public static final int MESSAGE_MEDIA_CHANNEL_AVAIABLE = 0;
	public static final int MESSAGE_MEDIA_SCAN_FINISH = 1;
	public static final int MESSAGE_MEDIA_SCAN_STOP = 2;
	public static final int MESSAGE_MEDIA_RELEASE = 3;
	private static final int MESSAGE_MEDIA_CHECK_TIME = 4;
	private static final int MEDIA_CHECK_TIME_INTERVAL = 60*1000;
	private static final long PLAYTIME_LIMIT = 10*60*60*1000;
	public static final int[] FREQUENCY_SET = new int[] {
			 525, 605, 685, 800, 880,
			 1710, 1790, 1870, 1950, 2030,
			 2110, 2190, 4740, 4820, 4900,
			 4980, 5060, 5140, 5220, 5300,
			 5380, 5460, 5540, 5620, 6100,
			 6180, 6260, 6340, 6420, 6500,
			 6580, 6660, 6740, 6820, 6900,
			 6980, 7060, 7140, 7220, 7300,
			 7380, 7460, 7540, 7620, 7700,
			 7780, 7860, 7940, 8020, 8100,
			 8180, 8260, 8340, 8420, 8500,
			 8580, 8660 };

	public static final int MESSAGE_RESET_PROGRAM = 2;

	private static final int INTERVAL_RESET_PROGRAM = 2000; // 2s

	private static DtmbPlayer sDtmbPlayer;
	private static Object mLock = new Object();

	private SurfaceView mSurfaceView;
	private MediaPlayer mMediaPlayer;
	private SurfaceHolder mSurfaceHolder;
	private OnScanListener mOnScanListener;
	private EventHandler mEventHandler;
	private boolean isScanning = false;
	private Channel mCurrentChannel;
	private Context mContext;
	private ScanTask mScanTask;
	private ResetProgramTask mResetProgramTask;

	private OnPreparedListener mOnDtmbPlayerPreparedListener;
	private OnErrorListener mOnDtmbPlayerErrorListener;
	private OnCompletionListener mOnDtmbPlayerCompletionListener;
	private OnChannelChangeListener mOnChannelChangeListener;
	private OnMediaReleaseListener mOnMediaReleaseListener;
	private ExecutorService mExecutorService;
	private long mMediaStartTime = -1;

	private native void native_setup();

	private native void native_setDataSource(MediaPlayer mediaPlayer);

	private native int native_setChannel(int frequency, int programId, boolean forceLock);

	private native void native_release();

	private native int native_getBER();

	private native boolean native_getLockedStatus();

	private native Object[] native_getFreqInfo(int freq);
	
	private native void native_clearSource();
	
	private native void native_setDebugStreamFilePath(String path);
	
	private native void native_startDebugRecord();
	
	private native void native_stopDebugRecord();

	static {
		Log.d(TAG, "loadLibrary");
		System.loadLibrary("DTMB");
		System.loadLibrary("avd_channel");
		System.loadLibrary("dtmbplayer_jni");
		System.loadLibrary("drm_algorithm");
	}

	private DtmbPlayer(Context context) {
		mContext = context;
		mEventHandler = new EventHandler();
		setup();
	}

	public static DtmbPlayer getInstance(Context context) {
		if (sDtmbPlayer == null) {
			synchronized (mLock) {
				if (sDtmbPlayer == null) {
					sDtmbPlayer = new DtmbPlayer(context);
				}
			}
		}
		return sDtmbPlayer;
	}
	
	public void clearSource() {
		native_clearSource();
	}

	public List<Channel> getFreqInfo(int freq) {
		List<Channel> result = null;
		Log.d(TAG, "try to lock:" + freq);
		Object[] infos = native_getFreqInfo(freq * 100000);
		if (infos != null) {
			result = new ArrayList<Channel>();
			for (int i = 0; i < infos.length; i++) {
				Log.d(TAG, infos[i].toString());
				result.add((Channel) infos[i]);
			}
		}
		return result;
	}

	private void setup() {
		Log.d(TAG, "setup");
		native_setup();
	}

	public int getBER() {
		return native_getBER();
	}

	public boolean getLockedStatus() {
		return native_getLockedStatus();
	}
	
	public void setDebugStreamFilePath(String path){
		native_setDebugStreamFilePath(path);
	}
	
	public void startDebugRecord() {
		native_startDebugRecord();
	}
	
	public void stopDebugRecord() {
		native_stopDebugRecord();
	}

	@Override
	public void setOnChannelAvaiableListener(OnChannelAvaiableListener listener) {
	}

	@Override
	public void startScan() {
		Log.d(TAG, "startScan");
		isScanning = true;
		if (mOnScanListener != null) {
			mOnScanListener.onStart();
		}
		clearDatabase();
		mScanTask = new ScanTask();
		mScanTask.executeOnExecutor(mExecutorService);
	}

	@Override
	public void stopScan() {
		Log.d(TAG, "stopScan");
		if(mScanTask!=null) {
			mScanTask.cancel(true);
			mScanTask = null;
		}
		isScanning = false;
		mEventHandler.sendEmptyMessage(MESSAGE_MEDIA_SCAN_STOP);
	}

	@Override
	public boolean setChannel(Channel channel, boolean forceLock, AsyncTask task) {
		Log.d(TAG, "setChannel:" + channel);
		releaseMediaPlayer();
		if(task.isCancelled()) {
			return false;
		}
		mCurrentChannel = channel;
		if(task.isCancelled()) {
			return false;
		}
		int i = native_setChannel(channel.frequency, channel.programId, forceLock);
		Log.d(TAG, "i:"+i);
		boolean success = i >= 0;
		if(task.isCancelled()) {
			return false;
		}
		if (mOnChannelChangeListener != null) {
			mOnChannelChangeListener.onChannelChange(channel, success);
		}
		if (!success) {
			mHandler.removeMessages(MESSAGE_RESET_PROGRAM);
			mHandler.sendEmptyMessageDelayed(MESSAGE_RESET_PROGRAM, INTERVAL_RESET_PROGRAM);
			return false;
		}
		if(task.isCancelled()) {
			return false;
		}
		changeChannel();
		return true;
	}

	public Channel getChannel() {
		return mCurrentChannel;
	}

	@Override
	public void release() {
		Log.d(TAG, "release");
		if(mScanTask != null) {
			mScanTask.cancel(true);
			mScanTask = null;
		}
		if(mResetProgramTask !=null) {
			mResetProgramTask.cancel(true);
			mScanTask = null;
		}
		Future<Void> future = mExecutorService.submit(new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				releaseMediaPlayer();
				if (sDtmbPlayer != null) {
					native_release();
					sDtmbPlayer = null;
				}
				return null;
			}
		});
		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public boolean isScanning() {
		return isScanning;
	}

	@Override
	public void setOnScanListener(OnScanListener listener) {
		mOnScanListener = listener;
	}

	@Override
	public void setSurfaceView(SurfaceView surfaceView) {
		mSurfaceView = surfaceView;
		mSurfaceView.getHolder().addCallback(mSurfaceCallback);
	}

	private SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			Log.d(TAG, "surfaceDestroyed");
			releaseMediaPlayer();
			mSurfaceHolder = null;
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			Log.d(TAG, "surfaceCreated");
			mSurfaceHolder = holder;
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

		}
	};

	class EventHandler extends Handler {

		@Override
		public void dispatchMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_MEDIA_CHANNEL_AVAIABLE:
				List<Channel> channels = (List<Channel>) msg.obj;
				updateDatabase(channels);
				break;
			case MESSAGE_MEDIA_SCAN_FINISH:
				isScanning = false;
				mScanTask = null;
				if (mOnScanListener != null) {
					mOnScanListener.onEnd();
				}
				break;
			case MESSAGE_MEDIA_DATA_ERROR:
				// todo
				break;
			case MESSAGE_MEDIA_SCAN_STOP:
				if (mOnScanListener != null) {
					mOnScanListener.onStop();
				}
				break;
			case MESSAGE_MEDIA_RELEASE:
				if (mOnMediaReleaseListener!=null) {
					mOnMediaReleaseListener.onMediaRelease();
				}
			default:
				break;
			}
		}
	}

	private OnErrorListener mOnErrorListener = new OnErrorListener() {

		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			Log.d(TAG, "onError");
			if (mOnDtmbPlayerErrorListener != null) {
				mOnDtmbPlayerErrorListener.onError(mp, what, extra);
			}
			releaseMediaPlayer();
			mHandler.removeMessages(MESSAGE_RESET_PROGRAM);
			mHandler.sendEmptyMessageDelayed(MESSAGE_RESET_PROGRAM, INTERVAL_RESET_PROGRAM);
			return true;
		}
	};

	private OnPreparedListener mOnPreparedListener = new OnPreparedListener() {

		@Override
		public void onPrepared(MediaPlayer mp) {
			Log.d(TAG, "onPrepared");
			if (mMediaPlayer==null) {
				return;
			}
			if (mOnDtmbPlayerPreparedListener != null) {
				mOnDtmbPlayerPreparedListener.onPrepared(mp);
			}
			mp.start();
			
			mMediaStartTime = System.currentTimeMillis();
			mHandler.sendEmptyMessageDelayed(MESSAGE_MEDIA_CHECK_TIME, MEDIA_CHECK_TIME_INTERVAL);
		}
	};

	private OnCompletionListener mOnCompletionListener = new OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {
			Log.d(TAG, "onCompletion");
			if (mOnDtmbPlayerCompletionListener != null) {
				mOnDtmbPlayerCompletionListener.onCompletion(mp);
			}
			releaseMediaPlayer();
			mHandler.removeMessages(MESSAGE_RESET_PROGRAM);
			mHandler.sendEmptyMessageDelayed(MESSAGE_RESET_PROGRAM, INTERVAL_RESET_PROGRAM);
		}
	};

	private void clearDatabase() {
		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		operations.add(ContentProviderOperation.newDelete(ProgramProvider.PROGRAM_INFO_CONTENT_URI).build());

		try {
			mContext.getContentResolver().applyBatch(ProgramProvider.AUTHORITY, operations);
		} catch (RemoteException | OperationApplicationException e) {
			e.printStackTrace();
		}
	}

	private void updateDatabase(List<Channel> channels) {
		Log.d(TAG, "updateDatabase");
		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		for (Channel cn : channels) {
			operations.add(ContentProviderOperation.newInsert(ProgramProvider.PROGRAM_INFO_CONTENT_URI).withValues(cn.getContentValues()).build());
		}

		try {
			mContext.getContentResolver().applyBatch(ProgramProvider.AUTHORITY, operations);
		} catch (RemoteException | OperationApplicationException e) {
			e.printStackTrace();
		}
	}

	public void setDataSource() {
		native_setDataSource(mMediaPlayer);
	}

	public void prepare() {
		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void changeChannel() {
		Log.d(TAG, "create mediaplayer");
		if(mSurfaceHolder==null) {
			return;
		}
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setDisplay(mSurfaceHolder);
		mMediaPlayer.setScreenOnWhilePlaying(true);
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mMediaPlayer.setOnErrorListener(mOnErrorListener);
		mMediaPlayer.setOnPreparedListener(mOnPreparedListener);
		mMediaPlayer.setOnCompletionListener(mOnCompletionListener);
		setDataSource();
//		mMediaPlayer.prepareAsync();
		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean releaseMediaPlayer() {
		mMediaStartTime = -1;
		mHandler.removeMessages(MESSAGE_MEDIA_CHECK_TIME);
		mHandler.removeMessages(MESSAGE_RESET_PROGRAM);
		if (mMediaPlayer != null) {
			Log.d(TAG, "releaseMediaPlayer");
			mEventHandler.sendEmptyMessage(MESSAGE_MEDIA_RELEASE);
			if (mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
			}
			clearSource();
			mMediaPlayer.release();
			mMediaPlayer = null;
			return true;
		}
		return false;
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_RESET_PROGRAM:
				Log.d(TAG, "reset program");
				mResetProgramTask = new ResetProgramTask();
				mResetProgramTask.executeOnExecutor(mExecutorService);
				break;
			case MESSAGE_MEDIA_CHECK_TIME:
				if(mMediaStartTime!=-1){
					if(System.currentTimeMillis()-mMediaStartTime >= PLAYTIME_LIMIT){
						Log.d(TAG, "play video too long, reset!");
						releaseMediaPlayer();
						mHandler.removeMessages(MESSAGE_RESET_PROGRAM);
						mHandler.sendEmptyMessageDelayed(MESSAGE_RESET_PROGRAM, INTERVAL_RESET_PROGRAM);
						break;
					}
					mHandler.sendEmptyMessageDelayed(MESSAGE_MEDIA_CHECK_TIME, MEDIA_CHECK_TIME_INTERVAL);
				}
			default:
				break;
			}
		}

	};

	public void setOnCompletionListener(OnCompletionListener listener) {
		mOnDtmbPlayerCompletionListener = listener;
	}

	public void setOnPreparedListener(OnPreparedListener listener) {
		mOnDtmbPlayerPreparedListener = listener;
	}

	public void setOnErrorListener(OnErrorListener listener) {
		mOnDtmbPlayerErrorListener = listener;
	}

	public void setOnChannelChangeListener(OnChannelChangeListener listener) {
		mOnChannelChangeListener = listener;
	}
	
	public void setOnMediaReleaseListener(OnMediaReleaseListener listener) {
		mOnMediaReleaseListener = listener;
	}

	public boolean isPlaing() {
		if (mMediaPlayer != null) {
			return mMediaPlayer.isPlaying();
		}
		return false;
	}

	public void start() {
		if (mMediaPlayer != null) {
			mMediaPlayer.start();
		}
	}

	public void pause() {
		if (mMediaPlayer != null) {
			mMediaPlayer.pause();
		}
	}

	private class ScanTask extends AsyncTask<Void, Integer, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			releaseMediaPlayer();
			publishProgress(0);
			for (int i = 0; i < FREQUENCY_SET.length; i++) {
				if (isCancelled())
					break;
				publishProgress(i);
				List<Channel> channels = getFreqInfo(FREQUENCY_SET[i]);
				if (channels != null) {
					fliterChannels(channels);
					Message message = mEventHandler.obtainMessage();
					message.what = MESSAGE_MEDIA_CHANNEL_AVAIABLE;
					message.arg1 = i;
					message.obj = channels;
					mEventHandler.sendMessage(message);
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mEventHandler.sendEmptyMessage(MESSAGE_MEDIA_SCAN_FINISH);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			Log.d(TAG, "onProgressUpdate");
			mOnScanListener.onFrequencyScan(values[0]);
		}
		
		private void fliterChannels(List<Channel> channels) {
			Iterator<Channel> iterator = channels.iterator();
			while(iterator.hasNext()) {
				Channel channel = iterator.next();
				if(channel.programName==null || channel.programName.isEmpty()) {
					Log.d(TAG, "channel name null");
					iterator.remove();
				}
			}
		}
	}
	
	private class ResetProgramTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			if (mCurrentChannel != null && !isCancelled()) {
				setChannel(mCurrentChannel, true, this);
			}
			return null;
		}
	}
	
	public void setThreadPool(ExecutorService executorService) {
		mExecutorService = executorService;
	}
	
	public interface OnMediaReleaseListener {
		public void onMediaRelease();
	}
}
