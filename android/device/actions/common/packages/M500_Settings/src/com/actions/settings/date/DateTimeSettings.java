package com.actions.settings.date;

import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.actions.settings.R;
import com.actions.settings.SettingsPreferenceFragment;
import com.android.internal.widget.DialogTitle;

@SuppressLint("NewApi")
public class DateTimeSettings extends SettingsPreferenceFragment implements
		OnSharedPreferenceChangeListener, OnDateSetListener, OnTimeSetListener {
	private static final String TAG = "DateTimeSettings";
	private static final String HOURS_12 = "12";
	private static final String HOURS_24 = "24";
	    
	private static final String KEY_AUTO_TIME = "auto_date";
	private static final String KEY_SET_TIME = "set_time";
	private static final String KEY_SET_DATE = "set_date";
	private static final String KEY_SET_24HOURS = "24hours";

	private static final int DIALOG_DATEPICKER = 0;
	private static final int DIALOG_TIMEPICKER = 1;

	private SwitchPreference mAutoTimePref;
	private Preference mTimePref;
	private Preference mDatePref;
	private Calendar mDummyDate;
	private SettingsDialogFragment mDialogFragment;
	private SwitchPreference use24HoursPref;

	public DateTimeSettings() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.date_preferences);
		initUI();
	}

	private void initUI() {
		boolean autoTimeEnabled = getAutoState(Settings.Global.AUTO_TIME);
		mAutoTimePref = (SwitchPreference) findPreference(KEY_AUTO_TIME);
		DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		if (dpm.getAutoTimeRequired()) {
			mAutoTimePref.setEnabled(false);
		}
		mAutoTimePref.setChecked(autoTimeEnabled);
		// mAutoTimePref.setOnPreferenceChangeListener(this);
		// mAutoTimePref.setOnPreferenceClickListener(this);
		use24HoursPref = (SwitchPreference) findPreference(KEY_SET_24HOURS);
		use24HoursPref.setChecked(is24Hour());
		
		mTimePref = findPreference(KEY_SET_TIME);
		mDatePref = findPreference(KEY_SET_DATE);

		mTimePref.setEnabled(!autoTimeEnabled);
		mDatePref.setEnabled(!autoTimeEnabled);

		String[] dateFormats = getResources().getStringArray(
				R.array.date_format_values);
		String[] formattedDates = new String[dateFormats.length];
		String currentFormat = getDateFormat();

		mDummyDate = Calendar.getInstance();
		mDummyDate.set(mDummyDate.get(Calendar.YEAR), mDummyDate.DECEMBER, 31,
				13, 0, 0);

		for (int i = 0; i < formattedDates.length; i++) {
			String formatted = DateFormat.getDateFormatForSetting(
					getActivity(), dateFormats[i]).format(mDummyDate.getTime());

			if (dateFormats[i].length() == 0) {
				formattedDates[i] = getResources().getString(
						R.string.normal_date_format, formatted);
			} else {
				formattedDates[i] = formatted;
			}
		}

	}

	private boolean getAutoState(String name) {
		try {
			return Settings.Global.getInt(getContentResolver(), name) > 0;
		} catch (SettingNotFoundException snfe) {
			return false;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final Activity activity = getActivity();
			if (activity != null) {
				updateTimeAndDateDisplay(activity);
			}
		}
	};

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);

		// Register for time ticks and other reasons for time change
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_TIME_TICK);
		filter.addAction(Intent.ACTION_TIME_CHANGED);
		filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
		getActivity().registerReceiver(mIntentReceiver, filter, null, null);
		updateTimeAndDateDisplay(getActivity());
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		getActivity().unregisterReceiver(mIntentReceiver);
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		updateTimeAndDateDisplay(getActivity());
	}

	public void updateTimeAndDateDisplay(Context context) {
		java.text.DateFormat shortDateFormat = DateFormat
				.getDateFormat(context);
		final Calendar now = Calendar.getInstance();

		mDummyDate.set(now.get(Calendar.YEAR), 11, 31, 13, 0, 0);
		Date dummyDate = mDummyDate.getTime();
		mTimePref.setSummary(DateFormat.getTimeFormat(getActivity()).format(
				now.getTime()));
		mDatePref.setSummary(shortDateFormat.format(now.getTime()));

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences preferences,
			String key) {
		// TODO Auto-generated method stub
		Log.v(TAG, " onSharedPreferenceChanged");
		if (key.equalsIgnoreCase(KEY_AUTO_TIME)) {
			Log.v(TAG, " onSharedPreferenceChanged KEY_AUTO_TIME");
			boolean autoEnabled = preferences.getBoolean(key, true);
			Settings.Global.putInt(getContentResolver(),
					Settings.Global.AUTO_TIME, autoEnabled ? 1 : 0);
			mTimePref.setEnabled(!autoEnabled);
			mDatePref.setEnabled(!autoEnabled);
		}
	}

	private void timeUpdated(boolean is24Hour) {
		Intent timeChanged = new Intent(Intent.ACTION_TIME_CHANGED);
		timeChanged.putExtra(Intent.EXTRA_TIME_PREF_24_HOUR_FORMAT, is24Hour);
		getActivity().sendBroadcast(timeChanged);
	}

	/* Get & Set values from the system settings */

	private boolean is24Hour() {
		return DateFormat.is24HourFormat(getActivity());
	}

	private void set24Hour(boolean is24Hour) {
		Settings.System.putString(getContentResolver(),
				Settings.System.TIME_12_24, is24Hour ? HOURS_24 : HOURS_12);
	}
	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		// TODO Auto-generated method stub
		if (preference == mTimePref) {
			Log.v(TAG, " onPreferenceTreeClick mTimePref");
			removeDialog(DIALOG_TIMEPICKER);
			showDialog(DIALOG_TIMEPICKER);
		} else if (preference == mDatePref) {
			Log.v(TAG, " onPreferenceTreeClick mDatePref");
			showDialog(DIALOG_DATEPICKER);
		} else if (preference == mAutoTimePref) {

		}else if (preference == use24HoursPref) {
            final boolean is24Hour = (use24HoursPref).isChecked();
            set24Hour(is24Hour);
            updateTimeAndDateDisplay(getActivity());
            timeUpdated(is24Hour);
        }
		
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	@Override
	public void onDetach() {
		if (isRemoving()) {
			if (mDialogFragment != null) {
				mDialogFragment.dismiss();
				mDialogFragment = null;
			}
		}
		super.onDetach();
	}

	private String getDateFormat() {
		return Settings.System.getString(getContentResolver(),
				Settings.System.DATE_FORMAT);
	}
	protected void showDialog(int dialogId) {
		if (mDialogFragment != null) {
			Log.e(TAG, "Old dialog fragment not null!");
		}
		mDialogFragment = new SettingsDialogFragment(this, dialogId);
		mDialogFragment.show(getChildFragmentManager(),
				Integer.toString(dialogId));
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public Dialog onCreateDialog(int id) {
		final Calendar calendar = Calendar.getInstance();
		switch (id) {
		case DIALOG_DATEPICKER:
			final DatePickerDialog date = new DatePickerDialog(getActivity(),
					DatePickerDialog.THEME_HOLO_LIGHT, this,
					calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
					calendar.get(Calendar.DAY_OF_MONTH));
			configureDatePicker(date.getDatePicker());
			date.setTitle(R.string.preference_date_set_title);

			date.setOnShowListener(new OnShowListener() {
				@Override
				public void onShow(DialogInterface arg0) {
					// TODO Auto-generated method stub
					resetDialogView(date);
					//date.getButton(AlertDialog.BUTTON_NEGATIVE).requestFocus();
					date.setOnKeyListener(new OnKeyListener() {
						
						@Override
						public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent arg2) {
							// TODO Auto-generated method stub
							if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
								date.getButton(AlertDialog.BUTTON_POSITIVE).callOnClick();
							}
							return false;
						}
					});
				}
			});
			return date;
		case DIALOG_TIMEPICKER:
			final TimePickerDialog timer = new TimePickerDialog(getActivity(),
					TimePickerDialog.THEME_HOLO_LIGHT, this,
					calendar.get(Calendar.HOUR_OF_DAY),
					calendar.get(Calendar.MINUTE),
					DateFormat.is24HourFormat(getActivity()));
			timer.setTitle(R.string.preference_time_set_time_title);

			timer.setOnShowListener(new OnShowListener() {
				@Override
				public void onShow(DialogInterface arg0) {
					// TODO Auto-generated method stub
					resetDialogView(timer);
					//timer.getButton(AlertDialog.BUTTON_NEGATIVE).requestFocus();
					timer.setOnKeyListener(new OnKeyListener() {
						@Override
						public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent arg2) {
							// TODO Auto-generated method stub
							if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
								if (timer.getButton(AlertDialog.BUTTON_NEGATIVE).isFocused()){
									return false;
								}else {
									timer.getButton(AlertDialog.BUTTON_POSITIVE).callOnClick();
								}		
							}
							return false;
						}
					});
				}
			});

			return timer;
		default:
			throw new IllegalArgumentException();
		}
	}

	private void resetDialogView(AlertDialog dialog) {
		dialog.getWindow().setLayout(680, 400);
		Button positiveButton = dialog
				.getButton(DialogInterface.BUTTON_POSITIVE);
		positiveButton.setTextSize(25);
		positiveButton.setBackgroundResource(R.drawable.dialog_positive_bg);
		positiveButton.setFocusable(true);
		LinearLayout view = (LinearLayout) positiveButton.getParent()
				.getParent().getParent();

		if (dialog instanceof TimePickerDialog) {
			ViewGroup vieGroup = ((ViewGroup) ((ViewGroup) ((FrameLayout) ((FrameLayout) view
					.getChildAt(2)).getChildAt(0)).getChildAt(0)).getChildAt(0));

			NumberPicker amPicker = (NumberPicker) vieGroup.getChildAt(0);
			if (amPicker instanceof NumberPicker) {
				setDatePickTextSize(amPicker);
			}

			ViewGroup HMPicker = (ViewGroup) vieGroup.getChildAt(1);

			NumberPicker hourPicker = (NumberPicker) HMPicker.getChildAt(0);
			if (hourPicker instanceof NumberPicker) {
				setDatePickTextSize(hourPicker);
			}
			TextView DDView = (TextView) HMPicker.getChildAt(1);
			DDView.setTextSize(28);
			NumberPicker minutePicker = (NumberPicker) HMPicker.getChildAt(2);
			if (minutePicker instanceof NumberPicker) {
				setDatePickTextSize(minutePicker);
			}
		}

		LinearLayout titleBar = (LinearLayout) ((LinearLayout) view
				.getChildAt(0));
		LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		linearParams.setMargins(8, 0, 8, 0);
		titleBar.setLayoutParams(linearParams);
		titleBar.setBackgroundColor(Color.WHITE);// title background

		LinearLayout linear = (LinearLayout) ((LinearLayout) view.getChildAt(0))
				.getChildAt(1);

		View divider = (View) ((LinearLayout) view.getChildAt(0)).getChildAt(2);
		divider.setVisibility(view.GONE);

		DialogTitle title = (DialogTitle) linear.getChildAt(1);
		title.setTextColor(Color.BLACK);
		title.setTextSize(25);

		Button negativeButton = dialog
				.getButton(DialogInterface.BUTTON_NEGATIVE);
		negativeButton.setBackgroundResource(R.drawable.dialog_negative_bg);
		negativeButton.setTextSize(25);

		LinearLayout.LayoutParams rel_btn = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		rel_btn.weight = 1;
		rel_btn.setMargins(10, 10, 10, 10);
		negativeButton.setLayoutParams(rel_btn);
		positiveButton.setLayoutParams(rel_btn);

		LinearLayout group = (LinearLayout) negativeButton.getParent()
				.getParent();
		group.setDividerDrawable(null);
		group.setShowDividers(group.SHOW_DIVIDER_NONE);

		LinearLayout negativeGroup = (LinearLayout) negativeButton.getParent();
		negativeGroup.setDividerDrawable(null);
		negativeGroup.setShowDividers(negativeGroup.SHOW_DIVIDER_NONE);

		LinearLayout positiveGroup = (LinearLayout) positiveButton.getParent();
		positiveGroup.setShowDividers(negativeGroup.SHOW_DIVIDER_NONE);
		positiveGroup.setDividerDrawable(null);
	}

	static void configureDatePicker(final DatePicker datePicker) {
		// The system clock can't represent dates outside this range.
		Calendar t = Calendar.getInstance();
		t.clear();
		t.set(1970, Calendar.JANUARY, 1);
		datePicker.setMinDate(t.getTimeInMillis());
		t.clear();
		t.set(2037, Calendar.DECEMBER, 31);
		datePicker.setMaxDate(t.getTimeInMillis());
		datePicker.setCalendarViewShown(false);

		changeDatePickText(datePicker);
	}

	private static void changeDatePickText(FrameLayout picker) {
		LinearLayout layoutPick;
		ViewGroup layout;
		if (picker instanceof TimePicker) {
			layout = null;
			Log.i(TAG, "counts:" + picker.getChildCount());
			for (int i = 0; i < picker.getChildCount(); i++) {
				layoutPick = (LinearLayout) picker.getChildAt(i);
				NumberPicker time = (NumberPicker) layoutPick.getChildAt(0);
				setDatePickTextSize(time);
			}
			return;

		} else {
			layoutPick = (LinearLayout) picker.getChildAt(0);
			layout = (ViewGroup) layoutPick.getChildAt(0);
		}

		// first
		NumberPicker first = (NumberPicker) layout.getChildAt(0);
		setDatePickTextSize(first);
		// second
		NumberPicker second = (NumberPicker) layout.getChildAt(1);
		setDatePickTextSize(second);
		// third
		NumberPicker third = (NumberPicker) layout.getChildAt(2);
		setDatePickTextSize(third);
	}

	private static void setDatePickTextSize(ViewGroup timePickerLayout) {
		timePickerLayout.setScaleX(1.0f);
		timePickerLayout.setScaleY(1.3f);
	}

	static void setTime(Context context, int hourOfDay, int minute) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, hourOfDay);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long when = c.getTimeInMillis();
		if (when / 1000 < Integer.MAX_VALUE) {
			((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
					.setTime(when);
		}
	}

	static void setDate(Context context, int year, int month, int day) {
		Calendar c = Calendar.getInstance();

		int oldYear = c.get(Calendar.YEAR);
		int oldMonth = c.get(Calendar.MONTH);
		int oldDay = c.get(Calendar.DAY_OF_MONTH);
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, day);
		long when = c.getTimeInMillis();

		if (when >= 0 && when / 1000 < Integer.MAX_VALUE && year >= 1970
				&& year <= 2038) {
			((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
					.setTime(when);
		} else {
			c.set(Calendar.YEAR, oldYear);
			c.set(Calendar.MONTH, oldMonth);
			c.set(Calendar.DAY_OF_MONTH, oldDay);
		}
	}

	@Override
	public void onDateSet(DatePicker arg0, int year, int month, int day) {
		// TODO Auto-generated method stub
		final Activity activity = getActivity();
		if (activity != null) {
			setDate(activity, year, month, day);
			updateTimeAndDateDisplay(activity);
		}
	}

	@Override
	public void onTimeSet(TimePicker arg0, int hourOfDay, int minute) {
		// TODO Auto-generated method stub

		final Activity activity = getActivity();
		if (activity != null) {
			setTime(activity, hourOfDay, minute);
			updateTimeAndDateDisplay(activity);
		}

	}

}
