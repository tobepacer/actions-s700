package com.actions.settings.widget;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.IPowerManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.preference.Preference;
import android.preference.PreferenceManager.OnActivityStopListener;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.actions.settings.R;

public class BrightnessPreference extends Preference implements
		SeekBar.OnSeekBarChangeListener, OnActivityStopListener,
		OnClickListener {

	private static final String TAG = "BrightnessPreference";
	private Context mContext;
	private int mScreenBrightnessMinimum;
	private int mScreenBrightnessMaximum;
	private SeekBar mSeekBar;
	private ImageButton mScreenBrightnessAdd;
	private ImageButton mScreenBrightnessSubtract;
	private int mCurBrightness = -1;
	private int mOldBrightness;
	private static final int SEEK_BAR_RANGE = 10000;
	private boolean mRestoredOldState;
	private int changeVlaue = 0;
	private int max = 0;
	private static final float BRIGHTNESS_ADJ_RESOLUTION = 100;

	public BrightnessPreference(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	private ContentObserver mBrightnessObserver = new ContentObserver(
			new Handler()) {
		@Override
		public void onChange(boolean selfChange) {
			mCurBrightness = -1;
			onBrightnessChanged();
		}
	};

	public BrightnessPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		mScreenBrightnessMinimum = pm.getMinimumScreenBrightnessSetting();
		mScreenBrightnessMaximum = pm.getMaximumScreenBrightnessSetting();
		setLayoutResource(R.layout.brightness_prefence);
		changeVlaue = (mScreenBrightnessMaximum- mScreenBrightnessMinimum) /10;
		getContext().getContentResolver().registerContentObserver(
				Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS),
				true, mBrightnessObserver);
		mContext = context;
	}

	@Override
	protected void onBindView(View view) {
		// TODO Auto-generated method stub
		mSeekBar = (SeekBar) view.findViewById(R.id.brightnessBar);
		mScreenBrightnessAdd = (ImageButton) view
				.findViewById(R.id.brightnessAdd);
		mScreenBrightnessSubtract = (ImageButton) view
				.findViewById(R.id.brightnessSubtract);

		mScreenBrightnessSubtract.setOnClickListener(this);
		mScreenBrightnessAdd.setOnClickListener(this);
		int max = mScreenBrightnessMaximum - mScreenBrightnessMinimum;
		mSeekBar.setMax(max);
		mOldBrightness = getBrightness();
		mSeekBar.setProgress(mOldBrightness);
		mSeekBar.setEnabled(true);

		mSeekBar.setOnSeekBarChangeListener(this);
		getPreferenceManager().registerOnActivityStopListener(this);
		super.onBindView(view);
	}

	private void onBrightnessChanged() {
		if (getBrightnessMode() == 1){
			return;
		}
		mSeekBar.setProgress(getBrightness());

	}

	public void onChanged(boolean auto) {	
		if (auto){
			final float adj = mSeekBar.getProgress()
					/ (BRIGHTNESS_ADJ_RESOLUTION / 2f) - 1;
			setBrightnessAdj(adj);
		}else {
			 final int val = mSeekBar.getProgress();
			 setBrightness(val,false);
		}
		
	}

	private void setBrightnessAdj(float adj) {
		try {
			IPowerManager power = IPowerManager.Stub.asInterface(ServiceManager
					.getService("power"));
			if (power != null) {
				power.setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(adj);
			}
		} catch (RemoteException ex) {
		}
	}
	private int getBrightnessMode (){
		int brightnessMode = Settings.System.getIntForUser(mContext.getContentResolver(),
				"Intelligent_brightness_mode", 0,UserHandle.USER_CURRENT);
		return brightnessMode;
	}
	private int getBrightness() {
		float brightness = 0;
		if (mCurBrightness < 0) {
			brightness = Settings.System.getIntForUser(mContext.getContentResolver(),
	                 Settings.System.SCREEN_BRIGHTNESS, mScreenBrightnessMaximum,
	                 UserHandle.USER_CURRENT);
		} else {
			brightness = mCurBrightness;
		}
		return (int) (brightness - mScreenBrightnessMinimum);
	}

	private void setBrightness(int brightness, boolean write) {		
		brightness = brightness + mScreenBrightnessMinimum;
		try {
			IPowerManager power = IPowerManager.Stub.asInterface(ServiceManager
					.getService("power"));
			if (power != null) {
				power.setTemporaryScreenBrightnessSettingOverride(brightness);
			}
			if (write) {
				mCurBrightness = -1;
				final ContentResolver resolver = getContext()
						.getContentResolver();
				Settings.System.putInt(resolver,
						Settings.System.SCREEN_BRIGHTNESS, brightness);
			} else {
				mCurBrightness = brightness;				
			}
		} catch (RemoteException doe) {
		}
	}

	@Override
	protected View onCreateView(ViewGroup parent) {
		// TODO Auto-generated method stub
		return super.onCreateView(parent);
	}

	@Override
	public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
		// TODO Auto-generated method stub
		setBrightness(progress, false);
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onActivityStop() {
		// TODO Auto-generated method stub
		setBrightness(mSeekBar.getProgress(), true);
		final ContentResolver resolver = getContext().getContentResolver();
		getPreferenceManager().unregisterOnActivityStopListener(this);
		resolver.unregisterContentObserver(mBrightnessObserver);
	}

	private void restoreOldState() {
		if (mRestoredOldState)
			return;

		setBrightness(mOldBrightness, false);
		mRestoredOldState = true;
		mCurBrightness = -1;
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();

		// Save the dialog state
		final SavedState myState = new SavedState(superState);
		// myState.automatic = mCheckBox.isChecked();
		myState.progress = mSeekBar.getProgress();

		myState.oldProgress = mOldBrightness;
		myState.curBrightness = mCurBrightness;

		// Restore the old state when the activity or dialog is being paused
		restoreOldState();
		return myState;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			// Didn't save state for us in onSaveInstanceState
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		mOldBrightness = myState.oldProgress;
		setBrightness(myState.progress, false);
		mCurBrightness = myState.curBrightness;
	}

	private static class SavedState extends BaseSavedState {

		boolean automatic;
		boolean oldAutomatic;
		int progress;
		int oldProgress;
		int curBrightness;

		public SavedState(Parcel source) {
			super(source);
			automatic = source.readInt() == 1;
			progress = source.readInt();
			oldAutomatic = source.readInt() == 1;
			oldProgress = source.readInt();
			curBrightness = source.readInt();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(automatic ? 1 : 0);
			dest.writeInt(progress);
			dest.writeInt(oldAutomatic ? 1 : 0);
			dest.writeInt(oldProgress);
			dest.writeInt(curBrightness);
		}

		public SavedState(Parcelable superState) {
			super(superState);
		}

		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.brightnessAdd:
			int add = mSeekBar.getProgress() + changeVlaue ;
			if (add > mScreenBrightnessMaximum) {
				add = mScreenBrightnessMaximum;
			}
			mSeekBar.setProgress(add);
			break;
		case R.id.brightnessSubtract:
			int subtract = mSeekBar.getProgress() - changeVlaue;
			if (subtract < 0) {
				subtract = 0;
			}
			mSeekBar.setProgress(subtract);
			break;
		}
	}

}
