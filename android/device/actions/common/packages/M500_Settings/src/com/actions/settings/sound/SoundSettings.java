package com.actions.settings.sound;

import com.actions.settings.R;
import com.actions.settings.SettingsPreferenceFragment;
import com.actions.settings.widget.VolumePreference;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class SoundSettings extends SettingsPreferenceFragment implements OnItemSelectedListener {
	private static final String TAG = "SoundSettings";
	
	private SwitchPreference mSoundEffects;
	private VolumePreference mVolume;
	private static final String KEY_SOUND_EFFECTS = "sound_effects";
	private static final String KEY_ADJUST_VOLUME = "volume_preference";
	private AudioManager mAudioManager;

	public SoundSettings() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		ContentResolver resolver = getContentResolver();
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		addPreferencesFromResource(R.xml.sound_preferences);
		mSoundEffects = (SwitchPreference) findPreference(KEY_SOUND_EFFECTS);
		mVolume = (VolumePreference)findPreference(KEY_ADJUST_VOLUME);
		mSoundEffects.setPersistent(false);
		mSoundEffects.setChecked(Settings.System.getInt(resolver,
				Settings.System.SOUND_EFFECTS_ENABLED, 1) != 0);

	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		getListView().setOnItemSelectedListener(this);
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mVolume.stopSample();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mVolume.cleanup();
	}
	public void addVolume(){
		mVolume.changeVolume(1);
		
	}
	public void subtractVolume(){
		mVolume.changeVolume(-1);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		// TODO Auto-generated method stub
		Log.v(TAG, "onPreferenceTreeClick");
		if (preference.getKey().equals(KEY_SOUND_EFFECTS)){
			Log.v(TAG, "KEY_SOUND_EFFECTS");
			if (mSoundEffects.isChecked()) {
				mAudioManager.loadSoundEffects();
			} else {
				mAudioManager.unloadSoundEffects();
			}
			Settings.System.putInt(getContentResolver(),
					Settings.System.SOUND_EFFECTS_ENABLED,
					mSoundEffects.isChecked() ? 1 : 0);
		}else {
			return super.onPreferenceTreeClick(preferenceScreen, preference);
		}
		return true;
	}
@Override
	public void onItemSelected(AdapterView<?> parent, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		if (arg2 == 1){
			parent.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS); 
		}else {
			parent.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS); 
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		parent.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);  
	}

	
}
