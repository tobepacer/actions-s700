package com.actions.settings.recovery;

import com.actions.settings.R;
import com.actions.settings.SettingsPreferenceFragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PrivacySettings extends SettingsPreferenceFragment {

	public PrivacySettings() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.reset_preferences);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		 getListView().setFocusable(true);
		getListView().setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
	}
}
