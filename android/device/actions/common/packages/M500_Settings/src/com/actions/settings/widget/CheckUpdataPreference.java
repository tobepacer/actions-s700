package com.actions.settings.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.actions.settings.R;

public class CheckUpdataPreference extends Preference {

	private ImageButton checkUpdataButton;

	private Context mContext;

	public CheckUpdataPreference(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public CheckUpdataPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
		setLayoutResource(R.layout.preference_widget_check_updata);
	}

	public CheckUpdataPreference(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
		setLayoutResource(R.layout.preference_widget_check_updata);
	}

	public void checkCallClick() {
		checkUpdataButton.callOnClick();

	}

	@Override
	protected View onCreateView(ViewGroup parent) {
		// TODO Auto-generated method stub
		return super.onCreateView(parent);
	}

	@Override
	protected void onBindView(View view) {
		// TODO Auto-generated method stub
		super.onBindView(view);
		checkUpdataButton = (ImageButton) view.findViewById(R.id.checkUpdata);
		checkUpdataButton.setFocusable(false);
		checkUpdataButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				ComponentName mComponentName = new ComponentName(
						"com.actions.ota", "com.actions.ota.MainActivity");
				intent.setComponent(mComponentName);
				mContext.startActivity(intent);
			}
		});
	}

}
