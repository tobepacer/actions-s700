package com.actions.settings;

import java.util.HashMap;
import java.util.List;

import com.actions.settings.R.color;

import com.actions.settings.R;
import com.actions.settings.preference.PreferenceActivity;
import com.actions.settings.sound.SoundSettings;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
//import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

@SuppressLint("NewApi")
public class Settings extends PreferenceActivity {
	private static final String LOG_TAG = "Settings";

	private static final String META_DATA_KEY_HEADER_ID = "com.actions.settings.TOP_LEVEL_HEADER_ID";
	private static final String META_DATA_KEY_FRAGMENT_CLASS = "com.actions.settings.FRAGMENT_CLASS";
	private static final String META_DATA_KEY_PARENT_TITLE = "com.actions.settings.PARENT_FRAGMENT_TITLE";
	private static final String META_DATA_KEY_PARENT_FRAGMENT_CLASS = "com.actions.settings.PARENT_FRAGMENT_CLASS";

	private static final String EXTRA_CLEAR_UI_OPTIONS = "settings:remove_ui_options";

	private static final String SAVE_KEY_CURRENT_HEADER = "com.actions.settings.CURRENT_HEADER";
	private static final String SAVE_KEY_PARENT_HEADER = "com.actions.settings.PARENT_HEADER";

	private String mFragmentClass;
	private int mTopLevelHeaderId;
	private Header mFirstHeader;
	private Header mCurrentHeader;
	private Header mParentHeader;
	private boolean mInLocalHeaderSwitch;
	private Header mLastHeader;
	protected HashMap<Integer, Integer> mHeaderIndexMap = new HashMap<Integer, Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getListView().setBackgroundResource(R.drawable.list_bg);
		getListView().setPadding(0, 0, 0, 0);
		getListView().setDivider(
				this.getDrawable(R.drawable.ico_setting_list_line));
		LinearLayout tv = (LinearLayout) findViewById(com.android.internal.R.id.headers);
		FrameLayout mListFooter = (FrameLayout) findViewById(com.android.internal.R.id.list_footer);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onIsMultiPane() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * Populate the activity with the top-level headers.
	 */
	@SuppressLint("NewApi")
	@Override
	public void onBuildHeaders(List<Header> headers) {
		loadHeadersFromResource(R.xml.settings_headers, headers);
		updateHeaderList(headers);
	}

	private void updateHeaderList(List<Header> target) {
		int i = 0;
		while (i < target.size()) {
			Header header = target.get(i);
			int id = (int) header.id;
			if (id == R.id.bluetooth_settings) {
				// Remove Bluetooth Settings if Bluetooth service is not
				// available.
				// if
				// (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH))
				// {
				target.remove(i);
				// }
			}

			if (target.get(i) == header) {
				// Hold on to the first header, when we need to reset to the
				// top-level
				if (mFirstHeader == null) {
					mFirstHeader = header;
				}
				mHeaderIndexMap.put(id, i);
				i++;
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean isMultiPane() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		// If it is not launched from history, then reset to top-level
		if ((intent.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0
				&& mFirstHeader != null
				&& !onIsHidingHeaders()
				&& onIsMultiPane()) {
			switchToHeaderLocal(mFirstHeader);
		}
	}

	private void switchToHeaderLocal(Header header) {
		mInLocalHeaderSwitch = true;
		switchToHeader(header);
		mInLocalHeaderSwitch = false;
	}

	/**
	 * Override initial header when an activity-alias is causing Settings to be
	 * launched for a specific fragment encoded in the android:name parameter.
	 */
	@Override
	public Header onGetInitialHeader() {
		String fragmentClass = getStartingFragmentClass(super.getIntent());
		if (fragmentClass != null) {
			Header header = new Header();
			header.fragment = fragmentClass;
			header.title = getTitle();
			header.fragmentArguments = getIntent().getExtras();
			mCurrentHeader = header;
			return header;
		}

		return mFirstHeader;
	}

	/**
	 * Checks if the component name in the intent is different from the Settings
	 * class and returns the class name to load as a fragment.
	 */
	protected String getStartingFragmentClass(Intent intent) {
		if (mFragmentClass != null)
			return mFragmentClass;

		String intentClass = intent.getComponent().getClassName();
		if (intentClass.equals(getClass().getName()))
			return null;

		if ("com.android.settings.ManageApplications".equals(intentClass)
				|| "com.android.settings.RunningServices".equals(intentClass)
				|| "com.android.settings.applications.StorageUse"
						.equals(intentClass)) {
			// Old names of manage apps.
			// intentClass =
			// com.android.settings.applications.ManageApplications.class.getName();
		}

		return intentClass;
	}

	private void highlightHeader(int id) {
		if (id != 0) {
			Integer index = mHeaderIndexMap.get(id);
			if (index != null) {
				getListView().setItemChecked(index, true);
				getListView().smoothScrollToPosition(index);
			}
		}
	}

	@Override
	public void onHeaderClick(Header header, int position) {
		boolean revert = false;

		super.onHeaderClick(header, position);

		if (revert && mLastHeader != null) {
			highlightHeader((int) mLastHeader.id);
		} else {
			mLastHeader = header;
		}
	}

	protected boolean isValidFragment(String fragmentName) {
		// TODO Auto-generated method stub
		// return super.isValidFragment(fragmentName);
		return true;
	}
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		if (mLastHeader != null) {
			if ((SoundSettings.class.toString()).contains(mLastHeader.fragment)) {
				SoundSettings fragment = (SoundSettings) getFragmentManager()
						.findFragmentByTag(mLastHeader.fragment);
				if (fragment != null) {
					if (event.getKeyCode() == event.KEYCODE_VOLUME_DOWN) {
						if (event.getAction() == event.ACTION_DOWN) {
							fragment.subtractVolume();
						}
						return true;
					} else if (event.getKeyCode() == event.KEYCODE_VOLUME_UP) {
						if (event.getAction() == event.ACTION_DOWN) {
							fragment.addVolume();
						}
						return true;
					}
				}
			}
			return super.dispatchKeyEvent(event);
		}
		return super.dispatchKeyEvent(event);
	}

}
