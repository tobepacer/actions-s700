package com.actions.settings.display;

import static android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE;
import static android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
import static android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL;

import java.util.Arrays;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.UserHandle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.PreferenceManager.OnPreferenceTreeClickListener;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.util.AttributeSet;
import android.util.Log;
import com.actions.settings.R;
import com.actions.settings.SettingsPreferenceFragment;
import com.actions.settings.widget.BrightnessPreference;
//import com.actions.hardware.PerformanceManager;
import com.actions.settings.display.TvoutUtils;

public class DisplaySettings extends SettingsPreferenceFragment implements
		OnPreferenceChangeListener {

	private static final String TAG = "DisplaySettings";
	private static final String KEY_AUTO_BRIGHTNESS = "auto_brightness_preference";
	private static final String KEY_BRIGHTNESS = "brightness_preference";
	private static final String KEY_HDMI_MODE_SELECTOR = "hdmi_mode_selector";
	private static final String KEY_TVOUT_SETTINGS = "tvout_settings";
	private static final String KEY_HDMI_FRAME = "hdmi_wh";
	private SwitchPreference mAutoBrightnessPreference;
	private BrightnessPreference brightnessPreference;
	private PreferenceCategory mTvoutSettingsCategory;
	private Preference mTvoutScreenResize;

	private String mIntelligentSettingsName = "Intelligent_brightness_mode";
	private String mOpenIntelligentBacklightReceivce = "actions.owlplayer.OpenIntelligentBacklight";
	private String mCloseIntelligentBacklightReceivce = "actions.owlplayer.CloseIntelligentBacklight";
	private Intent mIntelligentBacklightIntent;
	private Intent mNotIntelligentBacklightIntent;

	private boolean mSupportHdmi;
	private HdmiSettingsListPreference mHdmiModePref;
	private TvoutUtils.HdmiUtils mHdmiUtils;

	public DisplaySettings() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.brightness_preferences);

		if (isAutomaticBrightnessAvailable(getResources())) {
			mAutoBrightnessPreference = (SwitchPreference) findPreference(KEY_AUTO_BRIGHTNESS);
			mAutoBrightnessPreference.setOnPreferenceChangeListener(this);
			mAutoBrightnessPreference.setDisableDependentsState(false);
		} else {
			removePreference(KEY_AUTO_BRIGHTNESS);
		}
		mSupportHdmi = true;
		brightnessPreference = (BrightnessPreference) findPreference(KEY_BRIGHTNESS);

		// mPerformance = new PerformanceManager();
		mIntelligentBacklightIntent = new Intent();
		mIntelligentBacklightIntent
				.setAction(mOpenIntelligentBacklightReceivce);
		mNotIntelligentBacklightIntent = new Intent();
		mNotIntelligentBacklightIntent
				.setAction(mCloseIntelligentBacklightReceivce);
		mTvoutSettingsCategory = (PreferenceCategory) findPreference(KEY_TVOUT_SETTINGS);

		if (mSupportHdmi) {
			mHdmiModePref = new HdmiSettingsListPreference(getActivity());
			mHdmiModePref.setLayoutResource(R.layout.preference_tvout);
			mHdmiModePref.setKey(KEY_HDMI_MODE_SELECTOR);
			mHdmiModePref
					.setTitle(getResources().getString(R.string.hdmi_mode));
			mHdmiModePref.setPersistent(true);
			mTvoutSettingsCategory.addPreference(mHdmiModePref);
			mHdmiModePref.setDialogTitle(getResources().getString(
					R.string.hdmi_mode));
			mHdmiModePref.setOnPreferenceChangeListener(this);

			mTvoutScreenResize = new Preference(getActivity());
			mTvoutScreenResize.setTitle(getResources().getString(
					R.string.tvout_screen_resize));
			mTvoutSettingsCategory.addPreference(mTvoutScreenResize);
		} else {
			getPreferenceScreen().removePreference(mTvoutSettingsCategory);
		}

		mHdmiUtils = (TvoutUtils.HdmiUtils) TvoutUtils
				.getInstanceByName(TvoutUtils.TVOUT_HDMI);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.v("", "onCreateView");
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private static boolean isAutomaticBrightnessAvailable(Resources res) {
		return res
				.getBoolean(com.android.internal.R.bool.config_intelligent_brightness_available);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object arg1) {
		// TODO Auto-generated method stub
		final String key = preference.getKey();
		if (preference == mAutoBrightnessPreference) {
			boolean auto = (Boolean) arg1;

			brightnessPreference.setEnabled(!auto);
			if (auto) {
				// mPerformance.enbleAutoAdjustBacklight();
				Settings.System.putIntForUser(getActivity()
						.getContentResolver(), mIntelligentSettingsName, 1,
						UserHandle.USER_CURRENT);
				getActivity().sendBroadcast(mIntelligentBacklightIntent);

			} else {
				// mPerformance.disableAutoAdjustBacklight();
				Settings.System.putIntForUser(getActivity()
						.getContentResolver(), mIntelligentSettingsName, 0,
						UserHandle.USER_CURRENT);
				getActivity().sendBroadcast(mNotIntelligentBacklightIntent);

			}
			brightnessPreference.onChanged(auto);
		}
		if (mHdmiUtils.isCablePlugIn()) {
			if (KEY_HDMI_MODE_SELECTOR.equals(key)) {
				String value = (String) arg1;
				if (!value.equals(mHdmiUtils.getMode())) {
					mHdmiUtils.switchToSelectModeByModeValue(value);
					mHdmiModePref.setValue(value);
				}
			}

		}

		return true;
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		// TODO Auto-generated method stub
		if (preference == mTvoutScreenResize) {
			final Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.setClass(getActivity(), TvoutScreenResizeActivity.class);
			startActivity(intent);
		}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateState();

	}

	private void updateState() {
		// Update auto brightness if it is available.
		if (mAutoBrightnessPreference != null) {
			int brightnessMode = Settings.System.getIntForUser(getActivity()
					.getContentResolver(), mIntelligentSettingsName, 0,
					UserHandle.USER_CURRENT);

			if (brightnessMode == SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
				brightnessPreference.setEnabled(false);
			}
			mAutoBrightnessPreference.setChecked(brightnessMode != 0);
		}
	}

	public class HdmiSettingsListPreference extends ListPreference implements
			CompoundButton.OnCheckedChangeListener {

		private Switch mSwitchView = null;
		private static final String TAG = "HdmiSettingsListPreference";

		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			Log.d(TAG, "isChecked=" + isChecked);
			if (getLastSwitchStatus() != isChecked) {
				// setLastSwitchStatus(isChecked);
				mHdmiUtils.setHdmiEnable(isChecked);
				// if(!isChecked) {
				// mHdmiUtils.closeTvoutDisplay();
				// }
			}
		}

		public HdmiSettingsListPreference(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		public HdmiSettingsListPreference(Context context) {
			super(context, null);
		}

		private boolean getLastSwitchStatus() {
			return mHdmiUtils.getHdmiEnable();
		}

		protected void onClick() {
			if (mSwitchView != null && !mSwitchView.isChecked()) {
				return;
			}

			if (getDialog() != null && getDialog().isShowing())
				return;

			showDialog(null);
		}

		@Override
		protected void onBindView(View view) {
			super.onBindView(view);

			View checkableView = view.findViewById(R.id.tvoutSwitch);
			Log.d(TAG,
					"checkableView=" + checkableView + ",isEnabled="
							+ this.isEnabled());
			if (checkableView != null && checkableView instanceof Checkable) {
				((Checkable) checkableView).setChecked(getLastSwitchStatus());

				if (checkableView instanceof Switch) {
					mSwitchView = (Switch) checkableView;
					mSwitchView.setOnCheckedChangeListener(this);
				}
			}
			// syncSummaryView(view);
		}

		@Override
		protected void onPrepareDialogBuilder(Builder builder) {
			Log.d(TAG, "onPrepareDialogBuilder");
			updateTvoutMenuStatus(false);
			super.onPrepareDialogBuilder(builder);
		}

	}

	private void updateTvoutMenuStatus(boolean isPlugIn) {
		boolean resetValueFlag = isPlugIn;

		Log.d(TAG, "run in updateTvoutMenuStatus");

		if (!mSupportHdmi) {
			return;
		}
		/**
		 * if ListPreference' dialog is showing, may be need to update the hdmi
		 * mode list,when hdmi line plug,here just dismiss
		 */
		if (mHdmiModePref.getDialog() != null
				&& mHdmiModePref.getDialog().isShowing()) {
			mHdmiModePref.getDialog().dismiss();
			resetValueFlag = true;
		}

		String[] hdmiEntries = null;
		String[] hdmiValues = null;

		if (mHdmiUtils.isCablePlugIn()) {
			Log.d(TAG, "mHdmiUtils.isCablePlugIn true");
			hdmiEntries = hdmiValues = mHdmiUtils.getSupportedModesList();
			Log.d(TAG, "hdmiEntries " + Arrays.toString(hdmiValues)
					+ " length: " + hdmiValues.length);
			if (hdmiEntries != null) {
				mHdmiModePref.setEntries(hdmiEntries);
				mHdmiModePref.setEntryValues(hdmiValues);
				String str = mHdmiUtils.getMode();
				str.trim();
				Log.d(TAG, "getMode: " + str);
				mHdmiModePref.setValue(str);
			}
		}

		if (hdmiEntries == null || hdmiValues == null) {
			Log.d(TAG, "hdmiEntries == null || hdmiValues == null");
			hdmiEntries = getResources().getStringArray(
					R.array.tvout_hdmi_entries);
			hdmiValues = getResources().getStringArray(
					R.array.tvout_hdmi_entries);
			mHdmiModePref.setEntries(hdmiEntries);
			mHdmiModePref.setEntryValues(hdmiValues);
		}
	}

}
