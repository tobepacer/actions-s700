package com.actions.settings.about;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.actions.settings.R;
import com.actions.settings.SettingsPreferenceFragment;
import com.actions.settings.widget.CheckUpdataPreference;

public class DeviceInfoSettings extends SettingsPreferenceFragment {

	private static final String MODE_KEY = "about_model";
	private static final String BUILD_KEY = "about_build";
	private static final String CHECK_UPDATA_KEY = "check_new";
	private static final int KEY_DOWN_COUNTS = 8;

	private Preference modelPreference;
	private Preference buildPreference;
	private CheckUpdataPreference updatePreference;
	private int mDevHitCountdown;
	private int mGotoAndroidSetttings;
	private Toast mDevHitToast;

	public DeviceInfoSettings() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.about_preferences);

		modelPreference = findPreference(MODE_KEY);
		buildPreference = findPreference(BUILD_KEY);
		updatePreference = (CheckUpdataPreference)findPreference(CHECK_UPDATA_KEY);

		modelPreference.setSummary(Build.MODEL);
		buildPreference.setSummary(Build.DISPLAY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		getListView().setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDevHitCountdown = KEY_DOWN_COUNTS;
		mGotoAndroidSetttings = KEY_DOWN_COUNTS;
		mDevHitToast = null;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		// TODO Auto-generated method stub
		if (preference.getKey().equals(BUILD_KEY)) {
			if (mDevHitCountdown > 0) {
				mDevHitCountdown--;
				if (mDevHitCountdown == 0) {
					if (mDevHitToast != null) {
						mDevHitToast.cancel();
					}
					Intent mIntent = new Intent();
					ComponentName componentName = new ComponentName(
							"com.android.calculator2",
							"com.android.calculator2.Calculator");
					mIntent.setComponent(componentName);
					getActivity().startActivity(mIntent);
				} else if (mDevHitCountdown > 0 && mDevHitCountdown == 2) {
					if (mDevHitToast != null) {
						mDevHitToast.cancel();
					}
					mDevHitToast = Toast
							.makeText(
									getActivity(),
									R.string.preference_about_build_show_hardware_tools,
									Toast.LENGTH_SHORT);
					mDevHitToast.show();
				}
			}

		} else if (preference.getKey().equals(MODE_KEY)) {

			if (mGotoAndroidSetttings > 0) {
				mGotoAndroidSetttings--;
				if (mGotoAndroidSetttings == 0) {
					if (mDevHitToast != null) {
						mDevHitToast.cancel();
					}
					Intent mIntent = new Intent();
					ComponentName componentName = new ComponentName(
							"com.android.settings",
							"com.android.settings.Settings");
					mIntent.setComponent(componentName);
					getActivity().startActivity(mIntent);
				} else if (mGotoAndroidSetttings > 0
						&& mGotoAndroidSetttings == 2) {
					if (mDevHitToast != null) {
						mDevHitToast.cancel();
					}
					mDevHitToast = Toast
							.makeText(
									getActivity(),
									R.string.preference_about_build_show_android_settings,
									Toast.LENGTH_SHORT);
					mDevHitToast.show();
				}
			}

		}else if (preference.getKey().equals(CHECK_UPDATA_KEY)){
			Log.d("", "CHECK_UPDATA_KEY");
			updatePreference.checkCallClick();
		}

		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

}
