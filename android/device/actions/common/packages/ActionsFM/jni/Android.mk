LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := actionsfmjni
LOCAL_SRC_FILES := actionsfm.c
LOCAL_SHARED_LIBRARIES := fm
include $(BUILD_SHARED_LIBRARY)
include $(LOCAL_PATH)/prebuilt/Android.mk
