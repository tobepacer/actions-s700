package com.actions.fm;

import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class Utils {
	private static final String PREFERENCES_FILENAME = "Radio";
	private static final String TAG = "Utils";
	public Utils() {
		// TODO Auto-generated constructor stub
	}
	
	public static float screenDensity(Context ctx) {
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		display.getMetrics(dm);
		return dm.density;
	}

	public static boolean setPreferences(Context context, String key, Object value) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_WORLD_READABLE);
		SharedPreferences.Editor editor = settings.edit();
		if (value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		} else if (value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		} else if (value instanceof String) {
			editor.putString(key, (String) value);
		} else {
			Log.e(TAG, "Unexpected type:" + key + "=" + value);
		}
		return editor.commit();
	}

	/** get platform preference settings */
	public static Object getPreferences(Context context, String key, Object deft) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_WORLD_READABLE);
		Map<String, ?> settingMap = settings.getAll();
		Object obj = settingMap.get(key);
		return  (obj != null ? obj : deft);
	}
	public static void clearPreferences(Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_WORLD_READABLE);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
	}
	
	
}
