package com.actions.fm;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.actions.fm.fragment.RadioFragment;

public class FMActivity extends FragmentActivity implements
		OnAudioFocusChangeListener {
	private static final String TAG = "FMnActivity";
	public AudioManager mAudioManager;
	private WakeLock wakeLock = null;
	private static final String RADIO_FRAGMENT = "RadioFragment";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();
			fragmentTransaction.replace(R.id.container, new RadioFragment(),RADIO_FRAGMENT);
			fragmentTransaction.commit();
		}
		Log.v(TAG, "oncreate");

		initFm();

	}

	private void initFm() {
		mAudioManager = (AudioManager) this
				.getSystemService(Context.AUDIO_SERVICE);
		mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);

		acquireWakeLock();

		int init = AndroidFM.initFM();
		Log.i(TAG, "init:"+init);
		if(init != 0){
			Toast.makeText(getApplication(), "Init Fail!", Toast.LENGTH_LONG).show();
			finish();
			System.exit(0);
		}
		mAudioManager.setParameters("fm_enable=1");
		AndroidFM.setVolume(4);
		AndroidFM.setFrequency(AndroidFM.getFrequency());
		Toast.makeText(getApplication(), getString(R.string.fm_msg_good_linsten), Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.v(TAG, "onDestroy");
		mAudioManager.abandonAudioFocus(this);		
		mAudioManager.setParameters("fm_enable=0");					
		AndroidFM.releaseFm();				
		
		releaseWakeLock();
	}

	private void acquireWakeLock() {
		if (null == wakeLock) {
			PowerManager pm = (PowerManager) this
					.getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK
					| PowerManager.ON_AFTER_RELEASE, "FMService");
			if (null != wakeLock) {
				wakeLock.acquire();
			}
		}
	}

	private void releaseWakeLock() {
		if (null != wakeLock) {
			wakeLock.release();
			wakeLock = null;
		}
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// TODO Auto-generated method stub
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_LOSS:
			Log.v(TAG, "AudioFocus: received AUDIOFOCUS_LOSS");
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			Log.v(TAG, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT");

			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			Log.v(TAG,
					"AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");

			break;
		case AudioManager.AUDIOFOCUS_GAIN:
			Log.v(TAG, "AudioFocus: received AUDIOFOCUS_GAIN");
			break;
		default:
			Log.e(TAG, "Unknown audio focus change code");
		}
	}

	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		RadioFragment fragment = (RadioFragment) getSupportFragmentManager().findFragmentByTag(RADIO_FRAGMENT);
		if (event.getKeyCode() == event.KEYCODE_VOLUME_DOWN) {
			if (fragment!= null){
				fragment.reduceVolume();
			}
			return true;
		} else if (event.getKeyCode() == event.KEYCODE_VOLUME_UP) {
			if (fragment!= null){
				fragment.addVolume();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
