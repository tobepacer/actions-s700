package com.actions.fm.swipemenulistview;


/**
 * 
 * @author Actions.CSRD4.ESS
 * @date 2015-04-01
 * 
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
