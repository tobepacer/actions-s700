package com.actions.fm.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.actions.fm.AndroidFM;
import com.actions.fm.FMActivity;
import com.actions.fm.R;
import com.actions.fm.Utils;
import com.actions.fm.swipemenulistview.SwipeMenu;
import com.actions.fm.swipemenulistview.SwipeMenuCreator;
import com.actions.fm.swipemenulistview.SwipeMenuItem;
import com.actions.fm.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.actions.fm.swipemenulistview.SwipeMenuListView.OnSwipeListener;
import com.actions.fm.widget.RadioDial;
import com.actions.fm.widget.RadioDial.OnChannelChangeListener;

@SuppressLint("ResourceAsColor")
public class RadioFragment extends Fragment implements OnClickListener,
		OnCheckedChangeListener, OnItemClickListener {
	private static final String TAG = "RadioFragment";
	private static final String CHANNEL_NAME_KEY = "channnel_name";
	private static final String CHANNEL_FREQUENCY_KEY = "channnel_frequency";
	private static final String CHANNEL_FAVORATE_KEY = "channel_favorate";
	private static final String CHANNEL_COUNTS_KEY = "channnel_counts";
	private static final String COLLECT_CHANNEL_NUMBER_KEY = "collect_channel_number";
	private static final String MAP_KEY_FAVORATE = "map_favorate_channel";
	private static final int MSG_CHANNEL_CHANGE = 0X01;
	private static final int MSG_SET_CHANNEL = 0X02;
	private FMActivity mActivity;

	private int mCurrentChannelNum;
	private ArrayList<HashMap<String, Object>> saveList;
	private static final int CHANNEL_START = 875;
	private static final int CHANNEL_END = 1080;
	private static final int MAX_VOLUME = 15;
	private SearchAsyncTask mSearchAsyncTask = null;
	private boolean isPlaying = true;
	private int mCurPosion = -1;
	private boolean isRadioEnable = true;
	private int collectChannelNumber = 1;

	// widget
	private LinearLayout headBackLayout;
	private RadioDial mRadioDial;
	private Switch mRadioSwtich;
	private ImageButton mNextRadio;
	private ImageButton mPreRadio;
	private ImageButton mRadioAdd;
	private ImageButton mRadioReduce;
	private ImageButton mSaveChannel;
	private ImageButton mCollectChannel;
	private ImageButton mAutoSearch;
	private ImageButton mPlayButton;
	private com.actions.fm.swipemenulistview.SwipeMenuListView mChannelList;
	private ChannelAdapter listAdapter;
	private ImageButton mVolumeUp;
	private ImageButton mVolumeDown;
	private SeekBar mVolumeSeekBar;
	private ProgressDialog mypDialog;

	public RadioFragment() {
		// TODO Auto-generated constructor stub
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			int type = msg.what;
			switch (type) {
			case MSG_CHANNEL_CHANGE:
				Utils.clearPreferences(getActivity());
				Utils.setPreferences(getActivity(), CHANNEL_COUNTS_KEY,
						saveList.size());
				Utils.setPreferences(getActivity(), COLLECT_CHANNEL_NUMBER_KEY,
						collectChannelNumber);

				int times = 0;
				for (HashMap map : saveList) {
					Set set = map.entrySet();
					int k = 0;
					Iterator it = set.iterator();
					while (it.hasNext()) {
						Entry entry = (Map.Entry) it.next();
						switch (k) {
						case 0:
							Utils.setPreferences(getActivity(),
									CHANNEL_NAME_KEY + times,
									(String) entry.getKey());
							Utils.setPreferences(getActivity(),
									CHANNEL_FREQUENCY_KEY + times,
									(int) entry.getValue());
							break;

						case 1:
							Utils.setPreferences(getActivity(),
									CHANNEL_FAVORATE_KEY + times,
									entry.getValue());
							break;

						default:
							break;
						}
						k++;

					}

					times++;
				}
				break;
			case MSG_SET_CHANNEL:
				int frequency = (int) msg.obj;
				AndroidFM.setFrequency(frequency);
				mRadioDial.setFrequence(frequency * 100);

				break;

			default:
				break;
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mActivity = (FMActivity) getActivity();

	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_radio, container,
				false);
		saveList = new ArrayList<HashMap<String, Object>>();
		initRadioUI(rootView);
		getRadioPreference();
		return rootView;
	}

	private void initRadioUI(View rootView) {
		headBackLayout = (LinearLayout) rootView.findViewById(R.id.headerTitle);
		mRadioSwtich = (Switch) rootView.findViewById(R.id.btn_switch);
		mRadioSwtich.setChecked(true);
		mPreRadio = (ImageButton) rootView.findViewById(R.id.btn_seekbackward);
		mNextRadio = (ImageButton) rootView.findViewById(R.id.btn_seekforward);
		mRadioAdd = (ImageButton) rootView.findViewById(R.id.btn_add);
		mRadioReduce = (ImageButton) rootView.findViewById(R.id.btn_reduce);
		mPlayButton = (ImageButton) rootView.findViewById(R.id.btn_play);
		mAutoSearch = (ImageButton) rootView.findViewById(R.id.btn_search);
		mSaveChannel = (ImageButton) rootView.findViewById(R.id.btn_save);
		mCollectChannel = (ImageButton) rootView.findViewById(R.id.btn_collect);
		// mCollectChannel.setImageResource(R.drawable.collect_channel_bg);
		mVolumeUp = (ImageButton) rootView
				.findViewById(R.id.btn_volumeIncrease);
		mVolumeDown = (ImageButton) rootView
				.findViewById(R.id.btn_volumeDecrease);
		mVolumeSeekBar = (SeekBar) rootView.findViewById(R.id.volumeBar);

		mChannelList = (com.actions.fm.swipemenulistview.SwipeMenuListView) rootView
				.findViewById(R.id.channelList);
		listAdapter = new ChannelAdapter(getActivity(), saveList);
		mChannelList.setAdapter(listAdapter);
		mChannelList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		SwipeMenuCreator creator = new SwipeMenuCreator() {
			@Override
			public void create(SwipeMenu menu) {
				// TODO Auto-generated method stub

				SwipeMenuItem deleteItem = new SwipeMenuItem(mActivity);
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(R.drawable.ic_delete);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};

		mChannelList.setMenuCreator(creator);
		mChannelList.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public void onMenuItemClick(int position, SwipeMenu menu, int index) {
				// TODO Auto-generated method stub
				switch (index) {
				case 0:
					saveList.remove(position);
					listAdapter.notifyDataSetChanged();
					int showPosition = mChannelList.getCheckedItemPosition();
					// Log.v(TAG, "showPosition:"+showPosition);
					if (position < showPosition) {
						mChannelList.setItemChecked(showPosition - 1, true);
					}
					setRadioPreference();
					break;
				}
			}
		});

		mChannelList.setOnSwipeListener(new OnSwipeListener() {

			@Override
			public void onSwipeStart(int position) {
				// swipe start
			}

			@Override
			public void onSwipeEnd(int position) {
				// swipe end
			}
		});

		mPlayButton.setImageResource(R.drawable.fm_play_btn);

		headBackLayout.setOnClickListener(this);
		mChannelList.setOnItemClickListener(this);
		mPreRadio.setOnClickListener(this);
		mNextRadio.setOnClickListener(this);
		mRadioAdd.setOnClickListener(this);
		mRadioReduce.setOnClickListener(this);
		mPlayButton.setOnClickListener(this);
		mAutoSearch.setOnClickListener(this);
		mSaveChannel.setOnClickListener(this);
		mCollectChannel.setOnClickListener(this);

		mVolumeUp.setOnClickListener(this);
		mVolumeDown.setOnClickListener(this);
		mVolumeSeekBar.setMax(MAX_VOLUME);
		mVolumeSeekBar.setProgress(AndroidFM.getVolume());
		mVolumeSeekBar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
					@Override
					public void onProgressChanged(SeekBar arg0, int position,
							boolean arg2) {
						if (arg2 == true) {
							AndroidFM.setVolume(position);
						}
					}

					@Override
					public void onStartTrackingTouch(SeekBar arg0) {

					}

					@Override
					public void onStopTrackingTouch(SeekBar arg0) {
					}

				});

		mRadioDial = (RadioDial) rootView.findViewById(R.id.radioDial);
		mCurrentChannelNum = AndroidFM.getFrequency();
		if (mCurrentChannelNum < CHANNEL_START
				|| mCurrentChannelNum > CHANNEL_END) {
			mCurrentChannelNum = CHANNEL_START * 100;
		} else {
			mCurrentChannelNum = mCurrentChannelNum * 100;
		}
		mRadioDial.setFrequence(mCurrentChannelNum);
		mRadioSwtich.setOnCheckedChangeListener(this);

		mRadioDial.setOnChannelListener(new OnChannelChangeListener() {

			@Override
			public void onStopTrackingTouch(RadioDial dial) {
				mCurrentChannelNum = dial.getFrequence();
				// mRadioDial.setFrequence(mCurrentChannelNum);
				// Log.i(TAG, "mCurrentChannelNum:"+mCurrentChannelNum);
				// if (mBandnum != RadioDial.RADIO_SYSTEM_EUROPE) {
				// mRadioDial.setFrequence(mCurrentChannelNum);
				// }

				AndroidFM.setFrequency(mCurrentChannelNum / 100);
			}

			@Override
			public void onStartTrackingTouch(RadioDial dial) {

			}

			@Override
			public void onChannelChanged(RadioDial dial, int frequence) {
				// Log.v(TAG, "onChannelChanged frequence:"+frequence);
				if (frequence < CHANNEL_START * 100) {
					frequence = CHANNEL_START;
				} else if (frequence > CHANNEL_END * 100) {
					frequence = CHANNEL_END;
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// save Channel
		// setRadioPreference();
	}

	private void setRadioPreference() {
		handler.sendEmptyMessage(MSG_CHANNEL_CHANGE);
	}

	private int dp2px(int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				getResources().getDisplayMetrics());
	}

	private void getRadioPreference() {
		int counts = (Integer) Utils.getPreferences(getActivity(),
				CHANNEL_COUNTS_KEY, -1);
		collectChannelNumber = (Integer) Utils.getPreferences(getActivity(),
				COLLECT_CHANNEL_NUMBER_KEY, 1);

		LinkedHashMap<String, Object> map;
		for (int i = 0; i < counts; i++) {
			map = new LinkedHashMap<String, Object>();
			String key = (String) (Utils.getPreferences(getActivity(),
					CHANNEL_NAME_KEY + i, -1));
			int value = (Integer) (Utils.getPreferences(getActivity(),
					CHANNEL_FREQUENCY_KEY + i, -1));
			boolean favorate = (Boolean) (Utils.getPreferences(getActivity(),
					CHANNEL_FAVORATE_KEY + i, false));
			map.put(key, value);
			map.put(MAP_KEY_FAVORATE, favorate);
			saveList.add(map);
			if (value == AndroidFM.getFrequency()) {
				mChannelList.setItemChecked(i, true);
				updateCollectState(favorate);
				mCurPosion = i;
				mChannelList.smoothScrollToPosition(i);
			}
			listAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.headerTitle:
			getActivity().finish();
			break;
		// seek pre
		case R.id.btn_seekbackward:
			int position = mChannelList.getCheckedItemPosition() - 1;
			if (position < 0) {
				position = mCurPosion - 1;
			}
			if (saveList.size() > 0 && position < 0)
				return;
			mChannelList.setItemChecked(position, true);
			setViewFrequency(position);
			mCurPosion = position;
			break;
		// seek next
		case R.id.btn_seekforward:
			if (mCurPosion < 0) {
				mCurPosion = 0;
			}
			mCurPosion = mCurPosion + 1;
			if (saveList.size() > 0 && mCurPosion >= saveList.size())
				return;
			mChannelList.setItemChecked(mCurPosion, true);
			setViewFrequency(mCurPosion);

			break;
		case R.id.btn_add:
			AndroidFM.setFrequency(AndroidFM.getFrequency() + 1);
			setViewFrequency(-1);
			break;
		case R.id.btn_reduce:
			AndroidFM.setFrequency(AndroidFM.getFrequency() - 1);
			setViewFrequency(-1);
			break;
		case R.id.btn_play:
			playResume();
			break;
		case R.id.btn_search:
			if (saveList.size() > 0) {
				showWarnningDialog();
			} else {
				startAutoSearch();
			}
			break;
		case R.id.btn_save:
			saveChannel();
			break;
		case R.id.btn_collect:
			int cheeckPosition = mChannelList.getCheckedItemPosition();
			if (cheeckPosition < 0) {
				return;
			}
			HashMap<String, Object> values = saveList.get(cheeckPosition);

			boolean collect = (boolean) values.get(getKey(values)[1]);

			listAdapter.setFavorateChannelShow(cheeckPosition, !collect);
			updateCollectState(!collect);
			break;
		case R.id.btn_volumeDecrease:
			reduceVolume();
			break;
		case R.id.btn_volumeIncrease:
			addVolume();
			break;

		}

	}

	public void addVolume() {
		VolumeUp();
		mVolumeSeekBar.setProgress(AndroidFM.getVolume());
	}

	public void reduceVolume() {
		volumeDown();
		mVolumeSeekBar.setProgress(AndroidFM.getVolume());
	}

	public void VolumeUp() {
		int upValue = AndroidFM.getVolume();
		if (upValue >= 15) {
			upValue = 15;
		} else {
			upValue++;
		}
		AndroidFM.setVolume(upValue);
	}

	public void volumeDown() {
		int downValue = AndroidFM.getVolume();
		if (downValue <= 0) {
			downValue = 0;
		} else {
			downValue--;
		}
		AndroidFM.setVolume(downValue);
	}

	private void setViewFrequency(int position) {
		mCurrentChannelNum = AndroidFM.getFrequency() * 100;
		mRadioDial.setFrequence(mCurrentChannelNum);
		mPlayButton.setImageResource(R.drawable.fm_play_btn);
		isPlaying = true;

		if (position < 0) {
			mChannelList.setItemChecked(-1, true);
			return;
		}
		HashMap<String, Object> values;
		values = saveList.get(position);
		String key = getKey(values)[0];

		int frequency = (int) values.get(key);
		boolean collect = (boolean) values.get(getKey(values)[1]);

		updateCollectState(collect);

		mCurPosion = position;

		Message msg = handler.obtainMessage(MSG_SET_CHANNEL, frequency);
		handler.sendMessage(msg);

	}

	private void updateCollectState(boolean collected) {

		if (collected) {
			mCollectChannel
					.setImageResource(R.drawable.cancel_collect_channel_bg);
		} else {
			mCollectChannel.setImageResource(R.drawable.collect_channel_bg);
		}
	}

	private void enableWdiget(boolean enable) {
		mPreRadio.setEnabled(enable);
		mNextRadio.setEnabled(enable);
		mRadioAdd.setEnabled(enable);
		mRadioReduce.setEnabled(enable);
		mPlayButton.setEnabled(enable);
		mAutoSearch.setEnabled(enable);
		mSaveChannel.setEnabled(enable);
		mCollectChannel.setEnabled(enable);
		mVolumeUp.setEnabled(enable);
		mVolumeDown.setEnabled(enable);
		mVolumeSeekBar.setEnabled(enable);
		mRadioDial.setEnabled(enable);
		mChannelList.setEnabled(enable);
	}

	private void playResume() {
		if (isPlaying) {
			mPlayButton.setImageResource(R.drawable.fm_pause_btn);
			AndroidFM.setMute(1);
			isPlaying = false;
		} else {
			mPlayButton.setImageResource(R.drawable.fm_play_btn);
			isPlaying = true;
			AndroidFM.setMute(0);
		}
	}

	private void saveChannel() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		int frequency = AndroidFM.getFrequency();
		float saveFrequence = ((float) frequency) / 10;
		// Log.v(TAG, "saveFrequence:" + saveFrequence);
		map.put(getString(R.string.fm_manual_add) + collectChannelNumber,
				frequency);
		map.put(MAP_KEY_FAVORATE, false);
		for (int i = 0; i < saveList.size(); i++) {
			HashMap<String, Object> values = saveList.get(i);

			if ((int) values.get(getKey(values)[0]) == frequency) {
				Toast.makeText(getActivity(), R.string.fm_save_same_text_msg,
						Toast.LENGTH_SHORT).show();
				return;
			}
		}
		saveList.add(map);
		listAdapter.notifyDataSetChanged();
		mChannelList.smoothScrollToPosition(saveList.size() - 1);
		setRadioPreference();
		collectChannelNumber++;
	}

	private void showWarnningDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.fm_auto_search_dialog_title);
		builder.setMessage(R.string.fm_auto_search_dialog_msg);
		builder.setPositiveButton(R.string.fm_auto_search_dialog_ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						startAutoSearch();
					}
				});

		builder.setNegativeButton(R.string.fm_auto_search_dialog_cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub

					}
				});
		builder.show();
	}

	private void startAutoSearch() {

		saveList.clear();
		listAdapter.notifyDataSetChanged();
		if (mSearchAsyncTask != null) {
			if (!mSearchAsyncTask.isCancelled()) {
				mSearchAsyncTask.cancel(true);
			}
		}
		collectChannelNumber = 1;
		mSearchAsyncTask = new SearchAsyncTask();
		mRadioDial.setFrequence(CHANNEL_START * 100);
		mSearchAsyncTask.execute(1);

	}

	public class SearchAsyncTask extends AsyncTask<Integer, Integer, String> {
		private int tmpChannel = 0;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			mypDialog = ProgressDialog.show(
					getActivity(),
					getActivity().getResources().getString(
							R.string.fm_auto_search_progress_title), null,
					true, false);
		}

		@Override
		protected String doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			AndroidFM.setFrequency(CHANNEL_START);

			while (AndroidFM.autoSeek(1) != -1 && !this.isCancelled()) {
				Log.v(TAG, "doInBackground");
				int frequency = AndroidFM.getFrequency();
				publishProgress(frequency);
				if (frequency < tmpChannel) {
					break;
				}
				try {
					Thread.sleep(1000);// let this channel play 2s.
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
				tmpChannel = AndroidFM.getFrequency();
			}
			return "ok";
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			Log.e(TAG, "onCancelled");
			super.onCancelled();
			afterSeekPlay();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			
			int vlaue = values[0];
			
			float saveKay = ((float) vlaue) / 10;
			mRadioDial.setFrequence((int) vlaue * 100);
			LinkedHashMap<String, Object> s = new LinkedHashMap<String, Object>();
			
			s.put(String.valueOf(saveKay) + "MHz", vlaue);
			
			s.put(MAP_KEY_FAVORATE, false);
			saveList.add(s);
			
			
			listAdapter.notifyDataSetChanged();
			mChannelList.smoothScrollToPosition(saveList.size() - 1);
			if (((int) vlaue) >= CHANNEL_END) {
				this.cancel(true);
			}

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			Log.v(TAG, "onPostExecute");
			super.onPostExecute(result);
			afterSeekPlay();
		}

		private void afterSeekPlay() {
			HashMap<String, Object> values;
			mypDialog.dismiss();
			if (saveList.size() <= 0)
				return;
			values = saveList.get(0);
			String key = getKey(values)[0];

			int frequency = (int) values.get(key);
			AndroidFM.setFrequency(frequency);
			mRadioDial.setFrequence(frequency * 100);

			mChannelList.setItemChecked(0, true);
			updateCollectState(false);
			mChannelList.smoothScrollToPosition(1);
			Toast.makeText(
					getActivity(),
					getString(R.string.fm_auto_search_progress_over)
							+ saveList.size() + getString(R.string.fm_channel),
					Toast.LENGTH_LONG).show();

			setRadioPreference();
		}

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (buttonView.getId() == R.id.btn_switch) {
			Log.v(TAG, "isChecked:" + isChecked);
			if (isChecked) {
				AndroidFM.initFM();
				enableWdiget(true);
				mActivity.mAudioManager.setParameters("fm_enable=1");
				AndroidFM.setVolume(mVolumeSeekBar.getProgress());
				AndroidFM.setFrequency(AndroidFM.getFrequency());
				isRadioEnable = true;

			} else {
				mActivity.mAudioManager.setParameters("fm_enable=0");
				AndroidFM.releaseFm();
				enableWdiget(false);
				isRadioEnable = false;
			}
		}
	}

	class ChannelAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		private ArrayList<HashMap<String, Object>> mDataList;
		private ListItemHolder itemHolder;;

		public ChannelAdapter(Context context,
				ArrayList<HashMap<String, Object>> list) {
			this.mInflater = LayoutInflater.from(context);
			mDataList = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return (mDataList != null) ? mDataList.size() : 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.channelitem, null);
				itemHolder = new ListItemHolder();
				itemHolder.channelName = (TextView) convertView
						.findViewById(R.id.channelName);
				itemHolder.favorateChannel = (ImageView) convertView
						.findViewById(R.id.favorateFm);
				convertView.setTag(itemHolder);
			} else {
				itemHolder = (ListItemHolder) convertView.getTag();
			}

			if (mDataList.size() > 0) {
				HashMap<String, Object> s;
				s = mDataList.get(position);
				String key = getKey(s)[0];
				Boolean favorate = (Boolean) s.get(getKey(s)[1]);
				if (favorate) {
					itemHolder.favorateChannel.setVisibility(View.VISIBLE);
				} else {
					itemHolder.favorateChannel.setVisibility(View.GONE);
				}
				itemHolder.channelName.setText(key);

			}

			return convertView;
		}

		public void setFavorateChannelShow(int position, boolean favorate) {
			HashMap<String, Object> map;
			if (position < 0 || position > saveList.size()) {
				return;
			}
			map = saveList.get(position);
			map.put(MAP_KEY_FAVORATE, favorate);
			Utils.setPreferences(getActivity(),
					CHANNEL_FAVORATE_KEY + position, favorate);
			this.notifyDataSetChanged();
		}
	}

	public class ListItemHolder {
		public TextView channelName;
		public ImageView favorateChannel;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Log.v(TAG, "select positiom:" + position);

		if (!isRadioEnable) {
			mRadioSwtich.setChecked(true);
		}
		HashMap<String, Object> values;
		values = saveList.get(position);
		String key = getKey(values)[0];

		// Log.v(TAG, "select key:" + key);
		int frequency = (int) values.get(key);
		boolean collect = (boolean) values.get(getKey(values)[1]);
		updateCollectState(collect);
		mCurPosion = position;

		Message msg = handler.obtainMessage(MSG_SET_CHANNEL, frequency);
		handler.sendMessage(msg);
	}

	private String[] getKey(HashMap map) {
		Set set = map.entrySet();
		if (set.size() <= 0) {
			return null;
		}
		Iterator it = set.iterator();
		String key[] = new String[set.size()];
		int i = 0;
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry) it.next();
			key[i] = (String) me.getKey();
			//Log.v(TAG, "getKey -- key :" + key[i]);
			i++;
		}
		return key;
	}

}
