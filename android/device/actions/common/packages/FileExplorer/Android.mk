#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_JAVA_LIBRARIES := ant_dep cpdetector_dep antlr_dep chardet_dep jargs_dep
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_MODULE_PATH :=$(TARGET_OUT)/app
LOCAL_PACKAGE_NAME := FileExplorer
LOCAL_CERTIFICATE := platform
LOCAL_DEX_PREOPT := false
#LOCAL_SDK_VERSION := current
LOCAL_PROGUARD_ENABLED := disabled 

include $(BUILD_PACKAGE)


include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := ant_dep:libs/ant.jar \
                                        cpdetector_dep:libs/cpdetector_1.0.10.jar \
                                        antlr_dep:libs/antlr-2.7.4.jar \
                                        chardet_dep:libs/chardet-1.0.jar \
                                        jargs_dep:libs/jargs-1.0.jar
include $(BUILD_MULTI_PREBUILT)

    
include $(call all-makefiles-under, $(LOCAL_PATH))
