package com.actions.explorer;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class ShellRunTask extends AsyncTask<String, Void, ShellLog> {
	
	String  newShellDirPath;
	String shellDirPath ;
	
    @Override
    protected ShellLog doInBackground(String... arg0) {
        // TODO Auto-generated method stub
    	String shellFilePath = arg0[0];
        shellDirPath = shellFilePath.substring(0,shellFilePath.lastIndexOf('/'));
    	String  cpShellDirPath = "/data";
    	 newShellDirPath = cpShellDirPath+shellDirPath.substring(shellDirPath.lastIndexOf('/'),shellDirPath.length());
//    	 execShell("cp -a " + shellDirPath+" "+cpShellDirPath);
    	 cmd("cp -a " + shellDirPath+" "+cpShellDirPath);
   
//    	execShell("cp -a " + shellDirPath+" "+cpShellDirPath);
    	String  newShellFilePath = newShellDirPath+shellFilePath.substring(shellFilePath.lastIndexOf('/'),shellFilePath.length());
//    	execShell("cp " + newShellDirPath+"macaddr.txt"+" "+shellDirPath);
    	Log.i("newShellDirPath",newShellDirPath);
//    	cmd(newShellFilePath);
        return execShell(newShellFilePath);
    }

    public static String cmd(String cmd) {
        try {
            //Log.e(TAG,"start cmd -----");
            Process process = null;
            DataOutputStream os = null;
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(cmd + "\n");
            os.flush();
            os.writeBytes("exit\n");
            os.flush();
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            int read;
            char[] buffer = new char[2048];
            StringBuffer output = new StringBuffer();
            while ((read = reader.read(buffer)) > 0) {
                output.append(buffer, 0, read);
            }
            process.waitFor();
            reader.close();
            process.destroy();
            os.close();
            //Log.e(TAG,"return cmd -----");
            return output.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            // e.printStackTrace();
            return null;
        }

    }

    
    @Override
    protected void onPostExecute(ShellLog result) {
        // TODO Auto-generated method stub

        if(result.doesWorkOk()) {
			if(result.needWarn()) {
                AlertDialog ad = new AlertDialog.Builder(Main.mContext)
                .setTitle("MAC ADDRESSES LIMITED!")
                .setMessage("Warning:"+ result.getErrorString())
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    // TODO Auto-generated method stub
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
               
                    @Override
                   public void onClick(DialogInterface arg0, int arg1) {
                       // TODO Auto-generated method stub
                  }
                })
                .show();
            }else{
            	try {
					Process p = Runtime.getRuntime().exec("cp " + newShellDirPath+"macaddr.txt"+" "+shellDirPath);
					p.waitFor();
					p.destroy();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                Toast.makeText(Main.mContext, "SUCCESS", Toast.LENGTH_LONG).show();
			    Toast.makeText(Main.mContext, "SUCCESS", Toast.LENGTH_LONG).show();
			}
			
        }  else {
            Toast.makeText(Main.mContext, "FAILED" + result.getErrorString(), Toast.LENGTH_LONG).show();
			Toast.makeText(Main.mContext, "FAILED" + result.getErrorString(), Toast.LENGTH_LONG).show();
        }
    }
    static final boolean TEST = false;
    private ShellLog execShell(String path){
	       try{  
	            Process p = Runtime.getRuntime().exec("su");  
				int index = path.lastIndexOf('/');
	        	String dir = path.substring(0, index);
				Log.i("caichsh", "dir:" +dir);
	            OutputStream outputStream = p.getOutputStream();
	            DataOutputStream dataOutputStream=new DataOutputStream(outputStream);
				Log.i("caichsh", "/system/bin/sh " + path+" "+dir+" "+shellDirPath);
//	            dataOutputStream.writeBytes("/system/bin/sh " + path+" "+dir+" "+shellDirPath);
				dataOutputStream.writeBytes("/system/bin/sh " + path+" "+dir+" "+shellDirPath);
	            dataOutputStream.flush();
	            dataOutputStream.close();
	            outputStream.close();
	
				InputStream inputStream = null;
				if(TEST) {
				    File test = new File("/data/test.txt");
				    if(!test.exists()){
				    	test.createNewFile();
				    }
				    inputStream = new FileInputStream(test);
				} else {
					inputStream =p.getInputStream();
				}
				InputStreamReader isr = new InputStreamReader(inputStream);
				BufferedReader bf = new BufferedReader(isr);
				
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = bf.readLine()) != null) {
				    Log.i("caichsh", "read line:" +line);
				   sb.append(line+"\n");
				}
				inputStream.close();
				
				return parseLog(sb.toString());
				
	       } catch(Throwable t) {  
	             t.printStackTrace();  
	      } 
	       
	       return new ShellLog(ShellLog.ERR_UNCOUGHT);
	    }
    private ShellLog parseLog(String line) {
		// TODO Auto-generated method stub
	    Log.i("caichsh","got log:" + line);
		if(line == null || line.length() == 0)
		return new ShellLog(ShellLog.ERR_UNKNOWN);
	if(line.contains("warning:")) {
			int index = line.indexOf("warning:");
		int warnStart = index + 8;
			int warnEnd = line.indexOf("\n", warnStart);
			String warnLog = line.substring(warnStart, warnEnd);
			return new ShellLog(ShellLog.ERR_WARNNING, warnLog);
		    //return new ShellLog(ShellLog.ERR_WARNNING);
		}
		
		if(line.contains("success:"))
		    return new ShellLog(ShellLog.ERR_NO_ERROR);
		
		if(line.contains("error:")) {
		int index = line.indexOf("error:");
			int errStart = index + 5;
			int errEnd = line.indexOf("\n", errStart);
			String errLog = line.substring(errStart, errEnd);
			return new ShellLog(ShellLog.ERR_UNDEFINEED, errLog);
		}
		

		return new ShellLog(ShellLog.ERR_UNKNOWN);
	}   
}
