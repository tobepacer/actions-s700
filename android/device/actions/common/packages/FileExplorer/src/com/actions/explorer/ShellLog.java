package com.actions.explorer;

public class ShellLog {
	public static final int ERR_NO_ERROR = 0;
    public static final int ERR_UNDEFINEED = -1;
    public static final int ERR_WARNNING = -2;
    public static final int ERR_UNKNOWN = -9;
    public static final int ERR_UNCOUGHT = -10;
    
    private int errno;
    private String state;
    
    public ShellLog(int err) {
        errno = err;
    }
   
    public ShellLog(int err, String stat) {
        errno = err;
        state = stat;
   }
    
    public boolean doesWorkOk() {
        if((errno == ERR_NO_ERROR)||(errno == ERR_WARNNING))
            return true;
        return false;
   }

   public boolean needWarn() {
       if(errno == ERR_WARNNING)
          return true;
      return false;
  }
    public String getErrorString() {
       String s;
       switch (errno) {
            case ERR_NO_ERROR:
                s = "Work Perfectly!";
                break;

			case ERR_WARNNING:
				s = state;
               break;
            case ERR_UNDEFINEED:
               s = state;
                break;
            default:
                s = "Unknown error!";
                break;
        }
        return s;
    }
}
