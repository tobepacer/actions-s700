package com.actions.music.data;

import java.util.LinkedList;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.actions.mediacenter.utils.Preferences;
import com.actions.music.app.Config;
import com.actions.music.data.PlayList.ListItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PlayRecord {
	private static final int MAX_RECORD = Config.MAX_PLAY_RECORD;
	private static final int LAST_INDEX_POSITION = 1;
	private LinkedList<ListItem> mPlayRecodeList;
	private Context mContext;
	private static final Object mLock = new Object();
	private static PlayRecord sPlayRecord;
	private int mMusicBreakponit = 0;

	private PlayRecord(Context context) {
		mContext = context;
		initPlayRecode();
		initMusicBreakpoint();
	}

	public static PlayRecord getInstance(Context context) {
		if (sPlayRecord == null) {
			synchronized (mLock) {
				if (sPlayRecord == null) {
					sPlayRecord = new PlayRecord(context);
				}
			}
		}
		return sPlayRecord;
	}

	public void add(ListItem playIndex) {
		mPlayRecodeList.addFirst(playIndex);
		if (mPlayRecodeList.size() > MAX_RECORD) {
			mPlayRecodeList.removeLast();
		}
	}

	public int getCurrentIndex() {
		if(mPlayRecodeList.isEmpty()) {
			return -1;
		}
		return mPlayRecodeList.getFirst().index;
	}
	
	public ListItem getCurrentItem() {
		if(mPlayRecodeList.isEmpty()) {
			Log.d("recode", "getCurrentItem null");
			return null;
		}
		return mPlayRecodeList.getFirst();
	}

	public ListItem getLastItem() {
		ListItem lastItem = getCurrentItem();
		if(lastItem!=null) {
			if(mPlayRecodeList.size() > 1) {
				try {
					lastItem = mPlayRecodeList.get(LAST_INDEX_POSITION);
					mPlayRecodeList.poll();
					mPlayRecodeList.poll();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				mPlayRecodeList.poll();
			}
		}
		return lastItem;
	}

	public int size() {
		return mPlayRecodeList.size();
	}

	public int get(int position) {
		return mPlayRecodeList.get(position).index;
	}

	public void save(int breakpoint) {
		saveBreakPoint(breakpoint);
		savePlayRecode();
	}

	public void clear() {
		if(mPlayRecodeList.isEmpty()) {
			return;
		}
		ListItem item = mPlayRecodeList.getFirst();
		mPlayRecodeList.clear();
		mPlayRecodeList.add(item);
	}

	private void initPlayRecode() {
		Gson gson = new GsonBuilder().create();
		ListItem item = gson.fromJson((String)Preferences.getPreferences(mContext, Config.PREFERENCE_SAVE_PLAYRECORD, ""), ListItem.class);
		mPlayRecodeList = new LinkedList<>();
		if(item!=null){
			add(item);
		}
	}

	private void initMusicBreakpoint() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		mMusicBreakponit = sharedPreferences.getInt(Config.PREFERENCE_SAVE_BREAKPOINT, 0);
	}

	private void savePlayRecode() {
		Gson gson = new GsonBuilder().create();
		String result = gson.toJson(mPlayRecodeList.peek());
		Preferences.setPreferences(mContext, Config.PREFERENCE_SAVE_PLAYRECORD, result);
	}

	private void saveBreakPoint(int breakpoint) {
		mMusicBreakponit = breakpoint;
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(Config.PREFERENCE_SAVE_BREAKPOINT, breakpoint);
		editor.commit();
	}

	public void removeFirst() {
		mPlayRecodeList.poll();
	}

	public int getMusicBreakpoint() {
		return mMusicBreakponit;
	}
}
