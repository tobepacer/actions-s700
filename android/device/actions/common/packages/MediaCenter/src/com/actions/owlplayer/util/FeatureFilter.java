package com.actions.owlplayer.util;

import java.util.Arrays;
import java.util.List;

public class FeatureFilter {

	public static final String VOLUME = "volume";
	public static final String BRIGHTNESS = "brightness";
	public static final String NAVIGATION = "navigation";
	public static final String DLNA = "dlna";
	public static final String SWF = "swf";

	private static List<String> sFeatureList = null;
	private static boolean sFirstRun = true;

	public static synchronized boolean isSupport(String feature) {
		if (sFirstRun) {
			sFirstRun = false;

			String featureAll = Utils.getFeatureFilters();
			if (featureAll != null) {
				String[] featureArray = featureAll.split(",");
				if (featureArray != null) {
					sFeatureList = Arrays.asList(featureArray);
				}
			}
		}

		if (sFeatureList != null && sFeatureList.contains(feature)) {
			return false;
		}

		return true;
	}
}
