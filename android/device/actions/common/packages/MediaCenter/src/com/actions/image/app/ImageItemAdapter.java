package com.actions.image.app;

import android.support.v7.widget.GridLayoutManager.LayoutParams;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.actions.image.data.LocalImage;
import com.actions.image.data.LocalImageSet;
import com.actions.mediacenter.R;
import com.actions.mediacenter.widget.BaseRecyclerViewAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ImageItemAdapter extends BaseRecyclerViewAdapter<ImageItemAdapter.ViewHolder> {
	private static final String TAG = ImageItemAdapter.class.getSimpleName();
	private LocalImageSet mLocalImageSet;
	private ImageLoader mImageLoader;
	private OnImageItemClickListener mOnItemClickListener;
	private OnImageItemLongClickListener mOnImageItemLongClickListener;

	public ImageItemAdapter(LocalImageSet localImageSet) {
		mLocalImageSet = localImageSet;
		mImageLoader = ImageLoader.getInstance();
	}

	@Override
	public int getItemCount() {
		return mLocalImageSet.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		super.onBindViewHolder(viewHolder, position);
		viewHolder.itemView.setTag(position);
		viewHolder.itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mOnItemClickListener != null) {
					mOnItemClickListener.onItemClick(v, (int) v.getTag());
				}
			}
		});
		viewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				if (mOnImageItemLongClickListener != null) {
					return mOnImageItemLongClickListener.onItemLongClick(v, (int) v.getTag());
				}
				return false;
			}
		});
		LocalImage image = (LocalImage) mLocalImageSet.getItem(position);
		mImageLoader.displayImage(image.getImageUri(), viewHolder.imageThumbnail);
		if (image.isSelect()) {
			viewHolder.selectImage.setVisibility(View.VISIBLE);
		} else {
			viewHolder.selectImage.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_item, viewGroup, false);
		initLayout(viewGroup, view);
		return new ViewHolder(view);
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		ImageView imageThumbnail;
		ViewGroup selectImage;

		public ViewHolder(View rootView) {
			super(rootView);
			imageThumbnail = (ImageView) rootView.findViewById(R.id.image_thumbnail);
			selectImage = (ViewGroup) rootView.findViewById(R.id.media_select_image);
		}

	}

	public void setOnItemClickListener(OnImageItemClickListener listener) {
		mOnItemClickListener = listener;
	}

	public void setOnItemLongClickListener(OnImageItemLongClickListener listener) {
		mOnImageItemLongClickListener = listener;
	}

	public interface OnImageItemClickListener {
		public void onItemClick(View view, int position);
	}

	public interface OnImageItemLongClickListener {
		public boolean onItemLongClick(View view, int position);
	}

	public void initLayout(ViewGroup viewGroup, View view) {
		Config.initThumbLayoutParams(viewGroup, view);
		LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
		layoutParams.setMargins(Config.thumbnailMarginLeft, Config.thumbnailMarginTop, Config.thumbnailMarginRight, Config.thumbnailMarginButtom);
		layoutParams.height = Config.thumbnailHeight;
		layoutParams.width = Config.thumbnailWidth;
		view.setLayoutParams(layoutParams);
	}
}
