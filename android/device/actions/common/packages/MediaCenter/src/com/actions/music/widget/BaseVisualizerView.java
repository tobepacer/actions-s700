package com.actions.music.widget;

import java.util.Arrays;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.view.View;

import com.actions.mediacenter.R;

public class BaseVisualizerView extends View implements Visualizer.OnDataCaptureListener {
	private static final String TAG = "BaseVisualizerView";

	// DN_W and DN_SL represent width ratio,DN_H and DN_SW represent height ratio,correct ratio to modify cylinder height and width
	private static final int DN_W = 480;
	private static final int DN_H = 160;
	private static final int DN_SL = 20;
	private static final int DN_SW = 6;

	private int hgap = 0;
	private int vgap = 0;
	private int levelStep = 0;
	private float strokeWidth = 0;
	private float strokeLength = 0;
	private Context mContext;
	/**
	 * It is the max level.
	 */
	protected final static int MAX_LEVEL = 13;

	/**
	 * It is the cylinder number.
	 */
	protected final static int CYLINDER_NUM = 12;

	/**
	 * It is the visualizer.
	 */
	protected Visualizer mVisualizer = null;

	private Equalizer mEqualizer = null;
	/**
	 * It is the paint which is used to draw to visual effect.
	 */
	protected Paint mPaint = null;

	/**
	 * It is the buffer of fft.
	 */
	protected byte[] mData = new byte[CYLINDER_NUM];

	boolean mDataEn = true;

	/**
	 * It constructs the base visualizer view.
	 * 
	 * @param context
	 *            It is the context of the view owner.
	 */
	public BaseVisualizerView(Context context) {
		super(context);
		mContext = context;
		initPaint();
	}

	private void initPaint() {
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setColor(mContext.getResources().getColor(R.color.white));
		mPaint.setStrokeJoin(Join.ROUND);
		mPaint.setStrokeCap(Cap.ROUND);
	}

	public BaseVisualizerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
		initPaint();
	}

	public BaseVisualizerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initPaint();
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		float w, h;
		w = getWidth();
		h = getHeight();

		strokeWidth = DN_SW * h / DN_H;
		strokeLength = DN_SL * w / DN_W;
		hgap = (int) ((w - strokeLength * CYLINDER_NUM) / (CYLINDER_NUM + 1));
		vgap = (int) (h / (MAX_LEVEL + 2));
		mPaint.setStrokeWidth(strokeWidth);
	}

	protected void drawCylinder(Canvas canvas, float x, byte value) {
		if (value <= 0)
			value = 1;
		for (int i = 0; i < value; i++) {
			float y = getHeight() - i * vgap - vgap;
			canvas.drawLine(x, y, x + strokeLength, y, mPaint);
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		for (int i = 0; i < CYLINDER_NUM; i++) {
			float x = strokeWidth / 2 + hgap + i * (hgap + strokeLength);
			drawCylinder(canvas, x, mData[i]);
		}
	}

	/**
	 * It sets the visualizer of the view. DO set the viaulizer to null when exit the program.
	 * 
	 * @parma visualizer It is the visualizer to set.
	 */
	public void setVisualizer(Visualizer visualizer) {
		mVisualizer = visualizer;
		if (mVisualizer != null) {
			mVisualizer.setCaptureSize(Visualizer.getMaxCaptureRate() / 2);
			levelStep = 128 / (MAX_LEVEL - 1);
			mVisualizer.setDataCaptureListener(this, Visualizer.getMaxCaptureRate() / 2, false, true);
			mVisualizer.setEnabled(true);
		}
	}
	
	public void enableEqualizer() {
		if(mEqualizer!=null && !mEqualizer.getEnabled()) {
			mEqualizer.setEnabled(true);
		}
	}
	
	/**
	 * 
	 * @param audioSession
	 * @param createEqualizer
	 *            true if want to avoid FFT data affected by device volume, to enable Equalizer after Visualizer
	 */
	public void setVisualizer(int audioSession, boolean createEqualizer) {
		setVisualizer(new Visualizer(audioSession));
		if (createEqualizer) {
			mEqualizer = new Equalizer(0, audioSession);
			mEqualizer.setEnabled(false);
		}
	}

	public void releaseVisualizer() {
		initView();
		if (mVisualizer != null) {
			mVisualizer.setEnabled(false);
			mVisualizer.release();
			mVisualizer = null;
		}

		if (mEqualizer != null) {
			mEqualizer.setEnabled(false);
			mEqualizer.release();
			mEqualizer = null;
		}
	}

	@Override
	public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
		byte[] model = new byte[fft.length / 2 + 1];
		if (mDataEn) {
			model[0] = (byte) Math.abs(fft[1]);
			int j = 1;
			for (int i = 2; i < fft.length;) {
				model[j] = (byte) Math.hypot(fft[i], fft[i + 1]);
				i += 2;
				j++;
			}
		} else {
			for (int i = 0; i < CYLINDER_NUM; i++) {
				model[i] = 0;
			}
		}
		for (int i = 0; i < CYLINDER_NUM; i++) {
			byte a = (byte) (Math.abs(model[i + 1]) / levelStep);
			a++;
			final byte b = mData[i];
			if (a > b) {
				mData[i] = a;
			} else {
				if (b > 1) {
					mData[i]--;
				}
			}
		}
		postInvalidate();
	}

	@Override
	public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
		// Do nothing.
	}

	/**
	 * It enables or disables the data processs.
	 * 
	 * @param en
	 *            If this value is true it enables the data process..
	 */
	public void enableDataProcess(boolean en) {
		mDataEn = en;
	}
	
	public void resume() {
		if(mVisualizer!=null){
			mVisualizer.setEnabled(true);
		}
	}
	
	public void pause() {
		if(mVisualizer!=null){
			mVisualizer.setEnabled(false);
		}
	}
	
	private void initView() {
		Arrays.fill(mData, (byte) 0);
		postInvalidate();
	}
}
