package com.actions.image.data;

import java.util.ArrayList;
import java.util.List;

import com.actions.mediacenter.data.MediaObject;
import com.actions.mediacenter.data.MediaSet;
import com.actions.mediacenter.data.Path;
import com.actions.mediacenter.utils.CommonUtil;
import com.actions.mediacenter.widget.SelectedItemShowView;

public class LocalImageSet extends MediaSet {
	private List<LocalImage> mImageList;
	private Path mPath;
	private boolean isSelected = false;
	private int mSelectedItemCount;
	private SelectedItemShowView mSelectedItemShowView;

	public LocalImageSet(Path path) {
		mPath = path;
		mImageList = new ArrayList<LocalImage>();
	}

	@Override
	public MediaObject getItem(int position) {
		if (CommonUtil.isAvaiablePosition(position, mImageList)) {
			return mImageList.get(position);
		}
		return null;
	}

	public int getPosition(LocalImage image) {
		if (mImageList.contains(image)) {
			return mImageList.indexOf(image);
		}
		return -1;
	}

	@Override
	public void addItem(MediaObject item) {
		mImageList.add((LocalImage) item);
	}

	@Override
	public int size() {
		return mImageList.size();
	}

	@Override
	public String getPath() {
		return mPath.getPath();
	}

	@Override
	public String getName() {
		return mPath.getName();
	}

	@Override
	public void clear() {
		mImageList.clear();
	}

	public String getSetImageUri() {
		return "file://" + getItem(0).getMediaPath().getPath();
	}

	public Path getMediaPath() {
		return mPath;
	}

	public void selectClick() {
		isSelected = !isSelected;
	}

	public boolean isSelect() {
		return isSelected;
	}

	public void clearItemSelect() {
		for (LocalImage image : mImageList) {
			image.setSelect(false);
		}
		mSelectedItemCount = 0;
	}

	public void setSelect(boolean select) {
		isSelected = select;
	}

	public void delete() {
		for (LocalImage image : mImageList) {
			image.delete();
		}
	}

	public void videoItemSelectClick(int position) {
		LocalImage image = (LocalImage) getItem(position);
		image.selectClick();
		if (image.isSelect()) {
			mSelectedItemCount++;
		} else {
			mSelectedItemCount--;
		}
		if (mSelectedItemShowView != null) {
			mSelectedItemShowView.update(mSelectedItemCount);
		}
	}

	public int getSelectedSetCount() {
		return mSelectedItemCount;
	}

	public void setSelectedCountShowView(SelectedItemShowView selectedItemShowView) {
		mSelectedItemShowView = selectedItemShowView;
	}
}
