package com.actions.owlplayer.player;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PointF;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.storage.StorageVolume;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actions.mediacenter.R;
import com.actions.mediacenter.app.RollerVolume;
import com.actions.mediacenter.utils.CommonUtil;
import com.actions.owlplayer.app.Config;
import com.actions.owlplayer.app.OWLApplication;
import com.actions.owlplayer.data.LocalVideoSet;
import com.actions.owlplayer.data.MediaItem;
import com.actions.owlplayer.data.VideoSetManager;
import com.actions.owlplayer.util.FeatureFilter;
import com.actions.owlplayer.util.Preferences;
import com.actions.owlplayer.util.Utils;
import com.actions.owlplayer.widget.MoviePlayer;
import com.actions.owlplayer.widget.VerticalSeekBar;

@SuppressLint("NewApi")
public class PlayActivity extends Activity implements View.OnClickListener {
	private static final String TAG = "PlayActivity";
	private static final int BRIGHTNESS_MAX = 255;
	private static final int TOUCH_MODE_NONE = 0;
	private static final int TOUCH_MODE_DRAG = 1;
	private static final int TOUCH_MODE_ZOOM = 2;
	private static final int BRIGHTNESS_PROGRESS_MAX = 100;
	private static final int VOLUME_PROGRESS_MAX = 100;
	private int mScreenWidth = 0;

	// UI widget
	private View mRootView;
	private LinearLayout mSeekLayout;
	private TextView mSeekStartPosition;
	private TextView mSeekDeltaPosition;
	private LinearLayout mAdjustProgressLayout;
	private VerticalSeekBar mAdjustVolumeProgressBar;
	private VerticalSeekBar mAdjustBrightnessProgressBar;
	private TextView mAdjustProgressInfo;
	private ImageButton mPrevButton;
	private ImageButton mNextButton;
	private ImageButton mRepeatButton;
	private ImageButton mInfoButton;
	private TextView mMovieTitle;
	private SeekBar mProgressBar;
	private int[] mRepeatImages = { R.drawable.owl_repeat_one_button, R.drawable.owl_repeat_all_button, R.drawable.owl_repeat_random_button };
	private AlertDialog mAlertDialog = null;

	// touch recognize
	private int mTouchMode = TOUCH_MODE_NONE;
	private float mLastDistance;
	private PointF mPointStart = new PointF();
	private PointF mPointMid = new PointF();
	private PointF mPointCurrent = new PointF();
	private PointF mLastDragPoint = new PointF(-1, -1);
	private float mScale = 1.0f;

	private static final int DRAG_NONE = 0;
	private static final int DRAG_VIDEO_SEEK = 1;
	private static final int DRAG_VOLUME_SET = 2;
	private static final int DRAG_BRIGHTNESS_SET = 3;
	private int mDragMode = DRAG_NONE;
	private int mCurrentVolumeState;
	private int mCurrentBrightnessState;
	private int mIntelligentBrightnessMode = 0;
	private boolean mFirstDrag = true;
	private int mDeltaSeekPosition = 0;

	// service
	private boolean mIntentFromWindow = false;
	// /////////////////////////////
	// new
	private OWLApplication mApplication;
	private Context mContext;
	private MoviePlayer mMoviePlayer;
	private MediaItem mMediaItem;
	private int mLoopMode;
	private String mDisplayTitle;
	private AudioManager mAudioManager;
	private int mMaxVolume;
	private LocalVideoSet mVideoSet;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.v(TAG, "onCreate");

		mContext = this;
		mApplication = (OWLApplication) getApplication();

		Resources res = getResources();
		Configuration config = res.getConfiguration();
		config.fontScale = 1.0f;
		DisplayMetrics displayMetrics = res.getDisplayMetrics();
		res.updateConfiguration(config, displayMetrics);

		mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		mMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater mLayoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		mRootView = mLayoutInflater.inflate(R.layout.player_fullscreen_view, null);
		setContentView(mRootView);

		// But for the performance (and battery), we remove the background here.
		getWindow().setBackgroundDrawable(null);
		// todo

		initLayout();

		if (savedInstanceState == null) {
			initMediaItemFromIntent(getIntent());
		} else {
			Log.v(TAG, "with savedInstanceState");
			mMediaItem = savedInstanceState.getParcelable(Config.INTENT_KEY_MEIDA_ITEM);
		}

		if (mMediaItem != null && mMediaItem.data != null) {
			Log.d(TAG, "playback video:" + mMediaItem.data);
		}
		initMisc();

	}

	private void initMediaItemFromIntent(Intent intent) {
		Log.d(TAG, "initMediaItemFromIntent");
		int videoItemPosition = intent.getIntExtra(Config.INTENT_KEY_VIDEO_ITEM_POSITION, -1);
		int videoSetPosition = intent.getIntExtra(Config.INTENT_KEY_VIDEO_SET_POSITION, -1);
		if (videoSetPosition != -1 && videoItemPosition != -1) {
			mVideoSet = (LocalVideoSet) VideoSetManager.getInstance(this).get(videoSetPosition);
			mMediaItem = (MediaItem) mVideoSet.getItem(videoItemPosition);
		}
		if (mMediaItem == null) {
			mMediaItem = Utils.createMediaItem(mContext, intent.getData());
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				mMediaItem.displayName = bundle.getString("owl-title", mMediaItem.displayName);
				mMediaItem.bookmark = bundle.getInt("owl-position", mMediaItem.bookmark);
			}
		}

	}

	private void initMisc() {
		// FIXME here underlies no orientation changes
		DisplayMetrics dm = getResources().getDisplayMetrics();
		mScreenWidth = dm.widthPixels;

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
		intentFilter.addDataScheme("file");
		registerReceiver(mBroadcastReceiver, intentFilter);

		intentFilter = new IntentFilter();
		intentFilter.addAction("actions.owlplayer.OpenIntelligentBacklight");
		intentFilter.addAction("actions.owlplayer.CloseIntelligentBacklight");
		registerReceiver(mBroadcastReceiver, intentFilter);

	}

	private void getBrightnessFromSettings() {
		mCurrentBrightnessState = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, BRIGHTNESS_MAX) * BRIGHTNESS_PROGRESS_MAX
				/ BRIGHTNESS_MAX;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(Config.INTENT_KEY_MEIDA_ITEM, mMediaItem);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.v(TAG, "onNewIntent");
		// deal last item
		updateItemInfo();

		initMediaItemFromIntent(intent);

		play();
	}

	private void initLayout() {
		mMovieTitle = (TextView) findViewById(R.id.title);
		mMovieTitle.setOnClickListener(this);

		mSeekLayout = (LinearLayout) findViewById(R.id.video_seek_layout);
		mSeekLayout.setVisibility(View.GONE);
		mSeekStartPosition = (TextView) findViewById(R.id.seek_start_position);
		mSeekDeltaPosition = (TextView) findViewById(R.id.seek_delta_position);
		mProgressBar = (SeekBar) findViewById(R.id.progress);

		mAdjustProgressLayout = (LinearLayout) findViewById(R.id.adjust_layout);
		mAdjustProgressLayout.setVisibility(View.GONE);
		mAdjustVolumeProgressBar = (VerticalSeekBar) findViewById(R.id.adjust_volume_seekbar);
		mAdjustVolumeProgressBar.setMaximum(mMaxVolume);
		mAdjustBrightnessProgressBar = (VerticalSeekBar) findViewById(R.id.adjust_brightness_seekbar);
		mAdjustBrightnessProgressBar.setMaximum(VOLUME_PROGRESS_MAX);

		mAdjustProgressInfo = (TextView) findViewById(R.id.adjust_info);

		mInfoButton = (ImageButton) findViewById(R.id.info);
		mPrevButton = (ImageButton) findViewById(R.id.prev);
		mNextButton = (ImageButton) findViewById(R.id.next);
		mRepeatButton = (ImageButton) findViewById(R.id.repeat);
		if (mRepeatButton != null) {
			mLoopMode = Integer.parseInt((String) Preferences.getPreferences(mContext, Preferences.CYCLE_MODE, Integer.toString(Config.LoopMode.ORDINAL)));
			updateLoopMode();
		}

		mInfoButton.setOnClickListener(this);
		mPrevButton.setOnClickListener(this);
		mNextButton.setOnClickListener(this);
		mRepeatButton.setOnClickListener(this);

		mProgressBar.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				switch (keyCode) {
				case KeyEvent.KEYCODE_DPAD_LEFT:
					if (event.getAction() == KeyEvent.ACTION_DOWN) {
						mMoviePlayer.setDeltaPercentWithLimit(-0.01f, false, 10);
					}
					return true;
				case KeyEvent.KEYCODE_DPAD_RIGHT:
					if (event.getAction() == KeyEvent.ACTION_DOWN) {
						mMoviePlayer.setDeltaPercentWithLimit(0.01f, false, 10);
					}
					return true;
				default:
					break;
				}
				return false;
			}
		});
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		savePlayParams();
		release();
	}

	private int getPosition() {
		if (mMoviePlayer != null) {
			return mMoviePlayer.getPosition();
		} else {
			return 0;
		}
	}

	private void savePlayParams() {
		if (mMoviePlayer != null) {
			// FIXME while onSaveedInstanceState, namely been killed under
			// screen off
			// Activity comes to onCreate->onResume->onPause
			// onResume have no enough time to play before onPause is called
			if (getPosition() > 0) {
				mMediaItem.bookmark = getPosition();
				if(mVideoSet!=null) {
					((MediaItem) mVideoSet.getItem(mVideoSet.findCurrentPosition(mMediaItem))).bookmark = mMediaItem.bookmark;
				}
			}

			mMediaItem.audioTrack = mMoviePlayer.getAudioTrack();
			mMediaItem.subTrack = mMoviePlayer.getSubTrack();
			mMediaItem.volume = mMoviePlayer.getVolume();
		}
	}

	private void savePlayParamsByResetPostion() {
		if (mMoviePlayer != null) {
			// FIXME while onSaveedInstanceState, namely been killed under
			// screen off
			// Activity comes to onCreate->onResume->onPause
			// onResume have no enough time to play before onPause is called

			mMediaItem.bookmark = 0;
			((MediaItem) mVideoSet.getItem(mVideoSet.findCurrentPosition(mMediaItem))).bookmark = mMediaItem.bookmark;

			mMediaItem.audioTrack = mMoviePlayer.getAudioTrack();
			mMediaItem.subTrack = mMoviePlayer.getSubTrack();
			mMediaItem.volume = mMoviePlayer.getVolume();
		}
	}

	private void updateItemInfo() {
		if (mMediaItem != null) {
			Utils.setBookmark(mContext, mMediaItem, mMediaItem.bookmark);
			Utils.removeNewTag(mContext, mMediaItem);
		}
	}

	@Override
	protected void onResume() {
		Log.v(TAG, "onResume");
		super.onResume();
		if (mMediaItem == null) {
			Log.v(TAG, "has not mediaitem to playback");
			finish();
			return;
		}

		play();
		mCurrentVolumeState = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		initBrightness();
	}

	private void initBrightness() {
		if (!FeatureFilter.isSupport(FeatureFilter.BRIGHTNESS)) {
			return;
		}

		getBrightnessFromSettings();
		mIntelligentBrightnessMode = Settings.System.getInt(getContentResolver(), "Intelligent_brightness_mode", 0);
		if (mIntelligentBrightnessMode == 0) {
			setBrightness();
		} else {
			/*
			 * in intelligent brightness mode, current state have to be the same as the setting other than last state at the same time, restore the window
			 * brightness to default -1
			 */
			setWindowBrightness(-1.0f);
		}
	}

	private void play() {
		Log.v(TAG, "play");

		if (mAlertDialog != null) {
			mAlertDialog.dismiss();
			mAlertDialog = null;
		}

		if (mMoviePlayer == null) {
			mMoviePlayer = new MoviePlayer(this, mRootView);
			mMoviePlayer.setListener(mMoviePlayerListener);
		}

		Intent intent = new Intent();
		intent.setData(mMediaItem.getUri());
		intent.putExtra(MoviePlayer.KEY_VIDEO_POSITION, mMediaItem.bookmark);
		intent.putExtra(MoviePlayer.KEY_AUDIO_TRACK, mMediaItem.audioTrack);
		intent.putExtra(MoviePlayer.KEY_SUB_TRACK, mMediaItem.subTrack);
		intent.putExtra(MoviePlayer.KEY_SET_VOLUME, mMediaItem.volume);
		intent.putExtra(MoviePlayer.KEY_FULL_NAME, mMediaItem.data);
		if (mIntentFromWindow) {
			intent.putExtra(MoviePlayer.KEY_SHOW_MESSAGE, false);
		}
		intent.putExtra(MoviePlayer.KEY_WIDTH, mMediaItem.width);
		intent.putExtra(MoviePlayer.KEY_HEIGHT, mMediaItem.height);
		mMoviePlayer.setIntent(intent);

		updateTitle();

		mIntentFromWindow = false;
	}

	private void updateTitle() {
		if (mMediaItem.displayName != null) {
			mDisplayTitle = mMediaItem.displayName;
		} else {
			mDisplayTitle = "";
		}

		mMovieTitle.setText(mDisplayTitle);
	}

	@Override
	public void onDestroy() {
		Log.v(TAG, "onDestroy");
		super.onDestroy();
		updateItemInfo();
		release();

		unregisterReceiver(mBroadcastReceiver);

	}

	private void release() {
		if (mMoviePlayer != null) {
			mMoviePlayer.onDestroy();
			mMoviePlayer = null;
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		if (super.dispatchTouchEvent(event)) {
			return true;
		}

		if (mMoviePlayer != null) {
			if (mMoviePlayer.onExtraTouchEvent(event)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return event(event);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.info:
			mMoviePlayer.pause();
			showVideoDetailsDialog(mMediaItem);
			break;

		case R.id.repeat:
			setLoopMode();
			break;

		case R.id.prev:
			savePlayParams();
			playNewMediaItem(getMediaItem(false));
			break;

		case R.id.next:
			savePlayParams();
			playNewMediaItem(getMediaItem(true));
			break;

		case R.id.title:
			finish();
			break;

		default:
			Log.v(TAG, "click default");
			break;
		}

		if (mMoviePlayer != null) {
			mMoviePlayer.show();
		}
	}

	/**
	 * @param forward
	 *            true for next, false:
	 * @return
	 */
	private MediaItem getMediaItem(boolean forward) {
		if (mVideoSet != null) {
			return mVideoSet.navigate(mMediaItem, forward);
		}
		return null;
	}

	private void setLoopMode() {
		mLoopMode = (++mLoopMode > Config.LoopMode.END) ? Config.LoopMode.START : mLoopMode;
		updateLoopMode();
	}

	private MoviePlayer.Listener mMoviePlayerListener = new MoviePlayer.Listener() {

		@Override
		public void onCompletion() {
			mMediaItem.bookmark = 0;
			playNewMediaItem(getMediaItem(true));
		}

		@Override
		public boolean onError(int what, int extra) {
			finish();
			return true;
		}

		@Override
		public boolean onTouch(MotionEvent event) {
			return event(event);
		}
	};

	private float mDeltaYAddUp = 0;

	private void holdDragWindow(PointF point, boolean stop) {
		if (mLastDragPoint.x < 0 || mLastDragPoint.y < 0 || stop) {
			if (mDragMode == DRAG_VIDEO_SEEK && mMoviePlayer != null) {
				mMoviePlayer.setDeltaPercent(0, true);
				mDeltaSeekPosition = 0;
			}

			mAdjustProgressLayout.setVisibility(View.GONE);
			mSeekLayout.setVisibility(View.GONE);
			mDragMode = DRAG_NONE;
			if (stop) {
				mLastDragPoint.x = -1;
				mLastDragPoint.y = -1;
			} else {
				mLastDragPoint.x = point.x;
				mLastDragPoint.y = point.y;
			}

			mFirstDrag = true;
			return;
		}

		float deltaX = point.x - mLastDragPoint.x;
		float deltaY = point.y - mLastDragPoint.y;

		switch (mDragMode) {
		case DRAG_NONE:
			if (deltaX * deltaX + deltaY * deltaY < 4) {
				break;
			}
			if (Math.abs(deltaX / deltaY) > 1.732F) {
				mDragMode = DRAG_VIDEO_SEEK;
				mSeekLayout.setVisibility(View.VISIBLE);
				mSeekStartPosition.setText(Utils.convertMsToHHMMSS(getPosition()));

			} else if (Math.abs(deltaX / deltaY) < 0.577F) {
				if (point.x < mScreenWidth / 2) {
					mDragMode = DRAG_VOLUME_SET;
					mCurrentVolumeState = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
					mAdjustProgressLayout.setVisibility(View.VISIBLE);
					mAdjustProgressLayout.setBackgroundResource(R.drawable.ic_player_volume_adjust_bg);
					mAdjustVolumeProgressBar.setVisibility(View.VISIBLE);
					mAdjustVolumeProgressBar.setProgressAndThumb(mCurrentVolumeState);
					mAdjustProgressInfo.setText(Integer.toString(mCurrentVolumeState));
					mAdjustBrightnessProgressBar.setVisibility(View.GONE);

				} else {
					if (!FeatureFilter.isSupport(FeatureFilter.BRIGHTNESS)) {
						return;
					}

					mDragMode = DRAG_BRIGHTNESS_SET;
					mAdjustProgressLayout.setVisibility(View.VISIBLE);
					mAdjustProgressLayout.setBackgroundResource(R.drawable.ic_player_brightness_adjust_bg);
					mAdjustVolumeProgressBar.setVisibility(View.GONE);
					mAdjustBrightnessProgressBar.setVisibility(View.VISIBLE);
					mAdjustBrightnessProgressBar.setProgressAndThumb(mCurrentBrightnessState);
					mAdjustProgressInfo.setText(mCurrentBrightnessState + "%");
				}
			}
			return;

		case DRAG_VIDEO_SEEK:
			if (Math.abs(deltaX / deltaY) < 1F || (deltaX > -1.0F && deltaX < 1.0F)) {
				break;
			}

			float percent = deltaX / mScreenWidth;
			if (mMoviePlayer != null) {
				mDeltaSeekPosition += mMoviePlayer.setDeltaPercent(percent, false);
			}

			String tempstr;
			if (mDeltaSeekPosition >= 0) {
				tempstr = "+";
			} else {
				tempstr = "-";
			}
			tempstr = tempstr + Utils.convertMsToHHMMSS(Math.abs(mDeltaSeekPosition));
			mSeekDeltaPosition.setText(tempstr);
			break;

		case DRAG_VOLUME_SET:
			if (deltaY == 0 || Math.abs(deltaX / deltaY) > 1F) {
				break;
			}
			deltaY += mDeltaYAddUp;
			if (Math.abs(deltaY) > 10) {
				mCurrentVolumeState -= (int) deltaY / 10;
				mDeltaYAddUp = deltaY % 10;
			} else {
				mDeltaYAddUp = deltaY;
				break;
			}

			adjustVolume();
			break;

		case DRAG_BRIGHTNESS_SET:
			if (deltaY == 0 || Math.abs(deltaX / deltaY) > 1F) {
				break;
			}

			if (mIntelligentBrightnessMode > 0) {
				if (mFirstDrag) {
					Toast.makeText(mContext, R.string.message_intelligent_brightness, Toast.LENGTH_SHORT).show();
					mFirstDrag = false;
				}
			} else {
				if (Math.abs(deltaY) > 4) {
					mCurrentBrightnessState -= deltaY / 4;
				} else {
					mCurrentBrightnessState -= ((deltaY > 0) ? 1 : -1);
				}

				if (mCurrentBrightnessState > BRIGHTNESS_PROGRESS_MAX) {
					mCurrentBrightnessState = BRIGHTNESS_PROGRESS_MAX;
				} else if (mCurrentBrightnessState < 0) {
					mCurrentBrightnessState = 0;
				}

				setBrightness();
			}

			mAdjustProgressInfo.setText(mCurrentBrightnessState + "%");
			mAdjustBrightnessProgressBar.setProgressAndThumb(mCurrentBrightnessState);
			break;

		default:
			break;
		}

		mLastDragPoint.x = point.x;
		mLastDragPoint.y = point.y;
	}

	private void setBrightness() {
		float brightness = (float) mCurrentBrightnessState / BRIGHTNESS_PROGRESS_MAX;
		if (brightness > 0 && brightness <= 1) {
			setWindowBrightness(brightness);
		}
	}

	private void setWindowBrightness(float brightness) {
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.screenBrightness = brightness;
		getWindow().setAttributes(lp);
	}

	private boolean event(MotionEvent event) {
		mPointCurrent.set(event.getRawX(), event.getRawY());
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			mPointStart.set(event.getX(), event.getY());
			mTouchMode = TOUCH_MODE_DRAG;
			mDeltaYAddUp = 0;
			holdDragWindow(null, true);
			break;

		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			mTouchMode = TOUCH_MODE_NONE;
			mDeltaYAddUp = 0;
			holdDragWindow(null, true);
			break;

		case MotionEvent.ACTION_POINTER_DOWN:
			mLastDistance = spacing(event);
			if (mLastDistance > 10f) {
				midPoint(mPointMid, event);
				mTouchMode = TOUCH_MODE_ZOOM;
			}
			break;

		case MotionEvent.ACTION_MOVE:
			if (mTouchMode == TOUCH_MODE_DRAG) {
				holdDragWindow(mPointCurrent, false);

			} else if (mTouchMode == TOUCH_MODE_ZOOM) {
				midPoint(mPointMid, event);
				float newDist = spacing(event);
				mScale = newDist - mLastDistance;
				if (Math.abs(mScale) > 3)
					mLastDistance = newDist;
			}
			break;
		default:
			break;
		}
		return false;
	}

	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);
	}

	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);

	}

	private void updateLoopMode() {
		Preferences.setPreferences(mContext, Preferences.CYCLE_MODE, Integer.toString(mLoopMode));
		mRepeatButton.setImageResource(mRepeatImages[mLoopMode]);
	}

	private void playNewMediaItem(MediaItem item) {
		Log.v(TAG, "playNewMediaItem");
		updateItemInfo();
		if (item == null) {
			Log.w(TAG, "change new mediaitem failed.");
			finish();
			return;
		}

		if (item.data == null || !CommonUtil.isFileExists(item.data)) {
			Log.w(TAG, "file do not exits");
			finish();
			return;
		}

		item.volume = mMediaItem.volume;
		mMediaItem = item;

		play();
	}

	private void adjustVolume() {
		if (mCurrentVolumeState > mMaxVolume) {
			mCurrentVolumeState = mMaxVolume;
		} else if (mCurrentVolumeState < 0) {
			mCurrentVolumeState = 0;
		}

		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentVolumeState, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
		if (!mAdjustProgressLayout.isShown() || mAdjustBrightnessProgressBar.isShown()) {
			mAdjustProgressLayout.setVisibility(View.VISIBLE);
			mAdjustProgressLayout.setBackgroundResource(R.drawable.ic_player_volume_adjust_bg);
			mAdjustBrightnessProgressBar.setVisibility(View.GONE);
			mAdjustVolumeProgressBar.setVisibility(View.VISIBLE);
		}
		/**
		 * for safe volume limitation, so before show the real volume, read it back
		 */
		int indexReal = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		if (mCurrentVolumeState > indexReal) {
			mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentVolumeState, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
					| AudioManager.FLAG_SHOW_UI);
			mCurrentVolumeState = indexReal;
		}

		mAdjustVolumeProgressBar.setProgressAndThumb(mCurrentVolumeState);
		mAdjustProgressInfo.setText(Integer.toString(mCurrentVolumeState));

	}

	private Handler mHandler = new Handler();

	private Runnable mHideProgressLayout = new Runnable() {
		@Override
		public void run() {
			mAdjustProgressLayout.setVisibility(View.GONE);
		}
	};

	private void adjustVolumeByKey(int direction) {
		int volume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		if (direction == AudioManager.ADJUST_RAISE) {
			volume++;
		} else {
			volume--;
		}

		mCurrentVolumeState = volume;

		mHandler.removeCallbacks(mHideProgressLayout);
		mHandler.postDelayed(mHideProgressLayout, 3000);
		adjustVolume();
	}
	
	private void adjustVolumeByRoller(int volume) {
		mCurrentVolumeState = volume;

		mHandler.removeCallbacks(mHideProgressLayout);
		mHandler.postDelayed(mHideProgressLayout, 3000);
		adjustVolume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			unmuteVolume();
			adjustVolumeByKey(AudioManager.ADJUST_RAISE);
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			unmuteVolume();
			adjustVolumeByKey(AudioManager.ADJUST_LOWER);
			return true;
		case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
		case KeyEvent.KEYCODE_MEDIA_PAUSE:
		case KeyEvent.KEYCODE_MEDIA_PLAY:
			mMoviePlayer.togglePlay();
			return true;
		case KeyEvent.KEYCODE_MENU:
		case KeyEvent.KEYCODE_ENTER:
		case KeyEvent.KEYCODE_DPAD_CENTER:
			mMoviePlayer.togglePlay();
			return true;

		case KeyEvent.KEYCODE_DPAD_UP:
			savePlayParams();
			playNewMediaItem(getMediaItem(false));
			return true;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			savePlayParams();
			playNewMediaItem(getMediaItem(true));
			return true;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			event.startTracking();
			// prevent fast forward too fast
			if (mKeyLongPress && event.getRepeatCount() % 2 == 0) {
				fastForwardAndBackward(false);
			}
			return true;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			event.startTracking();
			if (mKeyLongPress && event.getRepeatCount() % 2 == 0 && !fastForwardAndBackward(true)) {
				savePlayParamsByResetPostion();
				playNewMediaItem(getMediaItem(true));
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_MUTE:
			toggleVolumeMute();
			showVolumeSeekbar();
			return true;
		default:
			int index = RollerVolume.getRollerIndex(keyCode, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
			if (index == -1) {
				break;
			}

			Log.i(TAG, "index:" + index);
			adjustVolumeByRoller(index);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	private void toggleVolumeMute() {
		if (isVolumeMute()) {
			mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		} else {
			mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
		}
	}

	private boolean mKeyLongPress = false;

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		mKeyLongPress = true;
		return super.onKeyLongPress(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			if (!mKeyLongPress) {
				savePlayParams();
				playNewMediaItem(getMediaItem(false));
			}
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			if (!mKeyLongPress) {
				savePlayParams();
				playNewMediaItem(getMediaItem(true));
			}
			break;
		case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
			savePlayParams();
			playNewMediaItem(getMediaItem(false));
			break;
		case KeyEvent.KEYCODE_MEDIA_NEXT:
			savePlayParams();
			playNewMediaItem(getMediaItem(true));
			break;
		default:
			break;
		}
		mKeyLongPress = false;
		return super.onKeyUp(keyCode, event);
	}

	private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.i(TAG, "BroadcastReceiver actions:" + action);
			if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
				/*
				 * finish in order to avoid operation back from mount/umount screen to PlayActivity
				 */
				StorageVolume volume = (StorageVolume) intent.getExtras().get("storage_volume");
				if (mMediaItem.data != null && mMediaItem.data.contains(volume.getPath())) {
					PlayActivity.this.finish();
				}
			} else if (action.equals("actions.owlplayer.OpenIntelligentBacklight") || action.equals("actions.owlplayer.CloseIntelligentBacklight")) {
				initBrightness();
			}
		}
	};

	private void showVideoDetailsDialog(final MediaItem item) {
		ScrollView sv = (ScrollView) LayoutInflater.from(this).inflate(R.layout.media_details, null);
		TextView tv = (TextView) sv.findViewById(R.id.mediaDetailsText);
		tv.setText(item.getDetails());
		new AlertDialog.Builder(this, R.style.HoloLargeDialog).setTitle(R.string.media_details).setView(sv)
				.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mMoviePlayer.togglePlay();
						dialog.dismiss();
					}
				}).setOnCancelListener(new OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						mMoviePlayer.togglePlay();
						dialog.dismiss();
					}
				}).show();
	}

	private boolean fastForwardAndBackward(boolean forward) {
		return mMoviePlayer.fastForwardAndBackward(forward);
	}

	private void showVolumeSeekbar() {
		mCurrentVolumeState = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		if (!mAdjustProgressLayout.isShown() || mAdjustBrightnessProgressBar.isShown()) {
			mAdjustProgressLayout.setVisibility(View.VISIBLE);
			mAdjustProgressLayout.setBackgroundResource(R.drawable.ic_player_volume_adjust_bg);
			mAdjustBrightnessProgressBar.setVisibility(View.GONE);
			mAdjustVolumeProgressBar.setVisibility(View.VISIBLE);
		}
		mAdjustVolumeProgressBar.setProgressAndThumb(mCurrentVolumeState);
		mAdjustProgressInfo.setText(Integer.toString(mCurrentVolumeState));

		mHandler.removeCallbacks(mHideProgressLayout);
		mHandler.postDelayed(mHideProgressLayout, 3000);
	}

	private boolean isVolumeMute() {
		return mAudioManager.isStreamMute(AudioManager.STREAM_MUSIC);
	}

	private void unmuteVolume() {
		if (isVolumeMute()) {
			mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		}
	}
}
