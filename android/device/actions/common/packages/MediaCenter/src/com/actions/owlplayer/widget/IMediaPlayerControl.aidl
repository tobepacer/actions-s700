package com.actions.owlplayer.widget;

interface IMediaPlayerControl {
	void    start();
	void    pause();
	int     getDuration();
	int     getCurrentPosition();
	void    seekTo(int pos);
	boolean isPlaying();
	int     getBufferPercentage();
}