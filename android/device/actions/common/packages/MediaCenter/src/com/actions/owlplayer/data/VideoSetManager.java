package com.actions.owlplayer.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.util.Log;

import com.actions.mediacenter.data.MediaSet;
import com.actions.mediacenter.data.Path;
import com.actions.mediacenter.widget.SelectedItemShowView;
import com.actions.owlplayer.app.OWLApplication;

public class VideoSetManager {
	private static final String TAG = VideoSetManager.class.getSimpleName();
	private MediaItemCache mMediaItemCache;
	private Map<Path, MediaSet> mMediaSets = new HashMap<Path, MediaSet>();
	private ArrayList<Path> mSetPathList = new ArrayList<Path>();
	private static Object mLock = new Object();
	private static VideoSetManager sVideoSetManager;
	private Activity mActivity;
	private int mSelectedSetCount = 0;
	private SelectedItemShowView mSelectedItemShowView;

	private VideoSetManager(Activity activity) {
		mActivity = activity;
		mMediaItemCache = ((OWLApplication) activity.getApplication()).getMediaItemCache();
		update();
	}

	public static VideoSetManager getInstance(Activity activity) {
		if (sVideoSetManager == null) {
			synchronized (mLock) {
				if (sVideoSetManager == null) {
					sVideoSetManager = new VideoSetManager(activity);
				}
			}
		}
		return sVideoSetManager;
	}

	public void update() {
		Log.d(TAG, "update");
		for (MediaSet mediaSet : mMediaSets.values()) {
			mediaSet.clear();
		}
		Log.d(TAG, "mMediaItemCache.size():" + mMediaItemCache.size());
		for (int i = 0; i < mMediaItemCache.size(); i++) {
			LocalVideo item = (LocalVideo) mMediaItemCache.get(i);
			if (item == null || item.getPath() == null) {
				Log.d(TAG, "media item " + i + " is null!!!");
				continue;
			}
			Path path = new Path(item.getMediaPath().getParent());
			if (mMediaSets.containsKey(path)) {
				mMediaSets.get(path).addItem(item);
			} else {
				MediaSet mediaSet = new LocalVideoSet(path, mActivity.getApplication());
				mediaSet.addItem(item);
				mMediaSets.put(path, mediaSet);
			}
		}
		Iterator<Entry<Path, MediaSet>> iterator = mMediaSets.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Path, MediaSet> entry = iterator.next();
			if (entry.getValue().size() == 0) {
				iterator.remove();
			}
		}
		mSetPathList.clear();
		mSetPathList.addAll(mMediaSets.keySet());
	}

	public int size() {
		return mMediaSets.size();
	}

	public MediaSet get(int position) {
		if (position >= 0 && position < mSetPathList.size()) {
			return mMediaSets.get(mSetPathList.get(position));
		}
		return null;
	}

	public void clearSetSelect() {
		for (MediaSet set : mMediaSets.values()) {
			((LocalVideoSet) set).setSelect(false);
		}
		mSelectedSetCount = 0;
	}

	public void videoSetSelectClick(int position) {
		LocalVideoSet videoSet = (LocalVideoSet) get(position);
		videoSet.selectClick();
		if (videoSet.isSelect()) {
			mSelectedSetCount++;
		} else {
			mSelectedSetCount--;
		}
		if (mSelectedItemShowView != null) {
			mSelectedItemShowView.update(mSelectedSetCount);
		}
	}

	public int getSelectedSetCount() {
		return mSelectedSetCount;
	}

	public void setSelectedCountShowView(SelectedItemShowView selectedItemShowView) {
		mSelectedItemShowView = selectedItemShowView;
	}
}
