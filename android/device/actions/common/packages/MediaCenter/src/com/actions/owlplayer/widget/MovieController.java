package com.actions.owlplayer.widget;

import java.util.Formatter;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.owlplayer.app.OWLApplication;
import com.actions.owlplayer.player.PlayActivity;

/**
 * A view containing controls for a MediaPlayer. Typically contains the buttons like "Play/Pause", "Prev/Next", "Subtitle/Audio Track", "Mute/unMute",
 * "Display Router" and a progress slider. It takes care of synchronizing the controls with the state of the MediaPlayer.
 * <p>
 * The way to use this class is to instantiate it programatically. If host does not specify an "controller" id in the xml, we will use default layout
 * movie_controller, RelativeLayout is better for parent.
 */
public class MovieController extends FrameLayout implements LayoutController {
	private static final String TAG = "MovieController";

	// timeout to hide the controller layout
	private static final int sDefaultTimeout = 3000;
	private static final long PROGRESS_MAX = 1000L;
	private static final long PROGRESS_APPROXIMATE_MAX = (PROGRESS_MAX * 99) / 100;
	// msg enums
	private static final int FADE_OUT = 1;
	private static final int SHOW_PROGRESS = 2;

	private static final float FAST_FORWARD_BACKWARD_INTERVAL = (float) 0.05;
	private static final int FAST_FORWARD_BACKWARD_MIN_TIME = 10000; // 10s

	private View mRoot;
	private View mControllerLayout;
	private View mTopLayout;
	private View mMainLayout;
	private Context mContext;

	private IMediaPlayerControl mControl = null;
	StringBuilder mFormatBuilder;
	Formatter mFormatter;

	// below are controller UI elements
	private ImageButton mPauseButton;
	private TextView mEndTime;
	private TextView mCurrentTime;
	private SeekBar mProgress;
	private boolean mKeepOnShow = false;
	private boolean mRemoveBackgroundEnable = false;
	// layout status
	private boolean mShowing = true;
	// seekbar status
	private boolean mDragging;
	private boolean mWindowMode = true;
	private int mDisplayScreen;
	private int mPrePosition = -1;
	private int mPreDuration = -1;
	private Activity mActivity;
	private TextView mSubLayer;

	/*
	 * construct the controller
	 * 
	 * @param parent The root view for the controller to attach. use as ViewGroup to inflate in order to
	 */
	public MovieController(Context context, View rootView) {
		super(context);
		Log.v(TAG, "create:" + this);
		mContext = context;
		mRoot = rootView;
		if (context instanceof PlayActivity) {
			mActivity = (Activity) context;
			mSubLayer = (TextView) mRoot.findViewById(R.id.subtitle_layer);
		}
		mControllerLayout = mRoot.findViewById(R.id.controller);

		mDisplayScreen = ((OWLApplication) mContext.getApplicationContext()).getDisplayScreen();
		mFormatBuilder = new StringBuilder();
		mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());

		initControllerView(mControllerLayout);

		mRoot.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
			@Override
			public void onSystemUiVisibilityChange(int visibility) {
				if (mWindowMode) {
					return;
				}

				if (visibility == 0) {
					show();
				}
			}
		});
	}

	private void showSystemUi(boolean visibility) {
		if (mWindowMode) {
			return;
		}

		int flag;
		flag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
		if (!visibility) {
			flag |= View.SYSTEM_UI_FLAG_LOW_PROFILE | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
		}

		mRoot.setSystemUiVisibility(flag);
	}

	/*
	 * apply for top and main in order to avoid touch in invalid area by mistake
	 */
	private View.OnTouchListener mControllerListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			show();
			return true;
		}
	};

	private void initControllerView(View v) {
		mPauseButton = (ImageButton) v.findViewById(R.id.pause);
		if (mPauseButton != null) {
			mPauseButton.requestFocus();
			mPauseButton.setOnClickListener(mPauseListener);
		}

		mCurrentTime = (TextView) v.findViewById(R.id.time_current);

		mProgress = (SeekBar) v.findViewById(R.id.progress);
		if (mProgress != null) {
			if (mProgress instanceof SeekBar) {
				SeekBar seeker = (SeekBar) mProgress;
				seeker.setOnSeekBarChangeListener(mSeekListener);
			}
			mProgress.setMax(1000);
		}

		mTopLayout = v.findViewById(R.id.top_control_layout);
		if (mTopLayout != null) {
			mTopLayout.setOnTouchListener(mControllerListener);
		}

		mMainLayout = v.findViewById(R.id.main_control_layout);
		if (mMainLayout != null) {
			mMainLayout.setOnTouchListener(mControllerListener);
		}

		mEndTime = (TextView) v.findViewById(R.id.time_end);
	}

	public void updatePausePlay() {
		if (mPauseButton == null)
			return;

		ImageButtonWithState btn = (ImageButtonWithState) mPauseButton;
		if (mControlWrapper.isPlaying()) {
			btn.setState(ImageButtonWithState.STATE_PAUSE);
		} else {
			btn.setState(ImageButtonWithState.STATE_PLAY);
		}
	}

	public boolean isPlaying() {
		if (mControlWrapper != null) {
			return mControlWrapper.isPlaying();
		}
		return false;
	}

	private void doPauseResume() {
		if (mControlWrapper.isPlaying()) {
			mControlWrapper.pause();
		} else {
			mControlWrapper.start();
		}

		updatePausePlay();
	}

	private OnClickListener mPauseListener = new OnClickListener() {
		public void onClick(View v) {
			togglePlay();
		}
	};

	public void togglePlay() {
		doPauseResume();
		show();
	}

	public void setMediaPlayer(IMediaPlayerControl control) {
		mControl = control;
	}

	private String stringForTime(int timeMs) {
		int totalSeconds = timeMs / 1000;

		int seconds = totalSeconds % 60;
		int minutes = (totalSeconds / 60) % 60;
		int hours = totalSeconds / 3600;

		mFormatBuilder.setLength(0);
		return mFormatter.format("%02d:%02d:%02d", hours, minutes, seconds).toString();
	}

	private int setProgress() {
		if (mControl == null || mDragging) {
			return 0;
		}

		int position = mControlWrapper.getCurrentPosition();
		int duration = mControlWrapper.getDuration();
		if (mPrePosition == position && mPreDuration == duration) {
			return position;
		}
		mPrePosition = position;
		mPreDuration = duration;
		if (mProgress != null) {
			if (duration > 0) {
				// use long to avoid overflow
				long pos = PROGRESS_MAX * position / duration;
				if (pos >= PROGRESS_APPROXIMATE_MAX) {
					pos = PROGRESS_MAX;
				}

				mProgress.setProgress((int) pos);
			}
			int percent = mControlWrapper.getBufferPercentage();
			mProgress.setSecondaryProgress(percent * 10);
		}

		if (mEndTime != null)
			mEndTime.setText(stringForTime(duration));
		if (mCurrentTime != null)
			mCurrentTime.setText(stringForTime(position));

		return position;
	}

	private final Runnable mRemoveBackground = new Runnable() {
		@Override
		public void run() {
			mRoot.setBackground(null);
		}
	};

	//
	public void show() {
		recoveryBackground();
		show(sDefaultTimeout);
	}

	private void fade(int timeout) {
		Message msg = mHandler.obtainMessage(FADE_OUT);
		// in pause state, always keep the controller visible
		mHandler.removeMessages(FADE_OUT);
		if (timeout != 0 && mControlWrapper.isPlaying()) {
			mHandler.sendMessageDelayed(msg, timeout);
		}
	}

	/**
	 * Show the controller on screen. It will go away automatically after 'timeout' milliseconds of inactivity.
	 * 
	 * @param timeout
	 *            The timeout in milliseconds. Use 0 to show the controller until hide() is called.
	 */
	public void show(int timeout) {
		if (isDestroyed()) {
			return;
		}

		if (!mShowing) {
			setProgress();
			if (mPauseButton != null) {
				mPauseButton.requestFocus();
			}

			showSystemUi(true);

			if (mTopLayout != null) {
				mTopLayout.setVisibility(View.VISIBLE);
			}

			if (mMainLayout != null) {
				mMainLayout.setVisibility(View.VISIBLE);
			}

			if (mHandler != null) {
				mHandler.removeCallbacks(mRemoveBackground);
			}
			mRoot.setBackgroundColor(Color.BLACK);

			mShowing = true;
		}

		updatePausePlay();

		// cause the progress bar to be updated even if mShowing
		// was already true. This happens, for example, if we're
		// paused with the progress bar showing the user hits play.
		if (mHandler != null) {
			mHandler.sendEmptyMessage(SHOW_PROGRESS);
		}

		fade(timeout);
	}

	public boolean isShowing() {
		return mShowing;
	}

	public void keepOnShow(boolean show) {
		mKeepOnShow = show;
	}

	/**
	 * Remove the controller from the screen.
	 */
	public void hide() {
		if (mKeepOnShow || isDestroyed()) {
			return;
		}

		if (mShowing) {

			if (mTopLayout != null) {
				mTopLayout.setVisibility(View.GONE);
			}

			if (mMainLayout != null) {
				mMainLayout.setVisibility(View.GONE);
			}

			if (mHandler != null) {
				mHandler.removeMessages(SHOW_PROGRESS);
				mHandler.removeCallbacks(mRemoveBackground);
			}
			// Wait for the slide out animation, one second should be enough
			// mHandler.postDelayed(mRemoveBackground, 1000);

			mShowing = false;
		}

		showSystemUi(false);
		removeBackground();
	}

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			int pos;
			switch (msg.what) {
			case FADE_OUT:
				if (mDisplayScreen != OWLApplication.HDMI_SCREEN) {
					hide();
				}
				break;
			case SHOW_PROGRESS:
				pos = setProgress();
				if (!mDragging && mShowing) {
					msg = obtainMessage(SHOW_PROGRESS);
					sendMessageDelayed(msg, 1000 - (pos % 1000));
				}
				break;
			}
		}
	};

	// There are two scenarios that can trigger the seekbar listener to trigger:
	//
	// The first is the user using the touchpad to adjust the posititon of the
	// seekbar's thumb. In this case onStartTrackingTouch is called followed by
	// a number of onProgressChanged notifications, concluded by
	// onStopTrackingTouch.
	// We're setting the field "mDragging" to true for the duration of the
	// dragging
	// session to avoid jumps in the position in case of ongoing playback.
	//
	// The second scenario involves the user operating the scroll ball, in this
	// case there WON'T BE onStartTrackingTouch/onStopTrackingTouch
	// notifications,
	// we will simply apply the updated position without suspending regular
	// updates.
	private OnSeekBarChangeListener mSeekListener = new OnSeekBarChangeListener() {
		public void onStartTrackingTouch(SeekBar bar) {
			show(3600000);

			mDragging = true;

			// By removing these pending progress messages we make sure
			// that a) we won't update the progress while the user adjusts
			// the seekbar and b) once the user is done dragging the thumb
			// we will post one of these messages to the queue again and
			// this ensures that there will be exactly one message queued up.
			mHandler.removeMessages(SHOW_PROGRESS);
		}

		public void onProgressChanged(SeekBar bar, int progress, boolean fromuser) {
			if (!fromuser) {
				// We're not interested in programmatically generated changes to
				// the progress bar's position.
				return;
			}

			long duration = mControlWrapper.getDuration();
			long newposition = (duration * progress) / PROGRESS_MAX;
			mControlWrapper.seekTo((int) newposition);
			if (mCurrentTime != null)
				mCurrentTime.setText(stringForTime((int) newposition));
		}

		public void onStopTrackingTouch(SeekBar bar) {
			mDragging = false;
			setProgress();
			updatePausePlay();
			show();

			// Ensure that progress is properly updated in the future,
			// the call to show() does not guarantee this because it is a
			// no-op if we are already showing.
			mHandler.sendEmptyMessage(SHOW_PROGRESS);
		}
	};

	private ControlWrapper mControlWrapper = new ControlWrapper();

	// wrapper the Exception Catch
	private class ControlWrapper {
		public void start() {
			try {
				if (mControl != null) {
					mControl.start();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}

		public void pause() {
			try {
				if (mControl != null) {
					mControl.pause();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		public int getDuration() {
			int duration = 0;
			try {
				if (mControl != null) {
					duration = mControl.getDuration();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			return duration;
		}

		public int getCurrentPosition() {
			int postion = 0;
			try {
				if (mControl != null) {
					postion = mControl.getCurrentPosition();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			return postion;
		}

		public void seekTo(int msec) {
			try {
				if (mControl != null) {
					mControl.seekTo(msec);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		public boolean isPlaying() {
			boolean status = false;
			try {
				if (mControl != null) {
					status = mControl.isPlaying();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			return status;
		}

		public int getBufferPercentage() {
			int percentage = 0;
			try {
				if (mControl != null) {
					percentage = mControl.getBufferPercentage();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			return percentage;
		}
	}

	public void pauseButtonHighLight() {
		if (mPauseButton != null) {
			mPauseButton.setPressed(true);
		}
	}

	public void onDestroy() {
		Log.v(TAG, "onDestroy:" + this);

		if (mTopLayout != null) {
			mTopLayout.setOnTouchListener(null);
		}

		if (mMainLayout != null) {
			mMainLayout.setOnTouchListener(null);
		}

		/*
		 * if (mPauseButton != null) { mPauseButton.setOnClickListener(null); }
		 */

		if (mProgress != null) {
			if (mProgress instanceof SeekBar) {
				SeekBar seeker = (SeekBar) mProgress;
				seeker.setOnSeekBarChangeListener(null);
			}
		}

		mRoot.setOnSystemUiVisibilityChangeListener(null);
		mHandler.removeMessages(FADE_OUT);
		mHandler.removeMessages(SHOW_PROGRESS);
		mHandler = null;
	}

	/**
	 * remove layout of control from surfaceFlinger
	 */
	private void removeBackground() {
		if (mActivity != null && !mSubLayer.isShown() && mRemoveBackgroundEnable) {
			mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SCALED, WindowManager.LayoutParams.FLAG_SCALED);
			// We set the background in the theme to have the launching animation.
			// But for the performance (and battery), we remove the background here.
			mActivity.getWindow().setBackgroundDrawable(null);
		}
	}

	/**
	 * recovery layout of control from surfaceFlinger
	 */
	private void recoveryBackground() {
		if (mActivity != null && !mSubLayer.isShown() && mRemoveBackgroundEnable) {
			mActivity.getWindow().setFlags(~WindowManager.LayoutParams.FLAG_SCALED, WindowManager.LayoutParams.FLAG_SCALED);
		}
	}

	private boolean isDestroyed() {
		return mHandler == null;
	}

	/**
	 * 
	 * @param forward
	 * @return return true if play next movie
	 */
	public boolean fastForwardAndBackward(boolean forward) {
		show();
		int duration = mControlWrapper.getDuration();
		if (duration < 0) {
			return true;
		}

		int currentPosition = mControlWrapper.getCurrentPosition();
		int delta = (int) (duration * FAST_FORWARD_BACKWARD_INTERVAL);
		delta = delta < FAST_FORWARD_BACKWARD_MIN_TIME ? FAST_FORWARD_BACKWARD_MIN_TIME : delta;
		delta *= forward ? 1 : -1;
		int nextPosition = currentPosition + delta;
		if (nextPosition > duration) {
			return false;
		}

		nextPosition = nextPosition < 0 ? 0 : nextPosition;
		mControlWrapper.seekTo(nextPosition);
		setProgress();
		return true;
	}
}
