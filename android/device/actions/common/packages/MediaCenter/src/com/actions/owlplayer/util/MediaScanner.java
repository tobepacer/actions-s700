package com.actions.owlplayer.util;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class MediaScanner extends Object {
	private static final String TAG = "MediaScanner";

	private static final String MEDIA_FILE_EXTENSION = "3g2|3gp|3gpp|amv|asf|avi|divx|evo|f4v|flv|m2t|m2ts|m4v|mkv|mov|mp4|mpeg|mpg|mts|ogm|rm|rmvb|ts|vob|webm|wmv";
	// others
	/* bin|crc|dat|db|dv|m4a|ogg|srt|tp|vmark|wgi|wma|xls|xv */

	private List<String> mMedias;
	private MediaFilter mFilter;
	private final String SWF_FILE = "swf";

	public MediaScanner() {
		mMedias = new ArrayList<String>();
		mFilter = new MediaFilter(SWF_FILE);
	}

	public List<String> scan(String path) {
		mMedias.clear();
		scanMediaFile(new File(path));
		return mMedias;
	}

	private void scanMediaFile(File root) {
		if (root == null || root.isFile() || !root.exists()) {
			return;
		}

		File[] medias = root.listFiles(mFilter);
		if (medias != null) {
			for (File file : medias) {
				mMedias.add(file.getAbsolutePath());
			}
		}
		File[] files = root.listFiles();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					scanMediaFile(file);
				}
			}
		}
	}

	private class MediaFilter implements FileFilter {
		private String[] mExtensions;

		public MediaFilter(String extension) {
			mExtensions = extension.split("\\|");
		}

		@Override
		public boolean accept(File pathname) {
			if (pathname.isDirectory()) {
				return false;
			}

			for (String suffix : mExtensions) {
				if (pathname.getName().toLowerCase().endsWith(suffix)) {
					return true;
				}
			}

			return false;
		}
	}
}
