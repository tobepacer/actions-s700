package com.actions.music.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio;
import android.util.Log;

public class MusicItemCache extends Observable {
	private static final String TAG = MusicItemCache.class.getSimpleName();
	
	private static final int MESSAGE_UPDATE_CACHE = 0;
	private static MusicItemCache sMusicItemCache;
	private static Object mLock = new Object();
	private List<LocalMusic> mMusicList;
	private Context mContext;

	public static MusicItemCache getInstance(Context context) {
		if (sMusicItemCache == null) {
			synchronized (mLock) {
				if (sMusicItemCache == null) {
					sMusicItemCache = new MusicItemCache(context);
				}
			}
		}
		return sMusicItemCache;
	}

	private MusicItemCache(Context context) {
		mContext = context;
		mMusicList = new ArrayList<LocalMusic>();
		registerObserver();
	}

	public int size() {
		return mMusicList.size();
	}

	public LocalMusic getItem(int position) {
		if (position >= 0 && position < size()) {
			return mMusicList.get(position);
		}
		return null;
	}
	
	public int getIndexById(int id) {
		for(int i=0; i<mMusicList.size(); i++) {
			if(mMusicList.get(i).getID()==id) {
				return i;
			}
		}
		return -1;
	}

	public void update() {
		Log.d(TAG, "update");
		List<LocalMusic> cacheList = new ArrayList<>();
		Cursor cursor = mContext.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, LocalMusic.PROJECTIONS, null, null, null);
		if (cursor == null) {
			return;
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			cacheList.add(new LocalMusic(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		if(cacheList.equals(mMusicList)) {
			Log.d(TAG, "new list is same to old, update cancel");
			return;
		}
		mMusicList.clear();
		mMusicList.addAll(cacheList);
		Log.d(TAG, "MusicItemCache:" + mMusicList.size());
		setChanged();
		notifyObservers();
	}

	private void registerObserver() {
		mContext.getContentResolver().registerContentObserver(Audio.Media.EXTERNAL_CONTENT_URI, false, mContentObserver);
	}

	private void unregisterObserver() {
		mContext.getContentResolver().unregisterContentObserver(mContentObserver);
	}

	private ContentObserver mContentObserver = new ContentObserver(new Handler()) {

		@Override
		public void onChange(boolean selfChange) {
			Log.d(TAG, "mContentObserver onChange:"+selfChange);
			super.onChange(selfChange);
			mHandler.removeMessages(MESSAGE_UPDATE_CACHE);
			mHandler.sendEmptyMessageDelayed(MESSAGE_UPDATE_CACHE, 300);
		}

	};

	public void release() {
		unregisterObserver();
		deleteObservers();
		sMusicItemCache = null;
	}
	
	private Handler mHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			if(msg.what==MESSAGE_UPDATE_CACHE) {
				update();
			}
		}
		
	};
}
