package com.actions.owlplayer.widget;

import android.media.MediaPlayer;

public class HwMediaPlayer extends MediaPlayer {
	private CustomTrackInfo[] mTrackInfo;

	HwMediaPlayer() {
		super();
	}

	public CustomTrackInfo[] getTrackMetadata() {
		TrackInfo[] trackInfo = this.getTrackInfo();

		mTrackInfo = new CustomTrackInfo[trackInfo.length];
		for (int track = 0; track < trackInfo.length; track++) {
			mTrackInfo[track] = new CustomTrackInfo(null, trackInfo[track].getLanguage(), trackInfo[track].getTrackType());
		}

		return mTrackInfo;
	}
}
