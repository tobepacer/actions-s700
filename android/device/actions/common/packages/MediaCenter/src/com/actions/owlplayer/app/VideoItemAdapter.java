package com.actions.owlplayer.app;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.GridLayoutManager.LayoutParams;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.mediacenter.data.MediaSet;
import com.actions.mediacenter.widget.BaseRecyclerViewAdapter;
import com.actions.owlplayer.app.BitmapLoader.ImageCallback;
import com.actions.owlplayer.data.LocalVideo;

public class VideoItemAdapter extends BaseRecyclerViewAdapter<VideoItemAdapter.ViewHolder> {
	private MediaSet mVideoSet;
	private BitmapLoader mBitmapLoader;
	private OnVideoItemClickListener mOnVideoItemClickListener;
	private OnVideoItemLongClickListener mOnVideoItemLongClickListener;
	private OWLApplication mApplication;

	public VideoItemAdapter(OWLApplication application, MediaSet mediaSet) {
		mApplication = application;
		mVideoSet = mediaSet;
		mBitmapLoader = BitmapLoader.getInstance(application);
	}

	@Override
	public int getItemCount() {
		return mVideoSet.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		super.onBindViewHolder(viewHolder, position);
		viewHolder.itemView.setTag(position);
		LocalVideo video = (LocalVideo) mVideoSet.getItem(position);
		viewHolder.itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mOnVideoItemClickListener != null) {
					mOnVideoItemClickListener.onClick(v, (int) v.getTag());
				}
			}
		});
		viewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				if (mOnVideoItemLongClickListener != null) {
					return mOnVideoItemLongClickListener.onLongClick(v, (int) v.getTag());
				}
				return false;
			}
		});
		viewHolder.name.setText(video.displayName);
		viewHolder.thumbnail.setTag(video.videoId);
		Bitmap bitmap = mBitmapLoader.loadBitmap(video, viewHolder.thumbnail, new ImageCallback() {

			@Override
			public void imageLoad(int viewTag, ImageView imageView, Bitmap bitmap) {
				if (bitmap != null && imageView.getTag().equals(viewTag)) {
					imageView.setImageBitmap(bitmap);
				}
			}
		});

		if (bitmap != null) {
			viewHolder.thumbnail.setImageBitmap(bitmap);
		} else {
			viewHolder.thumbnail.setImageBitmap(null);
		}
		if (video.isSelect()) {
			viewHolder.selectImage.setVisibility(View.VISIBLE);
		} else {
			viewHolder.selectImage.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_item, viewGroup, false);
		initLayout(viewGroup, view);
		return new ViewHolder(view);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		ImageView thumbnail;
		TextView name;
		View selectImage;

		public ViewHolder(View rootView) {
			super(rootView);
			thumbnail = (ImageView) rootView.findViewById(R.id.video_thumbnail);
			name = (TextView) rootView.findViewById(R.id.video_name);
			selectImage = rootView.findViewById(R.id.media_select_image);
		}

	}

	public void setOnVideoItemClickListener(OnVideoItemClickListener listener) {
		mOnVideoItemClickListener = listener;
	}

	public void setOnVideoItemLongClickListener(OnVideoItemLongClickListener listener) {
		mOnVideoItemLongClickListener = listener;
	}

	public interface OnVideoItemClickListener {
		public void onClick(View v, int position);
	}

	public interface OnVideoItemLongClickListener {
		public boolean onLongClick(View v, int position);
	}
	
	private void initLayout(ViewGroup viewGroup, View view) {
		Config.initThumbLayoutParams(viewGroup, view);
		LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
		layoutParams.setMargins(Config.thumbnailMarginLeft, Config.thumbnailMarginTop, Config.thumbnailMarginRight, Config.thumbnailMarginButtom);
		layoutParams.height = Config.thumbnailHeight;
		layoutParams.width = Config.thumbnailWidth;
		view.setLayoutParams(layoutParams);
	}

}
