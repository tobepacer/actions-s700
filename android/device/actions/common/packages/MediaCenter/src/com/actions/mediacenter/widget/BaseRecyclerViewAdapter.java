package com.actions.mediacenter.widget;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;

public abstract class BaseRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
	private static final String TAG_BASERECYCLERVIEW = BaseRecyclerViewAdapter.class.getSimpleName();
	// Start with first item selected
	private int focusedItem = 0;

	@Override
	public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
		super.onAttachedToRecyclerView(recyclerView);

		// Handle key up and key down and attempt to move selection
		recyclerView.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();

				// Return false if scrolled to the bounds and allow focus to move off the list
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					switch (keyCode) {
					case KeyEvent.KEYCODE_DPAD_DOWN:
					case KeyEvent.KEYCODE_DPAD_UP:
					case KeyEvent.KEYCODE_DPAD_LEFT:
					case KeyEvent.KEYCODE_DPAD_RIGHT:
						return tryMoveSelection(recyclerView, keyCode);
					case KeyEvent.KEYCODE_DPAD_CENTER:
						ViewHolder viewHolder = recyclerView.findViewHolderForPosition(focusedItem);
						if (viewHolder != null) {
							viewHolder.itemView.performClick();
						}
						return true;
					default:
						break;
					}
				}

				return false;
			}
		});
		recyclerView.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				notifyItemChanged(focusedItem);
			}
		});
	}

	private boolean tryMoveSelection(RecyclerView.LayoutManager lm, int direction) {
		int tryFocusItem = focusedItem + direction;

		// If still within valid bounds, move the selection, notify to redraw, and scroll
		if (tryFocusItem >= 0 && tryFocusItem < getItemCount()) {
			notifyItemChanged(focusedItem);
			focusedItem = tryFocusItem;
			notifyItemChanged(focusedItem);
			lm.scrollToPosition(focusedItem);
			return true;
		}

		return false;
	}

	private boolean tryMoveSelection(RecyclerView recyclerView, int direction) {
		GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
		int spanCount = gridLayoutManager.getSpanCount();
		int tryFocusItem = 0;
		switch (direction) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			tryFocusItem = focusedItem - 1;
			if (tryFocusItem % spanCount == spanCount - 1) {
				tryFocusItem = -1;
			}
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			tryFocusItem = focusedItem + 1;
			if (tryFocusItem % spanCount == 0) {
				tryFocusItem = -1;
			}
			break;
		case KeyEvent.KEYCODE_DPAD_UP:
			tryFocusItem = focusedItem - spanCount;
			break;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			tryFocusItem = focusedItem + spanCount;
			break;
		default:
			break;
		}
		// If still within valid bounds, move the selection, notify to redraw, and scroll
		if (tryFocusItem >= 0 && tryFocusItem < getItemCount()) {
			notifyItemChanged(focusedItem);
			focusedItem = tryFocusItem;
			notifyItemChanged(focusedItem);
			recyclerView.smoothScrollToPosition(focusedItem);
			return true;
		}

		return false;
	}

	@Override
	public void onBindViewHolder(VH viewHolder, int position) {
		// Set selected state; use a state list drawable to style the view
		viewHolder.itemView.setSelected(!viewHolder.itemView.isInTouchMode() && focusedItem == position);
	}

}
