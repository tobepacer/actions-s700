package com.actions.owlplayer.data;

import java.util.ArrayList;
import java.util.Random;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.actions.owlplayer.app.Config;
import com.actions.owlplayer.app.OWLApplication;
import com.actions.owlplayer.util.Preferences;

public class MediaItemCache implements ItemList, PlayList {
	private  final String TAG = getClass().getSimpleName();
	public static final int FLAG_ORDER_BY_TIME = 0x00;
	public static final int FLAG_ORDER_BY_NAME = 0x10;

	private final Object mLock = new Object();
	private OWLApplication mApplication;
	private ArrayList<MediaItem> mMediaList;
	private int mSortOrder;
	private int mLoopMode;
	private Random mRandom;
	private SharedPreferences mSharedPreferences;

	public MediaItemCache(OWLApplication application) {
		mApplication = application;
		mMediaList = new ArrayList<MediaItem>();
		mRandom = new Random(System.currentTimeMillis());
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mApplication);
		mSharedPreferences.registerOnSharedPreferenceChangeListener(mSPChangeListener);

		mApplication.getContentResolver().registerContentObserver(MediaProvider.VIDEO_CONTENT_URI, false, mContentObserver);
		updateLoopMode();
		refresh();
	}

	private ContentObserver mContentObserver = new ContentObserver(new Handler()) {
		@Override
		public boolean deliverSelfNotifications() {
			return super.deliverSelfNotifications();
		}

		@Override
		public void onChange(boolean selfChange) {
			Log.d(TAG, "mContentObserver onChange");
			super.onChange(selfChange);
			refresh();
		}
	};

	OnSharedPreferenceChangeListener mSPChangeListener = new OnSharedPreferenceChangeListener() {
		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
			if (key.equals(Preferences.SORT_ORDER)) {
				refresh();
			} else if (key.equals(Preferences.CYCLE_MODE)) {
				updateLoopMode();
			}
		}
	};

	public void refresh() {
		Log.d(TAG, "refresh");
		mSortOrder = Integer.parseInt((String) Preferences.getPreferences(mApplication, Preferences.SORT_ORDER, Integer.toString(Config.SortOrder.TIME)));

		String orderBy;
		if (mSortOrder == Config.SortOrder.TIME) {
			orderBy = MediaDatabaseOpenHelper.DB_COLUMN_DATE_MODIFIED + " DESC";
		} else {
			orderBy = MediaDatabaseOpenHelper.DB_COLUMN_DISPLAY_NAME + " ASC";
		}

		Cursor cursor = mApplication.getContentResolver().query(MediaProvider.VIDEO_CONTENT_URI, MediaDatabaseOpenHelper.DB_PROJECTION, null, null, orderBy);
		if (cursor == null)
			return;

		synchronized (mLock) {
			mMediaList.clear();
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				MediaItem mediaItem = new LocalVideo(mApplication, cursor);
				mMediaList.add(mediaItem);
			}
		}
		cursor.close();

		Intent i = new Intent(Config.ACTION_MEDIA_ITEM_CACHE_REFRESHED);
		mApplication.sendBroadcast(i);
	}

	@Override
	public int size() {
		return mMediaList.size();
	}

	@Override
	public MediaItem get(int position) {
		return mMediaList.get(position);
	}

	private int findCurrentPosition(MediaItem current) {
		for (int index = 0; index < mMediaList.size(); index++) {
			if (mMediaList.get(index).compare(current))
				return index;
		}
		return -1;
	}

	@Override
	public MediaItem previous(MediaItem current) {
		return navigate(current, false);
	}

	private boolean isInvalidPostion(int position) {
		return position >= mMediaList.size() || position < 0;
	}

	public MediaItem navigate(MediaItem mediaItem, boolean forward) {
		if (!(mediaItem instanceof LocalVideo)) {
			return null;
		}

		if (mLoopMode == Config.LoopMode.SINGLE) {
			return mediaItem;
		}

		int position = 0;
		if (mLoopMode == Config.LoopMode.RANDOM) {
			if (mMediaList.size() <= 0) {
				return null;
			} else {
				position = mRandom.nextInt(mMediaList.size());
			}
		} else {
			position = findCurrentPosition(mediaItem);
			if (isInvalidPostion(position)) {
				return null;
			}

			if (forward) {
				position++;
				if (isInvalidPostion(position)) {
					position = 0;
				}
			} else {
				position--;
				if (isInvalidPostion(position)) {
					position = mMediaList.size() - 1;
				}
			}
		}

		return mMediaList.get(position);
	}

	@Override
	public MediaItem next(MediaItem current) {
		return navigate(current, true);
	}

	public void close() {
		mApplication.getContentResolver().unregisterContentObserver(mContentObserver);
		mSharedPreferences.unregisterOnSharedPreferenceChangeListener(mSPChangeListener);
	}

	private void updateLoopMode() {
		mLoopMode = Integer.parseInt((String) Preferences.getPreferences(mApplication, Preferences.CYCLE_MODE, Integer.toString(Config.LoopMode.ORDINAL)));
	}
	
}
