package com.actions.owlplayer.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MediaDatabaseOpenHelper extends SQLiteOpenHelper {
	private static String TAG = "MediaDatabaseOpenHelper";

	public static final String DB_COLUMN_ID = "_id";
	public static final String DB_COLUMN_VIDEO_ID = "video_id";
	public static final String DB_COLUMN_BUCKET_ID = "bucket_id";
	public static final String DB_COLUMN_BUCKET_NAME = "bucket_name";
	public static final String DB_COLUMN_DATA = "data";
	public static final String DB_COLUMN_MIME_TYPE = "mime_type";
	public static final String DB_COLUMN_DISPLAY_NAME = "display_name";
	public static final String DB_COLUMN_TITLE = "title";
	public static final String DB_COLUMN_DURATION = "duration";
	public static final String DB_COLUMN_VIDEO_TRACK = "video_track";
	public static final String DB_COLUMN_AUDIO_TRACK = "audio_track";
	public static final String DB_COLUMN_SUB_TRACK = "sub_track";
	public static final String DB_COLUMN_BOOKMARK = "bookmark";
	public static final String DB_COLUMN_WIDTH = "width";
	public static final String DB_COLUMN_HEIGHT = "height";
	public static final String DB_COLUMN_DATE_MODIFIED = "date_modified";
	public static final String DB_COLUMN_FILE_SIZE = "file_size";
	public static final String DB_COLUMN_NEW = "new_tag";

	public static final int DB_INDEX_ID = 0;
	public static final int DB_INDEX_VIDEO_ID = 1;
	public static final int DB_INDEX_BUCKET_ID = 2;
	public static final int DB_INDEX_BUCKET_NAME = 3;
	public static final int DB_INDEX_DATA = 4;
	public static final int DB_INDEX_MIME_TYPE = 5;
	public static final int DB_INDEX_DISPLAY_NAME = 6;
	public static final int DB_INDEX_TITLE = 7;
	public static final int DB_INDEX_DURATION = 8;
	public static final int DB_INDEX_VIDEO_TRACK = 9;
	public static final int DB_INDEX_AUDIO_TRACK = 10;
	public static final int DB_INDEX_SUB_TRACK = 11;
	public static final int DB_INDEX_BOOKMARK = 12;
	public static final int DB_INDEX_WIDTH = 13;
	public static final int DB_INDEX_HEIGHT = 14;
	public static final int DB_INDEX_DATE_MODIFIED = 15;
	public static final int DB_INDEX_FILE_SIZE = 16;
	public static final int DB_INDEX_NEW = 17;

	public static final String[] DB_PROJECTION = new String[] { DB_COLUMN_ID, DB_COLUMN_VIDEO_ID, DB_COLUMN_BUCKET_ID, DB_COLUMN_BUCKET_NAME, DB_COLUMN_DATA,
			DB_COLUMN_MIME_TYPE, DB_COLUMN_DISPLAY_NAME, DB_COLUMN_TITLE, DB_COLUMN_DURATION, DB_COLUMN_VIDEO_TRACK, DB_COLUMN_AUDIO_TRACK,
			DB_COLUMN_SUB_TRACK, DB_COLUMN_BOOKMARK, DB_COLUMN_WIDTH, DB_COLUMN_HEIGHT, DB_COLUMN_DATE_MODIFIED, DB_COLUMN_FILE_SIZE, DB_COLUMN_NEW };

	public static final String DB_NAME = "videoplayer.db";
	public static final String DB_TAB_VIDEO = "video";
	private static final int DB_VERSION = 1;

	public MediaDatabaseOpenHelper(Context ctx) {
		super(ctx, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		String sqlCmd = String.format("CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY, %s INTEGER,"
				+ " %s INTEGER, %s VARCHAR, %s VARCHAR, %s VARCHAR, %s VARCHAR, %s VARCHAR, %s INTEGER, %s INTEGER,"
				+ " %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s LONG, %s LONG, %s INTEGER )", DB_TAB_VIDEO, DB_PROJECTION[0],
				DB_PROJECTION[1], DB_PROJECTION[2], DB_PROJECTION[3], DB_PROJECTION[4], DB_PROJECTION[5], DB_PROJECTION[6], DB_PROJECTION[7], DB_PROJECTION[8],
				DB_PROJECTION[9], DB_PROJECTION[10], DB_PROJECTION[11], DB_PROJECTION[12], DB_PROJECTION[13], DB_PROJECTION[14], DB_PROJECTION[15],
				DB_PROJECTION[16], DB_PROJECTION[17]);
		arg0.execSQL(sqlCmd);

		Log.v(TAG, "onCreate");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DB_TAB_VIDEO);
		onCreate(db);
		Log.v("TAG", "onUpgrade");
	}
}