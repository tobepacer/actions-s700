package com.actions.owlplayer.widget;

import android.media.MediaPlayer.OnTimedTextListener;
import android.net.Uri;
import android.view.MotionEvent;

public interface MovieViewInterface {
	void setOnTimedTextListener(OnTimedTextListener listener);

	void addTimedTextSource(String path, String mimeType);

	void addActTimedTextSource(String path, String mimeType);

	void setVideoURI(Uri uri);

	void setLayoutController(LayoutController controller);

	void stopPlayback();

	void start();

	void pause();

	int getDuration();

	int getCurrentPosition();

	void seekTo(int pos);

	boolean isPlaying();

	int getBufferPercentage();

	void setOnPreparedListener(OnPreparedListener l);

	void setOnCompletionListener(OnCompletionListener l);

	void setOnErrorListener(OnErrorListener l);

	void setVolume(float l, float r);

	void selectTrack(int index);

	void deselectTrack(int index);

	CustomTrackInfo[] getTrackMetadata();

	public interface OnErrorListener {
		/**
		 * Called when a media source report an error.
		 */
		abstract boolean onError(int what, int extra);
	}

	public interface OnCompletionListener {
		/**
		 * Called when the end of a media source is reached during playback.
		 */
		abstract void onCompletion();
	}

	public interface OnPreparedListener {
		/**
		 * Called when a media source is prepared.
		 */
		abstract void onPrepared();
	}

	public boolean onExtraTouchEvent(MotionEvent event);
}
