package com.actions.owlplayer.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;

import com.actions.mediacenter.data.MediaObject;
import com.actions.mediacenter.data.MediaSet;
import com.actions.mediacenter.data.Path;
import com.actions.mediacenter.widget.SelectedItemShowView;
import com.actions.owlplayer.app.Config;
import com.actions.owlplayer.util.Preferences;

public class LocalVideoSet extends MediaSet implements PlayList {
	private static final String TAG = LocalVideoSet.class.getSimpleName();
	private Path mPath;
	private List<MediaItem> mVideoItems = new ArrayList<MediaItem>();
	private Random mRandom;
	private Context mContext;
	private boolean isSelect = false;
	private int mSelectedItemCount;
	private SelectedItemShowView mSelectedItemShowView;

	public LocalVideoSet(Path path, Context context) {
		mPath = path;
		mContext = context;
		mRandom = new Random(System.currentTimeMillis());
	}

	@Override
	public MediaObject getItem(int position) {
		return mVideoItems.get(position);
	}

	@Override
	public int size() {
		return mVideoItems.size();
	}

	@Override
	public String getPath() {
		if (mPath != null) {
			return mPath.getPath();
		}
		return null;
	}

	@Override
	public String getName() {
		if (mPath != null) {
			return mPath.getName();
		}
		return null;
	}

	@Override
	public void addItem(MediaObject item) {
		mVideoItems.add((MediaItem) item);
	}

	@Override
	public void clear() {
		mVideoItems.clear();
	}

	@Override
	public MediaItem previous(MediaItem current) {
		return navigate(current, false);
	}

	@Override
	public MediaItem next(MediaItem current) {
		return navigate(current, true);
	}

	public MediaItem navigate(MediaItem mediaItem, boolean forward) {
		if (!(mediaItem instanceof LocalVideo)) {
			return null;
		}

		if (getLoopMode() == Config.LoopMode.SINGLE) {
			return mediaItem;
		}

		int position = 0;
		if (getLoopMode() == Config.LoopMode.RANDOM) {
			if (mVideoItems.size() <= 0) {
				return null;
			} else {
				position = mRandom.nextInt(mVideoItems.size());
			}
		} else {
			position = findCurrentPosition(mediaItem);
			if (isInvalidPostion(position)) {
				return null;
			}

			if (forward) {
				position++;
				if (isInvalidPostion(position)) {
					position = 0;
				}
			} else {
				position--;
				if (isInvalidPostion(position)) {
					position = mVideoItems.size() - 1;
				}
			}
		}
		return mVideoItems.get(position);
	}

	public int findCurrentPosition(MediaItem current) {
		for (int index = 0; index < mVideoItems.size(); index++) {
			if (mVideoItems.get(index).videoId == current.videoId)
				return index;
		}
		return -1;
	}

	private boolean isInvalidPostion(int position) {
		return position >= mVideoItems.size() || position < 0;
	}

	private int getLoopMode() {
		return Integer.parseInt((String) Preferences.getPreferences(mContext, Preferences.CYCLE_MODE, Integer.toString(Config.LoopMode.ORDINAL)));
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void selectClick() {
		isSelect = !isSelect;
	}

	public void setSelect(boolean select) {
		isSelect = select;
	}

	public void delete() {
		for (int i = 0; i < mVideoItems.size(); i++) {
			LocalVideo video = (LocalVideo) mVideoItems.get(i);
			video.delete();
		}
	}

	public void clearItemSelect() {
		for (int i = 0; i < mVideoItems.size(); i++) {
			LocalVideo video = (LocalVideo) mVideoItems.get(i);
			video.setSelect(false);
		}
		mSelectedItemCount = 0;
	}

	public void videoItemSelectClick(int position) {
		LocalVideo video = (LocalVideo) getItem(position);
		video.selectClick();
		if (video.isSelect()) {
			mSelectedItemCount++;
		} else {
			mSelectedItemCount--;
		}
		if (mSelectedItemShowView != null) {
			mSelectedItemShowView.update(mSelectedItemCount);
		}
	}

	public int getSelectedSetCount() {
		return mSelectedItemCount;
	}

	public void setSelectedCountShowView(SelectedItemShowView selectedItemShowView) {
		mSelectedItemShowView = selectedItemShowView;
	}
}
