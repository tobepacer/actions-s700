package com.actions.image.app;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;

public class Config {
	private static final String TAG = Config.class.getSimpleName();
	public static final String ACTION_IMAGE_CACHE_REFRESH = "com.actions.mediacenter.IMAGE_CACHE_REFRESH";

	public static final int BROWSER_SPANCOUNT = 4;
	public static final int BROWSER_ROWCOUNT = 2;

	public static final int DISK_CACHE_SIZE = 50 * 1024 * 1024;

	public static final int MAX_WIDTH_IN_MEMORY_CACHE = 200;
	public static final int MAX_HEIGHT_IN_MEMORY_CACHE = 200;

	public static final int MAX_WIDTH_IN_DISK_CACHE = 200;
	public static final int MAX_HEIGHT_IN_DISK_CACHE = 200;

	public static final int MAX_THREAD_POOL_SIZE = 3;

	public static final String INTENT_KEY_IMAGE_SET_POSITION = "media-set-position";
	public static final String INTENT_KEY_IMAGE_ITEM_POSITION = "media-item-position";

	public static int thumbnailHeight;
	public static int thumbnailWidth;
	public static int thumbnailMarginTop;
	public static int thumbnailMarginLeft = 4;
	public static int thumbnailMarginButtom;
	public static int thumbnailMarginRight;
	private static boolean isInitThumbParams = false;

	public static void initThumbLayoutParams(ViewGroup viewGroup, View view) {
		if (!isInitThumbParams) {
			thumbnailHeight = viewGroup.getHeight();
			thumbnailWidth = viewGroup.getWidth();
			thumbnailWidth = thumbnailWidth / BROWSER_SPANCOUNT;
			thumbnailMarginTop = (thumbnailHeight - (thumbnailWidth-thumbnailMarginLeft*2) * BROWSER_ROWCOUNT) / (BROWSER_ROWCOUNT * 2 + 1);
			thumbnailHeight = (thumbnailHeight - thumbnailMarginTop) / BROWSER_ROWCOUNT;
			isInitThumbParams = true;

			thumbnailMarginRight = thumbnailMarginLeft;
			thumbnailMarginButtom = thumbnailMarginTop;
			LayoutParams viewGrouParams = (LayoutParams) viewGroup.getLayoutParams();
			viewGrouParams.setMargins(0, thumbnailMarginTop, 0, 0);
			viewGroup.setLayoutParams(viewGrouParams);
		}
	}

	public static void setInitThumbParams(boolean init){
		isInitThumbParams = init;
	}
}
