package com.actions.owlplayer.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Log;

import com.actions.mediacenter.R;
import com.actions.owlplayer.util.ThreadPool.Job;
import com.actions.owlplayer.util.ThreadPool.JobContext;
import com.actions.owlplayer.util.Utils;

public class LocalVideo extends MediaItem {
	private boolean isSelect = false;
	
	public LocalVideo(Context context, Cursor cursor) {
		super();
		mContext = context;
		if (cursor != null && !cursor.isClosed()) {
			this.videoId = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_VIDEO_ID);
			this.bucketId = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_BUCKET_ID);
			this.bucketName = cursor.getString(MediaDatabaseOpenHelper.DB_INDEX_BUCKET_NAME);
			this.data = cursor.getString(MediaDatabaseOpenHelper.DB_INDEX_DATA);
			this.mimeType = cursor.getString(MediaDatabaseOpenHelper.DB_INDEX_MIME_TYPE);
			this.displayName = cursor.getString(MediaDatabaseOpenHelper.DB_INDEX_DISPLAY_NAME);
			this.title = cursor.getString(MediaDatabaseOpenHelper.DB_INDEX_TITLE);
			this.duration = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_DURATION);
			this.videoTrack = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_VIDEO_TRACK);
			this.audioTrack = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_AUDIO_TRACK);
			this.subTrack = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_SUB_TRACK);
			this.bookmark = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_BOOKMARK);
			this.width = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_WIDTH);
			this.height = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_HEIGHT);
			this.dateModified = cursor.getLong(MediaDatabaseOpenHelper.DB_INDEX_DATE_MODIFIED);
			this.fileSize = cursor.getLong(MediaDatabaseOpenHelper.DB_INDEX_FILE_SIZE);
			this.newTag = cursor.getInt(MediaDatabaseOpenHelper.DB_INDEX_NEW);
		}
	}

	public LocalVideo(Context context) {
		this(context, null);
	}

	public LocalVideo(Parcel parcel) {
		mContext = null;

		this.videoId = parcel.readInt();
		this.bucketId = parcel.readInt();
		this.bucketName = parcel.readString();
		this.data = parcel.readString();
		this.mimeType = parcel.readString();
		this.displayName = parcel.readString();
		this.title = parcel.readString();
		this.duration = parcel.readInt();
		this.videoTrack = parcel.readInt();
		this.audioTrack = parcel.readInt();
		this.subTrack = parcel.readInt();
		this.bookmark = parcel.readInt();
		this.width = parcel.readInt();
		this.height = parcel.readInt();
		this.dateModified = parcel.readLong();
		this.fileSize = parcel.readLong();
		this.newTag = parcel.readInt();
		this.volume = parcel.readFloat();
		this.uri = (Uri) parcel.readParcelable(ClassLoader.getSystemClassLoader());
	}

	public ContentValues getContentValues() {
		ContentValues values = new ContentValues();
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_VIDEO_ID, this.videoId);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_BUCKET_ID, this.bucketId);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_BUCKET_NAME, this.bucketName);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_DATA, this.data);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_MIME_TYPE, this.mimeType);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_DISPLAY_NAME, this.displayName);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_TITLE, this.title);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_DURATION, this.duration);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_VIDEO_TRACK, this.videoTrack);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_AUDIO_TRACK, this.audioTrack);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_SUB_TRACK, this.subTrack);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_BOOKMARK, this.bookmark);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_WIDTH, this.width);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_HEIGHT, this.height);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_DATE_MODIFIED, this.dateModified);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_FILE_SIZE, this.fileSize);
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_NEW, this.newTag);
		return values;
	}

	@Override
	public void delete() {
		if (mContext == null)
			return;

		Uri baseUri = Video.Media.EXTERNAL_CONTENT_URI;
		mContext.getContentResolver().delete(baseUri, "_id=?", new String[] { String.valueOf(videoId) });
	}

	@Override
	public String getDetails() {
		if (mContext == null)
			return null;

		StringBuilder details = new StringBuilder().append(mContext.getString(R.string.media_detail_title) + "  " + displayName + "\n")
				.append(mContext.getString(R.string.media_detail_time) + "  " + Utils.convertDateTime(dateModified) + "\n")
				.append(mContext.getString(R.string.media_detail_width) + "  " + width + "\n")
				.append(mContext.getString(R.string.media_detail_height) + "  " + height + "\n")
				.append(mContext.getString(R.string.media_detail_duration) + "  " + Utils.convertMsToHHMMSS(duration) + "\n")
				.append(mContext.getString(R.string.media_detail_size) + "  " + Utils.convertBytes(mContext, fileSize) + "\n")
				.append(mContext.getString(R.string.media_detail_path) + "  " + data);
		return details.toString();
	}

	private Uri getContentUri() {
		Uri baseUri = Video.Media.EXTERNAL_CONTENT_URI;
		return baseUri.buildUpon().appendPath(String.valueOf(videoId)).build();
	}

	@Override
	public Uri getUri() {
		if (uri != null) {
			return uri;
		} else {
			return getContentUri();
		}
	}

	@Override
	public boolean compare(MediaItem item) {
		return (item.data != null && item.data.equalsIgnoreCase(data) && item.videoId == videoId) ? true : false;
	}

	@Override
	public Job<Bitmap> requestImage() {
		if (mContext == null)
			return null;
		return new LocalVideoRequest(mContext, videoId);
	}

	public static class LocalVideoRequest implements Job<Bitmap> {
		private static final String TAG = "LocalVideoRequest";
		private Context mContext;
		private int mVideoId;

		LocalVideoRequest(Context context, int id) {
			mContext = context;
			mVideoId = id;
		}

		@Override
		public Bitmap run(JobContext jc) {
			Bitmap bitmap = onDecodeThumbnail(jc);
			if (jc.isCancelled())
				return null;

			if (bitmap == null) {
				Log.w(TAG, "decode failed!");
				return null;
			}

			return bitmap;
		}

		public Bitmap onDecodeThumbnail(JobContext jc) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = Thumbnails.getThumbnail(mContext.getContentResolver(), mVideoId, Thumbnails.MINI_KIND, options);

			if (bitmap == null || jc.isCancelled())
				return null;
			return bitmap;
		}
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public static final Parcelable.Creator<LocalVideo> CREATOR = new Parcelable.Creator<LocalVideo>() {
		public LocalVideo createFromParcel(Parcel in) {
			return new LocalVideo(in);
		}

		public LocalVideo[] newArray(int size) {
			return new LocalVideo[size];
		}
	};

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(this.videoId);
		parcel.writeInt(this.bucketId);
		parcel.writeString(this.bucketName);
		parcel.writeString(this.data);
		parcel.writeString(this.mimeType);
		parcel.writeString(this.displayName);
		parcel.writeString(this.title);
		parcel.writeInt(this.duration);
		parcel.writeInt(this.videoTrack);
		parcel.writeInt(this.audioTrack);
		parcel.writeInt(this.subTrack);
		parcel.writeInt(this.bookmark);
		parcel.writeInt(this.width);
		parcel.writeInt(this.height);
		parcel.writeLong(this.dateModified);
		parcel.writeLong(this.fileSize);
		parcel.writeInt(this.newTag);
		parcel.writeFloat(this.volume);
		parcel.writeParcelable(uri, 0);
	}
	
	public boolean isSelect(){
		return isSelect;
	}
	
	public void selectClick(){
		isSelect = !isSelect;
	}
	
	public void setSelect(boolean select){
		isSelect = select;
	}
}
