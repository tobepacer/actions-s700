package com.actions.mediacenter.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actions.image.app.ImageBrowserActivity;
import com.actions.mediacenter.R;
import com.actions.music.app.MusicPlayActivity;
import com.actions.owlplayer.app.VideoBrowserActivity;
import com.actions.owlplayer.util.Utils;

public class MainActivity extends Activity implements OnClickListener {
	private static final String TAG = MainActivity.class.getSimpleName();
	
	private Button mVideoButton;
	private Button mMusicButton;
	private Button mImageButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_browser_view);
		initView();
	}
	
	private void initView(){
		mVideoButton = (Button) findViewById(R.id.video_browser);
		mMusicButton = (Button) findViewById(R.id.music_browser);
		mImageButton = (Button) findViewById(R.id.image_browser);
		
		mVideoButton.setOnClickListener(this);
		mMusicButton.setOnClickListener(this);
		mImageButton.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.video_browser:
			intent.setClass(this, VideoBrowserActivity.class);
			break;
		case R.id.music_browser:
			intent.setClass(this, MusicPlayActivity.class);
			break;
		case R.id.image_browser:
			intent.setClass(this, ImageBrowserActivity.class);
			break;
		default:
			break;
		}
		startActivity(intent);
	}
}
