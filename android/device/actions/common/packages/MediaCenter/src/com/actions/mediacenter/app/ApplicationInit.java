package com.actions.mediacenter.app;


import android.content.Context;
import android.graphics.Bitmap;

import com.actions.image.app.Config;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ApplicationInit {
	
	public static void init(Context context){
		initImageLoader(context);
	}
	
	private static void initImageLoader(Context context){
		DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
										.cacheInMemory(true)
										.cacheOnDisk(true)
										.bitmapConfig(Bitmap.Config.RGB_565)
										.resetViewBeforeLoading(true)
										.build();
		
		ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(context)
										.threadPoolSize(Config.MAX_THREAD_POOL_SIZE)
										.memoryCacheExtraOptions(Config.MAX_WIDTH_IN_MEMORY_CACHE, Config.MAX_HEIGHT_IN_MEMORY_CACHE)
										.diskCacheExtraOptions(Config.MAX_WIDTH_IN_DISK_CACHE, Config.MAX_HEIGHT_IN_DISK_CACHE, null)
										.diskCacheSize(Config.DISK_CACHE_SIZE)
										.denyCacheImageMultipleSizesInMemory()
										.defaultDisplayImageOptions(displayImageOptions)
										.build();
		ImageLoader.getInstance().init(configuration);
	}
}
