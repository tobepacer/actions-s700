package com.actions.image.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;


public class CustomImageView extends ImageView implements GestureRecognizer.Listener {
    private static final String TAG = "CustomImageView";

    private enum MODE {
        NONE, DRAG, SCALE
    };
    
    private static final int ANIMATION_NONE     = 0;
    private static final int ANIMATION_FEEDBACK = 1;
    private static final int ANIMATION_ZOOMIN   = 2;
    private static final int ANIMATION_ZOOMOUT  = 3;
    
    private Activity mActivity;
    private int mDisplayWidth = 0;
    private int mDisplayHeight = 0;
    private int mBitmapWidth, mBitmapHeight;
    private int mMaxBmWidth, mMaxBmHeight, mMinBmWidth, mMinBmHeight;
    private Rect mCurrentRect;
    private Rect mTargetRect;
    private Rect mStartRect;
    private int mStartX, mStartY, mCurrentX, mCurrentY;
    private float mBeforeLenght, mAfterLenght;
    private float mScaleTemp;

    private MODE mMode = MODE.NONE;
    private boolean isControlVertical = false;
    private boolean isControlHorizontal = false;

    private ScaleAnimation mScaleAnimation;
    private MyAsyncTask myAsyncTask;

    // private MyGestureListener mGestureListener;
    // private GestureRecognizer mGestureRecognizer;

    public CustomImageView(Context context) {
        super(context);
        initImageView(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initImageView(context);
    }

    private void initImageView(Context context) {
        mCurrentRect = new Rect();
        mTargetRect = new Rect();
        mStartRect = new Rect();
        mStartRect.right = -1;
        mStartRect.top = -1;
        mStartRect.right = -1;
        mStartRect.bottom = -1;
    }

    public void setmActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    public void setDisplayWidth(int width) {
        mDisplayWidth = width;
    }

    public void setDisplayHeight(int height) {
        mDisplayHeight = height;
    }
    
    public Rect getShowRange(int width, int height) {
        Rect rect = new Rect();
        float ratioW = (float) mDisplayWidth / (float) width;
        float ratioH = (float) mDisplayHeight / (float) height;
    
        if (ratioW <= ratioH) {
            mMinBmWidth = (int) (mDisplayWidth * 0.75);
            mMaxBmWidth = (int) (mDisplayWidth * 2.25);
            mMinBmHeight = mMinBmWidth * height / width;
            mMaxBmHeight = mMaxBmWidth * height / width;
            rect.left = 0;
            rect.top = (mDisplayHeight - mDisplayWidth * height / width) / 2;
            rect.right = mDisplayWidth;
            rect.bottom = mDisplayHeight - rect.top;
        } else {
            mMinBmHeight = (int) (mDisplayHeight * 0.75);
            mMaxBmHeight = (int) (mDisplayHeight * 2.25);
            mMinBmWidth = width * mMinBmHeight / height;
            mMaxBmWidth = width * mMaxBmHeight / height;
            rect.left = (mDisplayWidth - width * mDisplayHeight / height) / 2;
            rect.top = 0;
            rect.right = mDisplayWidth - rect.left;
            rect.bottom = mDisplayHeight;
        }
        
        return rect;
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (bm == null)
            return;

        if (mDisplayWidth == 0 || mDisplayHeight == 0) {
            Log.e(TAG, "IllegalStateException, set display range first!");
            return;
        }

        mBitmapWidth = bm.getWidth();
        mBitmapHeight = bm.getHeight();
        
        mCurrentRect = getShowRange(mBitmapWidth, mBitmapHeight);
        setFrame(mCurrentRect.left, mCurrentRect.top, mCurrentRect.right, mCurrentRect.bottom);
        mStartRect.left = mCurrentRect.left;
        mStartRect.top = mCurrentRect.top;
        mStartRect.right = mCurrentRect.right;
        mStartRect.bottom = mCurrentRect.bottom;
    }

    private void recenter() {
        mCurrentRect.left = getLeft();
        mCurrentRect.top = getTop();
        mCurrentRect.right = getRight();
        mCurrentRect.bottom = getBottom();

        mTargetRect.left = (mDisplayWidth - mCurrentRect.width()) / 2;
        mTargetRect.top = (mDisplayHeight - mCurrentRect.height()) / 2;
        mTargetRect.right = mTargetRect.left + mCurrentRect.width();
        mTargetRect.bottom = mTargetRect.top + mCurrentRect.height();
        setFrame(mTargetRect.left, mTargetRect.top, mTargetRect.right, mTargetRect.bottom);
    }
    
    public boolean isViewOutScreen() {
        return (mCurrentRect.width() > mDisplayWidth 
                || mCurrentRect.height() > mDisplayHeight);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

    }

    @Override
    public boolean onSingleTapUp(float x, float y) {
        Log.v(TAG, "onSingleTapUp");
        return true;
    }

    @Override
    public boolean onDoubleTap(float x, float y) {
        Log.v(TAG, "onDoubleTap");
        
        mCurrentRect.left = this.getLeft();
        mCurrentRect.top = this.getTop();
        mCurrentRect.right = this.getRight();
        mCurrentRect.bottom = this.getBottom();
        
        if (isViewOutScreen()) {
            doScaleAnim(ANIMATION_ZOOMOUT);
        } else {
            doScaleAnim(ANIMATION_ZOOMIN);
        }
        
        return true;
    }

    @Override
    public boolean onScroll(float dx, float dy, float totalX, float totalY) {
        Log.v(TAG, "onScroll");
        if(isViewOutScreen()) { 
            int deltaX = 0;
            int deltaY = 0;
            if (getLeft() <= 0 && getRight() >= mDisplayWidth) {
                if (dx >= 0) {
                    if (getRight() - dx >= mDisplayWidth) {
                        deltaX = (int)dx;
                    } else {
                        deltaX = getRight() - mDisplayWidth;
                    }
                } else {
                    if (getLeft() - dx <= 0) {
                        deltaX = (int)dx;
                    } else {
                        deltaX = getLeft();
                    }
                }
            }
            
            if (getTop() <= 0 && getBottom() >= mDisplayHeight) {
                if (dy >= 0) {
                    if (getBottom() - dy >= mDisplayHeight) {
                        deltaY = (int)dy;
                    } else {
                        deltaY = getBottom() - mDisplayHeight;
                    }
                } else {
                    if (getTop() - dy <= 0) {
                        deltaY = (int)dy;
                    } else {
                        deltaY = getTop();
                    }
                }
            }
            
            mTargetRect.left = getLeft() - deltaX;
            mTargetRect.top = getTop() - deltaY;
            mTargetRect.right = getRight() - deltaX;
            mTargetRect.bottom = getBottom() - deltaY;
            
            setFrame(mTargetRect.left, mTargetRect.top, mTargetRect.right, mTargetRect.bottom);
        }
        return true;
    }

    @Override
    public boolean onFling(float velocityX, float velocityY) {
        Log.v(TAG, "onFling");
        return true;
    }

    private boolean isLargeEnough(int width, int height) {
        return width >= mMaxBmWidth || height >= mMaxBmHeight;
    }

    private boolean isSmallEnough(int width, int height) {
        return width <= mMinBmWidth || height <= mMinBmHeight;
    }

    @Override
    public boolean onScaleBegin(float focusX, float focusY) {
        Log.v(TAG, "onScaleBegin");
        mMode = MODE.SCALE;
        return true;
    }

    @Override
    public boolean onScale(float focusX, float focusY, float scale) {
        Log.v(TAG, "onScale " + scale);

        mCurrentRect.left = this.getLeft();
        mCurrentRect.top = this.getTop();
        mCurrentRect.right = this.getRight();
        mCurrentRect.bottom = this.getBottom();

        if (mMode == MODE.SCALE) {
            int targetWidth = (int) (mCurrentRect.width() * scale);
            int targetHeight = (int) (mCurrentRect.height() * scale);
            Log.v(TAG, "targetWidth=" + targetWidth + ", targetHeight=" + targetHeight);

            if (scale >= 1f && isLargeEnough(targetWidth, targetHeight)) {
                targetWidth = mMaxBmWidth;
                targetHeight = mMaxBmHeight;
            }

            if (scale < 1f && isSmallEnough(targetWidth, targetHeight)) {
                targetWidth = mMinBmWidth;
                targetHeight = mMinBmHeight;
            }
            
            mTargetRect.left = (mDisplayWidth - targetWidth) / 2;
            mTargetRect.top = (mDisplayHeight - targetHeight) / 2;
            mTargetRect.right = mTargetRect.left + targetWidth;
            mTargetRect.bottom = mTargetRect.top + targetHeight;
            setFrame(mTargetRect.left, mTargetRect.top, mTargetRect.right, mTargetRect.bottom);
        }
        return true;
    }

    @Override
    public void onScaleEnd() {
        Log.v(TAG, "onScaleEnd");
        // recenter();
        if (getWidth() < mDisplayWidth && getHeight() < mDisplayHeight) {
            doScaleAnim(ANIMATION_FEEDBACK);
        }
        mMode = MODE.NONE;
    }

    @Override
    public void onDown(float x, float y) {
        Log.v(TAG, "onDown");
        mMode = MODE.DRAG;
    }

    @Override
    public void onUp() {
        Log.v(TAG, "onUp");
        mMode = MODE.NONE;
    }

    void onTouchDown(MotionEvent event) {
        mMode = MODE.DRAG;

        mCurrentX = (int) event.getRawX();
        mCurrentY = (int) event.getRawY();

        mStartX = (int) event.getX();
        mStartY = mCurrentY - this.getTop();

    }

    void onPointerDown(MotionEvent event) {
        if (event.getPointerCount() == 2) {
            mMode = MODE.SCALE;
            mBeforeLenght = getDistance(event);
        }
    }

    void onTouchMove(MotionEvent event) {
        int left = 0, top = 0, right = 0, bottom = 0;

        if (mMode == MODE.DRAG) {
            left = mCurrentX - mStartX;
            right = mCurrentX + this.getWidth() - mStartX;
            top = mCurrentY - mStartY;
            bottom = mCurrentY - mStartY + this.getHeight();

            if (isControlHorizontal) {
                if (left >= 0) {
                    left = 0;
                    right = this.getWidth();
                }
                if (right <= mDisplayWidth) {
                    left = mDisplayWidth - this.getWidth();
                    right = mDisplayWidth;
                }
            } else {
                left = this.getLeft();
                right = this.getRight();
            }

            if (isControlVertical) {
                if (top >= 0) {
                    top = 0;
                    bottom = this.getHeight();
                }

                if (bottom <= mDisplayHeight) {
                    top = mDisplayHeight - this.getHeight();
                    bottom = mDisplayHeight;
                }
            } else {
                top = this.getTop();
                bottom = this.getBottom();
            }
            if (isControlHorizontal || isControlVertical)
                this.setPosition(left, top, right, bottom);

            mCurrentX = (int) event.getRawX();
            mCurrentY = (int) event.getRawY();

        }
    }

    float getDistance(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);

        return FloatMath.sqrt(x * x + y * y);
    }

    private void setPosition(int left, int top, int right, int bottom) {
        this.layout(left, top, right, bottom);
    }

    public void doScaleAnim(int anim) {
        myAsyncTask = new MyAsyncTask(this.getWidth(), this.getHeight(), anim);
        myAsyncTask.setRectangle(this.getLeft(), this.getTop(), this.getRight(), this.getBottom());
        myAsyncTask.execute();
    }

    class MyAsyncTask extends AsyncTask<Void, Integer, Void> {
        private int mCurrentWidth, mCurrentHeight;
        private int mLeft, mTop, mRight, mBottom;
        private float mStep = 8f;
        private float mStepH, mStepV;
        private int mAnimType;

        public MyAsyncTask(int width, int height, int anim) {
            super();
            mCurrentWidth = width;
            mCurrentHeight = height;
            mStepH = mStep;
            mStepV = mCurrentHeight * mStep / mCurrentWidth;
            mAnimType = anim;
        }

        public void setRectangle(int left, int top, int right, int bottom) {
            this.mLeft = left;
            this.mTop = top;
            this.mRight = right;
            this.mBottom = bottom;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (mAnimType == ANIMATION_FEEDBACK) {
                while (mCurrentWidth < mDisplayWidth) {
                    mLeft -= mStepH;
                    mTop -= mStepV;
                    mRight += mStepH;
                    mBottom += mStepV;
                    mCurrentWidth += 2 * mStepH;
    
                    mLeft = Math.max(mLeft, mStartRect.left);
                    mTop = Math.max(mTop, mStartRect.top);
                    mRight = Math.min(mRight, mStartRect.right);
                    mBottom = Math.min(mBottom, mStartRect.bottom);
                    
                    onProgressUpdate(new Integer[] {mLeft, mTop, mRight, mBottom});
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (mAnimType == ANIMATION_ZOOMIN) {
                while (mCurrentWidth < 1.25 * mDisplayWidth || mCurrentHeight < 1.25 * mDisplayHeight) {
                    mLeft -= mStepH;
                    mTop -= mStepV;
                    mRight += mStepH;
                    mBottom += mStepV;
                    mCurrentWidth += 2 * mStepH;
                    mCurrentHeight += 2 * mStepH;
    
                    mLeft = Math.min(mLeft, mStartRect.left);
                    mTop = Math.min(mTop, mStartRect.top);
                    mRight = Math.max(mRight, mStartRect.right);
                    mBottom = Math.max(mBottom, mStartRect.bottom);
                    
                    onProgressUpdate(new Integer[] {mLeft, mTop, mRight, mBottom});
                    try {
                        Thread.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (mAnimType == ANIMATION_ZOOMOUT) {
                float leftStep = (mLeft - mStartRect.left) / 20;
                float topStep = (mTop - mStartRect.top) / 20;
                float rightStep = (mRight - mStartRect.right) / 20;
                float bottomStep = (mBottom - mStartRect.bottom) / 20;
                
                while (mCurrentWidth > mDisplayWidth || mCurrentHeight > mDisplayHeight) {
                    mLeft = (int)(mLeft - leftStep);
                    mTop = (int)(mTop - topStep);
                    mRight = (int)(mRight - rightStep);
                    mBottom = (int)(mBottom - bottomStep);
                    
                    mLeft = Math.min(mLeft, mStartRect.left);
                    mTop = Math.min(mTop, mStartRect.top);
                    mRight = Math.max(mRight, mStartRect.right);
                    mBottom = Math.max(mBottom, mStartRect.bottom);
                    
                    onProgressUpdate(new Integer[] {mLeft, mTop, mRight, mBottom});
                    try {
                        Thread.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    
                    mCurrentWidth = getWidth();
                    mCurrentHeight = getHeight();
                }
            }
            
            return null;
        }

        @Override
        protected void onProgressUpdate(final Integer... values) {
            super.onProgressUpdate(values);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setFrame(values[0], values[1], values[2], values[3]);
                    recenter();
                }
            });
        }
    }

	@Override
	public boolean onSingleTapConfirmed(float x, float y) {
		return true;
	}
}
