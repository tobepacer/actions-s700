package com.actions.music.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.util.Log;

import com.actions.mediacenter.utils.CommonUtil;

public class PlayList implements Observer {
	private static final String TAG = PlayList.class.getSimpleName();
	
	private MusicItemCache mMusicItemCache;
	private List<ListItem> mPlayList;
	private List<ListItem> mPlayListCopy;
	private MusicLooper mMusicLooper;
	private PlayRecord mPlayRecord;
	private OnPlayListUpdateListener mOnPlayListUpdateListener;

	public PlayList(Context context, PlayRecord playRecord) {
		mMusicItemCache = MusicItemCache.getInstance(context);
		mMusicLooper = MusicLooper.getInstance(context);
		mPlayRecord = playRecord;

		reset();
		if (mMusicLooper.isRandomMode()) {
			shuffle();
		}
		mMusicItemCache.addObserver(this);
		ListItem currentItem = mPlayRecord.getCurrentItem();
		if(currentItem!=null) {
			int newIndex = getIndexById(currentItem);
			if(newIndex!=-1) {
				if(newIndex!=currentItem.index) {
					currentItem.index = newIndex;
				}
			} else {
				mPlayRecord.save(0);
				mPlayRecord.removeFirst();
			}
		}
	}

	@Override
	public void update(Observable observable, Object data) {
		Log.d(TAG, "update");
		reset();
		if (mMusicLooper.isRandomMode()) {
			shuffle();
		}
		
		if (mOnPlayListUpdateListener!=null) {
			mOnPlayListUpdateListener.onPlayListUpdate();
		}
	}

	public int getIndex(int position) {
		if (CommonUtil.isAvaiablePosition(position, mPlayList)) {
			return mPlayList.get(position).index;
		}
		return -1;
	}
	
	public ListItem getItem(int position) {
		return mPlayList.get(position);
	}

	public int size() {
		return mPlayList.size();
	}

	public void shuffle() {
		Collections.shuffle(mPlayList);
		mPlayRecord.clear();
	}

	public void reset() {
		Log.d(TAG, "reset mMusicItemCache size:"+mMusicItemCache.size());
		mPlayList = new ArrayList<>(mMusicItemCache.size());
		for (int i = 0; i < mMusicItemCache.size(); i++) {
			ListItem item = new ListItem();
			item.index = i;
			item.id = mMusicItemCache.getItem(i).getID();
			mPlayList.add(item);
		}
		mPlayListCopy = new ArrayList<>();
		mPlayListCopy.addAll(mPlayList);
		Log.d("playlist", "mPlayList size:"+mPlayList.size());
	}

	public int navigate(boolean forward, boolean fromUser) {
		if (size() == 0) {
			return -1;
		}
		int result = -1;
		if (fromUser) {
			if (mMusicLooper.isRandomMode()) {
				if (!forward) {
					Collections.shuffle(mPlayList);
					result = mPlayList.indexOf(mPlayRecord.getLastItem());
				} else {
					result = (mPlayList.indexOf(mPlayRecord.getCurrentItem()) + 1) % size();
				}
			} else {
				if (forward) {
					result = (mPlayRecord.getCurrentIndex() + 1) % size();
				} else {
					result = (mPlayRecord.getCurrentIndex() - 1 + size()) % size();
				}
			}
		} else {
			if (mMusicLooper.isSingleMode()) {
				result = mPlayRecord.getCurrentIndex();
			} else if (mMusicLooper.isOrderMode()) {
				if (mPlayRecord.getCurrentIndex() != size() - 1) {
					result = mPlayRecord.getCurrentIndex() + 1;
				}
			} else if (mMusicLooper.isLoopMode()) {
				result = (mPlayRecord.getCurrentIndex() + 1) % size();
			} else if (mMusicLooper.isRandomMode()) {
				result = (mPlayList.indexOf(mPlayRecord.getCurrentItem()) + 1) % size();
			}
		}
		return result;
	}
	
	public int getIndexById(ListItem item) {
		return mPlayListCopy.indexOf(item);
	}
	
	public ListItem getItemByPosition(int position) {
		if(CommonUtil.isAvaiablePosition(position, mPlayListCopy)) {
			return mPlayListCopy.get(position);
		}
		return null;
	}

	public class ListItem {
		public int index = 0;
		public int id = 0;

		public ListItem(){
			
		}
		
		public ListItem(int position, int id) {
			super();
			this.index = position;
			this.id = id;
		}

		@Override
		public boolean equals(Object o) {
			if (o instanceof ListItem) {
				return ((ListItem) o).id == id;
			}
			return false;
		}

	}
	
	public void setOnPlayListUpdateListener(OnPlayListUpdateListener listener){
		mOnPlayListUpdateListener = listener;
	}
	
	public interface OnPlayListUpdateListener{
		public void onPlayListUpdate();
	}
}
