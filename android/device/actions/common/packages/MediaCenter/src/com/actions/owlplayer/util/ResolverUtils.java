package com.actions.owlplayer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Video.VideoColumns;
import android.util.Log;

import com.actions.owlplayer.data.LocalVideo;
import com.actions.owlplayer.data.MediaItem;

public class ResolverUtils {
	private static String TAG = "ResolverUtils";

	public static final String COLUMN_VIDEO_ID = "_id";
	public static final String COLUMN_DATA = "_data";
	public static final String COLUMN_DISPLAY_NAME = "_display_name";
	public static final String COLUMN_MIME_TYPE = "mime_type";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_DURATION = "duration";
	public static final String COLUMN_BUCKET_ID = "bucket_id";
	public static final String COLUMN_BUCKET_NAME = "bucket_display_name";
	public static final String COLUMN_RESOLUTION = "resolution";
	public static final String COLUMN_DATE_MODIFIED = "date_modified";
	public static final String COLUMN_FILE_SIZE = "_size";

	private static final String[] PROJECTION = new String[] { VideoColumns._ID, VideoColumns.DATA, VideoColumns.DISPLAY_NAME, VideoColumns.SIZE,
			VideoColumns.MIME_TYPE, VideoColumns.TITLE, VideoColumns.DURATION, VideoColumns.BUCKET_ID, VideoColumns.BUCKET_DISPLAY_NAME,
			VideoColumns.RESOLUTION, VideoColumns.DATE_MODIFIED, VideoColumns.SIZE };

	public static Cursor getExternalVideoCursor(Context context) {
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(Video.Media.EXTERNAL_CONTENT_URI, PROJECTION, null, null, null);
		return cursor;
	}

	public static Cursor getSingleVideoCursor(Context context, Uri uri) {
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, PROJECTION, null, null, null);
		return cursor;
	}

	public static Map<String, Integer> getColumnIndex(Cursor cursor) {
		try {
			Map<String, Integer> columnIndexMap = new HashMap<String, Integer>();
			columnIndexMap.put(COLUMN_VIDEO_ID, cursor.getColumnIndexOrThrow(COLUMN_VIDEO_ID));
			columnIndexMap.put(COLUMN_DATA, cursor.getColumnIndexOrThrow(COLUMN_DATA));
			columnIndexMap.put(COLUMN_DISPLAY_NAME, cursor.getColumnIndexOrThrow(COLUMN_DISPLAY_NAME));
			columnIndexMap.put(COLUMN_MIME_TYPE, cursor.getColumnIndexOrThrow(COLUMN_MIME_TYPE));
			columnIndexMap.put(COLUMN_TITLE, cursor.getColumnIndexOrThrow(COLUMN_TITLE));
			columnIndexMap.put(COLUMN_DURATION, cursor.getColumnIndexOrThrow(COLUMN_DURATION));
			columnIndexMap.put(COLUMN_BUCKET_ID, cursor.getColumnIndexOrThrow(COLUMN_BUCKET_ID));
			columnIndexMap.put(COLUMN_BUCKET_NAME, cursor.getColumnIndexOrThrow(COLUMN_BUCKET_NAME));
			columnIndexMap.put(COLUMN_RESOLUTION, cursor.getColumnIndexOrThrow(COLUMN_RESOLUTION));
			columnIndexMap.put(COLUMN_DATE_MODIFIED, cursor.getColumnIndexOrThrow(COLUMN_DATE_MODIFIED));
			columnIndexMap.put(COLUMN_FILE_SIZE, cursor.getColumnIndexOrThrow(COLUMN_FILE_SIZE));
			return columnIndexMap;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static void parseResolution(String resolution, MediaItem item) {
		if (resolution == null)
			return;
		int m = resolution.indexOf('x');
		if (m == -1)
			return;
		try {
			item.width = Integer.parseInt(resolution.substring(0, m));
			item.height = Integer.parseInt(resolution.substring(m + 1));
		} catch (Throwable t) {
			Log.w(TAG, t);
		}
	}

	public static ArrayList<MediaItem> getMediaItems(Context context, Cursor cursor) {
		ArrayList<MediaItem> items = new ArrayList<MediaItem>();

		Map<String, Integer> columnIndexMap = ResolverUtils.getColumnIndex(cursor);
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			MediaItem mediaItem = new LocalVideo(context);
			mediaItem.videoId = cursor.getInt(columnIndexMap.get(ResolverUtils.COLUMN_VIDEO_ID));
			mediaItem.data = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_DATA));
			mediaItem.displayName = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_DISPLAY_NAME));
			mediaItem.mimeType = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_MIME_TYPE));
			mediaItem.title = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_TITLE));
			mediaItem.duration = cursor.getInt(columnIndexMap.get(ResolverUtils.COLUMN_DURATION));
			mediaItem.bucketId = cursor.getInt(columnIndexMap.get(ResolverUtils.COLUMN_BUCKET_ID));
			mediaItem.bucketName = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_BUCKET_NAME));
			mediaItem.dateModified = cursor.getLong(columnIndexMap.get(ResolverUtils.COLUMN_DATE_MODIFIED));
			mediaItem.fileSize = cursor.getLong(columnIndexMap.get(ResolverUtils.COLUMN_FILE_SIZE));
			parseResolution(cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_RESOLUTION)), mediaItem);
			items.add(mediaItem);
		}
		return items;
	}

	public static MediaItem getMediaItem(Context context, Cursor cursor, MediaItem item) {
		if (cursor == null || cursor.getCount() != 1)
			return null;

		Map<String, Integer> columnIndexMap = ResolverUtils.getColumnIndex(cursor);
		cursor.moveToFirst();
		item.videoId = cursor.getInt(columnIndexMap.get(ResolverUtils.COLUMN_VIDEO_ID));
		item.data = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_DATA));
		item.displayName = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_DISPLAY_NAME));
		item.mimeType = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_MIME_TYPE));
		item.title = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_TITLE));
		item.duration = cursor.getInt(columnIndexMap.get(ResolverUtils.COLUMN_DURATION));
		item.bucketId = cursor.getInt(columnIndexMap.get(ResolverUtils.COLUMN_BUCKET_ID));
		item.bucketName = cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_BUCKET_NAME));
		item.dateModified = cursor.getLong(columnIndexMap.get(ResolverUtils.COLUMN_DATE_MODIFIED));
		item.fileSize = cursor.getLong(columnIndexMap.get(ResolverUtils.COLUMN_FILE_SIZE));
		parseResolution(cursor.getString(columnIndexMap.get(ResolverUtils.COLUMN_RESOLUTION)), item);
		return item;
	}

	public static void getVideoBuckets() {

	}
}