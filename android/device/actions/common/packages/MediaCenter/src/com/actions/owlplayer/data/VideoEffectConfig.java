package com.actions.owlplayer.data;

import java.io.StringWriter;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;

import com.actions.owlplayer.util.Preferences;

public class VideoEffectConfig {
	private ArrayList<VideoEffect> mEffectList = new ArrayList<VideoEffectConfig.VideoEffect>();

	public VideoEffectConfig(Context context) {
		mEffectList.clear();
		changeConfig(Preferences.VIDEO_DEINTERLACING, (Boolean) Preferences.getPreferences(context, Preferences.VIDEO_DEINTERLACING, false));
		changeConfig(Preferences.DISPLAY_ENHANCE, (Boolean) Preferences.getPreferences(context, Preferences.DISPLAY_ENHANCE, false));
		changeConfig(Preferences.VISION_CROP, (Boolean) Preferences.getPreferences(context, Preferences.VISION_CROP, false));
		changeConfig(Preferences.POWER_SAVING, (Boolean) Preferences.getPreferences(context, Preferences.POWER_SAVING, false));

	}

	private class VideoEffect {
		public String mName;
		public String mValue;

		VideoEffect(String name, boolean value) {
			mName = name;
			mValue = Boolean.toString(value);
		}
	}

	public void changeConfig(String name, boolean value) {
		for (VideoEffect ve : mEffectList) {
			if (ve.mName.equals(name)) {
				mEffectList.remove(ve);
				break;
			}
		}

		mEffectList.add(new VideoEffect(name, value));
	}

	public String produceXml() {
		StringWriter writer = new StringWriter();

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlSerializer serializer = factory.newSerializer();
			serializer.setOutput(writer);

			serializer.startDocument("utf-8", true);
			serializer.startTag(null, "map");

			for (VideoEffect ve : mEffectList) {
				serializer.startTag(null, "boolean");

				serializer.attribute(null, "name", ve.mName);
				serializer.attribute(null, "value", ve.mValue);

				serializer.endTag(null, "boolean");
			}

			serializer.endTag(null, "map");
			serializer.endDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return writer.toString();
	}
}
