/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actions.owlplayer.widget;

public class CustomTrackInfo {
	public static final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
	public static final int MEDIA_TRACK_TYPE_VIDEO = 1;
	public static final int MEDIA_TRACK_TYPE_AUDIO = 2;
	public static final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;

	public CustomTrackInfo(String name, String language, int type, int index) {
		mTrackName = name;
		mTrackType = type;
		mLanguage = language;
		mEmbeddedIndex = index;
	}

	public CustomTrackInfo(String name, String language, int type) {
		this(name, language, type, -1);
	}

	public String getTrackName() {
		return mTrackName;
	}

	/**
	 * Gets the track type.
	 * 
	 * @return TrackType which indicates if the track is video, audio, timed text.
	 */
	public int getTrackType() {
		return mTrackType;
	}

	/**
	 * Gets the language code of the track.
	 * 
	 * @return a language code in either way of ISO-639-1 or ISO-639-2. When the language is unknown or could not be determined, ISO-639-2 language code, "und",
	 *         is returned.
	 */
	public String getLanguage() {
		return mLanguage;
	}

	public int getEmbeddedIndex() {
		return mEmbeddedIndex;
	}

	public void setEmbededIndex(int index) {
		mEmbeddedIndex = index;
	}

	final String mTrackName;
	final int mTrackType;
	final String mLanguage;
	private int mEmbeddedIndex;
}
