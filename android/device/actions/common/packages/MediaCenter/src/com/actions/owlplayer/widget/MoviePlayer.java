package com.actions.owlplayer.widget;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actions.mediacenter.R;
import com.actions.owlplayer.sub.HwSubtitleManager;
import com.actions.owlplayer.sub.SubtitleManager;
import com.actions.owlplayer.util.Preferences;
import com.actions.owlplayer.util.Utils;

public class MoviePlayer implements View.OnTouchListener, View.OnClickListener, AudioManager.OnAudioFocusChangeListener {

	private final String TAG = "MoviePlayer";
	private static final int sDefaultUpateInterval = 500;

	private static final int AUTO_UPDATE = 1;

	public static final String KEY_VIDEO_POSITION = "video-position";
	public static final String KEY_SHOW_MESSAGE = "position-message";
	public static final String KEY_FULL_NAME = "full-name";
	public static final String KEY_VOLUME_MUTE = "volume_mute";
	public static final String KEY_AUDIO_TRACK = "audio_track";
	public static final String KEY_SUB_TRACK = "sub_track";
	public static final String KEY_SET_VOLUME = "volume_set";
	public static final String KEY_WIDTH = "width";
	public static final String KEY_HEIGHT = "height";
	// in ms
	private static final int DELTA_THRESHOLD = 100;

	private static final int AUDIO_TRACK_OFFSET = 0;
	private static boolean mVitamioUsing = false;

	private Context mContext = null;
	AudioManager mAudioManager = null;
	private MovieViewInterface mPlayer;
	private Uri mUri;
	private View mRoot;
	private View mView;
	private ImageButton mSubtitleButton;
	private ImageButton mAudioButton;
	private TextView mSubLayer;
	private ProgressBar mProgressLoad;
	// window cover to make sure the window figure can be notified
	// otherwise, the window is transparent while surface is visible
	private ImageView mWindowCover;
	private MovieController mController;
	private SubtitleManager mSubtitleManager;
	// time set by caller
	private int mPosition = 0;
	// time fetched by auto-update
	private int mPeriodPosition;
	private boolean mUseHardware;
	private Listener mListener = null;
	private String mFilename;
	private float mLeftVolume = 1.0f;
	private float mRightVolume = 1.0f;
	private SparseArray<CustomTrackInfo> mTrackInfo = new SparseArray<CustomTrackInfo>();
	private int mAudioTrackSelected = 0;
	private int mSubTrackSelected = -1;
	// dismiss while destroy
	private AlertDialog mAlertDialog = null;
	private AudioBecomingNoisyReceiver mNoisyReceiver = new AudioBecomingNoisyReceiver();

	/*
	 * Create MoviePlayer, which will create controller inside use MoviePlayer to simplify your application because it takes care of both VideoView and
	 * Controller
	 * 
	 * @param parent The parent view or Layout of surfaceview/layout
	 * 
	 * @param root The root view
	 */
	public MoviePlayer(Context context, View root) {
		Log.v(TAG, "create:" + this);

		mContext = context;
		mRoot = root;

		mController = new MovieController(mContext, mRoot);

		mView = mRoot.findViewById(R.id.surface_view);
		if (mView != null) {
			mView.setOnTouchListener(this);
		}

		mProgressLoad = (ProgressBar) mRoot.findViewById(R.id.loading);
		mWindowCover = (ImageView) mRoot.findViewById(R.id.window_cover);
		init2((MovieView) mView);

		mSubtitleButton = (ImageButton) mRoot.findViewById(R.id.subtitle);
		if (mSubtitleButton != null) {
			mSubtitleButton.setOnClickListener(this);
		}

		mAudioButton = (ImageButton) mRoot.findViewById(R.id.audio);
		if (mAudioButton != null) {
			mAudioButton.setOnClickListener(this);
		}

		mSubLayer = (TextView) mRoot.findViewById(R.id.subtitle_layer);
		if (mSubLayer != null) {
			mSubLayer.setTextSize((float) Integer.parseInt((String) Preferences.getPreferences(mContext, Preferences.SUBTITLE_SIZE, Integer.toString(30))));

			mSubLayer
					.setTextColor(Integer.parseInt((String) Preferences.getPreferences(mContext, Preferences.SUBTITLE_COLOR, Integer.toString(0xffffff))) | 0xff000000);
		}

		mTimer.schedule(mTask, sDefaultUpateInterval, sDefaultUpateInterval);

		mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

		mNoisyReceiver.register();
	}

	public void onAudioFocusChange(int focusChange) {
		// TODO fill code while audio focus changed
		Log.v(TAG, "onAudioFocusChange: " + focusChange);
		return;
	}

	private void stopBackgroundMusic() {
		int focus = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		if (focus != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
			Log.i(TAG, "Audio focus request failed!");
		}
	}

	void preNewPlay() {
		stopBackgroundMusic();
		stopPlayback();
	}

	private boolean isValidExtension() {
		String extensionAll = Utils.getFilterOutExtensions();
		if (extensionAll != null) {
			String[] extensions = extensionAll.split(",");
			for (String suffix : extensions) {
				if (mFilename.toLowerCase().endsWith("." + suffix.toLowerCase())) {
					Log.i(TAG, "file extension not support:" + mFilename);
					Toast.makeText(mContext, R.string.message_unknown_error, Toast.LENGTH_SHORT).show();
					if (mListener != null) {
						mListener.onError(0, 0);
					}

					return false;
				}
			}
		}

		return true;
	}

	public void setIntent(Intent intent) {
		Log.v(TAG, "setIntent:" + this);
		preNewPlay();

		mUri = intent.getData();
		mPosition = intent.getIntExtra(KEY_VIDEO_POSITION, 0);
		mAudioTrackSelected = intent.getIntExtra(KEY_AUDIO_TRACK, 0);
		mSubTrackSelected = intent.getIntExtra(KEY_SUB_TRACK, -1);
		mFilename = intent.getStringExtra(KEY_FULL_NAME);
		if (mFilename != null) {
			Log.d(TAG, "playback file:" + mFilename);
		}

		if (mFilename != null && !isValidExtension()) {
			return;
		}

		if (intent.getBooleanExtra(KEY_VOLUME_MUTE, false)) {
			mLeftVolume = mRightVolume = 0.0f;
		} else {
			mLeftVolume = mRightVolume = intent.getFloatExtra(MoviePlayer.KEY_SET_VOLUME, mLeftVolume);
		}

		mUseHardware = true;
		/*
		 * KEY_SHOW_MESSAGE is used to distinguish window/activity switch, if true, need seek anyway
		 */
		if (intent.getBooleanExtra(KEY_SHOW_MESSAGE, true)) {
			if ((Boolean) Preferences.getPreferences(mContext, Preferences.SEEK_AND_PLAY, true)) {
				if (mPosition > 0 && (Boolean) Preferences.getPreferences(mContext, Preferences.REMINDER_DIALOG, false)) {
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.HoloSmallDialog);
					builder.setTitle(R.string.dialog_resume_playing_title);
					builder.setMessage(mContext.getString(R.string.message_notify_bookmark, Utils.convertMsToHHMMSS(mPosition)));
					builder.setCancelable(false);
					builder.setPositiveButton(R.string.dialog_resume_playing_resume, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							init();
						}
					});
					builder.setNegativeButton(R.string.dialog_resume_playing_restart, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mPosition = 0;
							init();
						}
					});

					// for show in Window
					mAlertDialog = builder.create();
					mAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
					mAlertDialog.show();
				} else {
					init();
				}
			} else {
				mPosition = 0;
				init();
			}
		} else {
			init();
		}
	}

	private void init2(MovieViewInterface player) {
		if (player == null) {
			return;
		}

		player.setOnPreparedListener(mPreparedListener);
		player.setOnCompletionListener(mCompletionListener);
		player.setOnErrorListener(mErrorListener);
		player.setLayoutController(mController);
	}

	private void init() {
		if (mUseHardware) {
			if (mView != null) {
				mPlayer = (MovieView) mView;
			}

		}

		if (mSubLayer != null) {
			mSubLayer.setVisibility(View.GONE);
		}

		if (mPlayer == null || mUri == null) {
			// keep the controller layout on
			Log.w(TAG, "null init");
		} else {
			if (mController != null) {
				mController.setMediaPlayer(mLocalControl);
			}

			mPlayer.setVideoURI(mUri);
			// if input is unusual stream , it maybe stopPlayback before seekto, so add NPE check
			if (mPlayer != null) {
				mPlayer.seekTo(mPosition);
				mPlayer.start();
			}

			mPeriodPosition = 0;
		}

		showProgress();
	}

	void showProgress() {
		if (mProgressLoad != null) {
			mProgressLoad.setVisibility(View.VISIBLE);
		}
	}

	private void setCoverVisible(int visibility) {
		if (mWindowCover != null) {
			mWindowCover.setVisibility(visibility);
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case AUTO_UPDATE:
				if (mPlayer != null && mPlayer.isPlaying()) {
					int position = mPlayer.getCurrentPosition();
					if (position > 0) {
						mPeriodPosition = position;
					}
					if (mSubtitleManager != null && mSubLayer != null) {
						String displaySub = mSubtitleManager.updateSubtitle(mPeriodPosition);
						if (displaySub != null) {
							/*
							 * FIXME to avoid the alias when textview overlap with surfaceview, the dirty area keeps visible
							 */
							if (mRoot.getBackground() == null) {
								mRoot.setBackgroundColor(Color.BLACK);
							}

							mSubLayer.setText(displaySub);
						}
					}
				}

				break;
			}
		}
	};

	public void onDestroy() {
		Log.v(TAG, "onDestroy:" + this);
		if (mController != null) {
			mController.setMediaPlayer(null);
		}

		if (mPlayer != null) {
			mPlayer.setLayoutController(null);
		}

		mTimer.cancel();
		mHandler.removeMessages(AUTO_UPDATE);
		mHandler = null;
		mTimer.purge();
		mTimer = null;

		if (mSubtitleManager != null) {
			mSubtitleManager.releaseSub();
		}
		mTrackInfo.clear();

		if (mAlertDialog != null) {
			mAlertDialog.dismiss();
			mAlertDialog = null;
		}

		hideProgress();

		if (mView != null) {
			mView.setOnTouchListener(null);
		}

		if (mSubtitleButton != null) {
			mSubtitleButton.setOnClickListener(null);
		}

		if (mAudioButton != null) {
			mAudioButton.setOnClickListener(null);
		}

		mAudioManager.abandonAudioFocus(this);
		stopPlayback();

		if (mController != null) {
			mController.onDestroy();
			mController = null;
		}

		mNoisyReceiver.unregister();
	}

	private void stopPlayback() {
		if (mPlayer != null) {
			setCoverVisible(View.VISIBLE);

			mPlayer.stopPlayback();
			mPlayer = null;

			if (!mUseHardware) {
				setVitamioInUse(false);
			}
		}
	}

	private final Runnable mRemoveCover = new Runnable() {
		@Override
		public void run() {
			setCoverVisible(View.INVISIBLE);
		}
	};

	private MovieViewInterface.OnPreparedListener mPreparedListener = new MovieViewInterface.OnPreparedListener() {
		public void onPrepared() {
			Log.d(TAG, "onPrepared");
			if (mWindowCover != null) {
				mHandler.removeCallbacks(mRemoveCover);
				mHandler.postDelayed(mRemoveCover, 500);
			}
			// **
			if (mUseHardware) {
				mSubtitleManager = new HwSubtitleManager(mPlayer, mTrackInfo, mSubTrackSelected);
			} 
			mPlayer.setVolume(mLeftVolume, mRightVolume);

			mTrackInfo.clear();
			initAudioTracks();

			boolean hasSubTrack = false;
			hasSubTrack = mSubtitleManager.initSubTracks(mFilename);
			if (mSubtitleButton != null) {
				if (!hasSubTrack) {
					mSubtitleButton.setEnabled(false);
					mSubtitleButton.setFocusable(false);
					mSubtitleButton.setAlpha(0.3f);
				} else {
					mSubtitleButton.setEnabled(true);
					mSubtitleButton.setFocusable(true);
					mSubtitleButton.setAlpha(1.0f);
				}
			}

			if (mSubTrackSelected != -1 && mSubLayer != null) {
				mSubLayer.setVisibility(View.VISIBLE);
			}
			hideProgress();
			show();
		}

		private void initAudioTracks() {
			// only embedded audio is fetched from mPlayer
			int track = 0;
			int size = mTrackInfo.size();
			CustomTrackInfo[] trackInfo = mPlayer.getTrackMetadata();
			if (trackInfo != null) {
				track = AUDIO_TRACK_OFFSET;
				for (CustomTrackInfo t : trackInfo) {
					if (t.getTrackType() == CustomTrackInfo.MEDIA_TRACK_TYPE_AUDIO) {
						mTrackInfo.append(track, t);
					}

					track++;
				}
			}

			if (mAudioButton != null) {
				if (trackInfo == null || mTrackInfo.size() <= size) {
					mAudioButton.setEnabled(false);
					mAudioButton.setFocusable(false);
					mAudioButton.setAlpha(0.3f);
				} else {
					mAudioButton.setEnabled(true);
					mAudioButton.setFocusable(true);
					mAudioButton.setAlpha(1.0f);
				}
			}

			if (mAudioTrackSelected > 0) {
				selectAudioTrack(mAudioTrackSelected);
			}
		}

	};

	private MovieViewInterface.OnCompletionListener mCompletionListener = new MovieViewInterface.OnCompletionListener() {
		public void onCompletion() {
			if (mListener != null) {
				mListener.onCompletion();
			}
		}
	};

	private MovieViewInterface.OnErrorListener mErrorListener = new MovieViewInterface.OnErrorListener() {
		public boolean onError(int what, int extra) {
			Log.e(TAG, "onError");
			stopPlayback();
			onErrorDialog();

			return true;
		}
	};

	private Timer mTimer = new Timer();
	private TimerTask mTask = new TimerTask() {
		public void run() {
			Message message = new Message();
			message.what = AUTO_UPDATE;
			mHandler.sendMessage(message);
		}
	};

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (mListener != null) {
			return mListener.onTouch(event);
		}

		return false;
	}

	private final DialogInterface.OnClickListener mAudioOptionListener = new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
			selectAudioTrack(which);

			mAudioTrackSelected = which;

			// to update the controller
			if (mController != null) {
				mController.togglePlay();
			}

			dialog.dismiss();
		}
	};

	private void showAudioTrackDialog() {
		ArrayList<String> list = new ArrayList<String>();
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.HoloSmallDialog);
		builder.setTitle(R.string.dialog_audio_option_title);
		int track = 0;
		for (int i = 0; i < mTrackInfo.size(); i++) {
			CustomTrackInfo t = mTrackInfo.valueAt(i);
			if (t.getTrackType() == CustomTrackInfo.MEDIA_TRACK_TYPE_AUDIO) {
				list.add(mContext.getString(R.string.message_audio_track, track, t.getLanguage()));
				track++;
			}
		}

		builder.setSingleChoiceItems(list.toArray(new String[0]), mAudioTrackSelected, mAudioOptionListener);
		builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// to update the controller
				if (mController != null) {
					mController.togglePlay();

				}
				dialog.dismiss();
			}
		});

		mAlertDialog = builder.create();
		mAlertDialog.show();
	}

	private void showSubTrackDialog() {
		ArrayList<String> list = new ArrayList<String>();
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.HoloSmallDialog);
		builder.setTitle(R.string.dialog_subtitle_option_title);
		list = mSubtitleManager.getAlertDialogShowList(mContext);

		builder.setSingleChoiceItems(list.toArray(new String[0]), mSubTrackSelected, mSingleChoiceClickListener);
		builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// to update the controller
				if (mController != null) {
					mController.togglePlay();
				}
				dialog.dismiss();
			}
		});

		mAlertDialog = builder.create();
		mAlertDialog.show();
	}

	private OnClickListener mSingleChoiceClickListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			mSubTrackSelected = mSubtitleManager.selectSubTrack(which);
			mSubLayer.setVisibility(View.VISIBLE);
			// to update the controller
			if (mController != null) {
				mController.togglePlay();
			}

			dialog.dismiss();
		}
	};

	@Override
	public void onClick(View v) {
		if (mPlayer == null || mController == null) {
			return;
		}

		mPlayer.pause();
		switch (v.getId()) {
		case R.id.audio:
			showAudioTrackDialog();
			break;
		case R.id.subtitle:
			showSubTrackDialog();
			break;
		}

		show();
	}

	private class Client extends IMediaPlayerControl.Stub {
		public void start() {
			if (mPlayer != null) {
				mPlayer.start();
			}
		}

		public void pause() {
			if (mPlayer != null) {
				mPlayer.pause();
			}
		}

		// cache duration as mDuration for faster access
		public int getDuration() {
			int duration = 0;
			if (mPlayer != null) {
				duration = mPlayer.getDuration();
			}

			return duration;
		}

		public int getCurrentPosition() {
			int position = 0;
			if (mPlayer != null) {
				position = mPlayer.getCurrentPosition();
			}

			return position;
		}

		public void seekTo(int msec) {
			if (mPlayer != null) {
				mPlayer.seekTo(msec);
			}
		}

		public boolean isPlaying() {
			boolean status = false;
			if (mPlayer != null) {
				status = mPlayer.isPlaying();
			}

			return status;
		}

		public int getBufferPercentage() {
			int percentage = 0;
			if (mPlayer != null) {
				percentage = mPlayer.getBufferPercentage();
			}

			return percentage;
		}
	}

	private final IMediaPlayerControl mLocalControl = new Client();
	private IMediaPlayerControl mRemoteControl = null;

	public IMediaPlayerControl getMediaPlayerControl() {
		return (IMediaPlayerControl) mLocalControl;
	}

	public void setMediaPlayerControl(IMediaPlayerControl control) {
		mRemoteControl = control;
		if (mController != null) {
			mController.setMediaPlayer(mRemoteControl);
		}
	}

	public int setDeltaPercent(float percent, boolean done) {
		if (mPlayer == null || mController == null) {
			return 0;
		}

		if (mPlayer.isPlaying()) {
			mPlayer.pause();
		}

		if (done) {
			// to update the controller
			mController.togglePlay();
		}

		int duration = mPlayer.getDuration();
		int delta = (int) (percent * duration);
		if (Math.abs(delta) < DELTA_THRESHOLD) {
			return 0;
		} else {
			int pos = mPlayer.getCurrentPosition() + delta;
			if (pos >= 0 || pos <= duration) {
				mPlayer.seekTo(pos);
			}
		}

		return delta;
	}

	public int setDeltaPercentWithLimit(float percent, boolean done, int limitSeconds) {
		if (mPlayer == null || mController == null) {
			return 0;
		}

		if (mPlayer.isPlaying()) {
			mPlayer.pause();
		}

		if (done) {
			// to update the controller
			mController.togglePlay();
		}

		int duration = mPlayer.getDuration();
		int delta = (int) (percent * duration);
		if (Math.abs(delta) < limitSeconds * 1000) {
			delta = (int) (percent / Math.abs(percent) * limitSeconds * 1000);
		}
		int pos = mPlayer.getCurrentPosition() + delta;
		if (pos >= 0 && pos <= duration) {
			mPlayer.seekTo(pos);
		} else if (pos < 0) {
			mPlayer.seekTo(0);
		} else {
			mPlayer.seekTo(duration);
		}
		return delta;
	}

	public void setVolume(float left, float right) {

		if (left > 1.0f) {
			left = 1.0f;
		} else if (left < 0.0f) {
			left = 0.0f;
		}
		if (right > 1.0f) {
			right = 1.0f;
		} else if (right < 0.0f) {
			right = 0.0f;
		}

		mLeftVolume = left;
		mRightVolume = right;
		if (mPlayer != null) {
			mPlayer.setVolume(mLeftVolume, mLeftVolume);
		}
	}

	public void selectTrack(int index) {
		if (mPlayer != null) {
			mPlayer.selectTrack(index);
		}
	}

	public void setListener(Listener l) {
		mListener = l;
	}

	public int getPosition() {
		int position = 0;
		if (mPlayer != null) {
			position = mPlayer.getCurrentPosition();
		}

		return (position <= 0) ? mPeriodPosition : position;
	}

	public int getAudioTrack() {
		return mAudioTrackSelected;
	}

	public int getSubTrack() {
		return mSubTrackSelected;
	}

	public float getVolume() {
		return mLeftVolume;
	}

	public void show() {
		if (mController != null) {
			mController.show();
		}
	}

	public static void setVitamioInUse(boolean inUse) {
		mVitamioUsing = inUse;
	}

	public static boolean isVitamioInUse() {
		return mVitamioUsing;
	}

	public boolean onExtraTouchEvent(MotionEvent event) {
		if (mPlayer == null) {
			return false;
		}

		return mPlayer.onExtraTouchEvent(event);
	}

	private void selectAudioTrack(int which) {
		if (mPlayer == null) {
			return;
		}

		mAudioTrackSelected = 0;

		int track = 0;
		for (int i = 0; i < mTrackInfo.size(); i++) {
			CustomTrackInfo t = mTrackInfo.valueAt(i);
			if (t.getTrackType() == CustomTrackInfo.MEDIA_TRACK_TYPE_AUDIO) {
				if (track == which) {
					mPlayer.selectTrack(mTrackInfo.keyAt(i) - AUDIO_TRACK_OFFSET);
					mAudioTrackSelected = track;
					break;
				}

				track++;
			}
		}
	}

	public void togglePlay() {
		mController.togglePlay();
	}

	private void hideProgress() {
		if (mProgressLoad != null) {
			mProgressLoad.setVisibility(View.GONE);
		}
	}

	public void hide() {
		if (mController != null) {
			mController.hide();
		}
	}

	public boolean isPlaying() {
		if (mController != null) {
			return mController.isPlaying();
		}
		return false;
	}

	public interface Listener {
		abstract public void onCompletion();

		abstract public boolean onError(int what, int extra);

		abstract public boolean onTouch(MotionEvent event);
	}

	private class AudioBecomingNoisyReceiver extends BroadcastReceiver {
		public void register() {
			mContext.registerReceiver(this, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
		}

		public void unregister() {
			mContext.unregisterReceiver(this);
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			if (mPlayer != null && mPlayer.isPlaying())
				mPlayer.pause();
		}
	}

	private void onErrorDialog() {
		OnCancelListener onCancel = new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				((Activity) mContext).finish();
			}
		};
		DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.HoloSmallDialog).setTitle(R.string.message_unknown_error)
				.setIcon(R.drawable.ic_dialog_alert).setMessage(R.string.message_unsupport_error).setNegativeButton(android.R.string.ok, onClick)
				.setOnCancelListener(onCancel);
		mAlertDialog = builder.show();
	}

	public void pause() {
		mPlayer.pause();
	}
	
	public boolean fastForwardAndBackward(boolean forward) {
		if(mPlayer!=null) {
			return mController.fastForwardAndBackward(forward);
		}
		return true;
	}
}