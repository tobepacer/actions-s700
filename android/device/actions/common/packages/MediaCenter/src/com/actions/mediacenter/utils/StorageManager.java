package com.actions.mediacenter.utils;

import java.util.List;

import android.content.Context;
import android.os.Environment;

public class StorageManager {
	private String mExternalStorage;
	private String mSdStorage;
	private String mUhostStorage;
	private static Object mLock = new Object();
	private static StorageManager sStorageManager;
	
	private StorageManager(Context context){
		List<String> storageLists = CommonUtil.getAvailableStoragePaths(context);
		for(String path: storageLists){
			if(path.contains("sd")){
				mSdStorage = path;
			}
			if(path.contains("host")){
				mUhostStorage = path;
			}
		}
		mExternalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
	}
	
	public static StorageManager getInstance(Context context){
		if(sStorageManager==null){
			synchronized (mLock) {
				if(sStorageManager==null){
					sStorageManager = new StorageManager(context);
				}
			}
		}
		return sStorageManager;
	}
	
	public String getUhostStorage(){
		return mUhostStorage;
	}
	
	public String getSdStorage(){
		return mSdStorage;
	}
	
	public String getExternalStorage(){
		return mExternalStorage;
	}
}	
