package com.actions.image.data;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore.Images;

import com.actions.mediacenter.R;
import com.actions.mediacenter.data.MediaObject;
import com.actions.mediacenter.data.Path;
import com.actions.owlplayer.util.Utils;

public class LocalImage extends MediaObject implements Parcelable{
	private Path path;
	private boolean isSelected = false;
	private Context mContext;
	public int id;
	public String title;
	public String data;
	public int width = 0;
	public int height = 0;
	public long dateModified = 0;
	public long fileSize = 0;
	public Uri uri = null;
	public String mimeType = null;
	public String displayName = null;
	
	public static final String COLUMN_KEY_ID = "_id";
	public static final String COLUMN_KEY_DATA = "_data";
	public static final String COLUMN_KEY_SIZE = "_size";
	public static final String COLUMN_KEY_TITLE = "title";
	public static final String COLUMN_KEY_DISPLAYNAME = "_display_name";
	public static final String COLUMN_KEY_MIMETYPE = "mime_type";
	public static final String COLUMN_KEY_DATEMODIFIED = "date_modified";
	public static final String COLUMN_KEY_WIDTH = "width";
	public static final String COLUMN_KEY_HEIGHT = "height";
	
	public static final int COLUMN_INDEX_ID = 0;
	public static final int COLUMN_INDEX_DATA = 1;
	public static final int COLUMN_INDEX_SIZE = 2;
	public static final int COLUMN_INDEX_TITLE = 3;
	public static final int COLUMN_INDEX_DISPLAYNAME = 4;
	public static final int COLUMN_INDEX_MIMETYPE = 5;
	public static final int COLUMN_INDEX_DATEMODIFIED = 6;
	public static final int COLUMN_INDEX_WIDTH = 7;
	public static final int COLUMN_INDEX_HEIGHT = 8;
	
	public static final String[] PROJECTIONS = {COLUMN_KEY_ID, COLUMN_KEY_DATA,COLUMN_KEY_SIZE, COLUMN_KEY_TITLE, COLUMN_KEY_DISPLAYNAME, COLUMN_KEY_MIMETYPE, COLUMN_KEY_DATEMODIFIED, COLUMN_KEY_WIDTH, COLUMN_KEY_HEIGHT};
	
	public LocalImage(Context context) {
		mContext = context;
	}
	
	public LocalImage(Context context, Cursor cursor) {
		mContext = context;
		if(cursor!=null && !cursor.isClosed()){
			data = cursor.getString(COLUMN_INDEX_DATA);
			path = new Path(data);
			fileSize = cursor.getLong(COLUMN_INDEX_SIZE);
			id = cursor.getInt(COLUMN_INDEX_ID);
			title = cursor.getString(COLUMN_INDEX_TITLE);
			displayName = cursor.getString(COLUMN_INDEX_DISPLAYNAME);
			mimeType = cursor.getString(COLUMN_INDEX_MIMETYPE);
			dateModified = cursor.getLong(COLUMN_INDEX_DATEMODIFIED);
			width = cursor.getInt(COLUMN_INDEX_WIDTH);
			height = cursor.getInt(COLUMN_INDEX_HEIGHT);
		}
	}

	@Override
	public String getDetails() {
		StringBuilder details = new StringBuilder()
				.append(mContext.getString(R.string.media_detail_title) + "  " + displayName + "\n")
				.append(mContext.getString(R.string.media_detail_time) + "  " + Utils.convertDateTime(dateModified) + "\n")
				.append(mContext.getString(R.string.media_detail_width) + "  " + width + "\n")
				.append(mContext.getString(R.string.media_detail_height) + "  " + height + "\n")
				.append(mContext.getString(R.string.media_detail_size) + "  " + Utils.convertBytes(mContext, fileSize) + "\n")
				.append(mContext.getString(R.string.media_detail_path) + "  " + data);
		return details.toString();
	}

	@Override
	public Uri getUri() {
		return null;
	}

	@Override
	public Path getMediaPath() {
		return path;
	}

	public int getID(){
		return id;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getImageUri(){
		return "file://"+path.getPath();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSelect(boolean select){
		isSelected = select;
	}
	
	public boolean isSelect(){
		return isSelected;
	}
	
	public void selectClick(){
		isSelected = !isSelected;
	}
	
	public void delete(){
		mContext.getContentResolver().delete(Images.Media.EXTERNAL_CONTENT_URI, "_id=?", new String[]{Integer.toString(id)});
	}
}
