package com.actions.mediacenter.data;

public class Path {
	private String mPath;
	private String mName;
	private String mParent;
	
	public Path(String path) {
		mPath = path;
		initInfo();
	}
	
	private void initInfo(){
		if(mPath!=null){
			int index = mPath.lastIndexOf("/");
			mName = mPath.substring(index+1);
			mParent = mPath.substring(0, index);
			if(mParent.equals("")){
				mParent = "/";
			}
			if(mName.equals("")){
				mName = "/";
			}
		}
	}
	
	public String getParent(){
		return mParent;
	}
	
	public String getName(){
		return mName;
	}
	
	public String getPath(){
		return mPath;
	}

	@Override
	public boolean equals(Object o) {
		return mPath.equals(((Path)o).mPath);
	}

	@Override
	public int hashCode() {
		if(mPath!=null){
			return mPath.hashCode();
		}
		return super.hashCode();
	}
	
}
