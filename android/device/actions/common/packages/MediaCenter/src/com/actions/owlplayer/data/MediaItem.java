package com.actions.owlplayer.data;

import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcelable;

import com.actions.mediacenter.data.MediaObject;
import com.actions.mediacenter.data.Path;
import com.actions.owlplayer.util.ThreadPool.Job;

public abstract class MediaItem  extends MediaObject  implements Parcelable{
	protected Context mContext = null;

	// database fields
	public int videoId = 0;
	public int bucketId = 0;
	public String bucketName = null;
	public String data = null;
	public String mimeType = null;
	public String displayName = null;
	public String title = null;
	public int duration = 0;
	public int videoTrack = 0;
	public int audioTrack = 0;
	public int subTrack = -1;
	public float volume = 1.0f;
	public int bookmark = 0;
	public int width = 0;
	public int height = 0;
	public long dateModified = 0;
	public long fileSize = 0;
	public int newTag = 0;
	public Path path;
	
	public Uri uri = null;

	public MediaItem() {
	}

	public boolean hasContext() {
		return mContext != null;
	}

	public void setContext(Context context) {
		mContext = context;
	}

	public abstract Job<Bitmap> requestImage();

	public String getPath() {
		return data;
	}

	public Uri getUri() {
		if (uri != null) {
			return uri;
		} else {
			return Uri.fromFile(new File(data));
		}
	}

	public ContentValues getContentValues() {
		return null;
	}

	public String getDetails() {
		return null;
	}

	public abstract boolean compare(MediaItem item);

	public int getSupportedOperations() {
		return 0;
	}

	public void delete() {
		throw new UnsupportedOperationException();
	}
	
	public Path getMediaPath(){
		if(path==null){
			path = new Path(data);
		}
		return path;
	}
}
