package com.actions.music.lyric;

import java.io.File;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Handler;
import android.os.Message;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.actions.mediacenter.R;

@SuppressLint("DrawAllocation")
public class MusicLyricView extends View {
	private static final String TAG = "MusicLyricView";

	private static final int MESSAGE_UPDATE_LYRIC = 0;
	private Lyric mLyric;
	private List<Sentence> mLyricSentences;
	private TextPaint mNormalPaint;
	private TextPaint mHighLightPaint;
	private int mTextNormalSize = 25;
	private int mTextHighLightSize = 30;
	private int mInterval = 11;
	private int mTextNormalHight;
	private int mTextHighLightHgiht;
	private StaticLayout mStaticLayout;
	private int mOffsetY = 200;
	private int mLyricIndex = -1;
	private int mViewHeight;
	private int mLyricStopHeight;
	private MediaPlayer mMediaPlayer;

	public MusicLyricView(Context context) {
		super(context);
		initPaint();
	}

	public MusicLyricView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initPaint();
	}

	public MusicLyricView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPaint();
	}

	private void initPaint() {
		mNormalPaint = new TextPaint();
		mHighLightPaint = new TextPaint();

		mNormalPaint.setColor(Color.WHITE);
		mNormalPaint.setAntiAlias(true);
		mNormalPaint.setDither(true);
		mNormalPaint.setTextSize(mTextNormalSize);
		mNormalPaint.setAlpha(255);

		mHighLightPaint.setColor(0xFF5CF6FE);
		mHighLightPaint.setAntiAlias(true);
		mHighLightPaint.setTextSize(mTextHighLightSize);
		mHighLightPaint.setAlpha(255);

		measureTextHight();
	}

	private void measureTextHight() {
		mStaticLayout = new StaticLayout("A", mNormalPaint, 100, Alignment.ALIGN_CENTER, 1, mInterval, false);
		mTextNormalHight = mStaticLayout.getHeight();
		mStaticLayout = new StaticLayout("A", mHighLightPaint, 100, Alignment.ALIGN_CENTER, 1, mInterval, false);
		mTextHighLightHgiht = mStaticLayout.getHeight();
	}

	private static boolean isInitLyricLineCount = false;

	private void initLyricLineCount() {
		if (!isInitLyricLineCount) {
			StaticLayout staticLayout;
			int startPosition = 0;
			for (int i = 0; i < mLyricSentences.size(); i++) {
				Sentence lyricObject = mLyricSentences.get(i);
				lyricObject.startPosition = startPosition;
				staticLayout = new StaticLayout(lyricObject.getContent(), mHighLightPaint, getWidth(), Alignment.ALIGN_CENTER, 1, mInterval, false);
				lyricObject.lineCount = staticLayout.getLineCount();
				int[] lineIndex = new int[lyricObject.lineCount];
				for (int j = 0; j < lyricObject.lineCount; j++) {
					lineIndex[j] = staticLayout.getLineEnd(j);
				}
				lyricObject.lineIndex = lineIndex;
				startPosition += lyricObject.lineCount * (mTextHighLightHgiht + mInterval);
			}
			isInitLyricLineCount = true;
		}
	}

	public void setLyricFile(File file) {
		if (file == null || !file.exists()) {
			return;
		}
		mLyric = new Lyric(file);
		mLyricSentences = mLyric.getLyric();
		isInitLyricLineCount = false;
		refreshLyricView();
		mHandler.sendEmptyMessage(MESSAGE_UPDATE_LYRIC);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (mLyric == null || mLyricIndex == -1) {
			drawNotLyric(canvas);
			return;
		}
		initLyricLineCount();
		Sentence temp = mLyricSentences.get(mLyricIndex);
		int currentPosition = mOffsetY + temp.startPosition;
		if (currentPosition > mViewHeight || currentPosition < 0) {
			mOffsetY = mViewHeight / 2 - temp.startPosition;
		}
		canvas.save();
		canvas.translate(0, mOffsetY + temp.startPosition);
		mStaticLayout = new StaticLayout(temp.getContent(), mHighLightPaint, getWidth(), Alignment.ALIGN_CENTER, 1, mInterval, false);
		mStaticLayout.draw(canvas);
		canvas.restore();
		// 画当前歌词之前的歌词
		for (int i = mLyricIndex - 1; i >= 0; i--) {
			temp = mLyricSentences.get(i);
			for (int j = 0; j < temp.lineCount; j++) {
				canvas.save();
				canvas.translate(0, mOffsetY + temp.startPosition + (mInterval + mTextHighLightHgiht) * j);
				mStaticLayout = new StaticLayout(temp.getContent(), j == 0 ? 0 : temp.lineIndex[j - 1], temp.lineIndex[j], mNormalPaint, getWidth(),
						Alignment.ALIGN_CENTER, 1, mInterval + (mTextHighLightHgiht - mTextNormalHight), false);
				mStaticLayout.draw(canvas);
				canvas.restore();
			}
			if (mOffsetY + temp.startPosition < 0) {
				break;
			}
		}
		// 画当前歌词之后的歌词
		for (int i = mLyricIndex + 1; i < mLyricSentences.size(); i++) {
			temp = mLyricSentences.get(i);
			if (mOffsetY + temp.startPosition > mViewHeight) {
				break;
			}
			for (int j = 0; j < temp.lineCount; j++) {
				canvas.save();
				canvas.translate(0, mOffsetY + temp.startPosition + (mInterval + mTextHighLightHgiht) * j);
				mStaticLayout = new StaticLayout(temp.getContent(), j == 0 ? 0 : temp.lineIndex[j - 1], temp.lineIndex[j], mNormalPaint, getWidth(),
						Alignment.ALIGN_CENTER, 1, mInterval + (mTextHighLightHgiht - mTextNormalHight), false);
				mStaticLayout.draw(canvas);
				canvas.restore();
			}
		}
	}

	private void drawNotLyric(Canvas canvas) {
		String content = getContext().getString(R.string.message_lyric_not_exits);
		canvas.translate(0, getHeight() / 2);
		mStaticLayout = new StaticLayout(content, mHighLightPaint, getWidth(), Alignment.ALIGN_CENTER, 1, mInterval, false);
		mStaticLayout.draw(canvas);
		canvas.restore();
	}

	public int setLyricIndex(int time) {
		if (mLyric == null) {
			return mLyricIndex = -1;
		}
		mLyricIndex = mLyric.getNowSentenceIndex(time);
		return mLyricIndex;
	}

	private int getLyricSpeed() {
		float speed = 0;
		if (mLyricIndex == -1) {
			return (int) speed;
		}
		int position = mLyricSentences.get(mLyricIndex).startPosition;
		if (mOffsetY + position > mLyricStopHeight) {
			speed = ((mOffsetY + position - mLyricStopHeight) / 40);

		} else if (mOffsetY + position < mLyricStopHeight - 100) {
			Log.i(TAG, "speed is too fast!!!");
			speed = 0;
		}
		return (int) speed;
	}

	public int getOffsetY() {
		return mOffsetY;
	}

	public void setOffsetY(int offsetY) {
		this.mOffsetY = offsetY;
	}

	public void setMediaPlayer(MediaPlayer player) {
		mMediaPlayer = player;
		mMediaPlayer.setOnSeekCompleteListener(new OnSeekCompleteListener() {

			@Override
			public void onSeekComplete(MediaPlayer mp) {
				refreshLyricView();
			}
		});
	}

	public void reset() {
		mMediaPlayer = null;
		mLyric = null;
		mLyricIndex = -1;
		mOffsetY = mViewHeight / 2;
		mHandler.removeMessages(MESSAGE_UPDATE_LYRIC);
		invalidate();
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (mLyric != null && mMediaPlayer != null && mMediaPlayer.isPlaying()) {
				setLyricIndex(mMediaPlayer.getCurrentPosition());
				mOffsetY -= getLyricSpeed();
				invalidate();
			}
			mHandler.sendEmptyMessageDelayed(MESSAGE_UPDATE_LYRIC, 100);
		}

	};

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		mViewHeight = h;
		mOffsetY = mViewHeight / 2;
		mLyricStopHeight = mViewHeight / 2 - 55;
	}

	public void setLyricTextFormat(int normalColor, int highLightColor, int normalSize, int hightLightSize) {
		mTextNormalSize = normalSize;
		mTextHighLightSize = hightLightSize;

		mNormalPaint.setColor(normalColor);
		mNormalPaint.setTextSize(mTextNormalSize);

		mHighLightPaint.setColor(highLightColor);
		mHighLightPaint.setTextSize(mTextHighLightSize);
		measureTextHight();

		isInitLyricLineCount = false;
	}

	private void refreshLyricView() {
		if (mLyric != null && mMediaPlayer != null) {
			setLyricIndex(mMediaPlayer.getCurrentPosition());
			mOffsetY -= getLyricSpeed();
			invalidate();
		}
	}

}
