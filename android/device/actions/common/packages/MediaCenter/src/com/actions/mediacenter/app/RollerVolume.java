package com.actions.mediacenter.app;

import android.view.KeyEvent;

public class RollerVolume {
	// max volume to map
	private static final int MAX_VOLUME = 15;

	private static final int[][] sVolumeMaps = { { KeyEvent.KEYCODE_F1, 0 }, { KeyEvent.KEYCODE_F2, 1 }, { KeyEvent.KEYCODE_F3, 2 },
			{ KeyEvent.KEYCODE_F4, 3 }, { KeyEvent.KEYCODE_F5, 4 }, { KeyEvent.KEYCODE_F6, 5 }, { KeyEvent.KEYCODE_F7, 6 }, { KeyEvent.KEYCODE_F8, 7 },
			{ KeyEvent.KEYCODE_F9, 8 }, { KeyEvent.KEYCODE_F10, 9 }, { KeyEvent.KEYCODE_F11, 10 }, { KeyEvent.KEYCODE_F12, 11 },
			{ KeyEvent.KEYCODE_NUMPAD_0, 12 }, { KeyEvent.KEYCODE_NUMPAD_1, 13 }, { KeyEvent.KEYCODE_NUMPAD_2, 14 }, { KeyEvent.KEYCODE_NUMPAD_3, 15 } };

	public static int getRollerIndex(int keyCode, int maxSystemVolume) {
		int volume = -1;
		for (int i = 0; i < sVolumeMaps.length; i++) {
			if (keyCode == sVolumeMaps[i][0]) {
				volume = sVolumeMaps[i][1];
				break;
			}
		}

		if (volume != -1) {
			return volume * maxSystemVolume / MAX_VOLUME;
		} else {
			return -1;
		}
	}

}
