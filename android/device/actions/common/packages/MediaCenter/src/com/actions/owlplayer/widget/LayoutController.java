package com.actions.owlplayer.widget;

public interface LayoutController {
	boolean isShowing();

	void show();

	void show(int timeout);

	void hide();
}