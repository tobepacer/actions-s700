package com.actions.owlplayer.app;

import java.util.List;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.view.MenuItem;

import com.actions.mediacenter.R;
import com.actions.owlplayer.util.Preferences;
import com.actions.owlplayer.util.Utils;

public class Settings extends PreferenceActivity {
	private final static String TAG = "Settings";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBar actionBar = getActionBar();
		actionBar.setIcon(R.drawable.ic_owl_player);
		actionBar.setTitle(R.string.title_settings);
		Drawable actionBarDrawable = getResources().getDrawable(R.color.grey);
		actionBarDrawable.setAlpha(64);
		actionBar.setBackgroundDrawable(actionBarDrawable);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();
	}

	/**
	 * Populate the activity with the top-level headers.
	 */
	@Override
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.preference_headers, target);
	}

	public static class BrowserFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preference_browser);
		}
	}

	public static class PlayerFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preference_player);

			/*
			 * int sdramcap = Utils.getRamSize(); ListPreference floatingAmount = (ListPreference) this .findPreference(Preferences.FLOATING_AMOUNT); if
			 * (sdramcap <= 512) { floatingAmount.setEntries(R.array.array_window_amount_512); floatingAmount
			 * .setEntryValues(R.array.array_window_amount_values_512); } else { floatingAmount.setEntries(R.array.array_window_amount_1024); floatingAmount
			 * .setEntryValues(R.array.array_window_amount_values_1024); }
			 */

			ListPreference floatingAmountPreference = (ListPreference) this.findPreference(Preferences.FLOATING_AMOUNT);
			int mFloatingWindowsAmount = Utils.getFloatingAmount();

			String[] floatingAmountEntries = new String[mFloatingWindowsAmount];
			for (int i = 0; i < mFloatingWindowsAmount; i++) {
				floatingAmountEntries[i] = (i + 1) + "";
			}
			floatingAmountPreference.setEntries(floatingAmountEntries);
			floatingAmountPreference.setEntryValues(floatingAmountEntries);
			if (mFloatingWindowsAmount <= 0) {
				((PreferenceCategory) this.findPreference(Preferences.FLOATING_CATEGORY)).setEnabled(false);
			} else if (mFloatingWindowsAmount == 1) {
				floatingAmountPreference.setValue("1");
			}

			ListPreference insetAmoutPreference = (ListPreference) this.findPreference(Preferences.INSET_AMOUNT);
			int mInsetWindowsAmout = mFloatingWindowsAmount - 1 < 0 ? 0 : mFloatingWindowsAmount - 1;
			String[] insetAmountEntries = new String[mInsetWindowsAmout];
			for (int i = 0; i < mInsetWindowsAmout; i++) {
				insetAmountEntries[i] = (i + 1) + "";
			}
			insetAmoutPreference.setEntries(insetAmountEntries);
			insetAmoutPreference.setEntryValues(insetAmountEntries);
			if (mInsetWindowsAmout <= 0) {
				((PreferenceCategory) this.findPreference(Preferences.INSET_CATEGORY)).setEnabled(false);
			}
		}
	}

	public static class SubtitleFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preference_subtitle);
		}
	}

	/*
	 * public static class AdvancedFragment extends PreferenceFragment {
	 * 
	 * @Override public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState);
	 * 
	 * // Load the preferences from an XML resource addPreferencesFromResource(R.xml.preference_advanced); } }
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, VideoBrowserActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected boolean isValidFragment(String fragmentName) {
		// TODO Auto-generated method stub
		return true;
	}
}
