/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actions.owlplayer.app;

import android.app.Application;
import android.content.Context;

import com.actions.mediacenter.app.ApplicationInit;
import com.actions.owlplayer.data.MediaItemCache;
import com.actions.owlplayer.util.ThreadPool;

public class OWLApplication extends Application {
	private static final String TAG = "OWLApplication";
	public static final int INSIDE_SCREEN = 0;
	public static final int HDMI_SCREEN = 1;

	private MediaItemCache mMediaItemCache;
	private ThreadPool mThreadPool;
	private int mWindowMode = 0;
	private boolean mVitamioChecked = false;
	private int mDisplayScreen = INSIDE_SCREEN;
	private boolean mFlashPlayerSupport = false;
	private boolean mDlnaSupport = false;

	@Override
	public void onCreate() {
		super.onCreate();
		ApplicationInit.init(this);
	}

	public Context getAndroidContext() {
		return this;
	}

	public synchronized MediaItemCache getMediaItemCache() {
		if (mMediaItemCache == null) {
			mMediaItemCache = new MediaItemCache(this);
		}
		return mMediaItemCache;
	}

	public synchronized ThreadPool getThreadPool() {
		if (mThreadPool == null) {
			mThreadPool = new ThreadPool();
		}
		return mThreadPool;
	}

	public void setWindowMode(int mode) {
		mWindowMode = mode;
	}

	public int getWindowMode() {
		return mWindowMode;
	}

	public void setVitamioChecked() {
		mVitamioChecked = true;
	}

	public boolean isVitamioChecked() {
		return mVitamioChecked;
	}

	public int getDisplayScreen() {
		return mDisplayScreen;
	}

	public void setDisplayScreen(int mDisplayScreen) {
		this.mDisplayScreen = mDisplayScreen;
	}

	public boolean isFlashPlayerSupprot() {
		return mFlashPlayerSupport;
	}

	public boolean isDlnaSupprot(){
		return mDlnaSupport;
	}
	
}
