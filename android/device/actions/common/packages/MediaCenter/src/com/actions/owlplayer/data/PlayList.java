package com.actions.owlplayer.data;

public interface PlayList {
	public MediaItem previous(MediaItem current);

	public MediaItem next(MediaItem current);
}
