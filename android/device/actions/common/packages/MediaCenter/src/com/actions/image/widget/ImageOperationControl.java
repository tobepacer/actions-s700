package com.actions.image.widget;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.actions.mediacenter.R;

public class ImageOperationControl {
	private static final String TAG = ImageOperationControl.class.getSimpleName();

	public static final int MESSAGE_HIDE_LAYOUT = 0;
	private static final int OPERATION_SHOW_GAP = 4000; // ms

	private View mRootView;
	private OnClickListener mOnButtonClickListener;
	private View mTitleView;
	private View mOperationLayout;
	private ImageButton mRotateLeftButton;
	private ImageButton mRotateRightButton;
	private ImageButton mInfoButton;
	private ImageButton mSlideShowButton;

	public ImageOperationControl(View rootView) {
		mRootView = rootView;
		mTitleView = mRootView.findViewById(R.id.image_action_bar);
		mOperationLayout = mRootView.findViewById(R.id.image_operation_layout);
		mRotateLeftButton = (ImageButton) mRootView.findViewById(R.id.image_rotate_left_button);
		mRotateRightButton = (ImageButton) mRootView.findViewById(R.id.image_rotate_right_button);
		mInfoButton = (ImageButton) mRootView.findViewById(R.id.image_info_button);
		mSlideShowButton = (ImageButton) mRootView.findViewById(R.id.image_slide_button);

		mRotateLeftButton.setOnClickListener(mOnClickListener);
		mRotateRightButton.setOnClickListener(mOnClickListener);
		mInfoButton.setOnClickListener(mOnClickListener);
		mSlideShowButton.setOnClickListener(mOnClickListener);
	}

	public void setOnClickListener(OnClickListener listener) {
		mOnButtonClickListener = listener;
	}

	public boolean isOperationShow() {
		return mOperationLayout.isShown();
	}

	public boolean isInTouchMode() {
		return mRootView.isInTouchMode();
	}

	public void show() {
		mTitleView.setVisibility(View.VISIBLE);
		mOperationLayout.setVisibility(View.VISIBLE);
		if (isInTouchMode()) {
			hide(OPERATION_SHOW_GAP);
		}
	}

	public void hide() {
		mTitleView.setVisibility(View.INVISIBLE);
		mOperationLayout.setVisibility(View.INVISIBLE);
	}

	public void hide(int delay) {
		mHandler.removeMessages(MESSAGE_HIDE_LAYOUT);
		mHandler.sendEmptyMessageDelayed(MESSAGE_HIDE_LAYOUT, delay);
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_HIDE_LAYOUT:
				hide();
				break;

			default:
				break;
			}
		}

	};

	public void toggleOperationLayout() {
		if (isOperationShow()) {
			hide();
		} else {
			show();
		}
	}

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (mOnButtonClickListener != null) {
				mOnButtonClickListener.onClick(v);
			}
		}
	};

	public void updateSlideShowButton(boolean slide) {
		if (mSlideShowButton != null) {
			mSlideShowButton.setActivated(slide);
		}
	}

}
