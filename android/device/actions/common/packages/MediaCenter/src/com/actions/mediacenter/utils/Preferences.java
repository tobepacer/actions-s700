package com.actions.mediacenter.utils;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Preferences {
	private static String TAG = "Preferences";

	/** Preferences */

	/** get preference settings */
	public static Object getPreferences(Context context, String key, Object deft) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		Map<String, ?> settingMap = settings.getAll();
		Object obj = settingMap.get(key);
		return obj != null ? obj : deft;
	}

	public static Map<String, ?> getPreferences(Context context) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getAll();
	}

	/** store preference settings */
	public static void setPreferences(Context context, String key, Object value) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = settings.edit();
		if (value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		} else if (value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		} else if (value instanceof String) {
			editor.putString(key, (String) value);
		} else {
			Log.e(TAG, "Unexpected type:" + key + "=" + value);
		}
		editor.commit();
	}

}