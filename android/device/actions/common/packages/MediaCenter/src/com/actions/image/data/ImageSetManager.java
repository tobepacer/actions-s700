package com.actions.image.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.actions.mediacenter.data.Path;
import com.actions.mediacenter.utils.CommonUtil;
import com.actions.mediacenter.widget.SelectedItemShowView;

public class ImageSetManager {
	private static ImageSetManager sImageSetManager;
	private static final Object mLock = new Object();
	private ImageCache mImageCache;
	private Map<Path, LocalImageSet> mImageSetList;
	private List<Path> mPathList;
	private int mSelectedSetCount;
	private SelectedItemShowView mSelectedItemShowView;

	private ImageSetManager(Context context) {
		mImageCache = ImageCache.getInstance(context);
		mImageSetList = new HashMap<Path, LocalImageSet>();
		mPathList = new ArrayList<Path>();
		update();
	}

	public static ImageSetManager getInstance(Context context) {
		if (sImageSetManager == null) {
			synchronized (mLock) {
				if (sImageSetManager == null) {
					sImageSetManager = new ImageSetManager(context);
				}
			}
		}
		return sImageSetManager;
	}

	public int size() {
		return mImageSetList.size();
	}

	public LocalImageSet get(int position) {
		if (CommonUtil.isAvaiablePosition(position, mPathList)) {
			return mImageSetList.get(mPathList.get(position));
		}
		return null;
	}

	public int getPosition(LocalImageSet set) {
		if (mPathList.contains(set.getMediaPath())) {
			return mPathList.indexOf(set.getMediaPath());
		}
		return -1;
	}

	public void update() {
		for (LocalImageSet imageSet : mImageSetList.values()) {
			imageSet.clear();
		}

		for (int i = 0; i < mImageCache.size(); i++) {
			LocalImage image = mImageCache.get(i);
			image.setSelect(false);
			Path path = new Path(image.getMediaPath().getParent());
			if (mImageSetList.containsKey(path)) {
				mImageSetList.get(path).addItem(image);
			} else {
				LocalImageSet imageSet = new LocalImageSet(path);
				imageSet.addItem(image);
				mImageSetList.put(path, imageSet);
			}
		}

		Iterator<Entry<Path, LocalImageSet>> iterator = mImageSetList.entrySet().iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getValue().size() == 0) {
				iterator.remove();
			}
		}

		mPathList.clear();
		mPathList.addAll(mImageSetList.keySet());
	}

	public void clearSetSelect() {
		for (LocalImageSet set : mImageSetList.values()) {
			set.setSelect(false);
		}
		mSelectedSetCount = 0;
	}

	public void videoSetSelectClick(int position) {
		LocalImageSet imageSet = get(position);
		imageSet.selectClick();
		if (imageSet.isSelect()) {
			mSelectedSetCount++;
		} else {
			mSelectedSetCount--;
		}
		if (mSelectedItemShowView != null) {
			mSelectedItemShowView.update(mSelectedSetCount);
		}
	}

	public int getSelectedSetCount() {
		return mSelectedSetCount;
	}

	public void setSelectedCountShowView(SelectedItemShowView selectedItemShowView) {
		mSelectedItemShowView = selectedItemShowView;
	}
}
