package com.actions.owlplayer.app;

import java.lang.ref.SoftReference;
import java.util.HashMap;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

import com.actions.owlplayer.data.MediaItem;
import com.actions.owlplayer.util.Future;
import com.actions.owlplayer.util.FutureListener;
import com.actions.owlplayer.util.ThreadPool;

public class BitmapLoader {
	private static final String TAG = "BitmapLoader";

	private static ThreadPool mThreadPool;
	private static BitmapLoader mBitmapLoader = null;
	private static HashMap<Integer, SoftReference<Bitmap>> mImageCache = null;

	public static BitmapLoader getInstance(OWLApplication application) {
		if (mBitmapLoader == null || mImageCache == null) {
			mBitmapLoader = new BitmapLoader();
			mImageCache = new HashMap<Integer, SoftReference<Bitmap>>();
		}

		mThreadPool = application.getThreadPool();
		return mBitmapLoader;
	}

	public Bitmap loadBitmap(final MediaItem item, final ImageView imageView, final ImageCallback imageCallback) {
		// 在内存缓存中，则返回Bitmap对象
		SoftReference<Bitmap> reference = mImageCache.get(item.videoId);
		if (reference != null) {
			Bitmap bitmap = reference.get();
			if (bitmap != null) {
				return bitmap;
			}
		}

		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				imageCallback.imageLoad(item.videoId,imageView, (Bitmap) msg.obj);
			}
		};

		mThreadPool.submit(item.requestImage(), new FutureListener<Bitmap>() {
			@Override
			public void onFutureDone(Future<Bitmap> future) {
				Bitmap bitmap = future.get();
				mImageCache.put(item.videoId, new SoftReference<Bitmap>(bitmap));

				Message msg = handler.obtainMessage(0, bitmap);
				handler.sendMessage(msg);
			}
		});
		return null;
	}

	public interface ImageCallback {
		public void imageLoad(final int viewTag,final ImageView imageView, final Bitmap bitmap);
	}
}
