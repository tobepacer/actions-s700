package com.actions.owlplayer.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.actions.owlplayer.util.ThreadPool.Job;

public class RemoteVideo extends MediaItem {
	public RemoteVideo(Uri uri) {
		this(null, uri);
	}

	public RemoteVideo(Context context, Uri uri) {
		super();

		mContext = context;
		if (uri != null) {
			this.uri = uri;
		}
	}

	public RemoteVideo(Parcel parcel) {
		this.uri = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
		this.videoId = parcel.readInt();
		this.bucketId = parcel.readInt();
		this.bucketName = parcel.readString();
		this.data = parcel.readString();
		this.mimeType = parcel.readString();
		this.displayName = parcel.readString();
		this.title = parcel.readString();
		this.duration = parcel.readInt();
		this.videoTrack = parcel.readInt();
		this.audioTrack = parcel.readInt();
		this.subTrack = parcel.readInt();
		this.bookmark = parcel.readInt();
		this.width = parcel.readInt();
		this.height = parcel.readInt();
		this.dateModified = parcel.readLong();
		this.fileSize = parcel.readLong();
		this.newTag = parcel.readInt();
	}

	@Override
	public boolean compare(MediaItem item) {
		if (uri != null) {
			return uri.compareTo(item.uri) == 0 ? true : false;
		} else {
			return false;
		}
	}

	@Override
	public Job<Bitmap> requestImage() {
		return null;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public static final Parcelable.Creator<RemoteVideo> CREATOR = new Parcelable.Creator<RemoteVideo>() {
		public RemoteVideo createFromParcel(Parcel in) {
			return new RemoteVideo(in);
		}

		public RemoteVideo[] newArray(int size) {
			return new RemoteVideo[size];
		}
	};

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeParcelable(this.uri, flags);
		parcel.writeInt(this.videoId);
		parcel.writeInt(this.bucketId);
		parcel.writeString(this.bucketName);
		parcel.writeString(this.data);
		parcel.writeString(this.mimeType);
		parcel.writeString(this.displayName);
		parcel.writeString(this.title);
		parcel.writeInt(this.duration);
		parcel.writeInt(this.videoTrack);
		parcel.writeInt(this.audioTrack);
		parcel.writeInt(this.subTrack);
		parcel.writeInt(this.bookmark);
		parcel.writeInt(this.width);
		parcel.writeInt(this.height);
		parcel.writeLong(this.dateModified);
		parcel.writeLong(this.fileSize);
		parcel.writeInt(this.newTag);
	}
}
