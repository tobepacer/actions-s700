package com.actions.mediacenter.utils;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Environment;
import android.os.storage.StorageManager;

public class CommonUtil {
	private static String[] getVolumePaths(StorageManager manager) {
		String[] result = null;
		result = (String[]) invokeMethodWithoutArgument(manager, "getVolumePaths");
		return result;
	}

	private static String getVolumeState(StorageManager manager, String storagePath) {
		Method methodId = null;
		String result = null;
		try {
			methodId = manager.getClass().getMethod("getVolumeState", String.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		if (methodId != null) {
			try {
				result = (String) methodId.invoke(manager, storagePath);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	private static Object invokeMethodWithoutArgument(Object object, String methodName) {
		Method methodId = null;
		Object result = null;
		try {
			methodId = object.getClass().getMethod(methodName);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		if (methodId != null) {
			try {
				result = methodId.invoke(object);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static List<String> getAvailableStoragePaths(Context context) {
		List<String> result = new ArrayList<String>();
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
		String[] volumePaths = getVolumePaths(storageManager);
		if (volumePaths != null) {
			for (String path : volumePaths) {
				String state = getVolumeState(storageManager, path);
				if (path.equals(externalStorage) || path.contains("sd") || path.contains("host")) {
					result.add(path);
				}
			}
		}
		return result;
	}
	
	public static boolean isAvaiablePosition(int position, List list){
		if(position>=0 && position<list.size()){
			return true;
		}
		return false;
	}
	
	public static String convertMsToHHMMSS(int millisecond) {
		DecimalFormat decimalFormat = new DecimalFormat("00");
		int hour = millisecond / 3600000;
		int minute = (millisecond % 3600000) / 60000;
		int second = (millisecond % 60000) / 1000;

		String timeStr = null;
		if (hour == 0) {
			timeStr = decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		} else {
			timeStr = hour + ":" + decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		}

		return timeStr;
	}

	public static String convertMsToHHMMSS(long millisecond) {
		DecimalFormat decimalFormat = new DecimalFormat("00");
		long hour = millisecond / 3600000;
		long minute = (millisecond % 3600000) / 60000;
		long second = (millisecond % 60000) / 1000;

		String timeStr = null;
		if (hour == 0) {
			timeStr = decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		} else {
			timeStr = hour + ":" + decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		}

		return timeStr;
	}
	
	public static boolean isFileExists(String path){
		File file = new File(path);
		return file.exists();
	}
}
