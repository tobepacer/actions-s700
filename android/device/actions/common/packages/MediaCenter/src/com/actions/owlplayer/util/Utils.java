package com.actions.owlplayer.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.SystemProperties;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.actions.owlplayer.app.Config;
import com.actions.owlplayer.data.LocalVideo;
import com.actions.owlplayer.data.MediaDatabaseOpenHelper;
import com.actions.owlplayer.data.MediaItem;
import com.actions.owlplayer.data.MediaProvider;
import com.actions.owlplayer.data.RemoteVideo;

public class Utils {
	private static String TAG = "Utils";

	private static final String PREFERENCES_FILE = "PlayerPreferences";
	private static final String PREFERENCE_SERVICE_CURRENT = "CurrentService";
	private static final String PREFERENCE_FULL_SCREEN = "FullScreen";
	private static final String WITHOUT_UUID_DEVICE = "WithoutUuidDevice";
	private static final String NO_DOCUMENT_URI = "NoDocumentUri";
	public static final String PACKAGE_NAME = "com.actions.owlplayer";
	public static final String[] PREFERENCE_SERVICE_NAMES = { "PlayServiceOne", "PlayServiceTwo", "PlayServiceThree", "PlayServiceFour", "PlayServiceFive",
			"PlayServiceSix" };

	public static boolean isServiceRunning(Context context, String name) {
		ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		List<RunningServiceInfo> list = am.getRunningServices(200);
		for (RunningServiceInfo info : list) {
			if (info.service.getClassName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static HashMap<String, Boolean> getAliveServices(Context context) {
		HashMap<String, Boolean> mAliveMap = new HashMap<String, Boolean>();
		for (String name : PREFERENCE_SERVICE_NAMES) {
			boolean isRunning = isServiceRunning(context, PACKAGE_NAME + "." + name);
			mAliveMap.put(name, isRunning);
		}

		return mAliveMap;
	}

	public static int getCurrentService(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		return settings.getInt(PREFERENCE_SERVICE_CURRENT, 1);
	}

	public static void setCurrentService(Context context, int index) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PREFERENCE_SERVICE_CURRENT, index);
		editor.commit();
		editor.clear();
	}

	public static int getFullScreenNum(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		return settings.getInt(PREFERENCE_FULL_SCREEN, 0);
	}

	public static void addFullScreenNum(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		int num = settings.getInt(PREFERENCE_FULL_SCREEN, 0) + 1;
		num = num > 6 ? 6 : num;
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PREFERENCE_FULL_SCREEN, num);
		editor.commit();
		editor.clear();
	}

	public static void decFullScreenNum(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
		int num = settings.getInt(PREFERENCE_FULL_SCREEN, 0) - 1;
		num = num <= 0 ? 0 : num;
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PREFERENCE_FULL_SCREEN, num);
		editor.commit();
		editor.clear();
	}

	public static String getMediaTitle(Context context, Uri uri) {
		String scheme = uri.getScheme();
		if (scheme == null || scheme.equals("file")) {
			return uri.getPath();
		}

		String title = null;
		String[] cursorCols = { MediaStore.Video.Media.TITLE };

		Cursor cursor = context.getContentResolver().query(uri, cursorCols, null, null, null);
		if (cursor == null || cursor.getCount() == 0) {
			title = null;
		} else {
			cursor.moveToFirst();
			title = cursor.getString(0);
		}

		if (cursor != null)
			cursor.close();
		return title;
	}

	public static int getMediaWidth(Context context, Uri uri) {
		int width = 0;
		String[] cursorCols = { MediaStore.Video.Media.WIDTH };

		Cursor cursor = context.getContentResolver().query(uri, cursorCols, null, null, null);
		if (cursor == null || cursor.getCount() == 0) {
			width = 0;
		} else {
			cursor.moveToFirst();
			width = cursor.getInt(0);
		}

		if (cursor != null)
			cursor.close();
		return width;
	}

	public static int getMediaHeight(Context context, Uri uri) {
		int height = 0;
		String[] cursorCols = { MediaStore.Video.Media.HEIGHT };

		Cursor cursor = context.getContentResolver().query(uri, cursorCols, null, null, null);
		if (cursor == null || cursor.getCount() == 0) {
			height = 0;
		} else {
			cursor.moveToFirst();
			height = cursor.getInt(0);
		}

		if (cursor != null)
			cursor.close();
		return height;
	}

	public static String convertMsToHHMMSS(int millisecond) {
		DecimalFormat decimalFormat = new DecimalFormat("00");
		int hour = millisecond / 3600000;
		int minute = (millisecond % 3600000) / 60000;
		int second = (millisecond % 60000) / 1000;

		String timeStr = null;
		if (hour == 0) {
			timeStr = decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		} else {
			timeStr = hour + ":" + decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		}

		return timeStr;
	}

	public static String convertMsToHHMMSS(long millisecond) {
		DecimalFormat decimalFormat = new DecimalFormat("00");
		long hour = millisecond / 3600000;
		long minute = (millisecond % 3600000) / 60000;
		long second = (millisecond % 60000) / 1000;

		String timeStr = null;
		if (hour == 0) {
			timeStr = decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		} else {
			timeStr = hour + ":" + decimalFormat.format(minute) + ":" + decimalFormat.format(second);
		}

		return timeStr;
	}

	public static String convertSecondToHHMMSS(int second) {
		DecimalFormat decimalFormat = new DecimalFormat("00");
		int h = second / 3600;
		int m = (second % 3600) / 60;
		int s = second % 60;

		return h + ":" + decimalFormat.format(m) + ":" + decimalFormat.format(s);
	}

	public static String convertDateTime(long second) {
		DateFormat formater = DateFormat.getDateTimeInstance();
		return formater.format(new Date(second * 1000));
	}

	public static String convertBytes(Context context, long bytes) {
		return Formatter.formatFileSize(context, bytes);
	}

	public static Map<String, Integer> getScreenResolution(Context context) {
		Map<String, Integer> resolution = new HashMap<String, Integer>();
		try {
			WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display vDisplay = windowManager.getDefaultDisplay();
			Class claz = Class.forName("android.view.Display");
			Method widthMethod = claz.getMethod("getRawWidth");
			Method heightMethod = claz.getMethod("getRawHeight");
			int width = (Integer) widthMethod.invoke(vDisplay);
			int height = (Integer) heightMethod.invoke(vDisplay);
			resolution.put("width", width);
			resolution.put("height", height);
			return resolution;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static int getResolutionType(int width, int height) {
		int resolution = width * height;
		int resUHD = (3840 * 2160 + 1920 * 1080) / 2;
		int res1080P = (1920 * 1080 + 1280 * 720) / 2;
		int res702P = (1280 * 720 + 720 * 480) / 2;

		if (width >= 3840 && resolution > resUHD) {
			return Config.ResolutionType.UHD;
		} else if (width >= 1920 && resolution > res1080P) {
			return Config.ResolutionType.P1080;
		} else if (width >= 1280 && resolution > res702P) {
			return Config.ResolutionType.P720;
		} else {
			return Config.ResolutionType.NORMAL;
		}
	}

	public static boolean checkNotNull(Object obj) {
		return obj != null ? true : false;
	}

	public static void setBookmark(Context context, MediaItem item, int bookmark) {
		ContentValues values = new ContentValues();
		values.put(MediaDatabaseOpenHelper.DB_COLUMN_BOOKMARK, bookmark);
		String where = MediaDatabaseOpenHelper.DB_COLUMN_VIDEO_ID + "=?";
		String[] selectionArgs = new String[] { Integer.toString(item.videoId) };
		context.getContentResolver().update(MediaProvider.VIDEO_CONTENT_URI, values, where, selectionArgs);
	}

	public static void printStack() {
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		for (StackTraceElement s : stackTraceElements) {
			Log.e("Stack", s.toString());
		}
	}

	public static MediaItem createMediaItem(Context context, Uri uri) {
		MediaItem item;
		if (uri == null)
			return null;

		String filePath = getPathFromDocumentUri(context, uri);
		if (filePath != NO_DOCUMENT_URI && filePath != WITHOUT_UUID_DEVICE) {
			uri = Uri.fromFile(new File(filePath));
		}
		String scheme = uri.getScheme();
		String authority = uri.getAuthority();
		if (filePath == WITHOUT_UUID_DEVICE) {
			item = new LocalVideo(context);
			Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
			String displayName = null;
			if (cursor != null && cursor.moveToNext()) {
				displayName = cursor.getString(cursor.getColumnIndex(DocumentsContract.Document.COLUMN_DISPLAY_NAME));
				Log.d(TAG, displayName);
			}
			cursor.close();
			if (displayName != null) {
				item.displayName = displayName;
				item.data = displayName;
			}
			item.uri = uri;
		} else if (scheme == null || scheme.equals("file")) {
			item = new LocalVideo(context);
			item.data = uri.getPath();
			item.uri = uri;
			String[] strs = uri.getPath().split("/");
			item.displayName = strs[strs.length - 1];

		} else if (scheme.equals("content") && authority.equals("media")) {
			item = new LocalVideo(context);
			Cursor cursor = ResolverUtils.getSingleVideoCursor(context, uri);
			ResolverUtils.getMediaItem(context, cursor, item);
			if (cursor != null)
				cursor.close();
		} else {
			item = new RemoteVideo(uri);
		}

		return item;
	}

	public static void removeNewTag(Context context, MediaItem item) {
		if ((item instanceof LocalVideo) && item.newTag == 0) {
			item.newTag = 1;
			ContentValues values = new ContentValues();
			values.put(MediaDatabaseOpenHelper.DB_COLUMN_NEW, item.newTag);
			context.getContentResolver().update(MediaProvider.VIDEO_CONTENT_URI, values, MediaDatabaseOpenHelper.DB_COLUMN_DATA + "=?",
					new String[] { String.valueOf(item.data) });
		}
	}

	public static int initViewMode(Context context) {
		int mode = Integer.parseInt((String) Preferences.getPreferences(context, Preferences.VIEW_MODE, Integer.toString(Config.ViewMode.UNKNOWN)));
		if (mode == Config.ViewMode.UNKNOWN) {
			/**
			 * first run, the preference is not set yet
			 * change init view mode from Fit to Extend
			 */
			mode = Integer.parseInt(SystemProperties.get("ro.owlplayer.viewmode", Integer.toString(Config.ViewMode.EXTEND)));
			Preferences.setPreferences(context, Preferences.VIEW_MODE, Integer.toString(mode));

		}

		return mode;
	}

	public static int getRamSize() {
		return SystemProperties.getInt("system.ram.total", 1024);
	}

	public static int getFloatingAmount() {
		return SystemProperties.getInt("ro.owlplayer.floating", 2);
	}

	public static String getFilterOutExtensions() {
		return SystemProperties.get("ro.owlplayer.ext", null);
	}

	public static String getFeatureFilters() {
		return SystemProperties.get("ro.owlplayer.feature", null);
	}

	public static String getSwfSearchPath() {
		return SystemProperties.get("ro.owlplayer.swf", null);
	}

	@SuppressLint("NewApi")
	public static String getPathFromDocumentUri(final Context context, final Uri uri) {

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/" + split[1];
				} else {
					StorageManager mStorageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
					StorageVolume[] mVolumes = mStorageManager.getVolumeList();
					for (StorageVolume volume : mVolumes) {
						boolean mounted = Environment.MEDIA_MOUNTED.equals(volume.getState()) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(volume.getState());
						if (!mounted) {
							continue;
						}
						if (volume.getUuid() != null && volume.getUuid().equals(type)) {
							return volume.getPath() + "/" + split[1];
						}
					}

					return WITHOUT_UUID_DEVICE;
				}
				// TODO handle non-primary volumes
			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] { split[1] };

				return getDataColumn(context, contentUri, selection, selectionArgs);
			}
		}
		return NO_DOCUMENT_URI;
	}

	public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = { column };

		try {
			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	private static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	private static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	private static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	private static boolean isGooglePhotosUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri.getAuthority());
	}
	
	public static boolean isEditOperationSupport() {
		Method getBooleanMethod = null;
		boolean result = true;
		try {
			Class<?> systemPropertiesClass = Class.forName("android.os.SystemProperties");
			getBooleanMethod = systemPropertiesClass.getMethod("getBoolean", String.class, boolean.class);
			if(getBooleanMethod!=null) {
				result = (boolean) getBooleanMethod.invoke(null, "ro.gallery.edit_support", false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}
