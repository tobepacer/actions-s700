package com.actions.image.app;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager.LayoutParams;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actions.image.data.ImageSetManager;
import com.actions.image.data.LocalImageSet;
import com.actions.mediacenter.R;
import com.actions.mediacenter.widget.BaseRecyclerViewAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ImageSetAdapter extends BaseRecyclerViewAdapter<ImageSetAdapter.ViewHolder> {
	private static final String TAG = ImageSetAdapter.class.getSimpleName();
	private ImageSetManager mImageSetManager;
	private OnImageSetClickListener mOnImageSetClickListener;
	private OnImageSetLongClickListener mOnImageSetLongClickListener;
	private ImageLoader mImageLoader;
    // Start with first item selected
	public ImageSetAdapter(Context context) {
		mImageSetManager = ImageSetManager.getInstance(context);
		mImageLoader = ImageLoader.getInstance();
	}

	@Override
	public int getItemCount() {
		return mImageSetManager.size();
	}

	@Override
	public void onBindViewHolder(final ViewHolder viewHolder, int position) {
		super.onBindViewHolder(viewHolder, position);
		viewHolder.itemView.setTag(position);
		viewHolder.itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mOnImageSetClickListener != null) {
					mOnImageSetClickListener.onItemClick(v, (int) v.getTag());
				}
			}
		});
		viewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				if (mOnImageSetLongClickListener != null) {
					return mOnImageSetLongClickListener.onItemLongClick(v, (int) v.getTag());
				}
				return false;
			}
		});
		LocalImageSet imageSet = mImageSetManager.get(position);
		if (imageSet.isSelect()) {
			viewHolder.selectImage.setVisibility(View.VISIBLE);
		} else {
			viewHolder.selectImage.setVisibility(View.INVISIBLE);
		}
		viewHolder.setTitle.setText(imageSet.getName());
		viewHolder.itemCount.setText(Integer.toString(imageSet.size()));
		mImageLoader.displayImage(imageSet.getSetImageUri(), viewHolder.setImage);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_set_item, viewGroup, false);
		initLayout(viewGroup, view);
		return new ViewHolder(view);
	}

	public void setOnImageSetClickListener(OnImageSetClickListener listener) {
		mOnImageSetClickListener = listener;
	}

	public void setOnImageSetLongClickListener(OnImageSetLongClickListener listener) {
		mOnImageSetLongClickListener = listener;
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		ImageView setImage;
		ViewGroup selectImage;
		TextView setTitle;
		TextView itemCount;

		public ViewHolder(View rootView) {
			super(rootView);
			setImage = (ImageView) rootView.findViewById(R.id.media_set_image);
			setTitle = (TextView) rootView.findViewById(R.id.media_set_title);
			itemCount = (TextView) rootView.findViewById(R.id.media_set_count);
			selectImage = (ViewGroup) rootView.findViewById(R.id.media_select_image);
		}

	}

	public interface OnImageSetClickListener {
		public void onItemClick(View view, int position);
	}

	public interface OnImageSetLongClickListener {
		public boolean onItemLongClick(View view, int position);
	}

	public void initLayout(ViewGroup viewGroup, View view) {
		Config.initThumbLayoutParams(viewGroup, view);
		LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
		layoutParams.setMargins(Config.thumbnailMarginLeft, Config.thumbnailMarginTop, Config.thumbnailMarginRight, Config.thumbnailMarginButtom);
		layoutParams.height = Config.thumbnailHeight;
		layoutParams.width = Config.thumbnailWidth;
		view.setLayoutParams(layoutParams);
	}

}
