package com.actions.owlplayer.app;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;

public class Config {
	public static final String INTENT_KEY_PATH = "file_path";
	public static final String INTENT_KEY_MEIDA_ITEM = "media_item_parcel";
	public static final String INTENT_KEY_VIDEO_SET_POSITION = "video_set_position";
	public static final String INTENT_KEY_VIDEO_ITEM_POSITION = "video_item_position";
	public static final String INTENT_KEY_WHICH_DEVICE = "display_on_which";

	public static final String ACTION_MEDIA_ITEM_CACHE_REFRESHED = "MEDIA_ITEM_CACHE_REFRESHED";
	public static final int BROWSER_SPANCOUNT = 4;
	public static final int BROWSER_ROWCOUNT = 3;

	/** Media resolution type */
	public class ResolutionType {
		public static final int NORMAL = 0;
		public static final int P720 = 1;
		public static final int P1080 = 2;
		public static final int UHD = 3;
	}

	public class SortOrder {
		public static final int TIME = 0;
		public static final int NAME = 1;
	}

	public class WindowMode {
		public static final int NORMAL = 0;
		public static final int FLOATING = 1;
	}

	public class ViewMode {
		public static final int UNKNOWN = -1;
		public static final int ORIGINAL = 0;
		public static final int FIT = 1;
		public static final int EXTEND = 2;
	}

	public class LoopMode {
		public static final int START = 0;
		public static final int SINGLE = 0;
		public static final int ORDINAL = 1;
		public static final int RANDOM = 2;
		public static final int END = 2;
	}

	public static int thumbnailHeight;
	public static int thumbnailWidth;
	public static int thumbnailMarginTop = 4;
	public static int thumbnailMarginLeft = 5;
	public static int thumbnailMarginButtom;
	public static int thumbnailMarginRight;
	private static boolean isInitThumbParams = false;

	public static void initThumbLayoutParams(ViewGroup viewGroup, View view) {
		if (!isInitThumbParams) {
			thumbnailHeight = viewGroup.getHeight();
			thumbnailWidth = viewGroup.getWidth();
			thumbnailWidth = thumbnailWidth / BROWSER_SPANCOUNT;
			thumbnailHeight = (thumbnailHeight - thumbnailMarginTop) / BROWSER_ROWCOUNT;
			isInitThumbParams = true;

			thumbnailMarginRight = thumbnailMarginLeft;
			thumbnailMarginButtom = thumbnailMarginTop;
			LayoutParams viewGrouParams = (LayoutParams) viewGroup.getLayoutParams();
			viewGrouParams.setMargins(0, thumbnailMarginTop, 0, 0);
			viewGroup.setLayoutParams(viewGrouParams);
		}
	}

	public static void setInitThumbParams(boolean init) {
		isInitThumbParams = init;
	}
}