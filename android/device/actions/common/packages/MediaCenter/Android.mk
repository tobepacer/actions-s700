LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_JAVA_LIBRARIES := \
				android-support-v4-22.1.1 \
				android-support-v7-recyclerview-22.1.1 \
				android-support-v4-internal_impl-22.1.1 \
				android-support-annotations-22.1.1 \
				antlr-2.7.4 \
				chardet-1.0 \
				cpdetector_1.0.10 \
				universal-image-loader-1.9.4-with-sources \
				gson-2.4
				
LOCAL_SRC_FILES := \
				$(call all-java-files-under, src) \
				src/com/actions/owlplayer/widget/IMediaPlayerControl.aidl
				
LOCAL_RESOURCE_DIR := \
				$(LOCAL_PATH)/res

LOCAL_AAPT_FLAGS := \
				--auto-add-overlay 

LOCAL_PACKAGE_NAME := MediaCenter

LOCAL_PROGUARD_ENABLED := full  obfuscation
LOCAL_PROGUARD_FLAG_FILES := proguard.flags
LOCAL_DEX_PREOPT := false

LOCAL_CERTIFICATE := platform

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := \
				android-support-v4-22.1.1:libs/android-support-v4-22.1.1.jar \
				android-support-v7-recyclerview-22.1.1:libs/android-support-v7-recyclerview-22.1.1.jar \
				android-support-v4-internal_impl-22.1.1:libs/internal_impl-22.1.1.jar \
				android-support-annotations-22.1.1:libs/android-support-annotations-22.1.1.jar \
				antlr-2.7.4:libs/antlr-2.7.4.jar \
				chardet-1.0:libs/chardet-1.0.jar \
				cpdetector_1.0.10:libs/cpdetector_1.0.10.jar \
				universal-image-loader-1.9.4-with-sources:libs/universal-image-loader-1.9.4-with-sources.jar \
				gson-2.4:libs/gson-2.4.jar

include $(BUILD_MULTI_PREBUILT)

include $(call all-makefiles-under,$(LOCAL_PATH))