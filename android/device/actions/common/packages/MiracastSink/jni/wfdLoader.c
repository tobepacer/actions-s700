/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string.h>
#include <jni.h>
#include <stdlib.h>
#include <stdio.h>
#include <android/log.h>

#define  LOG_TAG    "libwfdLoader"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
 
static int load_exec(const char *exeStr)
{
	int retval = 0;
	pid_t pid;
	char *argv[2];

	argv[0] = exeStr;
	argv[1] = NULL;

	/* prepare pipes from child to parent */
	LOGI("start fork:%s\n", argv[0]);
	pid = fork();
	LOGI("fork pid %d\n", pid);
	switch(pid) {
	case 0:
		execv(argv[0], argv);
		/* we should never reach this */
		LOGW("exec of program '%s' failed", argv[0]);
		_exit(1);
	case -1:
		//LOGE("fork of '%s' failed", argv[0]);
		return -1;
	default:
		/* read from child if requested */
		/*
		waitpid(pid, &status, 0);
		if (WIFEXITED(status)) {
			info("'%s' returned with status %i", argv[0], WEXITSTATUS(status));
			if (WEXITSTATUS(status) != 0)
				retval = -1;
		} else {
			err("'%s' abnormal exit", argv[0]);
			retval = -1;
		}
		*/
		break;
	}

	return retval;
}
 
int com_actions_wifidisplay_wfdJin_wfdLoader(JNIEnv* env,
                                                  jobject thiz, const jstring exeStr)
{
    int result;
    char *exestring = (*env)->GetStringUTFChars(env, exeStr, 0);
    LOGI("com_actions_wifidisplay_wfdJin_wfdLoaderP %s\n", exestring);
    result = load_exec(exestring);
    return result;
}

int com_actions_wifidisplay_wfdJin_killWFDServer(JNIEnv* env, jobject thiz)
{
    int result;
    
    return result;
}
