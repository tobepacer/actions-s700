/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/wangzh/mk2-wfd/vendor/rh/generic/wifidisplay/WiFiDisplayApplication/src/com/rh/wfd/IWFDSessionClient.aidl
 */
package com.actions.wfdinterface;
public interface IWFDSessionClient extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.actions.wfdinterface.IWFDSessionClient
{
private static final java.lang.String DESCRIPTOR = "com.actions.wfdinterface.IWFDSessionClient";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.actions.wfdinterface.IWFDSessionClient interface,
 * generating a proxy if needed.
 */
public static com.actions.wfdinterface.IWFDSessionClient asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.actions.wfdinterface.IWFDSessionClient))) {
return ((com.actions.wfdinterface.IWFDSessionClient)iin);
}
return new com.actions.wfdinterface.IWFDSessionClient.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_teardown:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.teardown();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_getSessionState:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getSessionState();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.actions.wfdinterface.IWFDSessionClient
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public int teardown() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_teardown, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * get current state of session, e.g. Caps-negotiated, Playing, ...
     */
public int getSessionState() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getSessionState, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_teardown = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getSessionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
public int teardown() throws android.os.RemoteException;
/**
     * get current state of session, e.g. Caps-negotiated, Playing, ...
     */
public int getSessionState() throws android.os.RemoteException;
}
