package com.actions.miracast;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pWfdInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class WFDSinkActivity extends Activity implements
		TextWatcher,
        WifiP2pManager.ChannelListener,
        WifiP2pManager.GroupInfoListener,
        WifiP2pManager.ConnectionInfoListener,
        WifiP2pManager.PeerListListener {
    private final String TAG = "ActionsMiracastSink";
    private final int CHANGE_DEVICE_NAME_DIALOG = 3;

    public static final int MAX_THROUGHPUT = 50;
    public static final int DEFAULT_CONTROL_PORT = 7236;
    public static final String IP_STR = "ip";
    public static final String PORT_STR = "port";
    public static final String RESOLUTION_STR = "resolution";
    public static final String REMOVE_GROUP = "com.actions.miracast.REMOVE_GROUP";

    private Button mSettingsButton;
    private TextView mConnDescText;
    private TextView mConnWarnText;
    private TextView mDevTitleText;
    private TextView mDevNameText;
    private TextView mPeerListText;
    private EditText mDevNameEdit;
    private ImageView mConnStatusImage;
    private MenuItem mResolutionMenu;

    private SharedPreferences mPreference;
    private SharedPreferences.Editor mPrefEditor;
    private ProgressDialog mProgressDialog;
    //private PowerManager.WakeLock mWakeLock;

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private WifiP2pDevice mDevice;
    private ArrayList<WifiP2pDevice> mPeers;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mReceiver;
    private RenameDevListener mRenameDevListener;
    private StopPeerListener mStopPeerListener;

    private String mIp;
    private String mPort;
    private String mSavedDevName;
    private String mPeerListStr;
    private String mWaitConnectStr;
    private boolean mIsEnabled;
    private boolean mRetryChannel;

	private String ip_client = null;
	private WFDSinkService mWFDSinkService = null;
	public WFDSinkStateMachine mWFDSinkStateMachine = null;
	public static SocketListener mythread;

    private WifiManager mWifiManager;
    private int mNetworkId;

    private AlertDialog mAlertDialog = null;
    private final Handler mHandler = new Handler();
    
    private void disableWifiNetwork(boolean isDisabled) {
		// check wifi enabled
    	if(!mWifiManager.isWifiEnabled()) {
    		return;
    	}
    	
    	if (isDisabled) {
	        // get wifi configs
	        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
	        
			// disable network
	        if (configs != null && configs.size() > 0) {
	            for (WifiConfiguration config : configs) {
	            	mWifiManager.disableNetwork(config.networkId);
	            }
	        }
	        Log.d(TAG, "Disable wifi network.");
    	} else {
    	    // enable network
            if (mNetworkId != -1) {
            	mWifiManager.enableNetwork(mNetworkId, true);
            }
            Log.d(TAG, "Enable wifi network.");
    	}
    }

	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.i(TAG, "WFDSinkService is connected.");
			mWFDSinkService = ((WFDSinkService.MyBinder) service).getService();
			if (mWFDSinkService.isWFDAVManagerConnected() == false) {
				Log.e(TAG, "Cannot find WFDAVManagerService.");
				if (isFinishing() == false) {
					finish();
				}
			}
			mWFDSinkStateMachine = mWFDSinkService.getWFDSinkStateMachine();
			mWFDSinkService.activity=WFDSinkActivity.this;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.e(TAG, "WFDSinkService is disconnected.");
			mWFDSinkService = null;
			if (isFinishing() == false) {
				finish();
			}
		}
	};

    public WFDSinkActivity() {
        mIsEnabled = false;
        mRetryChannel = false;
        mDevice = null;
        mProgressDialog = null;
        mPeers = new ArrayList();
    }
    
    protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate WFDSinkActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sink_main);

        mWifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if (mWifiManager == null) {
            Log.e(TAG, "Sorry! WifiManager is null!");
            return;
        }

        mManager = (WifiP2pManager)getSystemService(Context.WIFI_P2P_SERVICE);
        if (mManager == null) {
            Log.e(TAG, "Sorry! WifiP2pManager is null!");
            return;
        }

        mChannel = mManager.initialize(this, getMainLooper(), null);
        if (mChannel == null) {
            Log.e(TAG, "Sorry! WifiP2pManager.Channel is null!");
            return;
        }

        mReceiver = new MiracastSinkBroadcastReceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
        mIntentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        mPreference = PreferenceManager.getDefaultSharedPreferences(this);
        mPrefEditor = mPreference.edit();

        mRenameDevListener = new RenameDevListener();
        mStopPeerListener = new StopPeerListener();

        mPeerListStr = getString(R.string.peer_list);
        mWaitConnectStr = getString(R.string.connect_ready);
        
        // socket listener
		mythread = new SocketListener();
		mythread.start();
		
        //PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        //mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        //mWakeLock.acquire();

        mSettingsButton = (Button)findViewById(R.id.settings_btn);
        mConnDescText = (TextView)findViewById(R.id.show_connect_desc);
        mConnWarnText = (TextView)findViewById(R.id.show_desc_more);
        mPeerListText = (TextView)findViewById(R.id.peer_devices);
        mDevTitleText = (TextView)findViewById(R.id.device_title);
        mDevNameText = (TextView)findViewById(R.id.device_dec);
        mConnStatusImage = (ImageView)findViewById(R.id.show_connect);

        mConnDescText.setFocusable(true);
        mConnDescText.requestFocus();

        mSettingsButton.setOnClickListener(new View.OnClickListener() {            
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        mConnWarnText.setText(getString(R.string.p2p_off_warning));
        if (!isNetAvailiable()) {
            mConnWarnText.setVisibility(View.VISIBLE);
            mSettingsButton.setVisibility(View.VISIBLE);
            mConnDescText.setFocusable(false);
        }

        if (mDevice != null) {
            mSavedDevName = mDevice.deviceName;
            mDevNameText.setText(mSavedDevName);
        } else {
            mDevTitleText.setVisibility(View.INVISIBLE);
        }

        resetData();
        changeRole(false);

        // bind wfd service
		bindService(new Intent(WFDSinkActivity.this, WFDSinkService.class),
				mConnection, Context.BIND_AUTO_CREATE);
    }

    public void onResume() {
		Log.d(TAG, "onResume WFDSinkActivity");
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);

    	// save network id
    	if (mNetworkId == -1) {
            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
            if (wifiInfo != null) {
            	mNetworkId = wifiInfo.getNetworkId();
            } else {
            	mNetworkId = -1;
            }
            Log.d(TAG, "mNetworkId=" + mNetworkId);
        }

        // disable networks
        disableWifiNetwork(true);
    }

    public void onPause() {
        super.onPause();
		Log.d(TAG, "onPause WFDSinkActivity");
        unregisterReceiver(mReceiver);
    }
 
    protected void onDestroy() {
        super.onDestroy();
		Log.d(TAG, "onDestroy WFDSinkActivity");
		mythread.stopThread();
		unbindService(mConnection);
        //mWakeLock.release();
        changeRole(true);
        disableWifiNetwork(false);
    }
    
	private boolean isNetAvailiable() {
		boolean availiable = false;
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm != null) {
			NetworkInfo info = cm.getActiveNetworkInfo();
			if ((info != null) && (info.isAvailable())) {
				availiable = true;
			}
		}
		return availiable;
	}
    
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mConnStatusImage.setBackgroundResource(R.drawable.wifi_connect);
        ((AnimationDrawable)mConnStatusImage.getBackground()).start();
    }

    private void dismissProgressDialog() {
        if ((mProgressDialog != null) && (mProgressDialog.isShowing())) {
            mProgressDialog.dismiss();
        }
    }
    
    public void onPeersAvailable(WifiP2pDeviceList devicelist) {
        dismissProgressDialog();

        mPeers.clear();
        mPeers.addAll(devicelist.getDeviceList());

        for (int i = 0; i < mPeers.size(); i++) {
            WifiP2pDevice dev = mPeers.get(i);
            if (dev.status == WifiP2pDevice.CONNECTED) {
                mConnDescText.setText(getString(R.string.connecting_desc) + dev.deviceName);
                break;
            }
        }

        String listName = mPeerListStr;
        for (int i = 0; i < mPeers.size(); i++) {
            listName += mPeers.get(i).deviceName + " ";
        }
        mPeerListText.setText(listName);
    }
    
    public void onGroupInfoAvailable(WifiP2pGroup group) {
        if (group != null) {
            Log.d(TAG, "onGroupInfoAvailable true : " + group);
        } else {
            Log.d(TAG, "onGroupInfoAvailable false");
        }
    }

    public void onConnectionInfoAvailable(WifiP2pInfo info) {
    }
    
    public void onChannelDisconnected() {
        if (!mRetryChannel) {
            Toast.makeText(this, getString(R.string.channel_try), Toast.LENGTH_LONG).show();
            resetData();
            mRetryChannel = true;
            mManager.initialize(this, getMainLooper(), this);
        } else {
            Toast.makeText(this, getString(R.string.channel_close), Toast.LENGTH_LONG).show();
        }
    }
    

    public void setDevice(WifiP2pDevice device) {
        mDevice = device;
        if (mDevice != null) {
            mSavedDevName = mDevice.deviceName;
            mDevTitleText.setVisibility(View.VISIBLE);
            mDevNameText.setText(mSavedDevName);
        }
    }
    
    public void startSearch() {
        if (!mIsEnabled) {
            mConnWarnText.setVisibility(View.VISIBLE);
            mSettingsButton.setVisibility(View.VISIBLE);
            mConnDescText.setFocusable(false);
        } else {
            dismissProgressDialog();

            mProgressDialog = ProgressDialog.show(this, getString(R.string.find_title), 
                getString(R.string.find_progress), true, true, new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        mManager.stopPeerDiscovery(mChannel, mStopPeerListener);
                    }
            });

            mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
                public void onSuccess() {
                	Log.w(TAG, "Discover peer success.");
                    Toast.makeText(WFDSinkActivity.this, 
                        getString(R.string.discover_init), Toast.LENGTH_SHORT).show();
                }
                public void onFailure(int reasonCode) {
                    Toast.makeText(WFDSinkActivity.this, getString(R.string.discover_fail)
                        + reasonCode, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    public void resetData() {
        mConnStatusImage.setBackgroundResource(R.drawable.wifi_connect);
        mConnDescText.setText(mWaitConnectStr);
        mPeers.clear();
    }
    
    public void setIsWifiP2pEnabled(boolean enable) {
        mIsEnabled = enable;
        mConnDescText.setText(mWaitConnectStr);
        if (enable) {
            mConnWarnText.setVisibility(View.INVISIBLE);
            mSettingsButton.setVisibility(View.GONE);
            mConnDescText.setFocusable(false);
        } else {
            mConnWarnText.setVisibility(View.VISIBLE);
            mSettingsButton.setVisibility(View.VISIBLE);
            mConnDescText.setFocusable(true);
        }
    }
    
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_items, menu);
        /*
        mResolutionMenu = menu.findItem(R.id.setting_definition);
        if (mPreference != null) {
            if (mPreference.getBoolean("resolution", false)) {
                mResolutionMenu.setTitle(getString(R.string.setting_definition) + " : "
                        + getString(R.string.setting_definition_hd));
            } else {
                mResolutionMenu.setTitle(getString(R.string.setting_definition) + " : "
                        + getString(R.string.setting_definition_sd));
            }
        }*/
        return true;
    }

    public void onQuery(MenuItem item) {
        /*
        if (mResolutionMenu == null) {
            return;
        }
        switch (item.getItemId()) {
            case R.id.setting_sd:
                mResolutionMenu.setTitle(getString(R.string.setting_definition) + " : "
                        + getString(R.string.setting_definition_sd));
                mPrefEditor.putBoolean("resolution", false);
                mPrefEditor.commit();
                return;
            case R.id.setting_hd:
                mResolutionMenu.setTitle(getString(R.string.setting_definition) + " : "
                        + getString(R.string.setting_definition_hd));
                mPrefEditor.putBoolean("resolution", true);
                mPrefEditor.commit();
                break;
            default:
                break;
        }
        */
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.atn_direct_discover:
                mPeerListText.setText(mPeerListStr);
                startSearch();
                break;

            case R.id.setting_name:
                showDialog(CHANGE_DEVICE_NAME_DIALOG);
                break;

            //case R.id.setting_definition:
            //case R.id.setting_sd:
            //case R.id.setting_hd:
            //    return super.onOptionsItemSelected(item);

            case R.id.about_version:
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                if (getResources().getConfiguration().locale.getCountry().equals("CN")) {
                    builder.setMessage(getFromAssets("version_cn"));
                } else {
                    builder.setMessage(getFromAssets("version"));
                }
                builder.setTitle(R.string.about_version);
                builder.setPositiveButton(R.string.close_dlg, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
                break;
            }
        }
		return true;
    }
    
    public String getFromAssets(String fileName) {
        String result = "";
        try {
            InputStream in = getResources().getAssets().open(fileName);
            byte[] buffer = new byte[in.available()];
            in.read(buffer);
            result = new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public Dialog onCreateDialog(int id) {
        if (id == CHANGE_DEVICE_NAME_DIALOG) {
            mDevNameEdit = new EditText(this);
            if (mSavedDevName != null) {
                mDevNameEdit.setText(mSavedDevName);
                mDevNameEdit.setSelection(mSavedDevName.length());
            }
            mDevNameEdit.addTextChangedListener(this);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.change_name);
            builder.setView(mDevNameEdit);
            builder.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mSavedDevName = mDevNameEdit.getText().toString();
                        mManager.setDeviceName(mChannel, mSavedDevName, mRenameDevListener);
                    }
                });
            builder.setNegativeButton(getString(R.string.dlg_cancel), null);
            mAlertDialog = builder.create();
        }

        mHandler.post(new Runnable() {
            public void run() {
                enableSubmitIfAppropriate();
            }
        });
        
        return mAlertDialog;
    }

    private void changeRole(final boolean isSource) {
        WifiP2pWfdInfo wfdInfo = new WifiP2pWfdInfo();
        wfdInfo.setWfdEnabled(true);
        if (isSource) {
            wfdInfo.setDeviceType(WifiP2pWfdInfo.WFD_SOURCE);
        } else {
            wfdInfo.setDeviceType(WifiP2pWfdInfo.PRIMARY_SINK);
        }
        wfdInfo.setSessionAvailable(true);
        wfdInfo.setControlPort(DEFAULT_CONTROL_PORT);
        wfdInfo.setMaxThroughput(MAX_THROUGHPUT);

        mManager.stopPeerDiscovery(mChannel, mStopPeerListener);
        
        mManager.setWFDInfo(mChannel, wfdInfo, new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                Log.d(TAG, "Successfully set WFD info.");
            }
            public void onFailure(int reason) {
                Log.d(TAG, "Failed to set WFD info with reason " + reason + ".");
            }
        });
    }

    private class MiracastSinkBroadcastReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)) {
                if (intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1) ==
                        WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    setIsWifiP2pEnabled(true);
                } else {
                    setIsWifiP2pEnabled(false);
                    resetData();
                    
                    // clear saved network
                    mNetworkId = -1;
                }
            } else if (action.equals(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)) {
                mManager.requestPeers(mChannel, WFDSinkActivity.this);
            } else if (action.equals(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)) {
                NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra(
                        WifiP2pManager.EXTRA_NETWORK_INFO);
                if (networkInfo.isConnected()) {
                    //mManager.requestConnectionInfo(mChannel, WFDSinkActivity.this);
                } else {
                    resetData();
                    startSearch();
                }
            } else if (action.equals(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)) {
                resetData();
                setDevice((WifiP2pDevice)intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));
            } else if (action.equals(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION)) {
                if (intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, -1) == 
                        WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED) {
                    dismissProgressDialog();
                }
            } else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                WifiInfo info = mWifiManager.getConnectionInfo();
                if (networkInfo != null && networkInfo.getDetailedState() == DetailedState.CONNECTED
                    && info != null && info.getNetworkId() >= 0) {
                    Log.d(TAG, "CONNECTED: " + info);

                    // save network id
                    mNetworkId = info.getNetworkId();

                    // disable networks
                    disableWifiNetwork(true);                    
                }
            }
        }
    }

    private class RenameDevListener implements WifiP2pManager.ActionListener {
        public void onSuccess() {
            mDevNameText.setText(mSavedDevName);
        }
        public void onFailure(int reason) {
            Toast.makeText(WFDSinkActivity.this, getString(
                        R.string.wifi_p2p_failed_rename_message),
                    Toast.LENGTH_LONG).show();
        }
    }

    private class StopPeerListener implements WifiP2pManager.ActionListener {
        public void onSuccess() {
        	Log.w(TAG, "Stop peer success.");
        }
        public void onFailure(int reason) {
        	Log.w(TAG, "Stop peer failed.");
        }
    }

  //add by wfdsink 
  	class SocketListener extends Thread {
  		public boolean stop = false;
  		public LocalServerSocket server;
          @Override
          public void run() {
              try {          
              	server = new LocalServerSocket("com.wfd.localsocket");               		  
                  LocalSocket receiver;
                  byte[] ip = new byte[50];
                  int readed;
                  InputStream input;                             
                  while (!stop) {                 		                 
                      receiver = server.accept(); 
                      if(receiver != null) {
                  		input = receiver.getInputStream();                
  		                while( (readed = input.read(ip)) != -1) {              
  					        ip_client = new String(ip,0,readed);
  							Log.d(TAG,"socket client info:type="+" ip=" + ip_client);
  							break;
  						}	 
  					} 
  					if(ip_client != null && mWFDSinkService != null){
  						int port = mWFDSinkService.getRtspPort();
  						if(port != -1)
  							mWFDSinkService.setServerInfo(port,InetAddress.getByName(ip_client));
  						ip_client = null;
  						mWFDSinkService.getWFDSinkStateMachine().sendMessage(WFDSinkStateMachine.START_RTSP_CLIENT);
  					}
                  }
              } catch (IOException e) {
                  Log.e(getClass().getName(), e.getMessage());             
              }finally{            	
              		try{
              			server.close();
              		}catch(IOException e1){
              			Log.e(getClass().getName(), e1.getMessage());            
              		}
              }
          }
          
          public void stopThread() {
          		Log.d(TAG,"stopThread");        		
          		stop = true;				
          		try{
          			LocalSocket ender = new LocalSocket();
          			ender.connect(new LocalSocketAddress("com.wfd.localsocket"));
          			ender.close();
          		}catch(IOException e){
          			Log.e(getClass().getName(), e.getMessage()); 
          		}
          }
      }

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		mHandler.post(new Runnable() {
            public void run() {
                enableSubmitIfAppropriate();
            }
        });
	}

    /* show submit button if edit text are valid */
    void enableSubmitIfAppropriate() {
        Button submit = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if (submit == null) return;

        boolean enabled = mDevNameEdit.length() > 0;
        submit.setEnabled(enabled);
    }
}


