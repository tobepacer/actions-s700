package com.actions.miracast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class WFDSinkBootCompleteReceiver extends BroadcastReceiver {
	public static String TAG = "WFDSinkBootCompleteReceiver";
	SharedPreferences mPreferences;

	@Override
	public void onReceive(Context context, Intent intent) {
		String sinkMode = System.getProperty("WFDSinkMode", "true");
		if (sinkMode.equals("false")) {
		    Log.i(TAG, "start WFD Sink Service fail!");
			return;
		}
		
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Log.i(TAG, "start WFD Sink Service");
			Intent sIntent = new Intent();
			sIntent.setClassName("com.actions.wifidisplay.sink", "com.actions.wifidisplay.sink.WFDSinkService");
			context.startService(sIntent);
		}
	}
}
