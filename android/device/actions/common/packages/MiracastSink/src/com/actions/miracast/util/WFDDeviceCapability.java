package com.actions.miracast.util;

import java.util.List;

//import com.rh.wfd.WFD3DVideoFormats;
//import com.rh.wfd.WFDAudioFormats;
//import com.rh.wfd.WFDVideoFormats;
//delete by xusongzhou

public class WFDDeviceCapability {
    public static final short WFD_DEVICE_TYPE_MASK = 0x0003;
    public static final short wfd_device_src = 0x0000;
    public static final short wfd_device_pri_snk = 0x0001;
    public static final short wfd_device_sec_snk = 0x0002;
    public static final short wfd_device_src_or_pri_sink = 0x0003;

    public static final short WFD_COUPLED_SNK_OP_SUP_BY_SRC_MASK = 0x0004;
    public static final short WFD_COUPLED_SNK_OP_SUP_BY_SNK_MASK = 0x0008;
    public static final short wfd_coupled_snk_op_not_supported = 0x0;
    public static final short wfd_coupled_snk_op_supported = 0x1;

    public static final short WFD_AVAILABLE_FOR_WFD_SESSION_MASK = 0x0030;
    public static final short wfd_available_not_wfd_session = 0x0;
    public static final short wfd_available_wfd_session = 0x1;

    public static final short WFD_SERVICE_DISCOVER_MASK = 0x0040;
    public static final short wfd_dont_support_service_discover = 0x0;
    public static final short wfd_support_service_discover = 0x1;

    public static final short WFD_PREFERRED_CONNECTIVITY_MASK = 0x0080;
    public static final short wfd_preferred_p2p = 0x0;
    public static final short wfd_preferred_tdls = 0x1;

    public static final short WFD_CONTENT_PROTECTION_MASK = 0x0100;
    public static final short wfd_content_protection_not_support = 0x0;
    public static final short wfd_content_protection_support_hdcp = 0x1;

    public static final short WFD_TIME_SYNC_MSK = 0x0200;
    public static final short wfd_time_sync_not_support = 0x0;
    public static final short wfd_time_sync_support_8021_as = 0x1;

    public static final short WFD_DEVICE_INFO_RESERVED_BIT = (short) 0xfc00;

    // 0
    public short deviceInfo;
    public int rtspPort;
    public int maxThoughput;

    // 1
    public String associatedBSSID;

    public static final int notCoupled = 0;
    public static final int coupled = 1;
    public static final int tearDownCoupling = 2;
    // 6
    public int coupledSnkStatus;

    /* 3 */
    //WFDVideoFormats videoFormats;
    //by xusongzhou
    // 4
    //WFD3DVideoFormats threeDVideoFormats;
    //by xusongzhou
    // 2
    //WFDAudioFormats audioFormats;
    //by xusongzhou
    // 5
    public int contentProtection;
    public static final int CONTENT_PROTECTION_HDCP20_MASK = 0x1;
    public static final int CONTENT_PROTECTION_HDCP21_MASK = 0x2;

    // 7
    public int extentedCap;
    public static final int EX_CAP_UIBC_SUPPORT_MASK = 0X1;
    public static final int EX_CAP_I2C_SUPPORT_MASK = 0X2;
    public static final int EX_CAP_PREFERED_DISPLAY_MASK = 0X4;
    public static final int EX_CAP_STANDBY_RESUME_CNTRL_MASK = 0X8;

    // 8
    public String localIP;

    public int numberOfClients;

    public static final class WFDSessionInfo {
	public String deviceAddr;
	public String associatedBSSID;
	public short deviceInfo;
	public int maxThoughput;
	public int coupledSnkStatus;
	public String coupledSnk;
    }

    List<WFDSessionInfo> sessionList;

    public static final String FAKE_AVMANAGER_SERVICE_NAME = "FakeRHWFDAVManager";
    public static final String REAL_AVMANAGER_SERVICE_NAME = "WFDAVManager";
}
