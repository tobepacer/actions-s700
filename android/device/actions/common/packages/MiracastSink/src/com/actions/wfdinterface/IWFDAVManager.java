/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/wangzh/mk2-wfd/vendor/rh/generic/wifidisplay/WiFiDisplayApplication/src/com/rh/wfd/IWFDAVManager.aidl
 */
package com.actions.wfdinterface;
public interface IWFDAVManager extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.actions.wfdinterface.IWFDAVManager
{
private static final java.lang.String DESCRIPTOR = "com.actions.wfdinterface.IWFDAVManager";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.actions.wfdinterface.IWFDAVManager interface,
 * generating a proxy if needed.
 */
public static com.actions.wfdinterface.IWFDAVManager asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.actions.wfdinterface.IWFDAVManager))) {
return ((com.actions.wfdinterface.IWFDAVManager)iin);
}
return new com.actions.wfdinterface.IWFDAVManager.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_getRTSPPort:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getRTSPPort();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_startWFDSessionServer:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
int _arg3;
_arg3 = data.readInt();
com.actions.wfdinterface.IWFDSessionServerListener _arg4;
_arg4 = com.actions.wfdinterface.IWFDSessionServerListener.Stub.asInterface(data.readStrongBinder());
int _result = this.startWFDSessionServer(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_startStandaloneWFDSessionServer:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
com.actions.wfdinterface.IWFDSessionServerListener _arg1;
_arg1 = com.actions.wfdinterface.IWFDSessionServerListener.Stub.asInterface(data.readStrongBinder());
int _result = this.startStandaloneWFDSessionServer(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_startWFDSessionClient:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
com.actions.wfdinterface.IWFDSessionClientListener _arg3;
_arg3 = com.actions.wfdinterface.IWFDSessionClientListener.Stub.asInterface(data.readStrongBinder());
android.view.Surface _arg4;
if ((0!=data.readInt())) {
_arg4 = android.view.Surface.CREATOR.createFromParcel(data);
}
else {
_arg4 = null;
}
int _result = this.startWFDSessionClient(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_startStandaloneWFDSessionClient:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
com.actions.wfdinterface.IWFDSessionClientListener _arg1;
_arg1 = com.actions.wfdinterface.IWFDSessionClientListener.Stub.asInterface(data.readStrongBinder());
android.view.Surface _arg2;
if ((0!=data.readInt())) {
_arg2 = android.view.Surface.CREATOR.createFromParcel(data);
}
else {
_arg2 = null;
}
int _result = this.startStandaloneWFDSessionClient(_arg0, _arg1, _arg2);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_reset:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.reset();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.actions.wfdinterface.IWFDAVManager
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
//////////////////////////////////////////////////////////////////////
// primitives to query the supported WFD configurations
//////////////////////////////////////////////////////////////////////
// ref. to WFDSpec1.26#5.1.5
/**
     * get local WFD rtsp server listening port
     */
public int getRTSPPort() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getRTSPPort, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * used by Src&Primary-Sink to start a coupling/play session server
     *
     *  @param forWhat DO_COUPLE or DO_PLAY
     *  @param peerMAC peer MAC address
     *  @param localIP local ip address
     *  @param timeoutMs timeout value(ms)
     */
public int startWFDSessionServer(int forWhat, java.lang.String peerMAC, java.lang.String localIP, int timeoutMs, com.actions.wfdinterface.IWFDSessionServerListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(forWhat);
_data.writeString(peerMAC);
_data.writeString(localIP);
_data.writeInt(timeoutMs);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_startWFDSessionServer, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * start standalone session server, used to test source side w/o sink needed
     *
     *  @param args combined arguments string
     *  @param listener provided by caller to listen events of session server
     */
public int startStandaloneWFDSessionServer(java.lang.String args, com.actions.wfdinterface.IWFDSessionServerListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(args);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_startStandaloneWFDSessionServer, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * used by sink to start a coupling/play session client
     *
     * @param servIP server's IP
     * @param rtspPort server's rtsp port
     * @param timeoutMs timeout value(ms)
     * @param surface surface for preview
     */
public int startWFDSessionClient(java.lang.String servIP, int rtspPort, int timeoutMs, com.actions.wfdinterface.IWFDSessionClientListener listener, android.view.Surface surface) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(servIP);
_data.writeInt(rtspPort);
_data.writeInt(timeoutMs);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
if ((surface!=null)) {
_data.writeInt(1);
surface.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_startWFDSessionClient, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * start standalone session client, used to test sink side w/o source needed
     *
     * @param args combined arguments string
     * @param listener provided by caller to listen events of session client
     * @param surface surface to show video
     */
public int startStandaloneWFDSessionClient(java.lang.String args, com.actions.wfdinterface.IWFDSessionClientListener listener, android.view.Surface surface) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(args);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
if ((surface!=null)) {
_data.writeInt(1);
surface.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_startStandaloneWFDSessionClient, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * reset all pending stuff for both source&sink
     */
public int reset() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_reset, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_getSupportedWFDVideoFormats = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getSupportedWFD3DVideoFormats = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getSupportedWFDAudioFormats = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getSupportedWFDHDCPScheme = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_getSupportedWFDExtCaps = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_getRTSPPort = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_startWFDSessionServer = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_startStandaloneWFDSessionServer = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_startWFDSessionClient = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_startStandaloneWFDSessionClient = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_reset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
}
//////////////////////////////////////////////////////////////////////
// primitives to query the supported WFD configurations
//////////////////////////////////////////////////////////////////////
// ref. to WFDSpec1.26#5.1.5
/**
     * get local WFD rtsp server listening port
     */
public int getRTSPPort() throws android.os.RemoteException;
/**
     * used by Src&Primary-Sink to start a coupling/play session server
     *
     *  @param forWhat DO_COUPLE or DO_PLAY
     *  @param peerMAC peer MAC address
     *  @param localIP local ip address
     *  @param timeoutMs timeout value(ms)
     */
public int startWFDSessionServer(int forWhat, java.lang.String peerMAC, java.lang.String localIP, int timeoutMs, com.actions.wfdinterface.IWFDSessionServerListener listener) throws android.os.RemoteException;
/**
     * start standalone session server, used to test source side w/o sink needed
     *
     *  @param args combined arguments string
     *  @param listener provided by caller to listen events of session server
     */
public int startStandaloneWFDSessionServer(java.lang.String args, com.actions.wfdinterface.IWFDSessionServerListener listener) throws android.os.RemoteException;
/**
     * used by sink to start a coupling/play session client
     *
     * @param servIP server's IP
     * @param rtspPort server's rtsp port
     * @param timeoutMs timeout value(ms)
     * @param surface surface for preview
     */
public int startWFDSessionClient(java.lang.String servIP, int rtspPort, int timeoutMs, com.actions.wfdinterface.IWFDSessionClientListener listener, android.view.Surface surface) throws android.os.RemoteException;
/**
     * start standalone session client, used to test sink side w/o source needed
     *
     * @param args combined arguments string
     * @param listener provided by caller to listen events of session client
     * @param surface surface to show video
     */
public int startStandaloneWFDSessionClient(java.lang.String args, com.actions.wfdinterface.IWFDSessionClientListener listener, android.view.Surface surface) throws android.os.RemoteException;
/**
     * reset all pending stuff for both source&sink
     */
public int reset() throws android.os.RemoteException;
}
