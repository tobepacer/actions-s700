package com.actions.miracast;

import java.net.InetAddress;
import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.util.Log;

public class WFDSinkServiceReceiver extends BroadcastReceiver {
	public static String TAG = "WFDSinkServiceReceiver";

	private WifiP2pManager manager;
	private WFDSinkStateMachine statemachine;
	private WFDSinkService service;
	private Channel channel;

    private int rtspPort = 7236;
    private InetAddress clientip;
    private final ArrayList<WifiP2pDevice> mAvailableWifiDisplayPeers =
            new ArrayList<WifiP2pDevice>();

	private ConnectionInfoListener listener = new ConnectionInfoListener() {
		public void onConnectionInfoAvailable(WifiP2pInfo info) {
			//by xusongzhou
			if (info.groupOwnerAddress == null) {
				Log.d(TAG, "Ignore connection info : invalid p2p group owner address");
				return;
			}
			if (info.groupFormed) {
				if (info.isGroupOwner) {
					Log.d(TAG, "Connect to WFD server from p2p GO");
							//clientip = InetAddress.getByName(WFDSinkActivity.ip_client);
							//Log.d(TAG, "ip is:" + WFDSinkActivity.ip_client + "," + clientip.toString());
							//WFDSinkActivity.ip_client = null;

					//service.setServerInfo(rtspPort,clientip);
				} else {
					Log.d(TAG, "Connect to WFD server from p2p client");   	
					service.setServerInfo(rtspPort, info.groupOwnerAddress);
					statemachine.sendMessage(WFDSinkStateMachine.START_RTSP_CLIENT);
				}
				//statemachine.sendMessage(WFDSinkStateMachine.START_RTSP_CLIENT);
			}

		}
	};

	public WFDSinkServiceReceiver(WifiP2pManager manager, Channel channel, WFDSinkStateMachine statemachine,
			WFDSinkService service) {
		this.manager = manager;
		this.statemachine = statemachine;
		this.service = service;
		this.channel = channel;
	}
//
//    private void requestPeers() {
//            mConnectingDevice = mDesiredDevice;
//            WifiP2pConfig config = new WifiP2pConfig();
//            WpsInfo wps = new WpsInfo();
//            if (mConnectingDevice.wpsPbcSupported()) {
//                wps.setup = WpsInfo.PBC;
//            } else if (mConnectingDevice.wpsDisplaySupported()) {
//                // We do keypad if peer does display
//                wps.setup = WpsInfo.KEYPAD;
//            } else {
//                wps.setup = WpsInfo.DISPLAY;
//            }
//            config.wps = wps;
//            config.deviceAddress = mConnectingDevice.deviceAddress;
//            // Helps with STA & P2P concurrency
//            config.groupOwnerIntent = WifiP2pConfig.MIN_GROUP_OWNER_INTENT;
//
//            WifiDisplay display = createWifiDisplay(mConnectingDevice);
//            advertiseDisplay(display, null, 0, 0, 0);
//
//            final WifiP2pDevice newDevice = mDesiredDevice;
//            mWifiP2pManager.connect(mWifiP2pChannel, config, new ActionListener() {
//                @Override
//                public void onSuccess() {
//                    // The connection may not yet be established.  We still need to wait
//                    // for WIFI_P2P_CONNECTION_CHANGED_ACTION.  However, we might never
//                    // get that broadcast, so we register a timeout.
//                    Slog.i(TAG, "Initiated connection to Wifi display: " + newDevice.deviceName);
//
//                    mHandler.postDelayed(mConnectionTimeout, CONNECTION_TIMEOUT_SECONDS * 1000);
//                }
//
//                @Override
//                public void onFailure(int reason) {
//                    if (mConnectingDevice == newDevice) {
//                        Slog.i(TAG, "Failed to initiate connection to Wifi display: "
//                                + newDevice.deviceName + ", reason=" + reason);
//                        mConnectingDevice = null;
//                        handleConnectionFailure(false);
//                    }
//                }
//            });
//            return; // wait for asynchronous callback
//    }
//
//    private void requestPeers() {
//        manager.requestPeers(channel, new PeerListListener() {
//            @Override
//            public void onPeersAvailable(WifiP2pDeviceList peers) {
//
//                Slog.d(TAG, "Received list of peers.");
//                
//                mAvailableWifiDisplayPeers.clear();
//                for (WifiP2pDevice device : peers.getDeviceList()) {
//                    
//                        Log.d(TAG, "  " + describeWifiP2pDevice(device));
//                    if (isWifiDisplay(device)) {
//                        mAvailableWifiDisplayPeers.add(device);
//                    }
//                }
//
//                handleScanFinished();
//            }
//        });
//    }

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
			Log.d(TAG, "WIFI_P2P_STATION:__________________________ "+action);
		if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
			// p2p is enabled/disabled.
			int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
			if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
				service.setWifiP2pEnabled(true);
			} else {
				service.setWifiP2pEnabled(false);
			}
			Log.d(TAG, "P2P state changed - " + state);

		} else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
			// p2p connection is established/shutdown.
			NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
			if (networkInfo.isConnected()) {
			    Log.d(TAG, "network is connected:" + networkInfo);
				statemachine.sendMessage(WFDSinkStateMachine.P2P_CONNECTION_ESTABLISHED);
				manager.requestConnectionInfo(channel, listener);
			} else {
			    Log.d(TAG, "network not connected " + networkInfo);
				statemachine.sendMessage(WFDSinkStateMachine.P2P_CONNECTION_DESTROYED);
				/*manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                    public void onSuccess() {
                        //requestPeers();
                         Log.d(TAG, " discover success!");
                    }
                    public void onFailure(int reason) {
                        Log.d(TAG, " discover fail ");
                    }
                });*/
			}
			Log.d(TAG, "P2P connection changed");
			
		} else if (WFDSinkService.WIFI_DISPLAY_SINK_WPS_MESSAGE.equals(action)) {
			rtspPort = intent.getIntExtra("peerRTSPPort", 7236);
			service.removeStickyBroadcast(intent);
			Log.d(TAG, "Received RTSP port : " + rtspPort);

		} else if (WFDSinkService.WIFI_DISPLAY_RTSP_PORT_MESSAGE.equals(action)) {
			rtspPort = intent.getIntExtra("peerRTSPPort", 24);
			service.removeStickyBroadcast(intent);
			Log.d(TAG, "WIFI_DISPLAY_RTSP_PORT_MESSAGE Received RTSP port : " + rtspPort);
		}
	}

	public int getRtspPort(){
		return rtspPort;
	}
}
