# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for toro hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and toro, hence its name.
#

# Set zygote for 32bit
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += ro.zygote=zygote32

# Inherit from those products. Most specific first.
$(call inherit-product, device/actions/common/gsbase_product.mk)
$(call inherit-product, device/actions/s700_aio/device.mk)
$(call inherit-product, device/actions/common/prebuilt/gpu/s700/gpu.mk)
$(call inherit-product, device/actions/common/prebuilt/codec/s700/actcodec.mk)

PRODUCT_NAME := s700_aio
PRODUCT_DEVICE := s700_aio
PRODUCT_BRAND := Actions
PRODUCT_MODEL := S700 

#add by zivy
PRODUCT_COPY_FILES += \
	device/actions/updateIp.sh:system/bin/updateIp.sh \
	device/actions/s700_aio/init.extra_modules.sh:system/bin/init.extra_modules.sh
