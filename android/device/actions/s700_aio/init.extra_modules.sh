#!/system/bin/sh

# For 4G reset 
sleep 2
echo 112 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio112/direction
echo 0 > /sys/class/gpio/gpio112/value

#For Buzeer
sleep 1
echo 108 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio108/direction
echo 1 > /sys/class/gpio/gpio108/value
sleep 1
echo 0 > /sys/class/gpio/gpio108/value

exit

