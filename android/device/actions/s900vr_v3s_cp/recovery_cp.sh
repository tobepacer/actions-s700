#!/bin/bash

DEVICE_DIR=$1
RECOVERY_DEST_DIR=$2

if [ -e ${DEVICE_DIR} -a -e ${RECOVERY_DEST_DIR} ]; then
    echo "dir: ${DEVICE_DIR} and ${RECOVERY_DEST_DIR} exists"
    cp ${DEVICE_DIR}/recovery/*.ko ${RECOVERY_DEST_DIR}/lib/modules/
    cp ${DEVICE_DIR}/recovery/init.rc ${RECOVERY_DEST_DIR}/
    cp ${DEVICE_DIR}/../common/prebuilt/utils/bin/busybox ${RECOVERY_DEST_DIR}/sbin/
    cp ${DEVICE_DIR}/../common/prebuilt/utils/bin/busybox ${RECOVERY_DEST_DIR}/sbin/sh
    cp ${DEVICE_DIR}/../common/prebuilt/utils/bin/e2fsck ${RECOVERY_DEST_DIR}/sbin/
    cp ${DEVICE_DIR}/../common/prebuilt/utils/bin/ntfs-3g ${RECOVERY_DEST_DIR}/sbin/
fi
