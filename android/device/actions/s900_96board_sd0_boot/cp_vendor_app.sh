#!/system/bin/sh
first_boot_file="/data/system/cp_vendor_app"
if [ -f ${first_boot_file} ];then
    echo "not first boot"
else
    echo "first boot"
    cd /vendor/app/app/
    for file in *.apk ; do
        pm install /vendor/app/app/$file
    done
    touch $first_boot_file
    busybox chgrp 1000 $first_boot_file
    busybox chown 1000 $first_boot_file
fi