#ifndef __RESAMPLE_H__ 
#define __RESAMPLE_H__
#define MAX_CHANNEL 2

typedef struct buf
{
	int* pnt;
	struct buf* prev;
	struct buf* next;
	int pos;
	int size;		
}buf_t;

typedef struct
{
	int* p_coeff;
	int gain;
	int* data_buffer[MAX_CHANNEL];
	int* p_data[MAX_CHANNEL];
	int doff;
	int coff;
	int count_in;
	int count_out;
	int L;
	int M;
	int step;
	int in_size;
	int out_size;
	int data_len;
	int coeff_len;
	int channel;
	int* buffer_backup[MAX_CHANNEL];
	int* buffer_output[MAX_CHANNEL];
	int  obuffer_size;
	buf_t buffer_array[MAX_CHANNEL][2];
	buf_t* head[MAX_CHANNEL];
	buf_t* tail[MAX_CHANNEL];
	int	total_size;
}sampler_t;

typedef struct
{
    /*! pcm数据指针数组，包含各声道输出数据的起始地址 */
    int pcm[MAX_CHANNEL];
    /*! 当前输出包含的声道数 */
    int channels;
    /*! 当前输出包含的样本数，只计单个声道 */
    int samples;
    /*! 当前输出小数点的位数，整数取值0 */
    int frac_bits;
} audiout_pcm_t;
#endif